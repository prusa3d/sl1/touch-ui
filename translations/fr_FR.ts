<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr-FR">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="90"/>
        <source>Add Hidden Network</source>
        <translation>Ajouter un réseau caché</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Plugged in</source>
        <translation>Branché</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Unplugged</source>
        <translation>Débranché</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="37"/>
        <source>Network Info</source>
        <translation>Info réseau</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="38"/>
        <source>Wi-Fi Client</source>
        <translation>Client Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Voulez-vous vraiment oublier les paramètres de ce réseau ?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="105"/>
        <source>Forget network?</source>
        <translation>Oublier le réseau ?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="35"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="136"/>
        <source>Apply calibration results</source>
        <translation>Appliquez les résultats de la calibration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="134"/>
        <source>Calibrate center</source>
        <translation>Calibrer le centre</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="135"/>
        <source>Calibrate edge</source>
        <translation>Calibrer bordure</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="174"/>
        <source>Canceled</source>
        <translation>Annulé</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="131"/>
        <source>Check for UV calibrator</source>
        <translation>Vérifiez le calibreur UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="156"/>
        <source>Check ID:</source>
        <translation>Vérification ID :</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation>Effacer les profils Slicer téléchargés</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Clear UV calibration data</source>
        <translation>Supprimer les données de calibration UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Disable factory mode</source>
        <translation>Désactiver le mode usine</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="130"/>
        <source>Disable ssh, serial</source>
        <translation>Désactiver ssh, série</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Display test</source>
        <translation>Test de l&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Erase motion controller EEPROM</source>
        <translation>Effacer l&apos;EEPROM contrôleur de mouvement</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Erase projects</source>
        <translation>Effacer les projets</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="140"/>
        <source>Erase UV PWM settings</source>
        <translation>Effacer les réglages PWM UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="150"/>
        <source>Exposing debris</source>
        <translation>Exposition des débris</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="171"/>
        <source>Failure</source>
        <translation>Échec</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Make tank accessible</source>
        <translation>Rendre le réservoir accessible</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="148"/>
        <source>Moving platform down to safe distance</source>
        <translation>Déplacement de la plate-forme à une distance sûre</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="151"/>
        <source>Moving platform gently up</source>
        <translation>Déplacement doux de la plate-forme vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Moving printer to accept protective foam</source>
        <translation>Déplacer l&apos;imprimante pour accepter la mousse de protection</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Obtain calibration info</source>
        <translation>Obtenir les infos de calibration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Obtain system info</source>
        <translation>Obtenir les infos système</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="170"/>
        <source>Passed</source>
        <translation>Succès</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Platform calibration</source>
        <translation>Calibration de la plateforme</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="153"/>
        <source>Platform home</source>
        <translation>Prise d&apos;origine de la plateforme</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Platform range</source>
        <translation>Portée de la plateforme</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Pressing protective foam</source>
        <translation>Enfoncer la mousse de protection</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Printer calibration</source>
        <translation>Calibration de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>Recording changes</source>
        <translation>Enregistrement des modifications</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Release foam</source>
        <translation>Libérer la mousse</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Reset homing profiles</source>
        <translation>Réinitialiser les profils de prise d&apos;origine</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>Reset hostname</source>
        <translation>Réinitialiser le nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Reset Network settings</source>
        <translation>Réinitialiser les paramètres du réseau</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Reset NTP state</source>
        <translation>Réinitialisation de l&apos;état NTP</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset print configuration</source>
        <translation>Réinitialiser la configuration d&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Reset printer calibration status</source>
        <translation>Réinitialiser le statut de calibration de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Reset Prusa Connect</source>
        <translation>Réinitialiser Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>Reset PrusaLink</source>
        <translation>Réinitialiser PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Reset selftest status</source>
        <translation>Réinitialiser le statut du selftest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Reset system locale</source>
        <translation>Réinitialiser la localisation du système</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Reset timezone</source>
        <translation>Réinitialiser le fuseau horaire</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Resetting hardware counters</source>
        <translation>Remise à zéro des compteurs matériels</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="139"/>
        <source>Reset UI settings</source>
        <translation>Réinitialiser les réglages de l&apos;UI</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="154"/>
        <source>Reset update channel</source>
        <translation>Réinitialiser le canal de mise à jour</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Resin sensor</source>
        <translation>Capteur de résine</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="169"/>
        <source>Running</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Send printer data to MQTT</source>
        <translation>Envoyer les données de l&apos;imprimante au MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Serial number</source>
        <translation>Numéro de série</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Set new printer model</source>
        <translation>Paramétrer un nouveau modèle d&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Sound test</source>
        <translation>Test du son</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Tank calib. start</source>
        <translation>Début calib. réserv.</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Tank home</source>
        <translation>Prise d&apos;origine du réservoir</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="138"/>
        <source>Tank level</source>
        <translation>Niveau du réservoir</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Tank range</source>
        <translation>Portée du réservoir</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Tilt timming</source>
        <translation>Timing de l&apos;inclinaison</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="149"/>
        <source>Touching platform down</source>
        <translation>Descente de la plate-forme</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="173"/>
        <source>User action pending</source>
        <translation>Action utilisateur en attente</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="133"/>
        <source>UV calibrator placed</source>
        <translation>Calibreur UV positionné</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>UV LED</source>
        <translation>LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>UV LED and fans</source>
        <translation>LED UV et ventilateurs</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="132"/>
        <source>UV LED warmup</source>
        <translation>Préchauffage des LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="168"/>
        <source>Waiting</source>
        <translation>En attente</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="152"/>
        <source>Waiting for user</source>
        <translation>Attente de l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="137"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation>En attente du retrait du calibreur UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="172"/>
        <source>With Warning</source>
        <translation>Avec Avertissement</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="56"/>
        <source>Understood</source>
        <translation>Compris</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation>Erreur inconnue</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>A64 OVERHEAT</source>
        <translation>SURCHAUFFE DE L&apos;A64</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation>La température de l&apos;A64 est trop haute. Mesure : %(temperature).1f °C ! Extinction dans 10 secondes...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation>ADMIN NON DISPONIBLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="234"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation>TEMP. AMBIANTE TROP HAUTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="235"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation>TEMP. AMBIANTE TROP BASSE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>Analysis of the project failed</source>
        <translation>Échec de l&apos;analyse du projet</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation>Une autre action est déjà en cours. Terminez cette action en utilisant directement l&apos;écran tactile de l&apos;imprimante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>An unexpected error has occurred.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation>Une erreur inattendue s&apos;est produite.
Si un travail d&apos;impression est en cours, il doit être terminé.
Vous pouvez mettre l&apos;imprimante hors tension en appuyant sur le bouton d&apos;alimentation avant.
Consultez le manuel pour savoir comment enregistrer un fichier journal et nous l&apos;envoyer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation>Un avertissement inconnu a eu lieu. Redémarrez l&apos;imprimante et réessayez. Contactez notre assistance technique si le problème persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>A part of the LED panel is disconnected.</source>
        <translation>Une partie du panneau LED est déconnectée.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation>PROBLÈME AVEC LA CARTE BOOSTER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="231"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation>L&apos;EMPLACEMENT BOOTÉ A CHANGÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>Broken UV LED panel</source>
        <translation>Panneau UV LED défaillant</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>CALIBRATION ERROR</source>
        <translation>ERREUR DE CALIBRATION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>CALIBRATION FAILED</source>
        <translation>ÉCHEC DE LA CALIBRATION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation>ÉCHEC DU CHARGEMENT DE LA CALIBRATION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>Calibration project is invalid</source>
        <translation>Le projet de calibration est non-valide</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation>LE PROJET DE CALIBRATION EST NON-VALIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation>Impossible de se connecter au calibreur LED UV. Vérifiez la connexion et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Cannot export profile</source>
        <translation>Impossible d&apos;exporter le profil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Cannot find the selected file!</source>
        <translation>Impossible de trouver le fichier sélectionné !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation>Impossible d&apos;accéder au canal de mise à jour. Redémarrez l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>Cannot import profile</source>
        <translation>Impossible d&apos;importer le profil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>CANNOT READ PROJECT</source>
        <translation>IMPOSSIBLE DE LIRE LE PROJET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation>IMPOSSIBLE DE SUPPRIMER LE PROJET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation>Impossible d&apos;envoyer la configuration d&apos;usine vers la base de données (MQTT) ! Vérifiez la connexion réseau. Veuillez contacter le support technique.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation>Impossible de configurer le canal de mise à jour. Redémarrez l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="236"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation>IMPOSSIBLE DE COPIER LE PROJET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>CLEANING ADAPTOR MISSING</source>
        <translation>ADAPTATEUR DE NETTOYAGE MANQUANT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>Cleaning adaptor was not detected, it does not seem to be correctly attached to the print platform.
Attach it properly and try again.</source>
        <translation>L&apos;adaptateur de nettoyage n&apos;a pas été détecté, il ne semble pas être correctement fixé à la plate-forme d&apos;impression.
Fixez-le correctement et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Communication with the Booster board failed.</source>
        <translation>Échec de la communication avec la carte Booster.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation>Échec de la communication avec le calibreur LED UV. Vérifiez la connexion et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation>ERREUR LECTURE FICHIER CONFIG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>CONNECTION FAILED</source>
        <translation>ÉCHEC DE LA CONNEXION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation>Échec de la connexion aux serveurs Prusa, veuillez réessayer plus tard.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation>Des réglages corrects ont été trouvés, mais la déviation standard
(%(found).1f) est plus importante que la valeur autorisée (%(allowed).1f).
Vérifiez la position du calibreur de LED UV et la calibration, puis réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation>Les données proviennent d&apos;un capteur LED UV inconnu !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation>ÉCHEC DE RÉÉCRITURE DES DONNÉES</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation>ÉCHEC DE PRÉPARATION DES DONNÉES</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation>LE RÉPERTOIRE N&apos;EST PAS VIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>Disconnected UV LED panel</source>
        <translation>Panneau LED UV déconnecté</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>DISPLAY TEST ERROR</source>
        <translation>ERREUR TEST ÉCRAN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Display test failed.</source>
        <translation>Échec du test de l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>DISPLAY TEST FAILED</source>
        <translation>ÉCHEC DU TEST DE L&apos;ÉCRAN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>Display usage error</source>
        <translation>Erreur d&apos;utilisation de l&apos;écran</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>DOWNLOAD FAILED</source>
        <translation>ÉCHEC DU TÉLÉCHARGEMENT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Error displaying test image.</source>
        <translation>Erreur dans l&apos;affichage de l&apos;image de test.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>Error, there is no file to reprint.</source>
        <translation>Erreur, il n&apos;y a pas de fichier à ré-imprimer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation>Les exemples (projets quels qu&apos;ils soient) ne sont pas présents dans le stockage utilisateur. Téléchargez-les à nouveau à partir du menu &apos;Paramètres&apos;.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="246"/>
        <source>EXPECT OVERHEATING</source>
        <translation>SURCHAUFFE PROBABLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation>L&apos;écran d&apos;exposition actuellement connecté a déjà été utilisé sur cette imprimante. Cet écran a été utilisé pour la dernière fois pendant environ %(counter_h)d heures.

Si vous ne souhaitez pas utiliser cet écran : éteignez l&apos;imprimante, remplacez l&apos;écran et rallumez l&apos;imprimante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation>Impossible de changer le niveau du journal (détail). Redémarrez l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation>Échec du chargement de la calibration usine des ventilateurs et LEDs.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Failed to reach the tilt endstop.</source>
        <translation>Impossible d&apos;atteindre la butée d&apos;inclinaison.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tower endstop.</source>
        <translation>Impossible d&apos;atteindre la butée de la colonne.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation>Impossible de lire le fichier de configuration. Essayez de redémarrer l&apos;imprimante. Si le problème persiste, contactez le support technique.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation>Impossible d&apos;enregistrer les données de l&apos;Assistant. Redémarrez l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation>Échec de la sérialisation des données de l&apos;assistant. Redémarrez l&apos;imprimante et essayez à nouveau.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Failed to set hostname</source>
        <translation>Impossible de définir le nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>FAN FAILURE</source>
        <translation>ÉCHEC VENTILATEUR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation>RPM DU VENTILATEUR HORS DE PORTÉE POUR LE TEST</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="245"/>
        <source>FAN WARNING</source>
        <translation>AVERTISSEMENT VENTILATEUR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>FILE ALREADY EXISTS</source>
        <translation>LE FICHIER EXISTE DEJÀ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation>Le fichier existe déjà ! Supprimez-le d&apos;abord dans l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation>Le fichier a une extension non-valide ! Consultez l&apos;article concernant les extensions de fichier prises en charge.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FILE NOT FOUND</source>
        <translation>FICHIER NON TROUVÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="247"/>
        <source>FILL THE RESIN</source>
        <translation>REMPLIR LA RÉSINE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation>ÉCHEC DE LA MISE À JOUR DU FIRMWARE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>HOSTNAME ERROR</source>
        <translation>ERREUR DE NOM D&apos;HÔTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>Image preloader did not finish successfully!</source>
        <translation>Le préchargement de l&apos;image ne s&apos;est pas terminé correctement !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="237"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation>MODÈLE D&apos;IMPRIMANTE INCORRECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan.</source>
        <translation>Lecture des RPM incorrecte pour le ventilateur %(failed_fans_text)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation>Lecture incorrecte des RPM du ventilateur %(failed_fans_text)s. L&apos;impression peut continuer, mais il existe un risque de surchauffe.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>Incorrect RPM reading of the %(fan__map_HardwareDeviceId)s.</source>
        <translation>Lecture des RPM incorrecte pour le %(fan__map_HardwareDeviceId)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>INTERNAL ERROR</source>
        <translation>ERREUR INTERNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation>Erreur interne (échec du mappage DBUS), redémarrez l&apos;imprimante. Contactez le support technique si le problème persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation>LA MÉMOIRE INTERNE EST PLEINE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation>La mémoire interne est pleine. Supprimez d&apos;abord quelques uns de vos projets.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>INVALID API KEY</source>
        <translation>CLÉ API NON VALIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>INVALID FILE EXTENSION</source>
        <translation>EXTENSION DE FICHIER NON VALIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INVALID PASSWORD</source>
        <translation>MOT DE PASSE INVALIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>INVALID PROJECT</source>
        <translation>PROJET NON VALIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>Invalid tilt alignment position.</source>
        <translation>Position d&apos;alignement de l&apos;inclinaison invalide.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation>POSITION D&apos;ALIGNEMENT DE L&apos;INCLINAISON NON-VALIDE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>LANGUAGE NOT SET</source>
        <translation>LANGUE NON PARAMÉTRÉE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="241"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation>AVERTISSEMENT MASQUE NON DISPONIBLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>MC WRONG REVISION</source>
        <translation>MAUVAISE RÉVISION DE LA MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation>Le volume de résine mesuré est trop bas. L&apos;impression peut continuer, mais un remplissage pourra être nécessaire.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation>Le volume de résine mesuré %(volume_ml)d ml est plus important que ce qui est nécessaire pour cette impression. Assurez-vous que le niveau de résine ne dépasse pas le repère 100% et relancez l&apos;impression.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation>Le volume de résine mesuré %(volume_ml)d ml est moins important que ce qui est nécessaire pour cette impression. Remplissez le réservoir et relancez l&apos;impression.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation>Impossible de mesurer la résine. Vérifiez que la plateforme est bien présente et quelle quantité de résine est présente dans le réservoir.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <source>Moving the tilt failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Impossible de déplacer le système d&apos;inclinaison. Assurez-vous qu&apos;il n&apos;y a pas d&apos;obstacle sur son passage et répétez l&apos;action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Impossible de déplacer la colonne. Assurez-vous qu&apos;il n&apos;y a pas d&apos;obstacle sur son passage et répétez l&apos;action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation>ÉCHEC TÉLÉCHARGEMENT MQTT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>No calibration data to show!</source>
        <translation>Aucune donnée de calibration à afficher !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>No display usage data to show</source>
        <translation>Aucunes données d&apos;utilisation de l&apos;écran à afficher</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>NO FILE TO REPRINT</source>
        <translation>AUCUN FICHIER À RÉIMPRIMER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>NO INTERNET CONNECTION</source>
        <translation>AUCUNE CONNEXION INTERNET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation>Aucun problème détecté. Vous pouvez continuer à utiliser l&apos;imprimante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="238"/>
        <source>NOT ENOUGH RESIN</source>
        <translation>PAS ASSEZ DE RÉSINE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation>PAS ASSEZ DE COUCHES</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No USB storage present</source>
        <translation>Pas de stockage USB présent</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation>AUCUNES DONNÉES DE CALIBRATION UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="232"/>
        <source>NO WARNING</source>
        <translation>AUCUN AVERTISSEMENT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="242"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation>AVERTISSEMENT OBJET COUPÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation>L&apos;objet a été coupé car il était trop grand pour la surface d&apos;impression.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>OLD EXPO PANEL</source>
        <translation>ANCIEN PANNEAU D&apos;EXPO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>OPENING PROJECT FAILED</source>
        <translation>IMPOSSIBLE D&apos;OUVRIR LE PROJET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Échec de l&apos;ouverture du projet. Le fichier est corrompu. Veuillez re-découper ou ré-exporter le projet et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Échec de l&apos;ouverture du projet. Le fichier est peut-être corrompu. Veuillez re-découper ou ré-exporter le projet et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation>Impossible d&apos;ouvrir le projet, le fichier est peut-être corrompu. Redécoupez ou réexportez le projet et essayez à nouveau.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="239"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation>PARAMÈTRES HORS PLAGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="240"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation>AVERTISSEMENT PERPARTES NON DISPONIBLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="118"/>
        <source>Per-partes print not available.</source>
        <translation>L&apos;impression Per-partes n&apos;est pas disponible.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>Veuillez activer le HTTP digest (c&apos;est l&apos;option de sécurité recommandée) ou mettez à jour la clé API. Vous pouvez la trouver dans Réglages &gt; Réseau &gt; Autorisations de connexion.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>Pour enough resin for the selected file into the tank and close the lid. The minimal amount of the resin is displayed on the touchscreen.</source>
        <translation>Versez suffisamment de résine dans le réservoir pour le projet sélectionné et fermez le capot.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>PRELOAD FAILED</source>
        <translation>ÉCHEC DU PRÉCHARGEMENT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>PRINTER IS BUSY</source>
        <translation>L&apos;IMPRIMANTE EST OCCUPÉE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>PRINTER IS OK</source>
        <translation>L&apos;IMPRIMANTE EST OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation>L&apos;IMPRIMANTE N&apos;A PAS EU DE CALIBRATION UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="243"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation>AVERTISSEMENT : NON-CORRESPONDANCE DE VARIANTE D&apos;IMPRIMANTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation>EXEMPLES D&apos;IMPRESSION MANQUANTS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>PRINT JOB CANCELLED</source>
        <translation>TÂCHE D&apos;IMPRESSION ANNULÉE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>Print mask is missing.</source>
        <translation>Le masque d&apos;impression n&apos;est pas présent.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation>ERREUR EXPORTATION PROFIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation>ERREUR IMPORTATION PROFIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation>ÉCHEC DE L&apos;ANALYSE DU PROJET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation>LE PROJET EST CORROMPU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Reading of %(sensor__map_HardwareDeviceId)s not in range!

Measured temperature: %(temperature).1f °C, allowed range: %(min)d - %(max)d °C.

Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>La lecture de %(sensor__map_HardwareDeviceId)s n&apos;est pas dans la plage&#xa0;!

Température mesurée&#xa0;: %(temperature).1f °C, plage autorisée&#xa0;: %(min)d - %(max)d °C.

Conservez l&apos;imprimante à l&apos;abri de la lumière directe du soleil et à température ambiante (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation>Échec de la lecture de température des LED UV ! Cette valeur est essentielle pour la durée de vie des LED UV et la sécurité de l&apos;imprimante. Veuillez contacter le support technique ! Le travail d&apos;impression en cours va être annulé.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>REMOTE API ERROR</source>
        <translation>ERREUR API DISTANTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation>Il n&apos;est pas possible de supprimer ce projet. Le projet est verrouillé par un travail d&apos;impression.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation>L&apos;intensité demandée ne peut être obtenue avec la PWM max. autorisée.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation>L&apos;intensité demandée ne peut être obtenue avec la PWM min. autorisée.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="244"/>
        <source>RESIN LOW</source>
        <translation>NIVEAU DE RÉSINE BAS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>RESIN MEASURING FAILED</source>
        <translation>ÉCHEC MESURE RÉSINE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>RESIN SENSOR ERROR</source>
        <translation>ERREUR CAPTEUR RÉSINE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN TOO HIGH</source>
        <translation>NIVEAU DE RÉSINE TROP HAUT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>RESIN TOO LOW</source>
        <translation>NIVEAU DE RÉSINE TROP BAS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>RPM of %(fan__map_HardwareDeviceId)s not in range!

RPM data: %(min_rpm)d - %(max_rpm)d
Average: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</source>
        <translation>RPM de %(fan__map_HardwareDeviceId)s hors plage&#xa0;!

Données de RPM : %(min_rpm)d - %(max_rpm)d
Moyenne&#xa0;: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), erreur&#xa0;: %(error)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>RPM of %(fan)s not in range!
RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation>RPM de %(fan)s hors de la plage&#xa0;!
Données RPM : %(rpm)s
Moyenne : %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation>Échec de l&apos;enregistrement des nouvelles valeurs usine par défaut. Redémarrez l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>%(sensor)s hors plage ! Température mesurée : %(temperature).1f °C. Gardez l&apos;imprimante à l&apos;abri du soleil et à température ambiante (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation>ERREUR NUMÉRO DE SÉRIE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation>Les numéros de série ne sont pas au bon format ! A64 : %(a64)s MC : %(mc)s Veuillez contacter le support technique!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation>ÉCHEC DU RÉGLAGE DES INFOS DU JOURNAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation>ÉCHEC DU RÉGLAGE DE LA MISE À JOUR DU CANAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>Something went wrong with the printer firmware. Please contact our support and do not forget to attach the logs.
Crashed services are immediately started again. If, despite that, the printer does not behave correctly, try restarting it. </source>
        <translation>Un problème s&apos;est produit avec le firmware de l&apos;imprimante. Veuillez contacter notre support et n&apos;oubliez pas de joindre les journaux.
Les services en panne sont immédiatement redémarrés. Si malgré cela l’imprimante ne se comporte pas correctement, essayez de la redémarrer. </translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Speaker test failed.</source>
        <translation>Échec du test haut-parleur.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>SPEAKER TEST FAILED</source>
        <translation>ÉCHEC DU TEST HAUT-PARLEUR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="230"/>
        <source>SYSTEM SERVICE CRASHED</source>
        <translation>SERVICE SYSTÈME EN PANNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation>TEMPÉRATURE HORS PLAGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation>ÉCHEC DU CAPTEUR DE TEMPÉRATURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>The admin menu is not available.</source>
        <translation>Le menu admin n&apos;est pas disponible.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation>La température ambiante est trop haute, l&apos;impression peut continuer mais cela pourrait faire échouer celle-ci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation>La température ambiante est trop basse, l&apos;impression peut continuer mais cela pourrait faire échouer celle-ci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation>La quantité de résine dans le réservoir n&apos;est pas suffisante pour le projet en cours. Il faudra ajouter d&apos;avantage de résine pendant l&apos;impression.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation>La calibration UV LED automatique ne s&apos;est pas terminée correctement ! Relancez la calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation>La calibration ne s&apos;est pas terminée correctement ! Lancez à nouveau la calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The directory is not empty.</source>
        <translation>Le répertoire n&apos;est pas vide.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation>Échec du téléchargement. Vérifiez la connexion à internet et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation>La mémoire interne est pleine, le projet ne peut pas être copié. Vous pouvez continuer à imprimer. Néanmoins, vous ne devez pas retirer la clé USB pendant l&apos;impression, sinon celle-ci pourrait échouer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation>La langue n&apos;est pas paramétrée. Allez dans Réglages -&gt; Langue &amp; Heure -&gt; Paramétrez la langue et choisissez votre langue préférée.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation>Le modèle a été découpé pour un autre modèle d&apos;imprimante. Redécoupez le modèle en utilisant les bons réglages.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation>Le modèle a été découpé pour une variante d&apos;imprimante différente %(project_variant)s. La variante de votre imprimante est %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation>Le Motion Controller (MC) a rencontré une erreur inattendue. Redémarrez l&apos;imprimante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>The password is incorrect. Please check or update it in:

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

The password must be at least 8 characters long. Supported characters are letters, numbers, and dash.</source>
        <translation>Le mot de passe est incorrect. Veuillez le vérifier ou le mettre à jour dans :

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

Le mot de passe doit comporter au moins 8 caractères. Les caractères pris en charge sont les lettres, les chiffres et le tiret.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation>L&apos;imprimante a démarré à partir d&apos;un emplacement alternatif du fait d&apos;un échec de démarrage à partir de l&apos;emplacement primaire.
Mettez à jour l&apos;imprimante avec le firmware le plus récent dès que possible afin de récupérer l&apos;emplacement primaire.
Cela se produit généralement après un échec de mise à jour, ou du fait d&apos;une panne matérielle. Il se peut que les réglages de l&apos;imprimante aient été remis à zéro.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation>L&apos;imprimante n&apos;est pas calibrée. Veuillez d&apos;abord lancer l&apos;Assistant.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation>L&apos;imprimante ne s&apos;est pas connectée à internet. Vérifiez la connexion dans les réglages.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation>La calibration UV de l&apos;imprimante n&apos;a pas été effectuée. Connectez le calibreur UV et effectuez la calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer model was not detected.</source>
        <translation>Le modèle d&apos;imprimante n&apos;a pas été détecté.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>L&apos;imprimante utilise une sécurité HTTP digest. Veuillez également l&apos;activer dans votre slicer (recommandé), ou désactivez cette option de sécurité au niveau de l&apos;imprimante. Vous pouvez la trouver dans Réglages &gt; Réseau &gt; Autorisations d&apos;accès.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>The print job cancelled by the user.</source>
        <translation>Travail d&apos;impression annulé par l&apos;utilisateur.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="117"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation>Les paramètres d&apos;impression sont hors de la plage de l&apos;imprimante, le système peut tenter de réparer le projet. Continuer ?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>The project file is invalid!</source>
        <translation>Le fichier du projet n&apos;est pas valide !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The project must have at least one layer</source>
        <translation>Le projet doit présenter au moins une couche</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>There is no warning</source>
        <translation>Il n&apos;y a aucun avertissement</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured.</source>
        <translation>Le capteur de résine n&apos;a pas été enclenché. Vérifiez si le réservoir et la plateforme sont bien fixés.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>The %(sensor__map_HardwareDeviceId)s sensor failed.</source>
        <translation>Le capteur %(sensor__map_HardwareDeviceId)s est en panne.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>The %(sensor)s sensor failed.</source>
        <translation>Le capteur %(sensor)s est en panne.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation>Échec de la mise à jour du firmware ! Redémarrez l&apos;imprimante et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation>Le calibrateur LED UV a détecté un peu de lumière sur un écran sombre. Cela signifie qu&apos;il y a une légère &quot;fuite&quot; sous le calibrateur UV, ou que votre écran ne bloque pas assez la lumière UV. Vérifiez le positionnement du calibrateur UV sur l&apos;écran ou repositionnez l&apos;écran d&apos;exposition.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation>Le calibreur LED UV n&apos;a pas réussi à lire l&apos;intensité de lumière UV attendue. Vérifiez le positionnement du calibreur UV sur l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation>Calibrateur LED UV non-détecté. Vérifiez la connexion et réessayez.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The UV LED panel is not detected.</source>
        <translation>Le panneau LED UV n&apos;a pas été détecté.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>The wizard did not finish successfully!</source>
        <translation>L&apos;assistant ne s&apos;est pas terminé correctement !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>This project was prepared for a different printer</source>
        <translation>Ce projet a été préparé pour une imprimante différente</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation>Cette requête n&apos;est pas compatible avec l&apos;API distante Prusa. Consultez notre documentation pour plus d&apos;informations.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation>Cette procédure de l&apos;assistant ne peut pas être annulée, finissez d&apos;abord cette étape.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation>ÉCHEC VÉRIFICATION AXE INCLINAISON</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps</source>
        <translation>Échec de la vérification de l&apos;axe d&apos;inclinaison !
Position actuelle&#xa0;: %(position)d pas</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation>IMPOSSIBLE D&apos;ATTEINDRE LA BUTÉE D&apos;INCLINAISON</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TILT HOMING FAILED</source>
        <translation>ÉCHEC DE LA PRISE D&apos;ORIGINE DE L&apos;INCLINAISON</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation>Échec de la mise à zéro de l&apos;inclinaison, vérifiez son environnement et répétez l&apos;action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>TILT MOVING FAILED</source>
        <translation>LE MOUVEMENT D&apos;INCLINAISON A ÉCHOUÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation>ÉCHEC VÉRIFICATION AXE COLONNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation>Échec du test de l&apos;axe colonne !

Position actuelle : %(position_nm)d nm

Vérifiez si la vis à billes peut se déplacer sans encombre sur toute son amplitude.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation>IMPOSSIBLE D&apos;ATTEINDRE LA BUTÉE DE LA COLONNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TOWER HOMING FAILED</source>
        <translation>ÉCHEC DE LA PRISE D&apos;ORIGINE DE LA COLONNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Échec de la mise à zéro de la colonne, assurez-vous qu&apos;il n&apos;y a pas d&apos;obstacle sur son passage et répétez l&apos;action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>TOWER MOVING FAILED</source>
        <translation>ÉCHEC DU DÉPLACEMENT DE LA COLONNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation>La colonne ne se trouve à la position attendue.

La plateforme et le réservoir sont-ils montés et fixés correctement ?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>TOWER POSITION ERROR</source>
        <translation>ERREUR POSITION COLONNE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UNAUTHORIZED</source>
        <translation>NON AUTORISÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>UNEXPECTED ERROR</source>
        <translation>ERREUR INATTENDUE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation>ERREUR MC INATTENDUE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>Unknown printer model</source>
        <translation>Modèle d&apos;imprimante inconnu</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>Unknown UV LED calibrator error code: %(nonprusa_code)d</source>
        <translation>Code d&apos;erreur inconnu du calibrateur de LED UV : %(nonprusa_code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="233"/>
        <source>UNKNOWN WARNING</source>
        <translation>AVERTISSEMENT INCONNU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation>ÉCHEC DE LA MISE À JOUR DU CANAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation>CLÉ USB NON DÉTECTÉE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>UV CALIBRATION ERROR</source>
        <translation>ERREUR CALIBRATION UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>UV CALIBRATION FAILED</source>
        <translation>ÉCHEC DE LA CALIBRATION UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>UV DATA EROR</source>
        <translation>ERREUR DONNÉES UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>UV INTENSITY ERROR</source>
        <translation>ERREUR INTENSITÉ UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation>L&apos;intensité UV n&apos;est pas réglée. Veuillez effectuer la calibration UV avant de lancer une impression.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation>INTENSITÉ UV TROP ÉLEVÉE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation>INTENSITÉ UV TROP BASSE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation>ERREUR DE CONNEXION DU CALIBRATEUR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation>ERREUR CALIBREUR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation>ERREUR DE CONNEXION DU CALIBREUR UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation>CALIBRATEUR LED UV NON DÉTECTÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation>ERREUR DE LECTURE DU CALIBREUR UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation>ERREUR INCONNUE DU CALIBREUR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation>ÉCHEC DISSIPATEUR DE CHALEUR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>UV LED is overheating!</source>
        <translation>Les LED UV surchauffent !</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV LED TEMP. ERROR</source>
        <translation>ERREUR TEMP. LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation>ERREUR TENSION LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation>Les tensions LED UV diffèrent trop. Le module LED peut être défaillant. Contactez notre support technique.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>WIZARD DATA FAILURE</source>
        <translation>ÉCHEC DONNÉES ASSISTANT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>WIZARD FAILED</source>
        <translation>ÉCHEC DE L&apos;ASSISTANT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>WRONG PRINTER MODEL</source>
        <translation>MODÈLE D&apos;IMPRIMANTE ERRONÉ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation>Mauvaise révision du Motion Controller (MC). Contactez notre support technique.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>YOU SHALL NOT PASS</source>
        <translation>VOUS NE PASSEREZ PAS</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="110"/>
        <source>Already exists</source>
        <translation>Existe déjà</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="114"/>
        <source>Can&apos;t read</source>
        <translation>Lecture impossible</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="96"/>
        <source>Error: %1</source>
        <translation>Erreur : %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="108"/>
        <source>File not found</source>
        <translation>Fichier non trouvé</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="112"/>
        <source>Invalid extension</source>
        <translation>Extension non valide</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="106"/>
        <source>Storage full</source>
        <translation>Stockage plein</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>Hotspot</source>
        <translation>Point d&apos;accès</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="149"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="175"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="247"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La clé PSK doit faire au moins 8 caractères.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="232"/>
        <source>Security</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="216"/>
        <source>Show Password</source>
        <translation>Montrer le MDP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="153"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="253"/>
        <source>SSID must not be empty</source>
        <translation>Le SSID ne doit pas être vide</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="265"/>
        <source>Start AP</source>
        <translation>Démarrer l&apos;AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="138"/>
        <source>State:</source>
        <translation>État :</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation>Arrêter l&apos;AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="296"/>
        <source>Swipe for QRCode</source>
        <translation>Glissez pour voir le QR Code</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="377"/>
        <source>Swipe for settings</source>
        <translation>Glissez pour les paramètres</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation>(inchangé)</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="23"/>
        <source>About Us</source>
        <translation>À propos de nous</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="52"/>
        <source>Prusa Research a.s.</source>
        <translation>Prusa Research a.s.</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="48"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation>Pour avoir davantage d&apos;infos à notre sujet scannez le QR code ou utilisez le lien ci-dessous :</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation>Ajouter un réseau caché</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation>Connecter</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation>BON</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La clé PSK doit faire au moins 8 caractères.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation>Montrer le MDP</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation>Le SSID ne doit pas être vide.</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="40"/>
        <source>Admin API</source>
        <translation>API Admin</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="156"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="168"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="64"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation>Veuillez saisir le bon mot de passe pour le réseau &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="194"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La clé PSK doit faire au moins 8 caractères.</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation>Montrer le MDP</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation>(inchangé)</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Are you sure?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation>Pouvez-vous voir le logo de l&apos;entreprise sur l&apos;écran d&apos;exposition à travers le capot ?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="126"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Retirez délicatement l&apos;autocollant de protection de l&apos;écran d&apos;exposition.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="174"/>
        <source>Close the cover.</source>
        <translation>Fermez le capot.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="173"/>
        <source>Cover</source>
        <translation>Capot</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="57"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation>Voulez-vous vraiment annuler l&apos;assistant ?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="114"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation>Une fois que vous aurez mis en place le papier dans l&apos;imprimante, n&apos;oubliez pas de refermer le couvercle !</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="59"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation>La machine ne fonctionnera pas sans effectuer la procédure d&apos;Assistant intégralement.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="112"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation>Astuce : Si vous ne pouvez pas voir le logo clairement, essayez de positionner une feuille de papier sur l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation>Assistant</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="154"/>
        <source>Wizard canceled</source>
        <translation>Assistant annulé</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard failed</source>
        <translation>Échec de l&apos;assistant</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="136"/>
        <source>Wizard finished sucessfuly!</source>
        <translation>Procédure d&apos;Assistant effectuée avec succès!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation>L&apos;Assistant a du s&apos;arrêter en raison d&apos;un problème, réessayer ?</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="121"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="92"/>
        <source>Move Down</source>
        <translation>Déplacer vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="83"/>
        <source>Move Up</source>
        <translation>Déplacer vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation>Mouvement du Réservoir</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="109"/>
        <source>Tilt position:</source>
        <translation>Position d&apos;inclinaison :</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="160"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation>Ajustez la plateforme de façon à ce qu&apos;elle soit alignée avec l&apos;écran d&apos;exposition.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="195"/>
        <source>All done, happy printing!</source>
        <translation>C&apos;est terminé, bonnes impressions !</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="129"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation>Vérifiez que la plateforme est bien fixée grâce à la molette noire (maintenez-la en place et serrez la molette si nécessaire).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="145"/>
        <source>Close the cover.</source>
        <translation>Fermez le capot.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="131"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation>Ne faites pas pivoter la plateforme. Elle doit être positionnée comme sur la photo.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="162"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation>Les bordures avant de la plateforme et de l&apos;écran d&apos;exposition doivent être parallèles.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation>Tenez la plateforme d&apos;une main et appuyez légèrement dessus, vers le bas, de façon à ce qu&apos;elle reste bien en contact avec l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="50"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation>Si la plateforme n&apos;est pas encore insérée, insérez-la comme sur la photo selon un angle de 0° degré et fixez-la avec le bouton noir.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="86"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation>À l&apos;étape suivante, déplacez le plateau d&apos;inclinaison vers le haut/bas jusqu&apos;à ce qu&apos;il soit en contact direct avec le réservoir de résine. Le plateau d&apos;inclinaison et le réservoir doivent être parfaitement alignés.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation>Desserrez la petite vis sur le cantilever avec une clé Allen. Veillez à ne pas la dévisser complètement.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="109"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation>Assurez-vous que la plateforme, le réservoir et le plateau d&apos;inclinaison sont PARFAITEMENT propres.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation>Calibration de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="120"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation>Remettez le réservoir dans sa position initiale et verrouillez-le avec les vis du réservoir. Prenez soin de visser les deux vis de façon identique et avec une force équivalente.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="99"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation>Positionnez le plateau d&apos;inclinaison contre le réservoir de résine</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="64"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation>Certaines imprimantes SL1 peuvent avoir deux vis - voir le manuel pour plus d’informations.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="180"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation>Certaines imprimantes SL1 peuvent avoir deux vis - serrez-les de façon égale, petit à petit. Consultez le manuel pour plus d&apos;informations.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="111"/>
        <source>The image is for illustration purposes only.</source>
        <translation>La photo est là uniquement à titre d&apos;illustration.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="178"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation>Serrez la petite vis sur le cantilever avec une clé Allen.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="77"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation>Dévissez le réservoir, pivotez-le de 90° et positionnez-le à plat sur le plateau d&apos;inclinaison. Retirez complètement les vis du réservoir.</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="149"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="177"/>
        <source>Above Area Fill</source>
        <translation>Remplissage de la zone supérieure</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="171"/>
        <source>Above Area Fill Threshold Settings</source>
        <translation>Paramètres du seuil de remplissage de la zone supérieure</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="148"/>
        <source>Area Fill Threshold</source>
        <translation>Seuil de remplissage de la zone</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="197"/>
        <source>Below Area Fill</source>
        <translation>Remplissage de la zone inférieure</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="191"/>
        <source>Below Area Fill Threshold Settings</source>
        <translation>Seuil de remplissage sous la zone</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="72"/>
        <source>Exposure</source>
        <translation>Exposition</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="97"/>
        <source>Exposure Time Incr.</source>
        <translation>Incr. du temps d&apos;expo.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="124"/>
        <source>First Layer Expo.</source>
        <translation>Expo. Première Couche</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="41"/>
        <source>Print Settings</source>
        <translation>Réglages d&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="90"/>
        <location filename="../qml/PageChange.qml" line="118"/>
        <location filename="../qml/PageChange.qml" line="142"/>
        <source>s</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="194"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation>Balayez pour une image</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="207"/>
        <source>Connected to %1</source>
        <translation>Connecté à %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="196"/>
        <source>Connection to %1 failed.</source>
        <translation>La connexion à %1 a échoué.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation>Réseau caché</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="139"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="147"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La clé PSK doit faire au moins 8 caractères.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="122"/>
        <source>Security</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="115"/>
        <source>Show Password</source>
        <translation>Montrer le MDP</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="235"/>
        <source>Start AP</source>
        <translation>Démarrer l&apos;AP</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="185"/>
        <source>Working...</source>
        <translation>En cours...</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation>Refroidissement</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation>La température est de %1 C</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation>SURCHAUFFE DES LED UV !</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation>Test de l&apos;écran</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="91"/>
        <source>All done, happy printing!</source>
        <translation>C&apos;est terminé, bonnes impressions !</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="77"/>
        <source>Close the cover.</source>
        <translation>Fermez le capot.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="32"/>
        <source>Display Test</source>
        <translation>Test de l&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Desserrez le bouton noir et retirez la plateforme.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Veuillez dévisser et enlever le réservoir de résine.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="51"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation>Cette procédure vous aidera à vous assurer que votre écran d&apos;exposition fonctionne correctement.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>Welcome to the display wizard.</source>
        <translation>Bienvenue dans l&apos;assistant pour l&apos;écran.</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="78"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation>La configuration actuelle va maintenant être effacée.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="108"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation>Downgrade effectué. Au cours de la prochaine étape, l&apos;imprimante va être redémarrée.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation>Downgrade Matériel</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="99"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Utilisez seulement la plateforme fournie. Si vous utilisez une plateforme différente la résine peut s&apos;écouler et endommager votre imprimante !</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="69"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Veuillez noter que le downgrade n&apos;est pas pris en charge.

Le fait de downgrader votre imprimante va effacer votre calibration UV et votre imprimante ne fonctionnera pas correctement.

Vous devrez la recalibrer en utilisant un calibreur UV externe.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="48"/>
        <source>Proceed?</source>
        <translation>Continuer ?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="59"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation>Réassemblez les composants SL1S et mettez l&apos;imprimante sous tension. Cela restaurera l&apos;état original.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation>Composants SL1 détectés (downgrade depuis SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="79"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>L&apos;imprimante va demander un paramétrage initial après le reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="58"/>
        <source>The printer will power off now.</source>
        <translation>L&apos;imprimante va maintenant être mise hors tension.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>Pour terminer la procédure de downgrade, l&apos;imprimante doit effacer la configuration et rebooter.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="89"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation>Utilisez uniquement le réservoir de résine en métal qui est fourni. Le fait d&apos;utiliser d&apos;autres réservoirs peut provoquer des fuites de résine et endommager votre imprimante !</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Cleanup...</source>
        <translation>Nettoyage...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Copying...</source>
        <translation>Copie en cours...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Done.</source>
        <translation>Fini.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Downloading ...</source>
        <translation>Téléchargement ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="55"/>
        <source>Downloading examples...</source>
        <translation>Téléchargement des exemples...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation>Exemples</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="129"/>
        <source>Failure.</source>
        <translation>Échec.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Initializing...</source>
        <translation>Initialisation...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="132"/>
        <source>Unknown state.</source>
        <translation>État inconnu.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Unpacking...</source>
        <translation>Décompression...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="141"/>
        <source>View Examples</source>
        <translation>Voir les Exemples</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="44"/>
        <source>Error code:</source>
        <translation>Code d&apos;erreur :</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="48"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Pour plus d&apos;informations, veuillez scanner le QR code ou contacter votre revendeur.</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="202"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>Configuring the connection,</source>
        <translation>Configuration de la connexion,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="123"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="35"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="148"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation>Passerelle</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="136"/>
        <source>IP Address</source>
        <translation>Adresse IP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="117"/>
        <source>Network Info</source>
        <translation>Info réseau</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>please wait...</source>
        <translation>veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="219"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Rétablir</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="61"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="68"/>
        <source>Error code:</source>
        <translation>Code d&apos;erreur :</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="72"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Pour plus d&apos;informations, veuillez scanner le QR code ou contacter votre revendeur.</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="106"/>
        <source>Save Logs to USB</source>
        <translation>Enregistrer les journaux sur l&apos;USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="119"/>
        <source>Send Logs to Cloud</source>
        <translation>Envoyer les Journaux au Cloud</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="80"/>
        <source>Swipe to proceed</source>
        <translation>Glissez pour continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation>Erreur Système</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="140"/>
        <source>Turn Off</source>
        <translation>Éteindre</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="133"/>
        <source>Update Firmware</source>
        <translation>Mise à jour du firmware</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="45"/>
        <source>Factory Reset done.</source>
        <translation>Réinitialisation d&apos;usine effectuée.</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="32"/>
        <source>Factory Reset</source>
        <translation>Réinitalisation d&apos;Usine</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation>Fini</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation>Remplissez-moi</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation>Si vous ne voulez pas refaire le plein, appuyez sur le bouton Retour en haut de l&apos;écran.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation>Remplissage résine manuel.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation>Remplissez le réservoir jusqu&apos;au repère 100% et appuyez sur Effectué.</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation>Avant d&apos;imprimer, les étapes suivantes sont requises pour poursuivre :</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation>Calibrer ?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation>Le système actuel sera toujours disponible en allant dans Réglages -&gt; Firmware -&gt; Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation>Voulez-vous vraiment installer %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation>Voulez-vous commencer maintenant ?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation>insérez une clé USB ou téléchargez des exemples dans Réglages -&gt; Assistance.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation>Installer ?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation>Local</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation>
            <numerusform>%n élément</numerusform>
            <numerusform>%n éléments</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="314"/>
        <source>No usable projects were found,</source>
        <translation>Aucun projet utilisable n&apos;a pu être trouvé,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation>Impressions Précédentes</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation>Calibration de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation>Projets</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation>Distant</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation>Distant</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation>Racine</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation>L&apos;imprimante n&apos;est pas totalement calibrée.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation>Ensembles de Mises à Jour</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation>Mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation>Calibration UV</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="127"/>
        <source>CANCELED</source>
        <translation>ANNULÉ</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="207"/>
        <source>Consumed Resin</source>
        <translation>Résine consommée</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="126"/>
        <source>FAILED</source>
        <translation>ÉCHEC</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation>Fini</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="125"/>
        <location filename="../qml/PageFinished.qml" line="128"/>
        <source>FINISHED</source>
        <translation>FINI</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="277"/>
        <source>First Layer Exposure</source>
        <translation>Exposition de la Première Couche</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="328"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="253"/>
        <source>Layer Exposure</source>
        <translation>Exposition des Couches</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>Layer height</source>
        <translation>Hauteur de couche</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="183"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="405"/>
        <source>Loading, please wait...</source>
        <translation>Chargement, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="156"/>
        <source>Print Time</source>
        <translation>Temps d&apos;Impression</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="348"/>
        <source>Reprint</source>
        <translation>Ré-imprimer</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="338"/>
        <source>Resin Tank Cleaning</source>
        <translation>Nettoyage du Réservoir de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="261"/>
        <location filename="../qml/PageFinished.qml" line="285"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="300"/>
        <source>Swipe to continue</source>
        <translation>Faites glisser pour continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="368"/>
        <source>Turn Off</source>
        <translation>Éteindre</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation>Image Plein Écran</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="51"/>
        <source>Control</source>
        <translation>Contrôle</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="58"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="65"/>
        <source>Turn Off</source>
        <translation>Éteindre</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="81"/>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation>Définir la Langue</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation>Extraction des données du journal</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation>Export des journaux</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation>Export des journaux annulé</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation>L&apos;exportation du journal a échoué. Veuillez vérifier votre clé USB/ connexion internet.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation>Export des journaux terminé</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation>Les journaux ont été téléchargés avec succès vers le serveur Prusa. &lt;br /&gt;&lt;br /&gt; Veuillez contacter le support technique Prusa et communiquer le code suivant :</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation>Téléchargement du journal terminé</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation>Sauvegarde des données sur la clé USB</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation>Téléchargement des données sur le serveur</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation>Manuel</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation>Le fait de scanner le QR code lancera le téléchargement du manuel de cet appareil.

Sinon, vous pouvez également utiliser ce lien :</translation>
    </message>
</context>
<context>
    <name>PageModifyLayerProfile</name>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="144"/>
        <source>Delay After Exposure</source>
        <translation>Délai après exposition</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="130"/>
        <source>Delay Before Exposure</source>
        <translation>Délai avant exposition</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="375"/>
        <source>Disabling the &apos;Use tilt&apos; causes the object to separate away from the film in the vertical direction only.

&apos;Tower hop height&apos; has been set to recommended value of 5 mm.</source>
        <translation>Désactiver l&apos;option &apos;Utiliser l&apos;inclinaison&apos; provoque la séparation de l&apos;objet du film uniquement dans la direction verticale.

La hauteur de saut de colonne a été réglée sur la valeur recommandée de 5 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="29"/>
        <source>Edit Layer Profile</source>
        <translation>Editer le profil de la couche</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="385"/>
        <source>Enabling the &apos;Use tilt&apos; causes the object to separate away from the film mainly by tilt.

&apos;Tower hop height&apos; has been set to 0 mm.</source>
        <translation>L&apos;activation de l&apos;option &apos;Utiliser l&apos;inclinaison&apos; provoque la séparation de l&apos;objet du film principalement par inclinaison.

La hauteur de saut de colonne a été réglée sur 0 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="160"/>
        <source>mm</source>
        <comment>millimeters</comment>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="174"/>
        <source>mm/s</source>
        <comment>millimeters per second</comment>
        <translation>mm/s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="131"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="145"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="240"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="279"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="319"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="358"/>
        <source>s</source>
        <comment>seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="266"/>
        <source>Tilt Down Cycles</source>
        <translation>Cycles d&apos;inclinaison vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="278"/>
        <source>Tilt Down Delay</source>
        <translation>Délai d&apos;inclinaison vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="254"/>
        <source>Tilt Down Finish Speed</source>
        <translation>Vitesse finale d&apos;inclinaison vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="214"/>
        <source>Tilt Down Initial Speed</source>
        <translation>Vitesse initiale d&apos;inclinaison vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="226"/>
        <source>Tilt Down Offset</source>
        <translation>Décalage de l&apos;inclinaison vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="239"/>
        <source>Tilt Down Offset Delay</source>
        <translation>Délai de décalage de l&apos;inclinaison vers le bas</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="345"/>
        <source>Tilt Up Cycles</source>
        <translation>Cycles d&apos;inclinaison vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="357"/>
        <source>Tilt Up Delay</source>
        <translation>Délai d&apos;inclinaison vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="333"/>
        <source>Tilt Up Finish Speed</source>
        <translation>Vitesse de finition de l&apos;inclinaison vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="293"/>
        <source>Tilt Up Initial Speed</source>
        <translation>Vitesse initiale d&apos;inclinaison vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="305"/>
        <source>Tilt Up Offset</source>
        <translation>Décalage d&apos;inclinaison vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="318"/>
        <source>Tilt Up Offset Delay</source>
        <translation>Délai de décalage d&apos;inclinaison vers le haut</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="159"/>
        <source>Tower Hop Height</source>
        <translation>Hauteur de saut de la colonne</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="173"/>
        <source>Tower Speed</source>
        <translation>Vitesse de la colonne</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="185"/>
        <source>Use Tilt</source>
        <translation>Utiliser l&apos;inclinaison</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="374"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="384"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="227"/>
        <source>μ-step</source>
        <comment>tilt microsteps</comment>
        <translation>μ-étape</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="306"/>
        <source>μ-step</source>
        <comment>tilt micro steps</comment>
        <translation>μ-étape</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="216"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="256"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="295"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="335"/>
        <source>μ-step/s</source>
        <comment>microsteps per second</comment>
        <translation>μ-pas/s</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation>Contrôle</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation>Désactiver les Moteurs</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation>Prise d&apos;origine de la plateforme</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation>Prise d&apos;origine du réservoir</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tank, please wait...</source>
        <translation>Mise à zéro du réservoir, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tower, please wait...</source>
        <translation>Mise à zéro de la colonne, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="68"/>
        <source>Resin Tank Cleaning</source>
        <translation>Nettoyage du Réservoir de Résine</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="32"/>
        <source>New Exposure Panel</source>
        <translation>Nouveau Panneau d&apos;Exposition</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="45"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation>Un nouvel écran d&apos;exposition a été détecté.

L&apos;imprimante demandera la configuration initiale (selftest et calibration) pour s&apos;assurer que tout fonctionne correctement.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation>Mise à jour disponible vers %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation>Impression annulée : %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation>Échec de l&apos;impression : %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation>Impression terminée : %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="55"/>
        <source>Insert protective foam</source>
        <translation>Insérer la mousse de protection</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="46"/>
        <source>Packing done.</source>
        <translation>Emballage effectué.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="32"/>
        <source>Packing Wizard</source>
        <translation>Assistant d&apos;Emballage</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="7"/>
        <source>Do you really want to turn off the printer?</source>
        <translation>Voulez-vous vraiment éteindre l&apos;imprimante ?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="10"/>
        <source>Powering Off...</source>
        <translation>Extinction...</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Power Off?</source>
        <translation>Éteindre ?</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="121"/>
        <source>Cover</source>
        <translation>Capot</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="125"/>
        <source>Delayed Start</source>
        <translation>Démarrage différé</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="151"/>
        <source>Disabled</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="140"/>
        <source>Do not touch the printer!</source>
        <translation>Ne touchez pas l&apos;imprimante !</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="120"/>
        <source>Fan</source>
        <translation>Ventillateur</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation>Veuillez patienter</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="122"/>
        <source>Resin</source>
        <translation>Résine</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="123"/>
        <source>Starting Positions</source>
        <translation>Positions de départ</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="124"/>
        <source>Stirring</source>
        <translation>Brassage</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="150"/>
        <source>With Warning</source>
        <translation>Avec avertissement</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Action Pending</source>
        <translation>Action en attente</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="117"/>
        <source>Check Warning</source>
        <translation>Consulter l&apos;Avertissement</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="151"/>
        <source>Close Cover!</source>
        <translation>Fermez le capot !</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="113"/>
        <source>Continue?</source>
        <translation>Continuer ?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Cover Open</source>
        <translation>Capot ouvert</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation>Préparation de l&apos;imprimante pour l&apos;ajout de résine. Veuillez patienter.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Going down</source>
        <translation>Abaissement</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Going up</source>
        <translation>Levage</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="130"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation>Si vous ne souhaitez pas continuer, appuyez sur le bouton noir en haut de l&apos;écran et la tâche en cours sera annulée.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Moving platform to the bottom position</source>
        <translation>Déplacement de la plateforme en position basse</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Moving platform to the top position</source>
        <translation>Déplacer la plateforme en position haute</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Moving the resin tank down...</source>
        <translation>Déplacement du réservoir de résine vers le bas...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Paused.</source>
        <translation>En pause.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Please close the cover to continue</source>
        <translation>Veuillez fermer le couvercle pour continuer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="152"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation>Veuillez fermer le couvercle ! les radiations UV sont dangereuses.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="220"/>
        <location filename="../qml/PagePrint.qml" line="277"/>
        <source>Please wait...</source>
        <translation>Veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="243"/>
        <source>Reading data...</source>
        <translation>Lecture des données...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="129"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation>Libérez le mécanisme du réservoir et appuyez sur Continuer.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation>Les actions demandées seront exécutées une fois cette couche réalisée, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Setting start positions...</source>
        <translation>Réglage des positions de départ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring</source>
        <translation>Brassage</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring resin</source>
        <translation>Brassage de la résine</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="127"/>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Stuck Recovery</source>
        <translation>Récupération après Blocage</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Tank Moving Down</source>
        <translation>Abaissement du Réservoir</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="128"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation>L&apos;imprimante est bloquée et requiert l&apos;intervention de l&apos;utilisateur.</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="302"/>
        <source>Exposure Times</source>
        <translation>Temps d&apos;exposition</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="348"/>
        <source>Last Modified</source>
        <translation>Dernière Modification</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layer Height</source>
        <translation>Hauteur de couche</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layers</source>
        <translation>Couches</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="447"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Veuillez remplir le réservoir de résine au moins à %1 % et fermez le couvercle.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="493"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="483"/>
        <source>Print Settings</source>
        <translation>Réglages d&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="329"/>
        <source>Print Time Estimate</source>
        <translation>Estimation du Temps D&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="374"/>
        <source>Swipe to continue</source>
        <translation>Faites glisser pour continuer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="166"/>
        <source>Swipe to project</source>
        <translation>Glisser vers le projet</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="444"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>Le projet requiert %1 % de résine. Il sera nécessaire de rajouter de la résine en cours d&apos;impression.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="208"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="357"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation>Inconnu</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="239"/>
        <source>Area fill:</source>
        <translation>Remplissage de la zone :</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="492"/>
        <location filename="../qml/PagePrintPrinting.qml" line="498"/>
        <source>Cancel Print</source>
        <translation>Annuler l&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="431"/>
        <source>Consumed Resin</source>
        <translation>Résine consommée</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="516"/>
        <source>Enter Admin</source>
        <translation>Saisir Admin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="373"/>
        <source>Estimated End</source>
        <translation>Fin estimée</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="403"/>
        <source>Layer</source>
        <translation>Couche</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="225"/>
        <source>Layer:</source>
        <translation>Couche :</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Layer Height:</source>
        <translation>Hauteur de couche :</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="34"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="388"/>
        <source>Printing Time</source>
        <translation>Temps d&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="473"/>
        <source>Print Settings</source>
        <translation>Réglages d&apos;impression</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="274"/>
        <source>Progress:</source>
        <translation>Progression :</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="479"/>
        <source>Refill Resin</source>
        <translation>Recharger de la Résine</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="417"/>
        <source>Remaining Resin</source>
        <translation>Résine restante</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="358"/>
        <source>Remaining Time</source>
        <translation>Temps restant</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Today at</source>
        <translation>Aujourd&apos;hui à</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="499"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation>Afin de vous assurer que l&apos;impression ne s&apos;arrête pas accidentellement, veuillez faire glisser l&apos;écran vers la prochaine étape, où vous pouvez annuler l&apos;impression.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="110"/>
        <source>Tomorrow at</source>
        <translation>Demain à</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="363"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Veuillez remplir le réservoir de résine au moins à %1 % et fermez le couvercle.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation>Verser de la résine</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>Le projet requiert %1 % de résine. Il sera nécessaire de rajouter de la résine en cours d&apos;impression.</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation>QR code de la page</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation>Installer maintenant</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation>Plus tard</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation>Notes de Version</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="102"/>
        <source>Can you hear the music?</source>
        <translation>Entendez-vous la musique ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="77"/>
        <location filename="../qml/PageSelftestWizard.qml" line="140"/>
        <location filename="../qml/PageSelftestWizard.qml" line="173"/>
        <source>Close the cover.</source>
        <translation>Fermez le capot.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Desserrez le bouton noir et retirez la plateforme.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="155"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation>Assurez-vous que le réservoir de résine est installé et que les vis sont serrées.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="164"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation>Veuillez installer la plateforme selon un angle de 0° et serrez-la avec la molette noire.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="122"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation>Veuillez installer la plateforme selon un angle de 60° et la serrer avec la mollette noire.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="113"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation>Veuillez installer le réservoir de résine et serrer les vis de façon égale.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Veuillez dévisser et enlever le réservoir de résine.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="131"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation>Séparez la plateforme du cantilever et placez-la au centre du réservoir de résine selon un angle de 90°.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="51"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation>Cette procédure est obligatoire et permet de vérifier tous les composants de l&apos;imprimante.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="49"/>
        <source>Welcome to the selftest wizard.</source>
        <translation>Bienvenu dans l&apos;assistant du selftest.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="676"/>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation>Définir la Date</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only a-z, 0-9 and  &quot;-&quot;. Must not begin or end with &quot;-&quot;.</source>
        <translation>Peut contenir uniquement a-z, 0-9 et &quot;-&quot;. Ne doit pas commencer ou se terminer par &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaConnectRegistration</name>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="90"/>
        <source>Add Printer to Prusa Connect</source>
        <translation>Ajouter une imprimante à Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="79"/>
        <source>In Progress</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="78"/>
        <source>Not Registered</source>
        <translation>Non enregistré</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="31"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="80"/>
        <source>Registered</source>
        <translation>Enregistré</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="96"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="126"/>
        <source>Register Printer</source>
        <translation>Onglet Imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="75"/>
        <source>Registration Status</source>
        <translation>Statut d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="123"/>
        <source>Review the Registration QR Code</source>
        <translation>Examiner le code QR d&apos;enregistrement</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="98"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="128"/>
        <source>Scan the QR Code or visit &lt;b&gt;prusa.io/add&lt;/b&gt;. Log into your Prusa Account to add this printer.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</source>
        <translation>Scannez le code QR ou visitez &lt;b&gt;prusa.io/add&lt;/b&gt;. Connectez-vous à votre compte Prusa pour ajouter cette imprimante.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code : %1</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="81"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="111"/>
        <source>Unregister Printer from Prusa Connect</source>
        <translation>Désenregistrer l&apos;imprimante de Prusa Connect</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaLinkWebLogin</name>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="131"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation>Peut contenir uniquement les caractères a-z, A-Z, 0-9 et &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="117"/>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="125"/>
        <source>Must be at least 8 chars long</source>
        <translation>Doit être long d&apos;au moins 8 caractères</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="92"/>
        <source>Printer Password</source>
        <translation>Mot de passe de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="32"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="144"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="55"/>
        <source>User Name</source>
        <translation>Nom d&apos;Utilisateur</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation>Régler</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation>Régler l&apos;heure</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="134"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="128"/>
        <source>Region</source>
        <translation>Région</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="140"/>
        <source>Set</source>
        <translation>Définir</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation>Définir le fuseau horaire</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="60"/>
        <source>Calibration</source>
        <translation>Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="150"/>
        <source>Enter Admin</source>
        <translation>Saisir Admin</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="89"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="119"/>
        <source>Language &amp; Time</source>
        <translation>Langue &amp; Heure</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="69"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="79"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Plateforme &amp; Réservoir de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="29"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="99"/>
        <source>Settings &amp; Sensors</source>
        <translation>Réglages &amp; Capteurs</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Support</source>
        <translation>Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>System Logs</source>
        <translation>Journaux Système</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="109"/>
        <source>Touchscreen</source>
        <translation>Écran tactile</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="32"/>
        <source>Calibration</source>
        <translation>Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="94"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation>Avez-vous changé l&apos;ÉCRAN D&apos;EXPOSITION ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="107"/>
        <source>Did you replace the UV LED SET?</source>
        <translation>Avez-vous changé le SET LED UV ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="76"/>
        <source>Display Test</source>
        <translation>Test de l&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>New Display?</source>
        <translation>Nouvel écran ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>New UV LED SET?</source>
        <translation>Nouveau SET LED UV ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="58"/>
        <source>Printer Calibration</source>
        <translation>Calibration de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="51"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="68"/>
        <source>UV Calibration</source>
        <translation>Calibration UV</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation>Après avoir installé cette mise à jour, l&apos;imprimante peut bénéficier d&apos;un autre FW mais &lt;b&gt;il ne sera pas possible d&apos;imprimer.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="414"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;n&apos;est pas compatible avec votre modèle matériel actuel - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation>La vérification des mises à jour a échoué</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation>Vérification des mises a jour...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="215"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>Continue anyway?</source>
        <translation>Continuer quand même ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Downgrade?</source>
        <translation>Downgrader ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="347"/>
        <source>Downgrade Firmware?</source>
        <translation>Downgrader le Firmware ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation>Télécharger maintenant</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="182"/>
        <source>Do you really want to downgrade to FW</source>
        <translation>Voulez-vous vraiment rétrograder vers le FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation>Voulez-vous vraiment effectuer un retour aux paramètres d&apos;usine ?

Tous les réglages seront effacés !
Les projets ne craignent rien.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>Do you want to continue?</source>
        <translation>Voulez-vous continuer ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="248"/>
        <source>Factory Reset</source>
        <translation>Réinitialisation usine</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="36"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="334"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation>Impossible de trouver les méta-informations du fichier FW. &lt;br/&gt;&lt;br/&gt;Voulez-vous installer&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;quand même ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="290"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation>Infos FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="213"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation>Si vous changez, vous pourrez mettre à jour vers une autre version du FW ensuite &lt;b&gt;mais vous ne pourrez pas imprimer.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="413"/>
        <source>Incompatible FW!</source>
        <translation>FW incompatible !</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation>Version installée</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="333"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="382"/>
        <source>Install Firmware?</source>
        <translation>Installer le Firmware ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation>Installer maintenant</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>est inférieur ou égal au &lt;br/&gt;&lt;b&gt;%1&lt;/b&gt; actuel.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="291"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation>Chargement des méta-informations du fichier FW
(peut prendre jusqu&apos;à 20 secondes) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="239"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Plus récent que la SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="235"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="425"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="138"/>
        <source>Receive Beta Updates</source>
        <translation>Recevoir les Mises à Jour Bêta</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Switch to beta?</source>
        <translation>Basculer vers la bêta ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="169"/>
        <source>Switch to version:</source>
        <translation>Basculer vers la version :</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation>Le système est à jour</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="211"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>La version alternative du FW &lt;b&gt;%1&lt;/b&gt; n&apos;est pas compatible avec votre modèle d&apos;imprimante - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="428"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation>Mise à jour disponible</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation>Le téléchargement de la mise à jour a échoué</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation>La mise à jour est en cours de téléchargement</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation>La mise à jour est en cours de téléchargement</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation>Service de mise à jour inactif</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="350"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation>Version &lt;br/&gt;&lt;b&gt;%1&lt;/b&gt; du fichier FW sélectionné,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="157"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation>Attention ! Les mises à jour bêta peuvent être instables.&lt;br/&gt;&lt;br/&gt;Non recommandé pour les imprimantes de production.&lt;br/&gt;&lt;br/&gt; Continuer ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>qui a la version &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="383"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation>Vous avez sélectionné l&apos;ensemble de mise à jour &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="29"/>
        <source>Language &amp; Time</source>
        <translation>Langue &amp; Heure</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="35"/>
        <source>Set Language</source>
        <translation>Définir la Langue</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="41"/>
        <source>Time Settings</source>
        <translation>Réglages de l&apos;heure</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="37"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="74"/>
        <source>Hostname</source>
        <translation>Nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Hotspot</source>
        <translation>Point d&apos;accès</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="29"/>
        <source>Network</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="89"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="81"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="53"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="109"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="48"/>
        <source>Disable Steppers</source>
        <translation>Désactiver les moteurs pas à pas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="102"/>
        <source>Limit for Fast Tilt</source>
        <translation>Limite pour l&apos;Inclinaison rapide</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="34"/>
        <source>Move Platform</source>
        <translation>Déplacer la Plateforme</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="41"/>
        <source>Move Resin Tank</source>
        <translation>Déplacer le Réservoir de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="56"/>
        <source>Platform Axis Sensitivity</source>
        <translation>Sensibilité de l&apos;Axe de la Plateforme</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="128"/>
        <source>Platform Offset</source>
        <translation>Décalage de la Plateforme</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="28"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Plateforme &amp; Réservoir de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="159"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="79"/>
        <source>Tank Axis Sensitivity</source>
        <translation>Sensibilité de l&apos;axe du réservoir</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="152"/>
        <source>Tank Surface Cleaning Exposure</source>
        <translation>Exposition de Nettoyage de la Surface du Réservoir</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="135"/>
        <source>um</source>
        <translation>um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="112"/>
        <location filename="../qml/PageSettingsSensors.qml" line="148"/>
        <source>Are you sure?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="61"/>
        <source>Auto Power Off</source>
        <translation>Extinction Automatique</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="87"/>
        <source>Cover Check</source>
        <translation>Vérification du capot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="36"/>
        <source>Device hash in QR</source>
        <translation>Hash de l&apos;appareil dans le QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="113"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation>Désactiver le capteur du couvercle ? &lt;br/&gt;&lt;br/&gt;ATTENTION : Cela peut provoquer une exposition involontaire à la lumière UV ou à des blessures du fait de pièces mobiles. Cette action n&apos;est pas recommandée !</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="149"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation>Désactiver le capteur de résine ?&lt;br/&gt;&lt;br/&gt; ATTENTION : Cela pourrait entraîner des échecs d&apos;impression ou un débordement du réservoir de résine ! Cette action n&apos;est pas recommandée !</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="180"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="161"/>
        <source>Rear Fan Speed</source>
        <translation>Vitesse du ventilateur arrière</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="125"/>
        <source>Resin Sensor</source>
        <translation>Capteur de résine</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="168"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="28"/>
        <source>Settings &amp; Sensors</source>
        <translation>Réglages &amp; Capteurs</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="78"/>
        <source>About Us</source>
        <translation>À propos de nous</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="36"/>
        <source>Download Examples</source>
        <translation>Télécharger des Exemples</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="50"/>
        <source>Manual</source>
        <translation>Manuel</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="30"/>
        <source>Support</source>
        <translation>Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="69"/>
        <source>System Information</source>
        <translation>Informations Système</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="60"/>
        <source>Videos</source>
        <translation>Vidéos</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Aucun journal n&apos;a été téléchargé pour l&apos;instant.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>Last Seen Logs:</source>
        <translation>Derniers Journaux Consultés :</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="55"/>
        <source>Save to USB Drive</source>
        <translation>Enregistrer sur la clé USB</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="29"/>
        <source>System Logs</source>
        <translation>Journaux Système</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="65"/>
        <source>Upload to Server</source>
        <translation>Télécharger vers le serveur</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="98"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="63"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="58"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="36"/>
        <source>Screensaver timer</source>
        <translation>Minuterie de l&apos;économiseur d&apos;écran</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="80"/>
        <source>Touch Screen Brightness</source>
        <translation>Luminosité de l&apos;Écran Tactile</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="28"/>
        <source>Touchscreen</source>
        <translation>Écran tactile</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="81"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation>Jeton Temporaire</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation>Nom du paquet</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation>Paquets Logiciels</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation>SN du contrôleur A64</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>Ambient Temperature</source>
        <translation>Température Ambiante</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <source>Blower Fan</source>
        <translation>Ventilateur radial</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation>Numéro de série de la Carte Booster</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <location filename="../qml/PageSysinfo.qml" line="192"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Closed</source>
        <translation>Fermé</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>Cover State</source>
        <translation>État du capot</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>CPU Temperature</source>
        <translation>Température du CPU</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation>Adresse IP de l&apos;Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation>Numéro de série de l&apos;écran d&apos;exposition</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="199"/>
        <location filename="../qml/PageSysinfo.qml" line="209"/>
        <location filename="../qml/PageSysinfo.qml" line="219"/>
        <source>Fan Error!</source>
        <translation>Erreur Ventilateur !</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Finished Projects</source>
        <translation>Projets Terminés</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation>Version de l&apos;interface graphique</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation>État du matériel</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation>Révision du matérielle du contrôleur de mouvement</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation>SN du contrôleur de mouvement</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation>Version logicielle du contrôleur de mouvement</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation>État du réseau</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Plus récent que la SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Not triggered</source>
        <translation>Non activé</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation>Hors ligne</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation>En ligne</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation>Version de l&apos;image de l&apos;OS</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation>Autres Composants</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="240"/>
        <source>Power Supply Voltage</source>
        <translation>Tension d&apos;alimentation</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Print Display Time Counter</source>
        <translation>Chronomètre de l&apos;Affichage de l&apos;Impression</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation>Modèle d&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>Printer Password</source>
        <translation>Mot de passe de l&apos;imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>Rear Fan</source>
        <translation>Ventilateur arrière</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Resin Sensor State</source>
        <translation>État du capteur de résine</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="298"/>
        <source>Show software packages &amp; licenses</source>
        <translation>Afficher les paquets logiciels &amp; les licences</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Started Projects</source>
        <translation>Projets Démarrés</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation>Informations Système</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation>Temps d&apos;inclinaison rapide</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Time of High Viscosity Tilt:</source>
        <translation>Temps d&apos;Inclinaison pour la Haute Viscosité :</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation>Temps d&apos;inclinaison lente</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Layers Printed</source>
        <translation>Total des Couches Imprimées</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Print Time</source>
        <translation>Temps Total d&apos;Impression</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="287"/>
        <source>Total Resin Consumed</source>
        <translation>Consommation Totale de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Triggered</source>
        <translation>Activé</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="226"/>
        <source>UV LED</source>
        <translation>LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <source>UV LED Fan</source>
        <translation>Ventilateur des LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>UV LED Temperature</source>
        <translation>Température des LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>UV LED Time Counter</source>
        <translation>Compteur de temps des LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="241"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation>Adresse IP du Wifi</translation>
    </message>
</context>
<context>
    <name>PageTankSurfaceCleanerWizard</name>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="160"/>
        <source>Cleaning Adaptor</source>
        <translation>Adaptateur de Nettoyage</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="149"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="134"/>
        <source>Exposure[s]:</source>
        <translation>Exposition [s] :</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="81"/>
        <source>Here you can optionally adjust the exposure settings before cleaning starts.</source>
        <translation>Ici, vous pouvez éventuellement régler les paramètres d&apos;exposition avant le début du nettoyage.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="89"/>
        <source>Less</source>
        <translation>Moins</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="108"/>
        <source>More</source>
        <translation>Plus</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="172"/>
        <source>Now remove the cleaning adaptor along with the exposed film, careful not to tear it.</source>
        <translation>Maintenant, retirez l&apos;adaptateur de nettoyage avec la couche exposée. Faites attention à ne pas la déchirer.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="161"/>
        <source>Please insert the cleaning adaptor as is ilustrated in the picture on the left.</source>
        <translation>Veuillez insérer l&apos;adaptateur de nettoyage comme indiqué sur l&apos;image de gauche.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="171"/>
        <source>Remove Adaptor</source>
        <translation>Retirez l&apos;Adaptateur</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="33"/>
        <source>Resin Tank Cleaning</source>
        <translation>Nettoyage du Réservoir de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="61"/>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="69"/>
        <source>Tank Cleaning</source>
        <translation>Nettoyage du Réservoir</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="62"/>
        <source>This wizard will help you clean your resin tank and remove resin debris.

You will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine&apos;s internal storage. Or you can download it at printables.com</source>
        <translation>Cet assistant vous aidera à nettoyer votre réservoir de résine et à éliminer les débris de résine.

Vous aurez besoin d&apos;un outil spécial - un adaptateur de nettoyage. Vous pouvez l&apos;imprimer vous-même - il est inclus comme impression d&apos;exemple dans le stockage interne de cette machine. Ou vous pouvez le télécharger sur printables.com</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation>Descente rapide</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation>Montée rapide</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation>Déplacer le Réservoir de Résine</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation>Haut</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="139"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation>12 heures</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="140"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation>24 heures</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="163"/>
        <source>Automatic time settings using NTP ...</source>
        <translation>Réglage d&apos;heure automatique à l&apos;aide du NTP...</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="94"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="138"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation>Natif</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="73"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="133"/>
        <source>Time Format</source>
        <translation>Format de l&apos;heure</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="35"/>
        <source>Time Settings</source>
        <translation>Réglages de l&apos;heure</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="115"/>
        <source>Timezone</source>
        <translation>Fuseau horaire</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="47"/>
        <source>Use NTP</source>
        <translation>Réglage automatique de l&apos;heure</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation>Bas</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation>Descente rapide</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation>Montée rapide</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation>Déplacer la Plateforme</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation>Haut</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="79"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Retirez délicatement l&apos;autocollant de protection de l&apos;écran d&apos;exposition.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="69"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation>Retirez l&apos;autocollant de sécurité et ouvrez le couvercle.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="59"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation>Retirez la mousse noire des deux côtés de la plateforme.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Décompression</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="89"/>
        <source>Unpacking done.</source>
        <translation>Décompression terminée.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="49"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation>Dévissez et retirez le réservoir de résine et retirez la mousse noir sous celui-ci.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="46"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Retirez délicatement l&apos;autocollant de protection de l&apos;écran d&apos;exposition.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Décompression</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="56"/>
        <source>Unpacking done.</source>
        <translation>Décompression terminée.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="419"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation>Un problème est survenu pendant la mise à jour, veuillez nous informer de ce qu&apos;il s&apos;est passé (et envoyez-nous les journaux). Ne paniquez pas, votre imprimante fonctionne toujours comme auparavant. Appuyez sur &quot;Retour&quot; pour continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="296"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation>N&apos;éteignez pas l&apos;imprimante pendant la mise à jour&#xa0;!&lt;br/&gt;L&apos;imprimante sera redémarrée après une mise à jour réussie.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="229"/>
        <source>Download failed.</source>
        <translation>Échec du téléchargement.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation>Téléchargement</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation>Téléchargement du firmware, l&apos;installation démarrera juste après.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="255"/>
        <source>Installing Firmware</source>
        <translation>Installation du Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="360"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation>L&apos;imprimante redémarre avec le nouveau firmware(%1), veuillez patienter</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation>Mise à Jour de l&apos;Imprimante</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="343"/>
        <source>Updated to %1 failed.</source>
        <translation>La mise à jour vers %1 a échoué.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="386"/>
        <source>Update to %1 failed.</source>
        <translation>Échec de la mise à jour vers %1.</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="32"/>
        <source>Hardware Upgrade</source>
        <translation>Mise à Niveau du Matériel</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="90"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Utilisez seulement la plateforme fournie. Si vous utilisez une plateforme différente la résine peut s&apos;écouler et endommager votre imprimante !</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="100"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Veuillez noter que le downgrade n&apos;est pas pris en charge.

Le fait de downgrader votre imprimante va effacer votre calibration UV et votre imprimante ne fonctionnera pas correctement.

Vous devrez la recalibrer en utilisant un calibreur UV externe.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="49"/>
        <source>Proceed?</source>
        <translation>Continuer ?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="60"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation>Réassemblez les composants SL1 et mettez l&apos;imprimante sous tension. Cela va restaurer l&apos;état original.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation>Composants SL1S détectés (mise à niveau depuis SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="69"/>
        <source>The configuration is going to be cleared now.</source>
        <translation>La configuration va maintenant être effacée.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="70"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>L&apos;imprimante va demander un paramétrage initial après le reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="59"/>
        <source>The printer will power off now.</source>
        <translation>L&apos;imprimante va maintenant être mise hors tension.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="48"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>Pour terminer la procédure de mise à niveau, l&apos;imprimante doit effacer la configuration et redémarrer.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="109"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation>Mise à niveau effectuée. Au cours de la prochaine étape, l&apos;imprimante sera redémarrée.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="80"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation>Utilisez uniquement le réservoir de résine en plastique qui est fourni. Le fait d&apos;utiliser l&apos;ancien réservoir en métal peut provoquer des fuites de résine et endommager votre imprimante!</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation>1. Si le réservoir de résine est dans l&apos;imprimante, retirez-le ainsi que les vis.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="71"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation>1. Positionnez le calibreur UV sur l&apos;écran d&apos;impression et connectez-le au port USB avant.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="52"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="73"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation>2. Fermez le capot, ne l&apos;ouvrez pas ! Les rayons UV peuvent être dangereux !</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Intensity: center %1, edge %2</source>
        <translation>Intensité : centre %1, bords %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="84"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation>Ouvrez le capot, &lt;br&gt;enlevez et déconnectez&lt;/b&gt; le calibrateur UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="107"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation>L&apos;imprimante a été calibrée avec succès !
Voulez-vous appliquer les résultats de la calibration ?</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="100"/>
        <source>The result of calibration:</source>
        <translation>Le résultat de la calibration :</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="32"/>
        <source>UV Calibration</source>
        <translation>Calibration UV</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation>Intensité UV : %1, σ = %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="103"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation>Intensité UV min : %1, max : %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="101"/>
        <source>UV PWM: %1</source>
        <translation>PWM UV : %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="57"/>
        <source>Warm-up: %1 s</source>
        <translation>Réchauffement : %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>Welcome to the UV calibration.</source>
        <translation>Bienvenu dans la calibration UV.</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation>Chargement, veuillez patienter...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation>Le fait de scanner le QR code vous conduira sur notre playlist YouTube qui contient des vidéos concernant cet appareil.

Sinon, vous pouvez également utiliser ce lien :</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation>Vidéos</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation>Veuillez patienter</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="193"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="203"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation>Configuration de la connexion, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="233"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Voulez-vous vraiment oublier les paramètres de ce réseau ?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="232"/>
        <source>Forget network?</source>
        <translation>Oublier le réseau ?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="223"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation>Oublier le réseau</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation>Info réseau</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="212"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Rétablir</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation>Réglages du sans-fil</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="71"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="59"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="115"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation>Glissez pour continuer</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="102"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="265"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="174"/>
        <source>Swipe for a picture</source>
        <translation>Balayez pour une image</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="253"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation>Branché</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation>Débranché</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="113"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="172"/>
        <source>On</source>
        <translation>On</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation>Veuillez patienter...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation>Glissez pour confirmer</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation>Ne doit pas être vide, seuls 0-9, a-z, A-Z, _ et -  sont autorisés ici !</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="115"/>
        <source>back</source>
        <translation>retour</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="130"/>
        <source>cancel</source>
        <translation>annuler</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="144"/>
        <source>close</source>
        <translation>fermer</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation>Activation</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>ambient temperature</source>
        <translation>température ambiante</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>blower fan</source>
        <translation>ventilateur radial</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="650"/>
        <source>Cancel?</source>
        <translation>Annulé?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="665"/>
        <source>Cancel the current print job?</source>
        <translation>Annuler le travail d&apos;impression en cours ?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>CPU temperature</source>
        <translation>Température du CPU</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation>Désactivation</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="699"/>
        <location filename="../qml/main.qml" line="723"/>
        <source>DEPRECATED PROJECTS</source>
        <translation>PROJETS OBSOLÈTES</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="407"/>
        <source>Initializing...</source>
        <translation>Initialisation...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="417"/>
        <source>MC Update</source>
        <translation>Mise à jour de la MC</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="310"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="356"/>
        <source>Now it&apos;s safe to remove USB drive</source>
        <translation>Il est maintenant possible de retirer la clé USB en toute sécurité</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>Press &quot;cancel&quot; to abort.</source>
        <translation>Appuyez sur &quot;annuler&quot; pour abandonner.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="688"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation>L&apos;imprimante ne peut être éteinte avec ce statut. Terminez ou annulez l&apos;action en cours et réessayez.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation>Interface Utilisateur Écran Tactile SL1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>rear fan</source>
        <translation>ventilateur arrière</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="355"/>
        <source>Remove USB</source>
        <translation>Retirer la clé USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="339"/>
        <source>Remove USB flash drive?</source>
        <translation>Retirer la clé USB ?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="714"/>
        <location filename="../qml/main.qml" line="737"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="724"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation>Quelques projets non-compatibles ont été trouvés, vous pouvez les télécharger sur

http://%1/old-projects.

Voulez-vous les supprimer ?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="700"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation>Des projets incompatibles ont été trouvés, vous pouvez les télécharger sur

http://%1/old-projects

Voulez-vous les supprimer&#xa0;?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="418"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation>Mise à jour du firmware du Contrôleur de Mouvement.

Veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="408"/>
        <source>The printer is initializing, please wait ...</source>
        <translation>Initialisation de l&apos;imprimante, veuillez patienter...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>The USB drive cannot be safely removed because it is now in use, please wait.</source>
        <translation>Le lecteur USB est en cours d&apos;utilisation. Veuillez patienter.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="637"/>
        <source>Turn Off?</source>
        <translation>Éteindre ?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>unknown device</source>
        <translation>dispositif inconnu</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>UV LED fan</source>
        <translation>Ventilateur des LED UV</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="101"/>
        <source>UV LED temperature</source>
        <translation>Température des LED UV</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation>Moins d&apos;une minute</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n heure</numerusform>
            <numerusform>%n heures</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
</context>
</TS>
