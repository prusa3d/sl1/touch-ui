<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs-CZ">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="90"/>
        <source>Add Hidden Network</source>
        <translation>Přidat skrytou síť</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Plugged in</source>
        <translation>Zapojeno</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Unplugged</source>
        <translation>Odpojeno</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="37"/>
        <source>Network Info</source>
        <translation>Info o síti</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="38"/>
        <source>Wi-Fi Client</source>
        <translation>Wi-Fi klient</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Opravdu chcete smazat nastavení této sítě?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="105"/>
        <source>Forget network?</source>
        <translation>Zapomenout nastavení sítě?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="35"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="136"/>
        <source>Apply calibration results</source>
        <translation>Aplikování výsledků kalibrace</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="134"/>
        <source>Calibrate center</source>
        <translation>Kalibrování středu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="135"/>
        <source>Calibrate edge</source>
        <translation>Kalibrování okrajů</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="174"/>
        <source>Canceled</source>
        <translation>Zrušeno</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="131"/>
        <source>Check for UV calibrator</source>
        <translation>Kontrola UV kalibrátoru</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="156"/>
        <source>Check ID:</source>
        <translation>Kontrola ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation>Mazání profilů Sliceru</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Clear UV calibration data</source>
        <translation>Mazání dat UV kalibrace</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Disable factory mode</source>
        <translation>Vypnutí továrního režimu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="130"/>
        <source>Disable ssh, serial</source>
        <translation>Vypínání SSH, sériové komunikace</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Display test</source>
        <translation>Test displeje</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Erase motion controller EEPROM</source>
        <translation>Mazání EEPROM Motion Controlleru</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Erase projects</source>
        <translation>Mazání projektů</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="140"/>
        <source>Erase UV PWM settings</source>
        <translation>Mazání nastavení UV PWM</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="150"/>
        <source>Exposing debris</source>
        <translation>Ozařování nečistot</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="171"/>
        <source>Failure</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Make tank accessible</source>
        <translation>Zpřístupnění vaničky</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="148"/>
        <source>Moving platform down to safe distance</source>
        <translation>Přemístění platformy do bezpečné vzdálenosti</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="151"/>
        <source>Moving platform gently up</source>
        <translation>Jemný posun platformy vzhůru</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Moving printer to accept protective foam</source>
        <translation>Příprava tiskárny pro ochr. pěnu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Obtain calibration info</source>
        <translation>Získání kalibračních informací</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Obtain system info</source>
        <translation>Získání systémových informací</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="170"/>
        <source>Passed</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Platform calibration</source>
        <translation>Kalibrace platformy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="153"/>
        <source>Platform home</source>
        <translation>Parkování platformy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Platform range</source>
        <translation>Rozsah platformy</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Pressing protective foam</source>
        <translation>Stlačení ochranné pěny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Printer calibration</source>
        <translation>Kalibrace tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>Recording changes</source>
        <translation>Zaznamenávání změn</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Release foam</source>
        <translation>Uvolnění pěny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Reset homing profiles</source>
        <translation>Resetování parkovacích profilů</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>Reset hostname</source>
        <translation>Resetování hostname</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Reset Network settings</source>
        <translation>Obnovení nastavení sítě</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Reset NTP state</source>
        <translation>Resetování stavu NTP</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset print configuration</source>
        <translation>Resetování tiskové konfigurace</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Reset printer calibration status</source>
        <translation>Resetování kalibrace tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Reset Prusa Connect</source>
        <translation>Resetování Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>Reset PrusaLink</source>
        <translation>Resetování PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Reset selftest status</source>
        <translation>Resetování stavu selftestu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Reset system locale</source>
        <translation>Resetování jazyka</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Reset timezone</source>
        <translation>Resetování časového pásma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Resetting hardware counters</source>
        <translation>Resetuji hardwarové čítače</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="139"/>
        <source>Reset UI settings</source>
        <translation>Resetování rozhraní</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="154"/>
        <source>Reset update channel</source>
        <translation>Obnovení aktualizačního kanálu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Resin sensor</source>
        <translation>Senzor resinu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="169"/>
        <source>Running</source>
        <translation>Probíhá</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Send printer data to MQTT</source>
        <translation>Odeslání dat tiskárny do MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Serial number</source>
        <translation>Sériové číslo</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Set new printer model</source>
        <translation>Nastavení nového modelu tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Sound test</source>
        <translation>Test zvuku</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Tank calib. start</source>
        <translation>Start kalib. vaničky</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Tank home</source>
        <translation>Parkování vaničky</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="138"/>
        <source>Tank level</source>
        <translation>Vyrovnání vaničky</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Tank range</source>
        <translation>Rozsah pohybu vaničky</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Temperature</source>
        <translation>Teplota</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Tilt timming</source>
        <translation>Měření doby náklonu</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="149"/>
        <source>Touching platform down</source>
        <translation>Platforma se přesunuje dolů</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="173"/>
        <source>User action pending</source>
        <translation>Akce je v pořadí</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="133"/>
        <source>UV calibrator placed</source>
        <translation>UV kalibrátor umístěn</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>UV LED</source>
        <translation>UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>UV LED and fans</source>
        <translation>UV LED a ventilátory</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="132"/>
        <source>UV LED warmup</source>
        <translation>Zahřívání UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="168"/>
        <source>Waiting</source>
        <translation>Tiskárna čeká</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="152"/>
        <source>Waiting for user</source>
        <translation>Čekám na uživatele</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="137"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation>Čekání na odstranění UV kalibrátoru</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="172"/>
        <source>With Warning</source>
        <translation>S varováním</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="56"/>
        <source>Understood</source>
        <translation>Rozumím</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation>Neznámá chyba</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>A64 OVERHEAT</source>
        <translation>PŘEHŘÁTÍ A64</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation>Teplota A64 je příliš vysoká. Naměřeno: %(temperature).1f °C! Vypnutí za 10 sekund...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation>ADMIN NENÍ DOSTUPNÝ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="234"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation>OKOLNÍ TEPLOTA PŘÍLIŠ VYSOKÁ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="235"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation>OKOLNÍ TEPLOTA PŘÍLIŠ NÍZKÁ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>Analysis of the project failed</source>
        <translation>Analýza projektu selhala</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation>Už probíhá jiná akce. Ukončete tuto akci přímo pomocí dotykové obrazovky tiskárny.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>An unexpected error has occurred.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation>Došlo k nečekané chybě.
Pokud probíhá tisk, měl by být dokončen.
Tiskárnu můžete vypnout předním tlačítkem.
Budeme rádi, pokud nám pošlete log - v příručce najdete návod, jak log vygenerovat.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation>Neznámé varování. Restartujte tiskárnu a opakujte akci. Pokud problém přetrvává, kontaktujte podporu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>A part of the LED panel is disconnected.</source>
        <translation>Část LED panelu je odpojená.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation>POTÍŽE S BOOSTER DESKOU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="231"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation>ZMĚNA SPOUŠTĚCÍHO SLOTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>Broken UV LED panel</source>
        <translation>Rozbitý UV LED panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>CALIBRATION ERROR</source>
        <translation>CHYBA KALIBRACE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>CALIBRATION FAILED</source>
        <translation>KALIBRACE SELHALA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation>SELHALO NAČTENÍ KALIBRACE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>Calibration project is invalid</source>
        <translation>Neplatný kalibrační projekt</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation>NEPLATNÝ KALIBRAČNÍ PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation>Nelze navázat spojení s UV kalibrátorem. Zkontrolujte připojení a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Cannot export profile</source>
        <translation>Nelze exportovat profil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Cannot find the selected file!</source>
        <translation>Nelze nalézt vybraný soubor!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation>Nelze nastavit kanál pro aktualizace. Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>Cannot import profile</source>
        <translation>Nelze importovat profil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>CANNOT READ PROJECT</source>
        <translation>NELZE NAČÍST PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation>NELZE SMAZAT PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation>Nelze odeslat tovární konfiguraci do databáze (MQTT)! Zkontrolujte připojení k síti. Pokud problémy přetrvávají, kontaktujte podporu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation>Nelze nastavit kanál pro aktualizace. Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="236"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation>NELZE ZKOPÍROVAT PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>CLEANING ADAPTOR MISSING</source>
        <translation>CHYBÍ ČISTÍCÍ ADAPTÉR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>Cleaning adaptor was not detected, it does not seem to be correctly attached to the print platform.
Attach it properly and try again.</source>
        <translation>Čistící adaptér nebyl detekován - pravděpodobně není správně připojen k tiskové platformě. Ujistěte se, že je umístěn správně, a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Communication with the Booster board failed.</source>
        <translation>Komunikace s Booster boardem selhala.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation>Selhala komunikace s UV kalibrátorem. Zkontrolujte připojení a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation>CHYBA ČTENÍ KONFIGURAČNÍHO SOUBORU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>CONNECTION FAILED</source>
        <translation>PŘIPOJENÍ SE NEZDAŘILO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation>Připojení k serverům Prusa Research se nezdařilo. Opakujte akci později.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation>Nalezeny správné hodnoty, ale směrodatná odchylka
(%(found).1f) je větší než povolená hodnota (%(allowed).1f).
Ověřte umístění a kalibraci UV LED kalibrátoru a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation>Data jsou z neznámého UV LED senzoru!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation>PŘEPSÁNÍ DAT SELHALO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation>CHYBA PŘÍPRAVY DAT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation>ADRESÁŘ NENÍ PRÁZDNÝ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>Disconnected UV LED panel</source>
        <translation>Odpojený UV LED panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>DISPLAY TEST ERROR</source>
        <translation>CHYBA TESTU DISPLEJE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Display test failed.</source>
        <translation>Selhal test displeje.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>DISPLAY TEST FAILED</source>
        <translation>SELHAL TEST DISPLEJE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>Display usage error</source>
        <translation>Chyba použití displeje</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>DOWNLOAD FAILED</source>
        <translation>STAHOVÁNÍ SELHALO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Error displaying test image.</source>
        <translation>Nelze zobrazit testovací obraz.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>Error, there is no file to reprint.</source>
        <translation>Chyba, neexistuje žádný soubor pro opakovaný tisk.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation>V úložišti chybí ukázkové modely. Stáhněte je znovu přes menu Nastavení.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="246"/>
        <source>EXPECT OVERHEATING</source>
        <translation>HROZÍ PŘEHŘÍVÁNÍ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation>Osvitový displej, který je momentálně připojený, byl do této tiskárny připojen už dříve. Byl používán zhruba %(counter_h)d hodin.

Pokud nechcete tento displej používat, vypněte tiskárnu, vyměňte displej a tiskárnu opět zapněte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation>Nepodařilo se změnit úroveň protokolu. Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation>Chyba nahrávání tovární kalibrace ventilátorů a LED.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Failed to reach the tilt endstop.</source>
        <translation>Nenalezen koncový doraz náklonu (tiltu).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tower endstop.</source>
        <translation>Nepodařilo se dosáhnout koncového dorazu věže.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation>Načtení konfiguračního souboru se nezdařilo. Zkuste restartovat tiskárnu. Pokud problém přetrvává, kontaktujte naši podporu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation>Nepodařilo se uložit data Průvodce. Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation>Serializace dat v Průvodci se nezdařila. Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Failed to set hostname</source>
        <translation>Chyba při nastavování hostname</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>FAN FAILURE</source>
        <translation>CHYBA VENTILÁTORU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation>OTÁČKY VENTILÁTORU MIMO TESTOVACÍ ROZSAH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="245"/>
        <source>FAN WARNING</source>
        <translation>VENTILÁTOR: VAROVÁNÍ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>FILE ALREADY EXISTS</source>
        <translation>SOUBOR EXISTUJE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation>Soubor již existuje! Nejprve jej smažte z tiskárny a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation>Soubor má neplatnou koncovku! V příručce najdete informace o podporovaných souborech.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FILE NOT FOUND</source>
        <translation>SOUBOR NENALEZEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="247"/>
        <source>FILL THE RESIN</source>
        <translation>DOPLŇTE RESIN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation>SELHALA AKTUALIZACE FW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>HOSTNAME ERROR</source>
        <translation>CHYBA HOSTNAME</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>Image preloader did not finish successfully!</source>
        <translation>Neúspěšný preload obrazu!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="237"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation>NESPRÁVNÝ MODEL TISKÁRNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan.</source>
        <translation>Nesprávné čtení RPM %(failed_fans_text)s ventilátoru.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation>Nesprávná rychlost otáček %(failed_fans_text)s ventilátoru. Tisk může pokračovat, ale hrozí riziko přehřátí.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>Incorrect RPM reading of the %(fan__map_HardwareDeviceId)s.</source>
        <translation>Nesprávné čtení RPM %(fan__map_HardwareDeviceId)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>INTERNAL ERROR</source>
        <translation>INTERNÍ CHYBA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation>Interní chyba (selhalo mapování DBUS). Restartujte tiskárnu. Pokud problém přetrvává, kontaktujte podporu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation>PLNÁ INTERNÍ PAMĚŤ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation>Interní paměť je plná. Paměť uvolníte smazáním projektů.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>INVALID API KEY</source>
        <translation>NEPLATNÝ API KLÍČ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>INVALID FILE EXTENSION</source>
        <translation>NESPRÁVNÁ KONCOVKA PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INVALID PASSWORD</source>
        <translation>NEPLATNÉ HESLO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>INVALID PROJECT</source>
        <translation>NEPLATNÝ PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>Invalid tilt alignment position.</source>
        <translation>Nesprávná pozice zarovnání náklonu (tiltu).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation>NESPRÁVNÁ POZICE ZAROVNÁNÍ NÁKLONU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>LANGUAGE NOT SET</source>
        <translation>NENÍ NASTAVEN JAZYK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="241"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation>VAROVÁNÍ: MASKA NENÍ K DISPOZICI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>MC WRONG REVISION</source>
        <translation>NESPRÁVNÁ REVIZE MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation>Naměřeno nízké množství resinu. Tisk může pokračovat, ale možná bude potřeba resin doplnit.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation>Naměřený objem resinu %(volume_ml)d ml je vyšší, než je k tomuto tisku potřeba. Objem resinu nesmí přesahovat rysku 100 %. Uberte resin a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation>Naměřené množství resinu %(volume_ml)d ml je menší, než je potřeba pro dokončení tisku. Doplňte resin a restartujte tisk.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation>Měření resinu selhalo. Zkontrolujte, jestli je nasazená platforma a množství resinu ve vaničce.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <source>Moving the tilt failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Chyba pohybu náklonu (tiltu). Ujistěte se, že v cestě nemá žádné překážky. Pak akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Pohyb věže selhal. Ujistěte se, že se v cestě nenachází překážka, a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation>SELHALO NAHRÁVÁNÍ MQTT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>No calibration data to show!</source>
        <translation>Žádná kalibrační data k zobrazení!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>No display usage data to show</source>
        <translation>Žádná data o využití displeje</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>NO FILE TO REPRINT</source>
        <translation>ŽÁDNÝ SOUBOR K TISKU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>NO INTERNET CONNECTION</source>
        <translation>NEPŘIPOJENO K INTERNETU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation>Nebyl zaznamenán žádný problém. Tiskárnu můžete dál používat.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="238"/>
        <source>NOT ENOUGH RESIN</source>
        <translation>NEDOSTATEK RESINU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation>PŘÍLIŠ MÁLO VRSTEV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No USB storage present</source>
        <translation>USB disk nenalezen</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation>ŹÁDNÁ DATA UV KALIBRACE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="232"/>
        <source>NO WARNING</source>
        <translation>BEZ VAROVÁNÍ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="242"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation>VAROVÁNÍ: OŘÍZNUTÝ MODEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation>Objekt byl oříznut, protože se nevešel do tiskového objemu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>OLD EXPO PANEL</source>
        <translation>STARÝ EXPO PANEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>OPENING PROJECT FAILED</source>
        <translation>OTEVŘENÍ PROJEKTU SELHALO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Otevření tiskového projektu selhalo. Soubor je poškozen. Prosím zkuste soubor znovu naslicovat nebo vyexportovat.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Otevření tiskového projektu selhalo. Soubor je poškozen. Prosím zkuste soubor znovu naslicovat nebo vyexportovat.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation>Otevření projektu se nezdařilo, soubor může být poškozen. Zopakujte slicování a/nebo export projektu a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="239"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation>PARAMETRY MIMO ROZSAH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="240"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation>VAROVÁNÍ: PERPARTES N/A</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="118"/>
        <source>Per-partes print not available.</source>
        <translation>Tisk po částech není k dispozici.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>Prosím zapněte HTTP digest (doporučené nastavení pro zabezpečení) nebo aktualizujte API klíč. Naleznete jej v Nastavení -&gt; Síť -&gt; Přihlašovací údaje.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>Pour enough resin for the selected file into the tank and close the lid. The minimal amount of the resin is displayed on the touchscreen.</source>
        <translation>Nalijte do vaničky dostatečné množství pryskyřice pro vybraný soubor a zavřete víko. Minimální množství resinu se zobrazí na dotykovém displeji.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>PRELOAD FAILED</source>
        <translation>PRELOAD SELHAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>PRINTER IS BUSY</source>
        <translation>TISKÁRNA JE ZANEPRÁZDNĚNÁ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>PRINTER IS OK</source>
        <translation>TISKÁRNA OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation>NEBYLA PROVEDENA UV KALIBRACE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="243"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation>VAROVÁNÍ: NEPLATNÁ VERZE TISKÁRNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation>CHYBÍ VZOROVÉ MODELY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>PRINT JOB CANCELLED</source>
        <translation>TISKOVÁ ÚLOHA ZRUŠENA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>Print mask is missing.</source>
        <translation>Chybí tisková maska.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation>CHYBA EXPORTU PROFILU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation>IMPORT PROFILU SELHAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation>SELHALA ANALÝZA PROJEKTU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation>POŠKOZENÝ PROJEKT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Reading of %(sensor__map_HardwareDeviceId)s not in range!

Measured temperature: %(temperature).1f °C, allowed range: %(min)d - %(max)d °C.

Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>Čtení %(sensor__map_HardwareDeviceId)s není v rozsahu!

Naměřená teplota: %(temperature).1f °C, povolený rozsah: %(min)d - %(max)d °C.

Tiskárnu chraňte před přímým slunečním světlem a v pokojové teplotě (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation>Selhalo čtení teploty UV LED! Správné čtení této hodnoty je nezbytné pro bezpečný provoz tiskárny! Kontaktujte technickou podporu. Aktuální tisková úloha bude ukončena.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>REMOTE API ERROR</source>
        <translation>CHYBA REMOTE API</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation>Smazání projektu nyní není možné, je uzamčen tím, že probíhá tisková úloha.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation>Požadované intenzity nelze dosáhnout při max. povolené PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation>Nelze dosáhnout požadované intenzity při min. povolené PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="244"/>
        <source>RESIN LOW</source>
        <translation>NÍZKÁ HLADINA RESINU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>RESIN MEASURING FAILED</source>
        <translation>MĚŘENÍ RESINU SELHALO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>RESIN SENSOR ERROR</source>
        <translation>CHYBA SENZORU RESINU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN TOO HIGH</source>
        <translation>PŘÍLIŠ MNOHO RESINU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>RESIN TOO LOW</source>
        <translation>PŘÍLIŠ MÁLO RESINU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>RPM of %(fan__map_HardwareDeviceId)s not in range!

RPM data: %(min_rpm)d - %(max_rpm)d
Average: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</source>
        <translation>RPM %(fan__map_HardwareDeviceId)s není v rozsahu!

RPM data: %(min_rpm)d - %(max_rpm)d
Průměr: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), chyba: %(error)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>RPM of %(fan)s not in range!
RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation>RPM %(fan)s není v rozsahu!
RPM data: %(rpm)s
Průměr: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation>Selhalo ukládání nových výchozích hodnot. Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>%(sensor)s má hodnoty mimo rozsah! Naměřená teplota: %(temperature).1f °C. Tiskárna musí být umístěna mimo přímé sluneční světlo v místnosti o teplotě 18-32 °C.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation>CHYBA SÉRIOVÉHO ČÍSLA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation>Sériová čísla jsou ve špatném formátu! A64: %(a64)s MC: %(mc)s. Kontaktujte technickou podporu!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation>NASTAVENÍ ÚROVNĚ PODROBNOSTI PROTOKOLŮ SELHALO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation>SELHALO NASTAVENÍ KANÁLU AKTUALIZACÍ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>Something went wrong with the printer firmware. Please contact our support and do not forget to attach the logs.
Crashed services are immediately started again. If, despite that, the printer does not behave correctly, try restarting it. </source>
        <translation>Došlo k chybě ve firmwaru zařízení. Kontaktujte naši technickou podporu. Pokud možno, přiložte log soubory.
Havarované procesy se okamžitě spouští znovu. Pokud ani v takovém případě se nebude tiskárna chovat správně, zkuste ji restartovat. </translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Speaker test failed.</source>
        <translation>Selhal test reproduktoru.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>SPEAKER TEST FAILED</source>
        <translation>SELHAL TEST REPRODUKTORU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="230"/>
        <source>SYSTEM SERVICE CRASHED</source>
        <translation>SYSTÉMOVÁ SLUŽBA BYLA UKONČENA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation>TEPLOTA MIMO ROZSAH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation>SELHALO TEPLOTNÍ ČIDLO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>The admin menu is not available.</source>
        <translation>Admin menu není dostupné.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation>Okolní teplota je příliš vysoká. Můžete pokračovat v tisku, ale může dojít k selhání.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation>Okolní teplota je příliš nízká. Tisk může pokračovat, ale může v průběhu selhat.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation>Množství resinu ve vaničce není dostatečné pro aktuální projekt. Během tisku bude potřeba resin doplnit.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation>Automatická kalibrace UV LED nebyla dokončena! Spusťte kalibraci znovu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation>Kalibrace nebyla dokončena! Spusťte kalibraci znovu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The directory is not empty.</source>
        <translation>Adresář není prázdný.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation>Stahování selhalo. Zkontrolujte připojení k internetu a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation>Interní paměť je plná. Projekt nelze zkopírovat. V tisku můžete pokračovat, ale není možné v průběhu tisku vyjmout jednotku USB - tisk by pak selhal.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation>Není nastaven jazyk. Vyberte preferovaný jazyk v Nastavení -&gt; Jazyk a čas -&gt; Nastavit jazyk.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation>Model byl připraven pro jiný typ tiskárny. Model je potřeba vyslicovat znovu s použitím správných parametrů.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation>Projekt byl připraven pro jiný typ tiskárny - %(project_variant)s. Vaše tiskárna je %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation>Ovladač pohybu (Motion Controller) narazil na neočekávatelnou chybu. Restartujte tiskárnu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>The password is incorrect. Please check or update it in:

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

The password must be at least 8 characters long. Supported characters are letters, numbers, and dash.</source>
        <translation>Heslo je nesprávné. Zkontrolujte ho nebo aktualizujte v:

Nastavení &gt; Síť &gt; PrusaLink.
Prusa Slicer musí používat typ autorizace HTTP digest.

Heslo musí mít alespoň 8 znaků. Podporovanými znaky jsou písmena, číslice a pomlčka.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation>Tiskárna nastartovala ze záložního slotu kvůli neúspěšným pokusům o spuštění z primárního slotu.
Co nejdříve aktualizujte firmware tiskárny, aby došlo k obnovení primárního slotu.
K této situaci může dojít po neúspěšné aktualizaci nebo kvůli chybě hardwaru. Mohlo dojít k resetu nastavení tiskárny.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation>Tiskárna není zkalibrovaná. Nejprve spusťte Průvodce.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation>Tiskárna není připojená na internet. Zkontrolujte připojení v Nastavení.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation>Tiskárna nemá kalibrované UV hodnoty. Připojte UV kalibrátor a proveďte kalibraci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer model was not detected.</source>
        <translation>Nebyl rozpoznán typ tiskárny.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>Tiskárna využívá HTTP digest security. Umožněte tuto funkci i ve sliceru (doporučeno) nebo ji vypněte na tiskárně pomocí Nastavení &gt; Síť &gt; Přihlašovací údaje.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>The print job cancelled by the user.</source>
        <translation>Tisková úloha byla přerušena uživatelem.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="117"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation>Tiskové parametry jsou mimo rozsah této tiskárny. System se je může pokusit opravit. Pokračovat?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>The project file is invalid!</source>
        <translation>Soubor projektu je neplatný!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The project must have at least one layer</source>
        <translation>Projekt musí obsahovat alespoň jednu vrstvu</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>There is no warning</source>
        <translation>Bez varování</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured.</source>
        <translation>Senzor hladiny resinu nesepnul. Zkontrolujte, zda je dobře uchycena platforma i vanička.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>The %(sensor__map_HardwareDeviceId)s sensor failed.</source>
        <translation>%(sensor__map_HardwareDeviceId)s senzor selhal.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>The %(sensor)s sensor failed.</source>
        <translation>%(sensor)s senzor selhal.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation>Aktualizace firmwaru selhala! Restartujte tiskárnu a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation>UV LED kalibrátor detekoval malé množství světla i při zhasnutém displeji. Znamená to, že kalibrátor pravděpodobně správně nedoléhá na tiskový displej, nebo že displej dostatečně neblokuje UV světlo. Zkontrolujte umístění UV kalibrátoru na displeji nebo vyměňte tiskový displej.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation>UV LED kalibrátor nenaměřil očekávanou intenzitu UV světla. Zkontrolujte, zda je na tiskovém displeji umístěný správně.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation>Nebyl rozpoznán UV LED kalibrátor. Zkontrolujte zapojení a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The UV LED panel is not detected.</source>
        <translation>Nebyl detekován UV LED panel.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>The wizard did not finish successfully!</source>
        <translation>Průvodce nebyl úspěšně dokončen!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>This project was prepared for a different printer</source>
        <translation>Tato úloha byla připravená pro jiný typ tiskárny</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation>Požadavek není kompatibilní s programovacím rozhraním Prusa Remote API. Podrobnosti najdete v dokumentaci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation>Průvodce nelze přerušit. Nejprve dokončete všechny kroky.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation>SELHALA KONTROLA OSY NÁKLONU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps</source>
        <translation>Selhala kontrola osy tiltu!

Aktuální pozice: %(position)d kroků</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation>NENALEZEN KONCOVÝ DORAZ NÁKLONU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TILT HOMING FAILED</source>
        <translation>SELHAL HOMING NÁKLONU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation>Selhal homing náklonu. Zkontrolujte, zda není mechanismus zablokován a opakujte akci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>TILT MOVING FAILED</source>
        <translation>CHYBA POHYBU NÁKLONU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation>KONTROLA OSY VĚŽE SELHALA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation>Chyba při kontrole osy Z (věže)!

Aktuální pozice: %(position_nm)d nm

Zkontrolujte, zda se může kuličkový šroub plynule pohybovat v celém rozsahu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation>KONCOVÝ DORAZ VĚŽE NENALEZEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TOWER HOMING FAILED</source>
        <translation>SELHAL HOMING VĚŽE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Homing věže selhal. Ujistěte se, že v pohybu nebrání žádná překážka a akci opakujte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>TOWER MOVING FAILED</source>
        <translation>POHYB VĚŽE SELHAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation>Věž není v očekávané poloze.

Jsou platforma i vanička správně umístěné a zajištěné?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>TOWER POSITION ERROR</source>
        <translation>CHYBA POZICE VĚŽE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UNAUTHORIZED</source>
        <translation>NEAUTORIZOVÁNO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>UNEXPECTED ERROR</source>
        <translation>NEOČEKÁVANÁ CHYBA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation>NEOČEKÁVANÁ CHYBA MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>Unknown printer model</source>
        <translation>Neznámý model tiskárny</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>Unknown UV LED calibrator error code: %(nonprusa_code)d</source>
        <translation>Neznámý chybový kód kalibrátoru UV LED: %(nonprusa_code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="233"/>
        <source>UNKNOWN WARNING</source>
        <translation>NEZNÁMÉ VAROVÁNÍ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation>SELHAL AKTUALIZAČNÍ KANÁL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation>NEBYL DETEKOVÁN USB DISK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>UV CALIBRATION ERROR</source>
        <translation>CHYBA UV KALIBRACE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>UV CALIBRATION FAILED</source>
        <translation>KALIBRACE UV SELHALA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>UV DATA EROR</source>
        <translation>CHYBA DAT UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>UV INTENSITY ERROR</source>
        <translation>CHYBA INTENZITY UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation>Nebyla nastavena intenzita UV. Před začátkem tisku proveďte UV kalibraci.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation>PŘÍLIŠ VYSOKÁ INTENZITA UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation>PŘÍLIŠ NÍZKÁ INTENZITA UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation>CHYBA PŘIPOJENÍ UV LED KALIBRÁTORU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation>CHYBA UV LED KALIBRÁTORU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation>CHYBA SPOJENÍ S KALIBRÁTOREM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation>NEROZPOZNÁN UV LED KALIBRÁTOR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation>CHYBA ČTENÍ KALIBRÁTORU UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation>NEZNÁMÁ CHYBA UV LED KALIBRÁTORU</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation>CHYBA CHLADIČE UV LED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>UV LED is overheating!</source>
        <translation>UV LED se přehřívá!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV LED TEMP. ERROR</source>
        <translation>CHYBA UV LED TEPLOTY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation>CHYBA UV LED NAPĚTÍ</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation>Příliš velký rozdíl v napětí UV LED. LED modul může být poškozený. Kontaktujte podporu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>WIZARD DATA FAILURE</source>
        <translation>CHYBA DAT V PRŮVODCI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>WIZARD FAILED</source>
        <translation>PRŮVODCE SELHAL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>WRONG PRINTER MODEL</source>
        <translation>NESPRÁVNÝ MODEL TISKÁRNY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation>Špatné verze ovladače pohybu (Motion Controller). Kontaktujte naši podporu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>YOU SHALL NOT PASS</source>
        <translation>NEPROJDEŠ DÁL</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="110"/>
        <source>Already exists</source>
        <translation>Již existuje</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="114"/>
        <source>Can&apos;t read</source>
        <translation>Nelze přečíst</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="96"/>
        <source>Error: %1</source>
        <translation>Chyba: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="108"/>
        <source>File not found</source>
        <translation>Soubor nenalezen</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="112"/>
        <source>Invalid extension</source>
        <translation>Neplatná koncovka</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="106"/>
        <source>Storage full</source>
        <translation>Plné úložiště</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="149"/>
        <source>Inactive</source>
        <translation>Neaktivní</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="175"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="247"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK musí mít aspoň 8 znaků.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="232"/>
        <source>Security</source>
        <translation>Zabezpečení</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="216"/>
        <source>Show Password</source>
        <translation>Zobrazit heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="153"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="253"/>
        <source>SSID must not be empty</source>
        <translation>SSID nesmí být prázdné</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="265"/>
        <source>Start AP</source>
        <translation>Zapnout AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="138"/>
        <source>State:</source>
        <translation>Stav:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation>Zastavit AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="296"/>
        <source>Swipe for QRCode</source>
        <translation>Přejeďte pro QR kód</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="377"/>
        <source>Swipe for settings</source>
        <translation>Přejeďte pro nastavení</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation>(nezměněno)</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="23"/>
        <source>About Us</source>
        <translation>O nás</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="52"/>
        <source>Prusa Research a.s.</source>
        <translation>Prusa Research a.s.</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="48"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation>Chcete-li se o nás dozvědět více, naskenujte QR kód nebo navštivte odkaz níže:</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation>Přidat skrytou síť</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation>Připojit</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation>PASS</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK musí mít aspoň 8 znaků.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation>Zabezpečení</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation>Zobrazit heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation>SSID nesmí být prázdné.</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="40"/>
        <source>Admin API</source>
        <translation>Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="156"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="168"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="64"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation>Vložte správné heslo pro síť &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="194"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK musí mít aspoň 8 znaků.</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation>Zobrazit heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation>(nezměněno)</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Are you sure?</source>
        <translation>Jste si jistí?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation>Je vidět logo společnosti na expozičním displeji při pohledu skrz víko?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="126"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Opatrně odstraňte ochrannou nálepku z expozičního displeje.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="174"/>
        <source>Close the cover.</source>
        <translation>Zavřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="173"/>
        <source>Cover</source>
        <translation>Víko</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="57"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation>Opravdu chcete ukončit průvodce?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="114"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation>Po vložení papíru do tiskárny nezapomeňte kryt zavřít!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="59"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation>Tiskárna nebude fungovat bez kompletního absolvování průvodce (wizardu).</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="112"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation>Tip: Pokud nevidíte logo dostatečně dobře, položte na osvitový displej kus bílého papíru.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation>Průvodce</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="154"/>
        <source>Wizard canceled</source>
        <translation>Průvodce přerušen</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard failed</source>
        <translation>Průvodce selhal</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="136"/>
        <source>Wizard finished sucessfuly!</source>
        <translation>Průvodce úspěšně dokončen!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation>Průvodce narazil na problém a byl zastaven. Zkusit znovu?</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="121"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="92"/>
        <source>Move Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="83"/>
        <source>Move Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation>Pohyb vaničky</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="109"/>
        <source>Tilt position:</source>
        <translation>Pozice náklonu:</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="160"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation>Pootočte tiskovou platformu tak, aby byla zarovnaná s osvitovým displejem.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="195"/>
        <source>All done, happy printing!</source>
        <translation>Hotovo, tisku zdar!</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="129"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation>Zkontrolujte, zda je tisková platforma zajištěna černým šroubem. Pokud ne, jednou rukou podržte platformu a druhou dotáhněte šroub.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="145"/>
        <source>Close the cover.</source>
        <translation>Zavřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="131"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation>Neotáčejte platformu. Její pozice by měla odpovídat obrázku.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="162"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation>Přední okraj platformy a expoziční displeje musí být zarovnaný.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation>Platformu uchopte jednou rukou a mírnou silou ji přitlačte k osvitovému displeji, aby vznikl správný kontakt.


Následně použijte inbusový klíč k dotažení šroubu na konzole.

Pak můžete platformu pustit.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="50"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation>Pokud ještě není platforma vložena, vložte ji nyní dle obrázku v nulovém úhlu a zajistěte ji černým utahovacím šroubem.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="86"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation>V následujícím kroku je potřeba pomocí tlačítek na displeji pohybovat osvitovým displejem nahoru a dolů, dokud nebude perfektně zarovnaný se dnem vaničky - vanička se nesmí nadzvednout.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation>Povolte šroubek na konzole pomocí inbusu - nesmíte ho ale vyšroubovat úplně.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="109"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation>Ujistěte se, že jsou tisková platforma, vanička i osvitový displej PERFEKTNĚ čisté.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation>Kalibrace tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="120"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation>Vraťte vaničku do původní pozice a zajistěte ji dvěma šrouby. Utahujte je rovnoměrně a stejnou silou.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="99"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation>Zarovnejte vaničku s displejem</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="64"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation>Některé SL1 3D tiskárny mají šroubky dva. V příručce naleznete dodatečné informace.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="180"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation>Nyní utáhněte šroubek na konzole pomocí inbusu.

Některé modely mohou mít šroubky dva - dotahujete je střídavě po malých krocích. V příručce naleznete dodatečné informace.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="111"/>
        <source>The image is for illustration purposes only.</source>
        <translation>Obrázek je pouze pro ilustraci.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="178"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation>Utáhněte malý šroubek na konzole pomocí inbusu.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="77"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation>Odšroubujte vaničku, pootočte ji o 90° a položte ji napříč přes základnu tiskárny. Šrouby úplně odstraňte.</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="149"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="177"/>
        <source>Above Area Fill</source>
        <translation>Výplň nad plochou</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="171"/>
        <source>Above Area Fill Threshold Settings</source>
        <translation>Nastavení prahové hodnoty výplně nad oblastí</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="148"/>
        <source>Area Fill Threshold</source>
        <translation>Prahová hodnota naplnění plochy</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="197"/>
        <source>Below Area Fill</source>
        <translation>Výplň pod plochou</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="191"/>
        <source>Below Area Fill Threshold Settings</source>
        <translation>Nastavení prahové hodnoty výplně pod oblastí</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="72"/>
        <source>Exposure</source>
        <translation>Osvit</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="97"/>
        <source>Exposure Time Incr.</source>
        <translation>Navýšení expozice</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="124"/>
        <source>First Layer Expo.</source>
        <translation>Osvit první vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="41"/>
        <source>Print Settings</source>
        <translation>Nastavení tisku</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="90"/>
        <location filename="../qml/PageChange.qml" line="118"/>
        <location filename="../qml/PageChange.qml" line="142"/>
        <source>s</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="194"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation>Posuňte pro ilustraci</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="207"/>
        <source>Connected to %1</source>
        <translation>Připojeno k %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="196"/>
        <source>Connection to %1 failed.</source>
        <translation>Připojení k %1 selhalo.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation>Skrytá síť</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="139"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="147"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK musí mít aspoň 8 znaků.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="122"/>
        <source>Security</source>
        <translation>Zabezpečení</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="115"/>
        <source>Show Password</source>
        <translation>Zobrazit heslo</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="235"/>
        <source>Start AP</source>
        <translation>Zapnout AP</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="185"/>
        <source>Working...</source>
        <translation>Pracuji...</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation>Probíhá ochlazování</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation>Teplota je %1 C</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation>PŘEHŘÍVÁNÍ UV LED!</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation>Test displeje</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="91"/>
        <source>All done, happy printing!</source>
        <translation>Hotovo, tisku zdar!</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="77"/>
        <source>Close the cover.</source>
        <translation>Zavřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="32"/>
        <source>Display Test</source>
        <translation>Test displeje</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Povolte černý šroub a odstraňte tiskovou platformu.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Odšroubujte a odstraňte vaničku.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="51"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation>Tento proces zkontroluje, zda osvitový displej funguje tak, jak má.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>Welcome to the display wizard.</source>
        <translation>Vítejte v průvodci displejem.</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="78"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation>Nyní dojde k vymazání současné konfigurace.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="108"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation>Návrat ke starší verzi dokončen. Tiskárna se nyní restartuje.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation>Downgrade hardwaru</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="99"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Používejte pouze platformu, která patří k tiskárně. Použití jiné platformy může vést k vylití resinu a poškození tiskárny!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="69"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Prosím vezměte na vědomí, že přechod zpět na původní verzi není podporován.

Downgrade vymaže údaje UV kalibrace, vaše tiskárna pak nemusí fungovat správně.

Bude potřeba tiskárnu nově zkalibrovat pomocí externího UV kalibrátoru.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="48"/>
        <source>Proceed?</source>
        <translation>Pokračovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="59"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation>Znovu osaďte komponenty SL1S a zapněte tiskárnu. Dojde tím k navrácení do původního stavu.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation>Detekovány SL1 komponenty (downgrade z SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="79"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>Po restartování proběhne počáteční nastavení tiskárny.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="58"/>
        <source>The printer will power off now.</source>
        <translation>Tiskárna se nyní vypne.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>K dokončení návratu ke starší verzi je potřeba vyčistit konfiguraci a restartovat.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="89"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation>Používejte pouze přibalenou vaničku. Použití odlišných vaniček může vést k vylití resinu a poškození tiskárny!</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Cleanup...</source>
        <translation>Vyčištění...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Copying...</source>
        <translation>Kopírování...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Done.</source>
        <translation>Hotovo.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Downloading ...</source>
        <translation>Probíhá stahování...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="55"/>
        <source>Downloading examples...</source>
        <translation>Stahování vzorových modelů...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation>Příklady</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="129"/>
        <source>Failure.</source>
        <translation>Chyba.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Initializing...</source>
        <translation>Inicializace...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="132"/>
        <source>Unknown state.</source>
        <translation>Neznámý stav.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Unpacking...</source>
        <translation>Vybalování...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="141"/>
        <source>View Examples</source>
        <translation>Prohlížet vzorové modely</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="44"/>
        <source>Error code:</source>
        <translation>Chybový kód:</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="48"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Pro více informací naskenujte QR kód nebo kontaktujte vašeho prodejce.</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="202"/>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>Configuring the connection,</source>
        <translation>Probíhá konfigurace připojení,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="123"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="35"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="148"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation>Brána</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="136"/>
        <source>IP Address</source>
        <translation>IP adresa</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="117"/>
        <source>Network Info</source>
        <translation>Info o síti</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>please wait...</source>
        <translation>vyčkejte...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="219"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Vrátit zpět</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="61"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="68"/>
        <source>Error code:</source>
        <translation>Chybový kód:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="72"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Pro více informací naskenujte QR kód nebo kontaktujte vašeho prodejce.</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="106"/>
        <source>Save Logs to USB</source>
        <translation>Uložit logy na USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="119"/>
        <source>Send Logs to Cloud</source>
        <translation>Odeslat logy do cloudu</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="80"/>
        <source>Swipe to proceed</source>
        <translation>Táhnutím pokračujte</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation>Systémová chyba</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="140"/>
        <source>Turn Off</source>
        <translation>Vypnout</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="133"/>
        <source>Update Firmware</source>
        <translation>Aktualizace firmwaru</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="45"/>
        <source>Factory Reset done.</source>
        <translation>Proběhl reset do továrního nastavení.</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="32"/>
        <source>Factory Reset</source>
        <translation>Tovární nastavení</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation>Hotovo</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation>Doplnění resinu</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation>Pokud nechcete doplnit resin, stiskněte tlačítko Zpět v horní části displeje.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation>Ruční doplnění resinu.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation>Naplňte vaničku až k 100% rysce a stiskněte Hotovo.</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation>Před tiskem je potřeba projít těmito kroky:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation>Kalibrovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation>Současný systém bude stále k dispozici v Nastavení -&gt; Firmware -&gt; Návrat k nižší verzi</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation>Opravdu chcete nainstalovat %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation>Přejete si začít?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation>Vložte USB flash disk nebo si stáhněte vzorové modely v Nastavení -&gt; Podpora.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation>Instalovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation>Lokální</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation>Lokální</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation>
            <numerusform>%n položka</numerusform>
            <numerusform>%n položek</numerusform>
            <numerusform>%n položek</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="314"/>
        <source>No usable projects were found,</source>
        <translation>Nebyly nalezeny žádné použitelné projekty,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation>Předchozí tiskové úlohy</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation>Kalibrace tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation>Projekty</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation>Vzdálené ovládání</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation>Vzdálené ovládání</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation>Tiskárna není plně zkalibrovaná.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation>Aktualizační balíčky</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation>Aktualizace</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation>Kalibrace UV</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="127"/>
        <source>CANCELED</source>
        <translation>ZRUŠENO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="207"/>
        <source>Consumed Resin</source>
        <translation>Spotřebovaný resin</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="126"/>
        <source>FAILED</source>
        <translation>CHYBA</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation>Dokončeno</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="125"/>
        <location filename="../qml/PageFinished.qml" line="128"/>
        <source>FINISHED</source>
        <translation>DOKONČENO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="277"/>
        <source>First Layer Exposure</source>
        <translation>Osvit první vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="328"/>
        <source>Home</source>
        <translation>Úvodní stránka</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="253"/>
        <source>Layer Exposure</source>
        <translation>Osvit vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>Layer height</source>
        <translation>Výška vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="183"/>
        <source>Layers</source>
        <translation>Vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="405"/>
        <source>Loading, please wait...</source>
        <translation>Načítání, čekejte prosím...</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="156"/>
        <source>Print Time</source>
        <translation>Doba tisku</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="348"/>
        <source>Reprint</source>
        <translation>Tisk. znovu</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="338"/>
        <source>Resin Tank Cleaning</source>
        <translation>Čištění vaničky na resin</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="261"/>
        <location filename="../qml/PageFinished.qml" line="285"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="300"/>
        <source>Swipe to continue</source>
        <translation>Pokračujte tažením prstu</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="368"/>
        <source>Turn Off</source>
        <translation>Vypnout</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation>Velký obrázek</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="51"/>
        <source>Control</source>
        <translation>Ovládání</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation>Úvodní stránka</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="58"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="65"/>
        <source>Turn Off</source>
        <translation>Vypnout</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="81"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation>Nastavit jazyk</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation>Extrahování dat protokolů</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation>Export logů</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation>Export logů zrušen</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation>Export logů selhal. Zkontrolujte USB disk / připojení k internetu.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation>Export logů dokončen</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation>Logy byly úspěšně nahrány na server Prusa Research.&lt;br /&gt;&lt;br /&gt;Kontaktujte technickou podporu a nahlaste následující kód:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation>Nahrávání protokolu dokončeno</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation>Ukládání dat na USB disk</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation>Nahrávání dat na server</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation>Manuál</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation>Naskenováním QR kódu otevřete příručku pro tuto tiskárnu.

Alternativně můžete použít tento odkaz:</translation>
    </message>
</context>
<context>
    <name>PageModifyLayerProfile</name>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="144"/>
        <source>Delay After Exposure</source>
        <translation>Zpoždění po expozici</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="130"/>
        <source>Delay Before Exposure</source>
        <translation>Zpoždění před expozicí</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="375"/>
        <source>Disabling the &apos;Use tilt&apos; causes the object to separate away from the film in the vertical direction only.

&apos;Tower hop height&apos; has been set to recommended value of 5 mm.</source>
        <translation>Vypnutí možnosti &quot;Použít náklon&quot; způsobí, že se objekt oddělí od filmu pouze ve svislém směru.

&quot;Výška skoku věže&quot; byla nastavena na doporučenou hodnotu 5 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="29"/>
        <source>Edit Layer Profile</source>
        <translation>Upravit profil vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="385"/>
        <source>Enabling the &apos;Use tilt&apos; causes the object to separate away from the film mainly by tilt.

&apos;Tower hop height&apos; has been set to 0 mm.</source>
        <translation>Povolení možnosti &quot;Použít náklon&quot; způsobí, že se objekt oddělí od filmu hlavně náklonem.

&quot;Výška skoku věže&quot; byla nastavena na 0 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="160"/>
        <source>mm</source>
        <comment>millimeters</comment>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="174"/>
        <source>mm/s</source>
        <comment>millimeters per second</comment>
        <translation>mm/s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="131"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="145"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="240"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="279"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="319"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="358"/>
        <source>s</source>
        <comment>seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="266"/>
        <source>Tilt Down Cycles</source>
        <translation>Cykly náklonu dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="278"/>
        <source>Tilt Down Delay</source>
        <translation>Zpoždění náklonu dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="254"/>
        <source>Tilt Down Finish Speed</source>
        <translation>Rychlost dokončování náklonu dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="214"/>
        <source>Tilt Down Initial Speed</source>
        <translation>Počáteční rychlost náklonu dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="226"/>
        <source>Tilt Down Offset</source>
        <translation>Posunutí náklonu dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="239"/>
        <source>Tilt Down Offset Delay</source>
        <translation>Zpoždění posunu náklonu dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="345"/>
        <source>Tilt Up Cycles</source>
        <translation>Cykly náklonu nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="357"/>
        <source>Tilt Up Delay</source>
        <translation>Zpoždění náklonu nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="333"/>
        <source>Tilt Up Finish Speed</source>
        <translation>Rychlost dokončování naklonění nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="293"/>
        <source>Tilt Up Initial Speed</source>
        <translation>Počáteční rychlost náklonu nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="305"/>
        <source>Tilt Up Offset</source>
        <translation>Posunutí náklonu nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="318"/>
        <source>Tilt Up Offset Delay</source>
        <translation>Zpoždění posunu náklonu nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="159"/>
        <source>Tower Hop Height</source>
        <translation>Výška věže</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="173"/>
        <source>Tower Speed</source>
        <translation>Rychlost věže</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="185"/>
        <source>Use Tilt</source>
        <translation>Použití náklonu</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="374"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="384"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="227"/>
        <source>μ-step</source>
        <comment>tilt microsteps</comment>
        <translation>μ-krok</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="306"/>
        <source>μ-step</source>
        <comment>tilt micro steps</comment>
        <translation>μ-krok</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="216"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="256"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="295"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="335"/>
        <source>μ-step/s</source>
        <comment>microsteps per second</comment>
        <translation>μ-krok/s</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation>Ovládání</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation>Vypnout
motory</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation>Zaparkovat
platformu</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation>Zaparkovat
vaničku</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tank, please wait...</source>
        <translation>Homing vaničky, vyčkejte...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tower, please wait...</source>
        <translation>Probíhá homing věže, vyčkejte...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="68"/>
        <source>Resin Tank Cleaning</source>
        <translation>Čištění vaničky</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="32"/>
        <source>New Exposure Panel</source>
        <translation>Nový osvitový panel</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="45"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation>Byl detekován nový osvitový displej.

Tiskárna bude vyžadovat selftest a kalibraci pro kontrolu, zda vše funguje správně.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation>K dispozici je aktualizace na %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation>Oznámení</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation>Tisk zrušen: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation>Tisk se nezdařil: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation>Tisk dokončen: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="55"/>
        <source>Insert protective foam</source>
        <translation>Vložte ochrannou pěnovou destičku</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="46"/>
        <source>Packing done.</source>
        <translation>Balení je hotové.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="32"/>
        <source>Packing Wizard</source>
        <translation>Průvodce balením</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="7"/>
        <source>Do you really want to turn off the printer?</source>
        <translation>Skutečně chcete tiskárnu vypnout?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="10"/>
        <source>Powering Off...</source>
        <translation>Vypínání...</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Power Off?</source>
        <translation>Vypnout?</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="121"/>
        <source>Cover</source>
        <translation>Víko</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="125"/>
        <source>Delayed Start</source>
        <translation>Opožděný start</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="151"/>
        <source>Disabled</source>
        <translation>Vypnuto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="140"/>
        <source>Do not touch the printer!</source>
        <translation>Nedotýkejte se tiskárny!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="120"/>
        <source>Fan</source>
        <translation>Ventilátor</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation>Vyčkejte</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="122"/>
        <source>Resin</source>
        <translation>Resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="123"/>
        <source>Starting Positions</source>
        <translation>Výchozí pozice</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="124"/>
        <source>Stirring</source>
        <translation>Míchání</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Temperature</source>
        <translation>Teplota</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="150"/>
        <source>With Warning</source>
        <translation>S varováním</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Action Pending</source>
        <translation>Akce je v pořadí</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="117"/>
        <source>Check Warning</source>
        <translation>Zkontrolujte varování</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="151"/>
        <source>Close Cover!</source>
        <translation>Zavřete víko!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="113"/>
        <source>Continue?</source>
        <translation>Pokračovat?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Cover Open</source>
        <translation>Otevřené víko</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation>Nastavuji tiskárnu pro snazší přidání resinu. Vyčkejte.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Going down</source>
        <translation>Přesouvání dolů</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Going up</source>
        <translation>Pohyb vzhůru</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="130"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation>Pokud nechcete pokračovat, zrušte probíhající úlohu pomocí tlačítka Zpět v horní části displeje.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Moving platform to the bottom position</source>
        <translation>Přesun platformy do nejnižší pozice</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Moving platform to the top position</source>
        <translation>Přesun platformy do nejvyšší pozice</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Moving the resin tank down...</source>
        <translation>Vanička na resin se posouvá dolů...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Paused.</source>
        <translation>Pozastaveno.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Please close the cover to continue</source>
        <translation>Před pokračováním prosím zavřete víko</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="152"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation>Prosím, zavřete víko, UV záření je škodlivé.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="220"/>
        <location filename="../qml/PagePrint.qml" line="277"/>
        <source>Please wait...</source>
        <translation>Chvilku strpení...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="243"/>
        <source>Reading data...</source>
        <translation>Načítají se data...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="129"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation>Uvolněte mechanismus vaničky a stiskněte Pokračovat.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation>Prosím vyčkejte, požadované akce se provedou po dokončení této vrstvy...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Setting start positions...</source>
        <translation>Nastavování startovacích pozic...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring</source>
        <translation>Míchání</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring resin</source>
        <translation>Míchání resinu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="127"/>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Stuck Recovery</source>
        <translation>Pokus o obnovení po zaseknutí</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Tank Moving Down</source>
        <translation>Vanička se posouvá dolů</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="128"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation>Tiskárna se zasekla a potřebuje zásah ze strany uživatele.</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="302"/>
        <source>Exposure Times</source>
        <translation>Doby expozice</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="348"/>
        <source>Last Modified</source>
        <translation>Naposledy upraveno</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layer Height</source>
        <translation>Výška vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layers</source>
        <translation>Vrstvy</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="447"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Naplňte vaničku alespoň na %1 % a zavřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="493"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="483"/>
        <source>Print Settings</source>
        <translation>Nastavení tisku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="329"/>
        <source>Print Time Estimate</source>
        <translation>Odhad doby tisku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="374"/>
        <source>Swipe to continue</source>
        <translation>Pokračujte tažením prstu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="166"/>
        <source>Swipe to project</source>
        <translation>Přejeďte pro zobrazení projektu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="444"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>Tento projekt vyžaduje&lt;br/&gt;%1 % resinu. Bude nutné resin doplnit během tisku.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="208"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="357"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation>Neznámý</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="239"/>
        <source>Area fill:</source>
        <translation>Výplň plochy:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="492"/>
        <location filename="../qml/PagePrintPrinting.qml" line="498"/>
        <source>Cancel Print</source>
        <translation>Zrušit tisk</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="431"/>
        <source>Consumed Resin</source>
        <translation>Spotřeba resinu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="516"/>
        <source>Enter Admin</source>
        <translation>Vstup do admin menu</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="373"/>
        <source>Estimated End</source>
        <translation>Odhadovaný konec</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="403"/>
        <source>Layer</source>
        <translation>Vrstva</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="225"/>
        <source>Layer:</source>
        <translation>Vrstva:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Layer Height:</source>
        <translation>Výška vrstvy:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="34"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="388"/>
        <source>Printing Time</source>
        <translation>Doba tisku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="473"/>
        <source>Print Settings</source>
        <translation>Nastavení tisku</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="274"/>
        <source>Progress:</source>
        <translation>Průběh:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="479"/>
        <source>Refill Resin</source>
        <translation>Doplnit resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="417"/>
        <source>Remaining Resin</source>
        <translation>Zbývající resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="358"/>
        <source>Remaining Time</source>
        <translation>Zbývající čas</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Today at</source>
        <translation>Dnes v</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="499"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation>Ochrana proti náhodnému zrušení tisku: Tažením prstu po obrazovce pokračujte k dalšímu kroku, kde tisk zrušíte.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="110"/>
        <source>Tomorrow at</source>
        <translation>Zítra v</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="363"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Naplňte vaničku alespoň na %1 % a zavřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation>Nalijte resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>Tento projekt vyžaduje&lt;br/&gt;%1 % resinu. Bude nutné resin doplnit během tisku.</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation>QR kód stránky</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation>Instalovat nyní</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation>Později</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation>Poznámky k vydání</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="102"/>
        <source>Can you hear the music?</source>
        <translation>Slyšíte hudbu?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="77"/>
        <location filename="../qml/PageSelftestWizard.qml" line="140"/>
        <location filename="../qml/PageSelftestWizard.qml" line="173"/>
        <source>Close the cover.</source>
        <translation>Zavřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Povolte černý šroub a odstraňte tiskovou platformu.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="155"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation>Ujistěte se, že vanička na resin je nainstalovaná a šrouby jsou utažené.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="164"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation>Vložte platformu v nulovém úhlu a zajistěte ji černým utahovacím šroubem.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="122"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation>Vložte platformu v úhlu 60 stupňů a zajistěte ji černým utahovacím šroubem.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="113"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation>vložte vaničku a rovnoměrně utáhněte šrouby.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Odšroubujte a odstraňte vaničku.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="131"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation>Platformu sundejte z konzoly a umístěte ji otočenou o 90° na prostředek vaničky.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="51"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation>Tato procedura je nezbytná, zkontroluje všechny komponenty tiskárny.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="49"/>
        <source>Welcome to the selftest wizard.</source>
        <translation>Vítejte v průvodci nastavením.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="676"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation>Nastavit datum</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only a-z, 0-9 and  &quot;-&quot;. Must not begin or end with &quot;-&quot;.</source>
        <translation>Může obsahovat pouze a-z, 0-9 a &quot;-&quot;. Nesmí začínat nebo končit znakem &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation>Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaConnectRegistration</name>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="90"/>
        <source>Add Printer to Prusa Connect</source>
        <translation>Přidání tiskárny do Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="79"/>
        <source>In Progress</source>
        <translation>Probíhá</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="78"/>
        <source>Not Registered</source>
        <translation>Není registrován</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="31"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="80"/>
        <source>Registered</source>
        <translation>Registrováno</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="96"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="126"/>
        <source>Register Printer</source>
        <translation>Registrovat tiskárnu</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="75"/>
        <source>Registration Status</source>
        <translation>Stav registrace</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="123"/>
        <source>Review the Registration QR Code</source>
        <translation>Zkontrolujte registrační kód QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="98"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="128"/>
        <source>Scan the QR Code or visit &lt;b&gt;prusa.io/add&lt;/b&gt;. Log into your Prusa Account to add this printer.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</source>
        <translation>Naskenujte QR kód nebo navštivte &lt;b&gt;prusa.io/add&lt;/b&gt;. Přihlaste se ke svému účtu Prusa aCCOUNT a přidejte tuto tiskárnu.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Kód: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="81"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="111"/>
        <source>Unregister Printer from Prusa Connect</source>
        <translation>Odhlášení tiskárny z Prusa Connect</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaLinkWebLogin</name>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="131"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation>Může obsahovat jen znaky a-z, A-Z, 0-9 a &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="117"/>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="125"/>
        <source>Must be at least 8 chars long</source>
        <translation>Nutno min. 8 znaků</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="92"/>
        <source>Printer Password</source>
        <translation>Heslo tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="32"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="144"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="55"/>
        <source>User Name</source>
        <translation>Uživatelské jméno</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation>Hodina</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation>Minuta</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation>Nastavit čas</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="134"/>
        <source>City</source>
        <translation>Město</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="128"/>
        <source>Region</source>
        <translation>Oblast</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="140"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation>Časové pásmo</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="60"/>
        <source>Calibration</source>
        <translation>Kalibrace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="150"/>
        <source>Enter Admin</source>
        <translation>Vstup do admin menu</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="89"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="119"/>
        <source>Language &amp; Time</source>
        <translation>Jazyk a čas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="69"/>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="79"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Platforma a vanička na resin</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="29"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="99"/>
        <source>Settings &amp; Sensors</source>
        <translation>Nastavení a senzory</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Support</source>
        <translation>Podpora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>System Logs</source>
        <translation>Systémové logy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="109"/>
        <source>Touchscreen</source>
        <translation>Dotykový displej</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="32"/>
        <source>Calibration</source>
        <translation>Kalibrace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="94"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation>Nahradili jste OSVITOVÝ DISPLEJ?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="107"/>
        <source>Did you replace the UV LED SET?</source>
        <translation>Vyměnili jste UV LED sadu za novou?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="76"/>
        <source>Display Test</source>
        <translation>Test displeje</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>New Display?</source>
        <translation>Nový displej?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>New UV LED SET?</source>
        <translation>Nová sada UV LED?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="58"/>
        <source>Printer Calibration</source>
        <translation>Kalibrace tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="51"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="68"/>
        <source>UV Calibration</source>
        <translation>Kalibrace UV</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation>Po nainstalování této aktualizace bude možné do tiskárny nahrát další FW, ale &lt;b&gt;tisk nebude fungovat.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation>Jste si jistí?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="414"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;není kompatibilní s tímto modelem hardwaru - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation>Zkontrolovat aktualizace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation>Kontrola aktualizací selhala</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation>Kontroluji aktualizace...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="215"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>Continue anyway?</source>
        <translation>Chcete přesto pokračovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Downgrade?</source>
        <translation>Návrat ke starší verzi?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="347"/>
        <source>Downgrade Firmware?</source>
        <translation>Přejít na nižší verzi FW?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="182"/>
        <source>Do you really want to downgrade to FW</source>
        <translation>Opravdu chcete přejít na nižší verzi FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation>Opravdu chcete uvést tiskárnu do továrního nastavení?

Všechna nastavení budou smazána!
Tiskové projekty zůstanou nedotčené.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>Do you want to continue?</source>
        <translation>Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="248"/>
        <source>Factory Reset</source>
        <translation>Tovární nastavení</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="36"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="334"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation>Meta informace k souboru FW nebyly nalezeny.&lt;br/&gt;&lt;br/&gt;Chcete &lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;přesto nainstalovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="290"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation>FW info</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="213"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation>Pokud provedete změnu, můžete později nahrát jinou verzi firmwaru, ale&lt;b&gt; nebude možné tisknout.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="413"/>
        <source>Incompatible FW!</source>
        <translation>Nekompatibilní FW!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation>Instalovaná verze</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="333"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="382"/>
        <source>Install Firmware?</source>
        <translation>Nainstalovat firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation>Instalovat nyní</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>je stejná nebo menší než současná &lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="291"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation>Nahrávání FW souboru s meta informacemi
(může trvat až 20 sekund) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="239"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Novější než SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="235"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="425"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="138"/>
        <source>Receive Beta Updates</source>
        <translation>Získávat beta aktualizace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Switch to beta?</source>
        <translation>Přepnout na beta verzi?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="169"/>
        <source>Switch to version:</source>
        <translation>Přepnout na verzi:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation>Systém je aktuální</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="211"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>Alternativní verze FW &lt;b&gt;%1&lt;/b&gt; není kompatibilní s vaším typem tiskárny - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="428"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation>Aktualizace k dispozici</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation>Selhalo stažení aktualizace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation>Stahování aktualizace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation>Stahování aktualizace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation>Nečinná služba Updater</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="350"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation>Verze vybraného FW souboru&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="157"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation>Varování! Beta aktualizace mohou být nestabilní. &lt;br/&gt;&lt;br/&gt;Instalace není doporučena pro produkční tiskárny. &lt;br/&gt;&lt;br/&gt;Pokračovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>ve verzi &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="383"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation>Vybrali jste aktualizační balíček &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="29"/>
        <source>Language &amp; Time</source>
        <translation>Jazyk a čas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="35"/>
        <source>Set Language</source>
        <translation>Nastavit jazyk</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="41"/>
        <source>Time Settings</source>
        <translation>Nastavení času</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="37"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="74"/>
        <source>Hostname</source>
        <translation>Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="29"/>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="89"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="81"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="53"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="109"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="48"/>
        <source>Disable Steppers</source>
        <translation>Vypnout motory</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="102"/>
        <source>Limit for Fast Tilt</source>
        <translation>Limit rychlého tiltu</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="34"/>
        <source>Move Platform</source>
        <translation>Pohyb platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="41"/>
        <source>Move Resin Tank</source>
        <translation>Pohyb vaničky</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="56"/>
        <source>Platform Axis Sensitivity</source>
        <translation>Citlivost osy platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="128"/>
        <source>Platform Offset</source>
        <translation>Offset platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="28"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Platforma a vanička na resin</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="159"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="79"/>
        <source>Tank Axis Sensitivity</source>
        <translation>Citlivost osy vaničky</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="152"/>
        <source>Tank Surface Cleaning Exposure</source>
        <translation>Doba osvícení při čištění vaničky</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="135"/>
        <source>um</source>
        <translation>um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="112"/>
        <location filename="../qml/PageSettingsSensors.qml" line="148"/>
        <source>Are you sure?</source>
        <translation>Jste si jistí?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="61"/>
        <source>Auto Power Off</source>
        <translation>Auto vypnutí</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="87"/>
        <source>Cover Check</source>
        <translation>Kontrola víka</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="36"/>
        <source>Device hash in QR</source>
        <translation>Hash zařízení v QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="113"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation>Vypnout senzor víka?&lt;br/&gt;&lt;br/&gt;VAROVÁNÍ: Může dojít k nežádoucímu osvitu resinu UV světlem nebo k poškození zdraví. Tato akce není doporučena!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="149"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation>Vypnout senzor resinu?&lt;br/&gt;&lt;br/&gt;VAROVÁNÍ: Může dojít k selhání tisku nebo přetečení vaničky! Tato akce není doporučena!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="180"/>
        <source>Off</source>
        <translation>Vyp</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="161"/>
        <source>Rear Fan Speed</source>
        <translation>Rychlost zadního ventilátoru</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="125"/>
        <source>Resin Sensor</source>
        <translation>Senzor resinu</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="168"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="28"/>
        <source>Settings &amp; Sensors</source>
        <translation>Nastavení a senzory</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="78"/>
        <source>About Us</source>
        <translation>O nás</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="36"/>
        <source>Download Examples</source>
        <translation>Stáhnout modely</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="50"/>
        <source>Manual</source>
        <translation>Manuál</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="30"/>
        <source>Support</source>
        <translation>Podpora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="69"/>
        <source>System Information</source>
        <translation>Systémové informace</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="60"/>
        <source>Videos</source>
        <translation>Videa</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Nebyly nahrány žádné logy.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>Last Seen Logs:</source>
        <translation>Poslední zobrazené logy:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="55"/>
        <source>Save to USB Drive</source>
        <translation>Uložit na USB disk</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="29"/>
        <source>System Logs</source>
        <translation>Systémové logy</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="65"/>
        <source>Upload to Server</source>
        <translation>Nahrát na server</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="98"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="63"/>
        <source>Off</source>
        <translation>Vyp</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="58"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="36"/>
        <source>Screensaver timer</source>
        <translation>Časovač spořiče obrazovky</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="80"/>
        <source>Touch Screen Brightness</source>
        <translation>Jas dotykové obrazovky</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="28"/>
        <source>Touchscreen</source>
        <translation>Dotykový displej</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="81"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation>Dočasný token</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation>Jméno balíčku</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation>Softwarové balíky</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation>verze</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation>Sériové číslo A64</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>Ambient Temperature</source>
        <translation>Ambientní teplota</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <source>Blower Fan</source>
        <translation>Boční ventilátor</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation>Booster Board SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <location filename="../qml/PageSysinfo.qml" line="192"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Closed</source>
        <translation>Zavřené</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>Cover State</source>
        <translation>Stav víka</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>CPU Temperature</source>
        <translation>Teplota CPU</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation>IP adresa LAN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation>SN osvitového displeje</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="199"/>
        <location filename="../qml/PageSysinfo.qml" line="209"/>
        <location filename="../qml/PageSysinfo.qml" line="219"/>
        <source>Fan Error!</source>
        <translation>Chyba ventilátoru!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Finished Projects</source>
        <translation>Dokončené projekty</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation>Verze GUI</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation>Stav zařízení</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation>Motion Controller HW revize</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation>Sériové číslo MC</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation>Motion Controller SW verze</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation>Stav sítě</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Novější než SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Not triggered</source>
        <translation>Nesepnuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Open</source>
        <translation>Otevřené</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation>Verze obrazu OS</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation>Ostatní komponenty</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="240"/>
        <source>Power Supply Voltage</source>
        <translation>Napětí zdroje</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Print Display Time Counter</source>
        <translation>Ukazatel tiskového času displeje</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation>Model tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>Printer Password</source>
        <translation>Heslo tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>Rear Fan</source>
        <translation>Zadní ventilátor</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Resin Sensor State</source>
        <translation>Stav senzoru resinu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>seconds</source>
        <translation>sekundy</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="298"/>
        <source>Show software packages &amp; licenses</source>
        <translation>Zobrazit softwarové balíčky a licence</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Started Projects</source>
        <translation>Zahájené projekty</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>Statistics</source>
        <translation>Statistiky</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation>Systém</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation>Systémové informace</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation>Doba rychlého náklonu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Time of High Viscosity Tilt:</source>
        <translation>Čas tiltu pro vysokou viskozitu:</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation>Doba pomalého náklonu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Layers Printed</source>
        <translation>Celkový počet vytištěných vrstev</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Print Time</source>
        <translation>Celková doba tisku</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="287"/>
        <source>Total Resin Consumed</source>
        <translation>Celková spotřeba resinu</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Triggered</source>
        <translation>Sepnuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="226"/>
        <source>UV LED</source>
        <translation>UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <source>UV LED Fan</source>
        <translation>Ventilátor UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>UV LED Temperature</source>
        <translation>Teplota UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>UV LED Time Counter</source>
        <translation>Doba svitu UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="241"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation>IP adresa Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageTankSurfaceCleanerWizard</name>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="160"/>
        <source>Cleaning Adaptor</source>
        <translation>Čistící adaptér</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="149"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="134"/>
        <source>Exposure[s]:</source>
        <translation>Doba osvitu [s]:</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="81"/>
        <source>Here you can optionally adjust the exposure settings before cleaning starts.</source>
        <translation>Zde lze upravit nastavení expozice před začátkem čištění.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="89"/>
        <source>Less</source>
        <translation>Méně</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="108"/>
        <source>More</source>
        <translation>Více</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="172"/>
        <source>Now remove the cleaning adaptor along with the exposed film, careful not to tear it.</source>
        <translation>Nyní opatrně vyjměte čistící adaptér společně s vytvrzenou vrstvou. Postupujte opatrně, aby nedošlo k roztržení.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="161"/>
        <source>Please insert the cleaning adaptor as is ilustrated in the picture on the left.</source>
        <translation>Vložte čistící adaptér tak, jak je zobrazeno na obrázku vlevo.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="171"/>
        <source>Remove Adaptor</source>
        <translation>Odstraňte adaptér</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="33"/>
        <source>Resin Tank Cleaning</source>
        <translation>Čištění vaničky na resin</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="61"/>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="69"/>
        <source>Tank Cleaning</source>
        <translation>Čištění vaničky</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="62"/>
        <source>This wizard will help you clean your resin tank and remove resin debris.

You will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine&apos;s internal storage. Or you can download it at printables.com</source>
        <translation>Tento průvodce vám pomůže vyčistit vaničku a odstranit drobné vytvrzené částečky.

K tomu je vyžadován speciální nástroj - čistící adaptér. Vytisknout si jej můžete sami - je přiložen jako ukázkový výtisk v interním úložišti této tiskárny, případně si jej můžete stáhnout na Printables.com</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation>Rychle dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation>Rychle nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation>Pohyb vaničky</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="139"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation>12hodinový</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="140"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation>24hodinový</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="163"/>
        <source>Automatic time settings using NTP ...</source>
        <translation>Automatické nastavení času (NTP) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="94"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="138"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation>Nativní</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="73"/>
        <source>Time</source>
        <translation>Čas</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="133"/>
        <source>Time Format</source>
        <translation>Formát času</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="35"/>
        <source>Time Settings</source>
        <translation>Nastavení času</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="115"/>
        <source>Timezone</source>
        <translation>Časové pásmo</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="47"/>
        <source>Use NTP</source>
        <translation>Automatické nastavení času</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation>Rychle dolů</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation>Rychle nahoru</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation>Pohyb platformy</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="79"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Opatrně odstraňte ochrannou nálepku z expozičního displeje.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="69"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation>Odstraňte bezpečnostní nálepku a otevřete víko.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="59"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation>Sundejte ochranný materiál z obou stran tiskové platformy.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Vybalování</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="89"/>
        <source>Unpacking done.</source>
        <translation>Rozbalení je hotové.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="49"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation>Odšroubujte a odejměte vaničku, pak odstraňte ochrannou destičku.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="46"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Opatrně odstraňte ochrannou nálepku z expozičního displeje.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Vybalování</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="56"/>
        <source>Unpacking done.</source>
        <translation>Rozbalení je hotové.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="419"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation>Během aktualizace se objevil problém. Prosím, kontaktujte nás (pošlete logy). Tiskárna bude fungovat stejně jako předtím. Pokračujte stisknutím „zpět“</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="296"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation>Tiskárnu během aktualizace nevypínejte!&lt;br/&gt;Tiskárna se po úspěšné aktualizaci sama restartuje.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="229"/>
        <source>Download failed.</source>
        <translation>Stažení se nezdařilo.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation>Stahování</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation>Stahuji firmware. Instalace začne po stažení.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="255"/>
        <source>Installing Firmware</source>
        <translation>Instalace firmwaru</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="360"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation>Tiskárna bude restartována a spustí se s novým firmwarem(%1)</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation>Aktualizace tiskárny</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="343"/>
        <source>Updated to %1 failed.</source>
        <translation>Aktualizace na %1 selhala.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="386"/>
        <source>Update to %1 failed.</source>
        <translation>Aktualizace na %1 selhala.</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="32"/>
        <source>Hardware Upgrade</source>
        <translation>Upgrade hardwaru</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="90"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Používejte pouze platformu, která patří k tiskárně. Použití jiné platformy může vést k vylití resinu a poškození tiskárny!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="100"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Prosím vezměte na vědomí, že přechod zpět na původní verzi není podporován.

Downgrade vymaže údaje UV kalibrace, vaše tiskárna pak nemusí fungovat správně.

Bude potřeba tiskárnu nově zkalibrovat pomocí externího UV kalibrátoru.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="49"/>
        <source>Proceed?</source>
        <translation>Pokračovat?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="60"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation>Znovu osaďte komponenty SL1S a zapněte tiskárnu. Dojde tím k navrácení do původního stavu.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation>SL1S komponenty detekovány (upgrade z SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="69"/>
        <source>The configuration is going to be cleared now.</source>
        <translation>Nyní dojde k vymazání současné konfigurace.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="70"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>Po restartování proběhne počáteční nastavení tiskárny.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="59"/>
        <source>The printer will power off now.</source>
        <translation>Tiskárna se nyní vypne.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="48"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>K dokončení procesu upgradu tiskárna potřebuje vynulovat konfiguraci a restartovat se.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="109"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation>Aktualizace dokončena. V následujícím kroku se tiskárna restartuje.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="80"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation>Používejte pouze plastovou vaničku, která je součástí balení. Stará kovová vanička může způsobit vylití resinu a poškození tiskárny!</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation>1. Pokud je vanička v tiskárně, vyjměte ji spolu se šrouby.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="71"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation>1. Umístěte UV kalibrátor na osvitový displej a připojte jej k přednímu USB portu.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="52"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="73"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation>2. Zavřete víko a neotvírejte jej! UV záření je škodlivé!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Intensity: center %1, edge %2</source>
        <translation>Intenzita: střed %1, okraj %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="84"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation>Otevřete víko, &lt;b&gt;odstraňte a odpojte&lt;/b&gt; UV kalibrační sondu.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="107"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation>Tiskárna je úspěšně zkalibrovaná!
Chcete použít výsledky kalibrace?</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="100"/>
        <source>The result of calibration:</source>
        <translation>Výsledek kalibrace:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="32"/>
        <source>UV Calibration</source>
        <translation>Kalibrace UV</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation>Intenzita UV: %1,σ=%2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="103"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation>Intenzita UV min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="101"/>
        <source>UV PWM: %1</source>
        <translation>UV PWM: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="57"/>
        <source>Warm-up: %1 s</source>
        <translation>Zahřívání: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>Welcome to the UV calibration.</source>
        <translation>Vítejte v UV kalibraci.</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation>Načítání, vyčkejte...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation>Naskenováním QR kódu otevřete playlist našeho YouTube kanálu, kde naleznete užitečné video návody pro toto zařízení.

Alternativně můžete použít tento odkaz:</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation>Videa</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation>Vyčkejte</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="193"/>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="203"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation>Nastavuji připojení, vyčkejte...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation>Připojeno</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation>Odpojeno</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="233"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Opravdu chcete smazat nastavení této sítě?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="232"/>
        <source>Forget network?</source>
        <translation>Zapomenout nastavení sítě?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="223"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation>Zapomenout síť</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation>Info o síti</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="212"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Vrátit zpět</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation>Nastavení Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Jste si jisti?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="71"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="59"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation>Jste si jistí?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="115"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation>Táhnutím pokračujte</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="102"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Jste si jistí?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="265"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="174"/>
        <source>Swipe for a picture</source>
        <translation>Posuňte pro ilustraci</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="253"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation>Zapojeno</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation>Odpojeno</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="113"/>
        <source>Off</source>
        <translation>Vyp</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="172"/>
        <source>On</source>
        <translation>Zap</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation>Vyčkejte...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation>Potvrďte posunutím</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation>Nesmí být prázdný. Povolené znaky: 0-9, a-z, A-Z a &quot;-&quot;!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="115"/>
        <source>back</source>
        <translation>zpět</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="130"/>
        <source>cancel</source>
        <translation>zrušit</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="144"/>
        <source>close</source>
        <translation>zavřít</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation>Aktivuji</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>ambient temperature</source>
        <translation>ambientní teplota</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>blower fan</source>
        <translation>boční ventilátor</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="650"/>
        <source>Cancel?</source>
        <translation>Zrušit?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="665"/>
        <source>Cancel the current print job?</source>
        <translation>Zrušit aktuální tisk?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation>Připojeno</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>CPU temperature</source>
        <translation>Teplota CPU</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation>Deaktivováno</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation>Deaktivování</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="699"/>
        <location filename="../qml/main.qml" line="723"/>
        <source>DEPRECATED PROJECTS</source>
        <translation>NEPODPOROVANÉ PROJEKTY</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="407"/>
        <source>Initializing...</source>
        <translation>Inicializace...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="417"/>
        <source>MC Update</source>
        <translation>Aktualizace MC</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="310"/>
        <source>Notifications</source>
        <translation>Upozornění</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="356"/>
        <source>Now it&apos;s safe to remove USB drive</source>
        <translation>Nyní je možné bezpečně odebrat jednotku USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>Press &quot;cancel&quot; to abort.</source>
        <translation>Stisněte Zrušit pro ukončení.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="714"/>
        <location filename="../qml/main.qml" line="737"/>
        <source>&lt;printer IP&gt;</source>
        <translation>&lt;printer IP&gt;</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="688"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation>Tiskárnu nelze vypnout. Dokončete nebo zrušte aktuální úlohu a akci zopakujte.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation>Uživatelské rozhraní dotykové obrazovky SL1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>rear fan</source>
        <translation>zadní ventilátor</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="355"/>
        <source>Remove USB</source>
        <translation>Vyjměte USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="339"/>
        <source>Remove USB flash drive?</source>
        <translation>Odebrat USB flash disk?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="724"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation>Byly nalezeny nekompatibilní tiskové projekty, můžete si je stáhnout zde:

http://%1/old-projects.

Chcete je odstranit?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="700"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation>Byly nalezeny nekompatibilní projekty. Může si je stáhnout na

http://%1/old-projects

Přejete si je odstranit?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="418"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation>Firmware pro Motion Controller se aktualizuje.

Čekejte prosím...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="408"/>
        <source>The printer is initializing, please wait ...</source>
        <translation>Tiskárna se spouští, čekejte prosím...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>The USB drive cannot be safely removed because it is now in use, please wait.</source>
        <translation>Jednotku USB nelze bezpečně vyjmout, protože je nyní používána, vyčkejte prosím.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="637"/>
        <source>Turn Off?</source>
        <translation>Vypnout?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>unknown device</source>
        <translation>neznámé zařízení</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation>neznámý</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>UV LED fan</source>
        <translation>Ventilátor UV LED</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="101"/>
        <source>UV LED temperature</source>
        <translation>Teplota UV LED</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation>Méně než minuta</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n hodina</numerusform>
            <numerusform>%n hodin</numerusform>
            <numerusform>%n hodin</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n minuta</numerusform>
            <numerusform>%n minut</numerusform>
            <numerusform>%n minut</numerusform>
        </translation>
    </message>
</context>
</TS>
