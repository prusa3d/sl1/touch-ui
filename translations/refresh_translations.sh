#!/bin/bash
# refresh the ts files and possibly fix the broken context
lupdate $(find .. -name "*.qml") ../qml/utils.js  -ts *.ts -noobsolete

# check the language format
for FILE in en_US.ts it_IT.ts cs_CZ.ts es_ES.ts pl_PL.ts de_DE.ts fr_FR.ts
do
	if [[ -f $FILE ]]; then
		echo .
	else
		echo "File $FILE is missing !!!"
		exit 1
	fi

	if ! grep -q "<TS version=\"[0-9].[0-9]\" language=\"cs-CZ\|de-DE\|en-US\|es-ES\|fr-FR\|it-IT\|pl-PL\">" $FILE; then
       		echo "$FILE: language is NOT in correct format. Should be one of the following: cs-CZ, de-DE, en-US, es-ES, fr-FR, it-IT, pl-PL"
        	exit 1
	fi

done

# Produce alphabetically sorted file to ease import Prusalator
python3 ./sort_translation.py en_US.ts en_US_sorted_for_translators.ts

# lrelease *.ts # : *.qml files are generated in the build directory by cmake
