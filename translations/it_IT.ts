<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it-IT">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="90"/>
        <source>Add Hidden Network</source>
        <translation>Aggiungi rete nascosta</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Plugged in</source>
        <translation>Collegato</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Unplugged</source>
        <translation>Scollegato</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="37"/>
        <source>Network Info</source>
        <translation>Informazioni di rete</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="38"/>
        <source>Wi-Fi Client</source>
        <translation>Client Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Vuoi veramente eliminare le impostazioni di questa rete?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="105"/>
        <source>Forget network?</source>
        <translation>Dimenticare rete?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="35"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="136"/>
        <source>Apply calibration results</source>
        <translation>Applica i risultati della calibrazione</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="134"/>
        <source>Calibrate center</source>
        <translation>Calibra centro</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="135"/>
        <source>Calibrate edge</source>
        <translation>Calibra bordo</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="174"/>
        <source>Canceled</source>
        <translation>Annullato</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="131"/>
        <source>Check for UV calibrator</source>
        <translation>Controllo del Calibratore UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="156"/>
        <source>Check ID:</source>
        <translation>Controllo ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation>Cancella i profili di Slicer scaricati</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Clear UV calibration data</source>
        <translation>Cancella dati di calibrazione UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Disable factory mode</source>
        <translation>Disattivare la modalità di fabbrica</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="130"/>
        <source>Disable ssh, serial</source>
        <translation>Disabilita ssh, seriale</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Display test</source>
        <translation>Test Display</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Erase motion controller EEPROM</source>
        <translation>Cancellare la EEPROM del controller di movimento</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Erase projects</source>
        <translation>Cancella progetti</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="140"/>
        <source>Erase UV PWM settings</source>
        <translation>Cancellare le impostazioni PWM UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="150"/>
        <source>Exposing debris</source>
        <translation>Esposizione residui</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="171"/>
        <source>Failure</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Make tank accessible</source>
        <translation>Rendi il serbatoio accessibile</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="148"/>
        <source>Moving platform down to safe distance</source>
        <translation>Spostamento della piattaforma a distanza di sicurezza</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="151"/>
        <source>Moving platform gently up</source>
        <translation>Spostamento della piattaforma delicatamente verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Moving printer to accept protective foam</source>
        <translation>Spostamento della stampante per accogliere la schiuma protettiva</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Obtain calibration info</source>
        <translation>Ottieni informazioni sulla calibrazione</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Obtain system info</source>
        <translation>Ottieni informazioni di sistema</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="170"/>
        <source>Passed</source>
        <translation>Riuscito</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Platform calibration</source>
        <translation>Calibrazione piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="153"/>
        <source>Platform home</source>
        <translation>Home piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Platform range</source>
        <translation>Portata piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Pressing protective foam</source>
        <translation>Pressatura della schiuma protettiva</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Printer calibration</source>
        <translation>Calibrazione stampante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>Recording changes</source>
        <translation>Registrazione modifiche</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Release foam</source>
        <translation>Togli la schiuma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Reset homing profiles</source>
        <translation>Resettare i profili di homing</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>Reset hostname</source>
        <translation>Ripristina il nome dell&apos;host</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Reset Network settings</source>
        <translation>Ripristino delle impostazioni di rete</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Reset NTP state</source>
        <translation>Ripristina stato NTP</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset print configuration</source>
        <translation>Ripristina configurazione di stampa</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Reset printer calibration status</source>
        <translation>Ripristinare lo stato di calibrazione della stampante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Reset Prusa Connect</source>
        <translation>Resettare Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>Reset PrusaLink</source>
        <translation>Resettare PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Reset selftest status</source>
        <translation>Azzeramento dello stato del autotest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Reset system locale</source>
        <translation>Ripristinare la localizzazione del sistema</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Reset timezone</source>
        <translation>Reimposta fuso orario</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Resetting hardware counters</source>
        <translation>Reimpostazione contatori hardware</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="139"/>
        <source>Reset UI settings</source>
        <translation>Ripristina le impostazioni UI</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="154"/>
        <source>Reset update channel</source>
        <translation>Azzeramento del canale di aggiornamento</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Resin sensor</source>
        <translation>Sensore della Resina</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="169"/>
        <source>Running</source>
        <translation>In esecuzione</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Send printer data to MQTT</source>
        <translation>Invia i dati della stampante a MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Serial number</source>
        <translation>Numero seriale</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Set new printer model</source>
        <translation>Imposta nuovo modello stampante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Sound test</source>
        <translation>Test sonoro</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Tank calib. start</source>
        <translation>Inizio della calibrazione del serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Tank home</source>
        <translation>Home serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="138"/>
        <source>Tank level</source>
        <translation>Livello serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Tank range</source>
        <translation>Intervallo serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Tilt timming</source>
        <translation>Tempi di inclinazione</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="149"/>
        <source>Touching platform down</source>
        <translation>Toccare la piattaforma sottostante</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="173"/>
        <source>User action pending</source>
        <translation>Azione dell&apos;utente in sospeso</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="133"/>
        <source>UV calibrator placed</source>
        <translation>Calibratore UV posizionato</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>UV LED</source>
        <translation>LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>UV LED and fans</source>
        <translation>LED UV e ventole</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="132"/>
        <source>UV LED warmup</source>
        <translation>Riscaldamento LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="168"/>
        <source>Waiting</source>
        <translation>Attesa</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="152"/>
        <source>Waiting for user</source>
        <translation>Attesa utente</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="137"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation>In attesa che venga rimosso il calibratore UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="172"/>
        <source>With Warning</source>
        <translation>Con preavviso</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="56"/>
        <source>Understood</source>
        <translation>Capito</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation>Errore sconosciuto</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>A64 OVERHEAT</source>
        <translation>SURRISCALDAMENTO A64</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation>Temperatura A64 troppo alta. Misurata: %(temperature).1f °C! Spegnimento tra 10 secondi...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation>ADMIN NON DISPONIBILE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="234"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation>TEMPERATURA AMBIENTE TROPPO ALTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="235"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation>TEMPERATURA AMBIENTE TROPPO BASSA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>Analysis of the project failed</source>
        <translation>Analisi del progetto non riuscita</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation>È già in corso un&apos;altra azione. Completa questa azione direttamente usando il touchscreen della stampante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>An unexpected error has occurred.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation>Si è verificato un errore inaspettato.
Se è in corso un lavoro di stampa, questo sarà completato.
È possibile spegnere la stampante premendo il pulsante di accensione frontale.
Vedere il manuale per sapere come salvare un file di registro e inviarcelo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation>Si è verificato un avviso sconosciuto. Riavvia la stampante e riprova. Contatta il nostro supporto tecnico se il problema persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>A part of the LED panel is disconnected.</source>
        <translation>Una parte del pannello LED è scollegata.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation>PROBLEMA SCHEDA BOOSTER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="231"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation>SLOT DI BOOT CAMBIATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>Broken UV LED panel</source>
        <translation>Pannello LED UV guasto</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>CALIBRATION ERROR</source>
        <translation>ERRORE CALIBRAZIONE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>CALIBRATION FAILED</source>
        <translation>CALIBRAZIONE NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation>CARICAMENTO CALIBRAZIONE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>Calibration project is invalid</source>
        <translation>Il progetto di calibrazione non è valido</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation>IL PROGETTO DI CALIBRAZIONE NON È VALIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation>Non è possibile connettere il calibratore UV LED. Controllare la connessione e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Cannot export profile</source>
        <translation>Impossibile esportare il profilo</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Cannot find the selected file!</source>
        <translation>Impossibile trovare il file selezionato!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation>Impossibile ottenere il canale di aggiornamento. Riavviare la stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>Cannot import profile</source>
        <translation>Impossibile importare il profilo</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>CANNOT READ PROJECT</source>
        <translation>IMPOSSIBILE LEGGERE IL PROGETTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation>IMPOSSIBILE RIMUOVERE IL PROGETTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation>Non è possibile inviare la configurazione di fabbrica al database (MQTT)! Controllare la connessione di rete. Ti preghiamo di contattare il supporto.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation>Impossibile impostare il canale di aggiornamento. Riavviare la stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="236"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation>IMPOSSIBILE COPIARE IL PROGETTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>CLEANING ADAPTOR MISSING</source>
        <translation>ADATTATORE DI PULIZIA MANCANTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>Cleaning adaptor was not detected, it does not seem to be correctly attached to the print platform.
Attach it properly and try again.</source>
        <translation>L&apos;adattatore di pulizia non è stato rilevato, non sembra essere attaccato correttamente alla piattaforma di stampa.
Attaccalo correttamente e prova di nuovo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Communication with the Booster board failed.</source>
        <translation>Comunicazione con la scheda Booster non riuscita.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation>Comunicazione con il calibratore LED UV non riuscito. Controllare la connessione e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation>ERRORE LETTURA FILE CONFIGURAZIONE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>CONNECTION FAILED</source>
        <translation>CONNESSIONE NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation>Connessione ai server Prusa non riuscita, si prega di riprovare più tardi.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation>Sono state trovate impostazioni corrette, ma la deviazione standard
(%(found).1f) è maggiore del valore permesso (%(allowed).1f).
Verificare la posizione e la calibrazione del calibratore UV LED e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation>I dati provengono da un sensore LED UV sconosciuto!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation>SOVRASCRITTURA DATI NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation>PREPARAZIONE DATI NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation>DIRECTORY NON VUOTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>Disconnected UV LED panel</source>
        <translation>Pannello LED UV scollegato</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>DISPLAY TEST ERROR</source>
        <translation>ERRORE TEST DISPLAY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Display test failed.</source>
        <translation>Test Display non riuscito.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>DISPLAY TEST FAILED</source>
        <translation>TEST DISPLAY NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>Display usage error</source>
        <translation>Errore utilizzo display</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>DOWNLOAD FAILED</source>
        <translation>DOWNLOAD NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Error displaying test image.</source>
        <translation>Errore nella visualizzazione dell&apos;immagine di test.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>Error, there is no file to reprint.</source>
        <translation>Errore, nessun file da ristampare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation>Mancano esempi (qualsiasi progetto) nella memoria utente. Scaricarli nuovamente dal menu &apos;Impostazioni&apos;.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="246"/>
        <source>EXPECT OVERHEATING</source>
        <translation>PREVISTO SURRISCALDAMENTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation>Lo schermo di esposizione attualmente collegato è già stato utilizzato su questa stampante. Questo schermo è stato usato l&apos;ultima volta per circa %(counter_h)d ore.

Se non si vuole utilizzare questo schermo: spegnere la stampante, sostituire lo schermo e riaccendere la stampante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation>Impossibile modificare il livello di registro (dettaglio). Riavviare la stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation>Impossibile caricare la calibrazione di fabbrica di ventole e LED.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Failed to reach the tilt endstop.</source>
        <translation>Impossibile raggiungere il finecorsa del meccanismo di inclinazione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tower endstop.</source>
        <translation>Impossibile raggiungere il finecorsa della torre.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation>Impossibile leggere il file di configurazione. Prova a ripristinare la stampante. Se il problema persiste, contatta il nostro supporto.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation>Impossibile salvare i dati della procedura guidata. Riavvia la stampante e riprova.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation>Impossibile serializzare i dati della procedura guidata. Riavviare la stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Failed to set hostname</source>
        <translation>Impossibile impostare hostname</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>FAN FAILURE</source>
        <translation>GUASTO VENTOLA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation>RPM VENTOLA FUORI RANGE DEL TEST</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="245"/>
        <source>FAN WARNING</source>
        <translation>AVVISO VENTOLA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>FILE ALREADY EXISTS</source>
        <translation>FILE GIÀ ESISTENTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation>File già esistente! Cancellarlo prima dalla stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation>Il file ha un&apos;estensione non valida! Vedere l&apos;articolo per le estensioni dei file supportati.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FILE NOT FOUND</source>
        <translation>FILE NON TROVATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="247"/>
        <source>FILL THE RESIN</source>
        <translation>RIEMPIRE LA RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation>AGGIORNAMENTO FIRMWARE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>HOSTNAME ERROR</source>
        <translation>ERRORE NOME HOST</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>Image preloader did not finish successfully!</source>
        <translation>Il precaricatore d&apos;immagini non è riuscito a terminare correttamente!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="237"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation>MODELLO DI STAMPANTE ERRATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan.</source>
        <translation>Lettura RPM ventola %(failed_fans_text)s errata.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation>Lettura errata del numero di giri della ventola %(failed_fans_text)s. La stampa può continuare, ma c&apos;è il rischio di surriscaldamento.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>Incorrect RPM reading of the %(fan__map_HardwareDeviceId)s.</source>
        <translation>Lettura RPM errata ventola %(fan__map_HardwareDeviceId)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>INTERNAL ERROR</source>
        <translation>ERRORE INTERNO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation>Errore interno (mappatura DBUS fallita), riavviare la stampante. Contattare il supporto se il problema persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation>MEMORIA INTERNA PIENA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation>La memoria interna è piena. Elimina prima alcuni dei tuoi progetti.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>INVALID API KEY</source>
        <translation>CHIAVE API NON VALIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>INVALID FILE EXTENSION</source>
        <translation>ESTENSIONE FILE NON VALIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INVALID PASSWORD</source>
        <translation>PASSWORD NON VALIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>INVALID PROJECT</source>
        <translation>PROGETTO NON VALIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>Invalid tilt alignment position.</source>
        <translation>Posizione di allineamento del meccanismo di inclinazione non valida.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation>POSIZIONE DI ALLINEAMENTO INCLINAZIONE NON VALIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>LANGUAGE NOT SET</source>
        <translation>LINGUA NON IMPOSTATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="241"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation>AVVISO DI NON UTILIZZO DELLA MASCHERA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>MC WRONG REVISION</source>
        <translation>REVISIONE MC ERRATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation>Il volume di resina misurato è troppo basso. La stampa può continuare, ma potrebbe essere necessaria una ricarica.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation>Volume resina misurato %(volume_ml)d ml è più alto di quanto richiesto per questa stampa. Assicurarsi che il livello della resina non superi il segno del 100% e riavviare la stampa.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation>Volume della resina misurata %(volume_ml)d ml è minore di quanto richiesto per questa stampa. Riempire il serbatoio e riavviare la stampa.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation>Misurazione della resina non riuscita. Verificare la presenza della piattaforma e la quantità di resina nel serbatoio.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <source>Moving the tilt failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Spostamento del meccanismo di inclinazione non riuscito. Controllare che non vi siano ostacoli sul percorso e ripetere l&apos;azione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Spostamento torre non riuscito. Controllare che non vi siano ostacoli sul percorso e ripetere l&apos;azione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation>CARICAMENTO MQTT NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>No calibration data to show!</source>
        <translation>Nessun dato di calibrazione da mostrare!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>No display usage data to show</source>
        <translation>Nessun dato di utilizzo del display da mostrare</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>NO FILE TO REPRINT</source>
        <translation>NESSUN FILE DA RISTAMPARE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>NO INTERNET CONNECTION</source>
        <translation>NESSUNA CONNESSIONE AD INTERNET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation>Nessun problema rilevato. È possibile continuare ad utilizzare la stampante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="238"/>
        <source>NOT ENOUGH RESIN</source>
        <translation>RESINA NON SUFFICIENTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation>LAYER NON SUFFICIENTI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No USB storage present</source>
        <translation>Nessuna memoria USB presente</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation>NESSUN DATO CALIBRAZIONE UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="232"/>
        <source>NO WARNING</source>
        <translation>NESSUN AVVISO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="242"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation>AVVISO OGGETTO TAGLIATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation>L&apos;oggetto è stato ritagliato perché non rientra nell&apos;area di stampa.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>OLD EXPO PANEL</source>
        <translation>VECCHIO PANNELLO ESPOSIZIONE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>OPENING PROJECT FAILED</source>
        <translation>APERTURA PROGETTO NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Apertura del progetto fallita. Il file è corrotto. Si prega di ripetere lo slicing o di riesportare il progetto e di riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Apertura del progetto non riuscita. Il file potrebbe essere corrotto. Si prega di ripetere lo slicing o di esportare nuovamente il progetto e di riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation>Apertura del progetto non riuscita, il file potrebbe essere corrotto. Ripetere lo slicing o di riesportare il progetto e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="239"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation>PARAMETRI FUORI PORTATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="240"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation>AVVISO NON PERMESSO PER PARTI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="118"/>
        <source>Per-partes print not available.</source>
        <translation>Stampa per parte non disponibile.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>Attiva il digest HTTP (che è l&apos;opzione di sicurezza raccomandata) o aggiorna la chiave API. Puoi trovarla in Impostazioni &gt; Rete &gt; Credenziali di accesso.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>Pour enough resin for the selected file into the tank and close the lid. The minimal amount of the resin is displayed on the touchscreen.</source>
        <translation>Versare nel serbatoio la quantità di resina sufficiente per il file selezionato e chiudere il coperchio. La quantità minima di resina viene visualizzata sul touchscreen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>PRELOAD FAILED</source>
        <translation>PRECARICAMENTO NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>PRINTER IS BUSY</source>
        <translation>LA STAMPANTE È OCCUPATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>PRINTER IS OK</source>
        <translation>LA STAMPANTE È OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation>UV STAMPANTE NON CALIBRATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="243"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation>AVVISO INCOMPATIBILITÀ VARIANTE STAMPANTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation>ESEMPI DI STAMPA MANCANTI</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>PRINT JOB CANCELLED</source>
        <translation>LAVORO DI STAMPA ANNULLATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>Print mask is missing.</source>
        <translation>Maschera di stampa mancante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation>ERRORE ESPORTAZIONE PROFILO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation>ERRORE IMPORTAZIONE PROFILO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation>ANALISI PROGETTO NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation>PROGETTO CORROTTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Reading of %(sensor__map_HardwareDeviceId)s not in range!

Measured temperature: %(temperature).1f °C, allowed range: %(min)d - %(max)d °C.

Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>Lettura del %(sensor__map_HardwareDeviceId)s fuori portata!

Temperatura misurata: %(temperature).1f °C, intervallo consentito: %(min)d - %(max)d °C.

Tieni la stampante al riparo dalla luce diretta del sole e a temperatura ambiente (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation>La lettura della temperatura dei LED UV non è riuscita! Questo valore è essenziale per la durata di vita dei LED UV e la sicurezza della stampante. Si prega di contattare il supporto tecnico! Il lavoro di stampa in corso verrà annullato.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>REMOTE API ERROR</source>
        <translation>ERRORE REMOTE API</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation>Non è possibile rimuovere questo progetto. Il progetto è bloccato da un lavoro di stampa.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation>L&apos;intensità richiesta non può essere raggiunta con il PWM massimo consentito.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation>L&apos;intensità richiesta non può essere raggiunta con il PWM minimo consentito.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="244"/>
        <source>RESIN LOW</source>
        <translation>RESINA BASSA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>RESIN MEASURING FAILED</source>
        <translation>MISURAZIONE RESINA NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>RESIN SENSOR ERROR</source>
        <translation>ERRORE SENSORE RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN TOO HIGH</source>
        <translation>LIVELLO RESINA TROPPO ALTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>RESIN TOO LOW</source>
        <translation>LIVELLO RESINA TROPPO BASSO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>RPM of %(fan__map_HardwareDeviceId)s not in range!

RPM data: %(min_rpm)d - %(max_rpm)d
Average: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</source>
        <translation>RPM ventola %(fan__map_HardwareDeviceId)s fuori portata!

Dati RPM: %(min_rpm)d - %(max_rpm)d
Media: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), errore: %(error)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>RPM of %(fan)s not in range!
RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation>RPM della ventola %(fan)s fuori portata!
Dati RPM: %(rpm)s
Media: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation>Il salvataggio del nuovo valore predefinito di fabbrica non è riuscito. Riavviare la stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>%(sensor)s fuori portata! Temperatura misurata: %(temperature).1f °C. Tenere la stampante al riparo dalla luce diretta del sole a temperatura ambiente (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation>ERRORE NUMERO SERIALE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation>Numeri seriali nel formato sbagliato! A64: %(a64)s MC: %(mc)s Contatta il supporto!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation>IMPOSTAZIONE DETTAGLI REGISTRO NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation>IMPOSTAZIONE CANALE AGGIORNAMENTO NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>Something went wrong with the printer firmware. Please contact our support and do not forget to attach the logs.
Crashed services are immediately started again. If, despite that, the printer does not behave correctly, try restarting it. </source>
        <translation>Qualcosa è andato storto con il firmware della stampante. Contatta il nostro supporto e non dimenticare di allegare i log.
I servizi in crash vengono immediatamente riavviati. Se, nonostante ciò, la stampante non si comporta correttamente, prova a riavviarla. </translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Speaker test failed.</source>
        <translation>Test altoparlante non riuscito.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>SPEAKER TEST FAILED</source>
        <translation>TEST SPEAKER NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="230"/>
        <source>SYSTEM SERVICE CRASHED</source>
        <translation>IL SERVIZIO DI SISTEMA SI È BLOCCATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation>TEMPERATURA FUORI PORTATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation>SENSORE TEMPERATURA GUASTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>The admin menu is not available.</source>
        <translation>Il menù admin non è disponibile.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation>La temperatura ambiente è troppo alta, la stampa può continuare, ma potrebbe non riuscire.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation>La temperatura ambiente è troppo bassa, la stampa può continuare, ma potrebbe non riuscire.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation>La quantità di resina nel serbatoio non è sufficiente per questo progetto. Sarà necessario aggiungere altra resina durante la stampa.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation>La calibrazione automatica LED UV non è stata completata correttamente! Eseguire nuovamente la calibrazione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation>La calibrazione non è terminata con successo! Eseguire nuovamente la calibrazione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The directory is not empty.</source>
        <translation>La directory non è vuota.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation>Download non riuscito. Controllare la connessione ad internet e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation>La memoria interna è piena, il progetto non può essere copiato. È possibile continuare a stampare. Tuttavia, non è possibile rimuovere l&apos;unità USB durante la stampa, altrimenti il processo non andrà a buon fine.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation>La lingua non è impostata. Vai su Impostazioni -&gt; Lingua e ora -&gt; Imposta lingua e scegli la tua lingua preferita.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation>Il modello è stato processato per un modello di stampante differente. Rieseguire lo slicing del modello usando le impostazioni corrette.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation>Il modello è stato processato per una variante di stampante differente %(project_variant)s. La variante della tua stampante è %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation>Il Motion Controller (MC) ha riscontrato un errore inaspettato. Riavviare la stampante.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>The password is incorrect. Please check or update it in:

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

The password must be at least 8 characters long. Supported characters are letters, numbers, and dash.</source>
        <translation>La password non è corretta. Verificarla o aggiornarla in:

Impostazioni &gt; Rete &gt; PrusaLink.
PrusaSlicer deve utilizzare il tipo di autorizzazione HTTP digest.

La password deve essere lunga almeno 8 caratteri. I caratteri supportati sono lettere, numeri e trattini.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation>La stampante è stata avviata da uno slot alternativo a causa di tentativi di avvio non riusciti utilizzando lo slot principale.
Aggiornare la stampante con il firmware aggiornato al più presto per ripristinare lo slot principale.
Questo di solito accade dopo un aggiornamento non riuscito o a causa di un errore hardware. Le impostazioni della stampante potrebbero essere state ripristinate.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation>La stampante non è calibrata. Esegui la procedura guidata.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation>La stampante non è connessa a Internet. Controlla la connessione nelle Impostazioni.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation>LED UV della stampante non calibrato. Collegare il calibratore UV e completare la calibrazione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer model was not detected.</source>
        <translation>Il modello di stampante non è stato rilevato.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>La stampante utilizza la sicurezza HTTP digest. Si prega di abilitarlo anche nello slicer (consigliato), o di disattivare questa opzione di sicurezza nella stampante. È possibile trovarla in Impostazioni &gt; Rete &gt; Credenziali di accesso.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>The print job cancelled by the user.</source>
        <translation>Lavoro di stampa annullato dall&apos;utente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="117"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation>I parametri di stampa sono fuori dal range della stampante, il sistema può provare a correggere il progetto. Procedere?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>The project file is invalid!</source>
        <translation>Il file del progetto non è valido!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The project must have at least one layer</source>
        <translation>Il progetto deve avere almeno un layer</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>There is no warning</source>
        <translation>Non c&apos;è nessun avviso</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured.</source>
        <translation>Il sensore di resina non è stato attivato. Controllare se il serbatoio e la piattaforma sono fissati correttamente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>The %(sensor__map_HardwareDeviceId)s sensor failed.</source>
        <translation>Sensore %(sensor__map_HardwareDeviceId)s guasto.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>The %(sensor)s sensor failed.</source>
        <translation>Sensore %(sensor)s guasto.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation>Aggiornamento del firmware non riuscito! Riavviare la stampante e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation>Il calibratore UV LED ha rilevato un po&apos; di luce su un display scuro. Ciò significa che c&apos;è una &quot;infiltrazione&quot; di luce sotto il calibratore UV, oppure il vostro display non blocca abbastanza la luce UV. Controllare il posizionamento del calibratore UV sullo schermo o sostituire il display di esposizione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation>Il calibratore UV LED non è riuscito a leggere l&apos;intensità della luce UV prevista. Controllare il posizionamento del calibratore UV sullo schermo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation>Calibratore LED UV non rilevato. Controllare la connessione e riprovare.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The UV LED panel is not detected.</source>
        <translation>Il pannello LED UV non è stato rilevato.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>The wizard did not finish successfully!</source>
        <translation>Procedura guidata non completata correttamente!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>This project was prepared for a different printer</source>
        <translation>Questo progetto è stato preparato per una stampante diversa</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation>Questa richiesta non è compatibile con l&apos;API remota di Prusa. Vedi la nostra documentazione per maggiori dettagli.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation>La procedura guidata non può essere annullata, completare prima i passi.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation>CONTROLLO ASSE INCLINAZIONE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps</source>
        <translation>Controllo asse meccanismo inclinazione non riuscito!
Posizione attuale: %(position)d passi</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation>FINECORSA INCLINAZIONE NON RAGGIUNTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TILT HOMING FAILED</source>
        <translation>HOMING MECCANISMO INCLINAZIONE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation>Homing meccanismo di inclinazione non riuscito, controllare i dintorni e ripetere l&apos;azione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>TILT MOVING FAILED</source>
        <translation>MOVIMENTO DI INCLINAZIONE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation>CONTROLLO ASSE TORRE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation>Controllo dell&apos;asse della torre non riuscito!

Posizione attuale: %(position_nm)d nm

Controllare se la vite a sfera può muoversi senza problemi in tutto il suo raggio d&apos;azione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation>FINECORSA TORRE NON RAGGIUNTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TOWER HOMING FAILED</source>
        <translation>HOMING TORRE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Homing torre non riuscito, controllare non vi siano ostacoli sul percorso e ripetere l&apos;azione.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>TOWER MOVING FAILED</source>
        <translation>SPOSTAMENTO  TORRE NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation>La torre non è nella posizione prevista.

La piattaforma e il serbatoio sono montati e fissati correttamente?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>TOWER POSITION ERROR</source>
        <translation>ERRORE POSIZIONE TORRE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UNAUTHORIZED</source>
        <translation>NON AUTORIZZATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>UNEXPECTED ERROR</source>
        <translation>ERRORE INASPETTATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation>ERRORE INASPETTATO MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>Unknown printer model</source>
        <translation>Modello di stampante sconosciuto</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>Unknown UV LED calibrator error code: %(nonprusa_code)d</source>
        <translation>Codice di errore del calibratore LED UV sconosciuto: %(nonprusa_code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="233"/>
        <source>UNKNOWN WARNING</source>
        <translation>AVVISO SCONOSCIUTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation>CANALE AGGIORNAMENTO NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation>UNITÀ USB NON RILEVATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>UV CALIBRATION ERROR</source>
        <translation>ERRORE CALIBRAZIONE UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>UV CALIBRATION FAILED</source>
        <translation>CALIBRAZIONE UV NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>UV DATA EROR</source>
        <translation>ERRORE DATI UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>UV INTENSITY ERROR</source>
        <translation>ERRORE INTENSITÀ UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation>Intensità UV non impostata. Si prega di eseguire la calibrazione UV prima di avviare una stampa.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation>INTENSITÀ UV TROPPO ALTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation>INTENSITÀ UV TROPPO BASSA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation>ERRORE CONNESSIONE CALIBRATORE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation>ERRORE CALIBRATORE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation>ERRORE DI COLLEGAMENTO DEL CALIBRATORE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation>CALIBRATORE LED UV NON RILEVATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation>ERRORE DI LETTURA DEL CALIBRATORE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation>ERRORE SCONOSCIUTO CALIBRATORE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation>DISSIPATORE LED UV GUASTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>UV LED is overheating!</source>
        <translation>Il LED UV si sta surriscaldando!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV LED TEMP. ERROR</source>
        <translation>ERRORE TEMPERATURA LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation>ERRORE VOLTAGGIO LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation>I voltaggi del LED UV differiscono troppo. Il modulo LED potrebbe essere guasto. Contattare il supporto.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>WIZARD DATA FAILURE</source>
        <translation>ERRORE DATI CONFIGURAZIONE GUIDATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>WIZARD FAILED</source>
        <translation>CONFIGURAZIONE GUIDATA NON RIUSCITA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>WRONG PRINTER MODEL</source>
        <translation>MODELLO STAMPANTE ERRATO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation>Errata revisione del Motion Controller (MC). Contattate il nostro supporto.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>YOU SHALL NOT PASS</source>
        <translation>TU NON PUOI PASSARE</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="110"/>
        <source>Already exists</source>
        <translation>Già esistente</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="114"/>
        <source>Can&apos;t read</source>
        <translation>Impossibile leggere</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="96"/>
        <source>Error: %1</source>
        <translation>Errore: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="108"/>
        <source>File not found</source>
        <translation>File non trovato</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="112"/>
        <source>Invalid extension</source>
        <translation>Estensione non valida</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="106"/>
        <source>Storage full</source>
        <translation>Memoria piena</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="149"/>
        <source>Inactive</source>
        <translation>Inattivo</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="175"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="247"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La PSK deve essere lunga almeno 8 caratteri.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="232"/>
        <source>Security</source>
        <translation>Sicurezza</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="216"/>
        <source>Show Password</source>
        <translation>Mostra password</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="153"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="253"/>
        <source>SSID must not be empty</source>
        <translation>SSID non può essere vuoto</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="265"/>
        <source>Start AP</source>
        <translation>Avvia AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="138"/>
        <source>State:</source>
        <translation>Stato:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation>Ferma AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="296"/>
        <source>Swipe for QRCode</source>
        <translation>Scorri per il codice QR</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="377"/>
        <source>Swipe for settings</source>
        <translation>Scorri per le impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation>(invariato)</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="23"/>
        <source>About Us</source>
        <translation>Chi siamo</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="52"/>
        <source>Prusa Research a.s.</source>
        <translation>Prusa Research a.s.</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="48"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation>Per saperne di più su di noi scansiona il codice QR o utilizza il link sottostante:</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation>Aggiungi Rete Nascosta</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation>Connetti</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation>PASS</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La PSK deve essere lunga almeno 8 caratteri.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation>Sicurezza</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation>Mostra Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation>SSID non può essere vuoto.</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="40"/>
        <source>Admin API</source>
        <translation>Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="156"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="168"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="64"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation>Inserisci la password per la rete &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="194"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La PSK deve essere lunga almeno 8 caratteri.</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation>Mostra Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation>(invariato)</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Are you sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation>Riesci a vedere il logo dell&apos;azienda sul display dell&apos;esposizione attraverso il coperchio?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="126"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Staccare con cura l&apos;adesivo protettivo dal display dell&apos;esposizione.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="174"/>
        <source>Close the cover.</source>
        <translation>Chiudi il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="173"/>
        <source>Cover</source>
        <translation>Coperchio</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="57"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation>Vuoi davvero annullare la procedura guidata?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="114"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation>Una volta posizionata la carta all&apos;interno della stampante, non dimenticare di chiudere il coperchio!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="59"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation>La macchina non funzionerà senza il completamento della procedura guidata.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="112"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation>Suggerimento: se non riesci a vedere chiaramente il logo, prova a mettere un foglio di carta sullo schermo.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation>Configurazione guidata</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="154"/>
        <source>Wizard canceled</source>
        <translation>Configurazione guidata annullata</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard failed</source>
        <translation>Configurazione guidata non riuscita</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="136"/>
        <source>Wizard finished sucessfuly!</source>
        <translation>Configurazione guidata completata correttamente!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation>La procedura guidata si è interrotta a causa di un problema, riprovare?</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="121"/>
        <source>Done</source>
        <translation>Completato</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="92"/>
        <source>Move Down</source>
        <translation>Sposta giù</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="83"/>
        <source>Move Up</source>
        <translation>Sposta su</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation>Movimento serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="109"/>
        <source>Tilt position:</source>
        <translation>Posizione inclinazione:</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="160"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation>Regolare la piattaforma in modo che sia allineata con il display di esposizione.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="195"/>
        <source>All done, happy printing!</source>
        <translation>Tutto fatto, buona stampa!</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="129"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation>Controlla se la piattaforma è fissata correttamente con la manopola nera (tienila in posizione e stringi la manopola se necessario).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="145"/>
        <source>Close the cover.</source>
        <translation>Chiudi il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="131"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation>Non ruotare la piattaforma. Questa deve essere posizionata come nell&apos;immagine.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="162"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation>I bordi anteriori della piattaforma e il display dell&apos;esposizione devono essere paralleli.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation>Tenere la piattaforma ferma con una mano e applicare una leggera forza verso il basso su di essa, in modo che mantenga un buon contatto con lo schermo.


Poi, utilizzare una chiave a brugola per stringere la vite del cantilever.

Poi rilasciare la piattaforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="50"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation>Se la piattaforma non è ancora inserita, inserirla secondo l&apos;immagine con un angolo di 0° gradi e fissarla con la manopola nera.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="86"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation>Nel prossimo passo, sposta il piano di inclinazione in su/giù finché questo è in contatto con il serbatoio della resina. Il piano basculante e il serbatoio devono essere allineati in una linea perfetta.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation>Allenta la piccola vite sul cantilever con una chiave a brugola. Presta attenzione a non svitarla completamente.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="109"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation>Assicurati che la piattaforma, il serbatoio e il piano basculante siano PERFETTAMENTE puliti.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation>Calibrazione Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="120"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation>Riporta il serbatoio alla posizione originale e fissalo con le viti. Accertati di aver stretto entrambe le viti allo stesso modo e con la stessa forza.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="99"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation>Posiziona il piano inclinabile contro il serbatoio resina</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="64"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation>Alcune stampanti SL1 possono avere due viti - vedere il manuale per maggiori informazioni.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="180"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation>Alcune stampanti SL1 possono avere due viti: stringile in modo uniforme, poco a poco. Consulta il manuale per maggiori informazioni.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="111"/>
        <source>The image is for illustration purposes only.</source>
        <translation>L&apos;immagine è solo a scopo illustrativo.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="178"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation>Stringi la piccola vite sul cantilever con una chiave a brugola.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="77"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation>Svitare il serbatoio, ruotarlo di 90° e posizionarlo piatto sul piano basculante. Rimuovere completamente le viti del serbatoio.</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="149"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="177"/>
        <source>Above Area Fill</source>
        <translation>Riempimento dell&apos;area superiore</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="171"/>
        <source>Above Area Fill Threshold Settings</source>
        <translation>Impostazioni della soglia di riempimento dell&apos;area superiore</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="148"/>
        <source>Area Fill Threshold</source>
        <translation>Soglia Riempimento area</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="197"/>
        <source>Below Area Fill</source>
        <translation>Riempimento dell&apos;area sottostante</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="191"/>
        <source>Below Area Fill Threshold Settings</source>
        <translation>Impostazioni della soglia di riempimento dell&apos;area sottostante</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="72"/>
        <source>Exposure</source>
        <translation>Esposizione</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="97"/>
        <source>Exposure Time Incr.</source>
        <translation>Incremento tempo di espo.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="124"/>
        <source>First Layer Expo.</source>
        <translation>Esposizione Primo l.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="41"/>
        <source>Print Settings</source>
        <translation>Impostazioni di Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="90"/>
        <location filename="../qml/PageChange.qml" line="118"/>
        <location filename="../qml/PageChange.qml" line="142"/>
        <source>s</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="194"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation>Scorri per una foto</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="207"/>
        <source>Connected to %1</source>
        <translation>Connesso a %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="196"/>
        <source>Connection to %1 failed.</source>
        <translation>Connessione a %1 non riuscita.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation>Rete Nascosta</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="139"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="147"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>La PSK deve essere lunga almeno 8 caratteri.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="122"/>
        <source>Security</source>
        <translation>Sicurezza</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="115"/>
        <source>Show Password</source>
        <translation>Mostra password</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="235"/>
        <source>Start AP</source>
        <translation>Avvia AP</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="185"/>
        <source>Working...</source>
        <translation>In funzione...</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation>Raffreddamento</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation>La temperatura è %1 C</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation>SURRISCALDAMENTO LED UV!</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation>Test Display</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="91"/>
        <source>All done, happy printing!</source>
        <translation>Tutto fatto, buona stampa!</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="77"/>
        <source>Close the cover.</source>
        <translation>Chiudi il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="32"/>
        <source>Display Test</source>
        <translation>Test Display</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Allenta la manopola nera e rimuovi la piattaforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Ti preghiamo di svitare e rimuovere il serbatoio della resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="51"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation>Questa procedura ti aiuterà ad assicurarti che il display di esposizione funzioni correttamente.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>Welcome to the display wizard.</source>
        <translation>Benvenuto nel wizard per il display.</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="78"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation>La configurazione corrente verrà ora cancellata.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="108"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation>Effettuato il downgrade. Nella fase successiva, la stampante verrà riavviata.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation>Downgrade Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="99"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Utilizzare solo la piattaforma fornita. L&apos;utilizzo di una piattaforma diversa può causare la fuoriuscita della resina e danni alla stampante!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="69"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Si prega di notare che il downgrade non è supportato. 

Il downgrade della stampante cancellerà la calibrazione UV e la stampante non funzionerà correttamente. 

Sarà necessario ricalibrarla utilizzando un calibratore UV esterno.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="48"/>
        <source>Proceed?</source>
        <translation>Procedere?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="59"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation>Rimontare i componenti della SL1S e accendere la stampante. Questo ripristinerà lo stato originale.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation>Componenti SL1 rilevati (downgrade da SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="79"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>La stampante richiederà la configurazione iniziale dopo il riavvio.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="58"/>
        <source>The printer will power off now.</source>
        <translation>La stampante si spegnerà adesso.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>Per completare la procedura di downgrade, la stampante deve cancellare la configurazione e riavviare.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="89"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation>Utilizzare solo il serbatoio della resina fornito. L&apos;utilizzo di un serbatoio resina diverso può causare la fuoriuscita della resina e danni alla stampante!</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Cleanup...</source>
        <translation>Pulizia...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Copying...</source>
        <translation>Copia in corso...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Done.</source>
        <translation>Completato.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Downloading ...</source>
        <translation>Scaricamento ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="55"/>
        <source>Downloading examples...</source>
        <translation>Scaricamento esempi...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation>Esempi</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="129"/>
        <source>Failure.</source>
        <translation>Errore.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Initializing...</source>
        <translation>Inizializzazione...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="132"/>
        <source>Unknown state.</source>
        <translation>Stato sconosciuto.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Unpacking...</source>
        <translation>Disimballaggio...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="141"/>
        <source>View Examples</source>
        <translation>Vedi Esempi</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="44"/>
        <source>Error code:</source>
        <translation>Codice errore:</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="48"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Per ulteriori informazioni, scansiona il codice QR o contatta il tuo rivenditore.</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="202"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>Configuring the connection,</source>
        <translation>Configuro la connessione,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="123"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="35"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="148"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="136"/>
        <source>IP Address</source>
        <translation>Indirizzo IP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="117"/>
        <source>Network Info</source>
        <translation>Informazioni di rete</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>please wait...</source>
        <translation>attendere prego...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="219"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Ripristina</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="61"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="68"/>
        <source>Error code:</source>
        <translation>Codice errore:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="72"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Per ulteriori informazioni, scansiona il codice QR o contatta il tuo rivenditore.</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="106"/>
        <source>Save Logs to USB</source>
        <translation>Salva log su USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="119"/>
        <source>Send Logs to Cloud</source>
        <translation>Invia i registri al Cloud</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="80"/>
        <source>Swipe to proceed</source>
        <translation>Scorri per continuare</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation>Errore di Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="140"/>
        <source>Turn Off</source>
        <translation>Spegni</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="133"/>
        <source>Update Firmware</source>
        <translation>Aggiorna Firmware</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="45"/>
        <source>Factory Reset done.</source>
        <translation>Ripristino delle impostazioni di fabbrica completato.</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="32"/>
        <source>Factory Reset</source>
        <translation>Reset di Fabbrica</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation>Completato</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation>Alimentami</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation>Se non vuoi riempire, premi il pulsante Indietro nella parte superiore dello schermo.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation>Ricarica manuale della resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation>Riempi il serbatoio fino al segno di 100% e premi Fatto.</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation>Prima di stampare, sono necessari i seguenti passaggi:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation>Calibrare?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation>Il sistema attuale sarà ancora disponibile tramite Impostazioni -&gt; Firmware -&gt; Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation>Vuoi davvero installare %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation>Vuoi iniziare adesso?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation>inserisci un&apos;unità USB o scarica esempi da Impostazioni -&gt; Supporto.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation>Installare?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation>Locale</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation>Locale</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation>
            <numerusform>%n elemento</numerusform>
            <numerusform>%n elementi</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="314"/>
        <source>No usable projects were found,</source>
        <translation>Non sono stati trovati progetti utilizzabili,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation>Stampe Precedenti</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation>Calibrazione Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation>Progetti</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation>Remoto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation>Remoto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation>Autotest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation>La stampante non è completamente calibrata.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation>Bundle di aggiornamento</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation>Aggiornamenti</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation>Calibrazione UV</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="127"/>
        <source>CANCELED</source>
        <translation>ANNULLATO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="207"/>
        <source>Consumed Resin</source>
        <translation>Resina utilizzata</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="126"/>
        <source>FAILED</source>
        <translation>NON RIUSCITO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation>Completato</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="125"/>
        <location filename="../qml/PageFinished.qml" line="128"/>
        <source>FINISHED</source>
        <translation>COMPLETATO</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="277"/>
        <source>First Layer Exposure</source>
        <translation>Esposizione Primo Layer</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="328"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="253"/>
        <source>Layer Exposure</source>
        <translation>Espos. Layer</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>Layer height</source>
        <translation>Altezza layer</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="183"/>
        <source>Layers</source>
        <translation>Layer</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="405"/>
        <source>Loading, please wait...</source>
        <translation>Caricamento, attendere...</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="156"/>
        <source>Print Time</source>
        <translation>Tempo di stampa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="348"/>
        <source>Reprint</source>
        <translation>Ristampa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="338"/>
        <source>Resin Tank Cleaning</source>
        <translation>Pulizia Serbatoio Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="261"/>
        <location filename="../qml/PageFinished.qml" line="285"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="300"/>
        <source>Swipe to continue</source>
        <translation>Scorri per continuare</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="368"/>
        <source>Turn Off</source>
        <translation>Spegni</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation>Immagine a schermo intero</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="51"/>
        <source>Control</source>
        <translation>Controllo</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="58"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="65"/>
        <source>Turn Off</source>
        <translation>Spegni</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="81"/>
        <source>Set</source>
        <translation>Imposta</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation>Imposta lingua</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation>Estrazione dati registro</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation>Esportazione dei registri</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation>Esportazione dei registri annullata</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation>Esportazione del log non riuscita. Per favore controlla la flash drive / connessione internet.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation>Esportazione dei registri terminata</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation>I log sono stati caricati con successo sul server Prusa.&lt;br /&gt;&lt;br /&gt;Si prega di contattare il supporto Prusa e di condividere con loro il seguente codice:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation>Caricamento registro completato</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation>Salvataggio dei dati su unità USB</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation>Caricamento dati sul server</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation>Manuale</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation>La scansione del codice QR caricherà il manuale per questo dispositivo.

In alternativa, usa questo link:</translation>
    </message>
</context>
<context>
    <name>PageModifyLayerProfile</name>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="144"/>
        <source>Delay After Exposure</source>
        <translation>Ritardo dopo l&apos;esposizione</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="130"/>
        <source>Delay Before Exposure</source>
        <translation>Ritardo prima dell&apos;esposizione</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="375"/>
        <source>Disabling the &apos;Use tilt&apos; causes the object to separate away from the film in the vertical direction only.

&apos;Tower hop height&apos; has been set to recommended value of 5 mm.</source>
        <translation>Disabilitando l&apos;opzione &apos;Usa inclinazione&apos;, l&apos;oggetto si separa dalla pellicola solo in direzione verticale.

&apos;Altezza salto torre&apos; è stata impostata sul valore consigliato di 5 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="29"/>
        <source>Edit Layer Profile</source>
        <translation>Modifica profilo layer</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="385"/>
        <source>Enabling the &apos;Use tilt&apos; causes the object to separate away from the film mainly by tilt.

&apos;Tower hop height&apos; has been set to 0 mm.</source>
        <translation>Abilitando l&apos;opzione &apos;Usa inclinazione&apos;, l&apos;oggetto si separa dalla pellicola principalmente per inclinazione.

&apos;Altezza salto torre&apos; è stata impostata su 0 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="160"/>
        <source>mm</source>
        <comment>millimeters</comment>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="174"/>
        <source>mm/s</source>
        <comment>millimeters per second</comment>
        <translation>mm/s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="131"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="145"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="240"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="279"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="319"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="358"/>
        <source>s</source>
        <comment>seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="266"/>
        <source>Tilt Down Cycles</source>
        <translation>Cicli di inclinazione verso il basso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="278"/>
        <source>Tilt Down Delay</source>
        <translation>Ritardo inclinazione verso il basso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="254"/>
        <source>Tilt Down Finish Speed</source>
        <translation>Velocità di completamento dell&apos;inclinazione verso il basso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="214"/>
        <source>Tilt Down Initial Speed</source>
        <translation>Velocità iniziale di inclinazione verso il basso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="226"/>
        <source>Tilt Down Offset</source>
        <translation>Ritardo di offset dell&apos;inclinazione verso il basso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="239"/>
        <source>Tilt Down Offset Delay</source>
        <translation>Ritardo di offset dell&apos;inclinazione verso il basso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="345"/>
        <source>Tilt Up Cycles</source>
        <translation>Cicli di inclinazione verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="357"/>
        <source>Tilt Up Delay</source>
        <translation>Ritardo dell&apos;inclinazione verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="333"/>
        <source>Tilt Up Finish Speed</source>
        <translation>Velocità di completamento dell&apos;inclinazione verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="293"/>
        <source>Tilt Up Initial Speed</source>
        <translation>Velocità iniziale di inclinazione verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="305"/>
        <source>Tilt Up Offset</source>
        <translation>Offset dell&apos;inclinazione verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="318"/>
        <source>Tilt Up Offset Delay</source>
        <translation>Ritardo offset dell&apos;inclinazione verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="159"/>
        <source>Tower Hop Height</source>
        <translation>Altezza del salto della torre</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="173"/>
        <source>Tower Speed</source>
        <translation>Velocità Torre</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="185"/>
        <source>Use Tilt</source>
        <translation>Utilizzare l&apos;inclinazione</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="374"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="384"/>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="227"/>
        <source>μ-step</source>
        <comment>tilt microsteps</comment>
        <translation>μ-step</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="306"/>
        <source>μ-step</source>
        <comment>tilt micro steps</comment>
        <translation>μ-step</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="216"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="256"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="295"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="335"/>
        <source>μ-step/s</source>
        <comment>microsteps per second</comment>
        <translation>μ-step/s</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation>Controllo</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation>Disabilita
Motori</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation>Home
Piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation>Home
Serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tank, please wait...</source>
        <translation>Homing del serbatoio, attendere...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tower, please wait...</source>
        <translation>Homing della torre, attendere...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="68"/>
        <source>Resin Tank Cleaning</source>
        <translation>Pulizia Serbatoio Resina</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation>Rete</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="32"/>
        <source>New Exposure Panel</source>
        <translation>Nuovo Pannello Esposizione</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="45"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation>È stata rilevata un nuovo schermo di esposizione.

La stampante chiederà di effettuare il setup iniziale (autotest e calibrazione) per assicurarsi che tutto funzioni correttamente.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation>Disponibile aggiornamento a %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation>Stampa annullata: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation>Stampa non riuscita: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation>Stampa terminata: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="55"/>
        <source>Insert protective foam</source>
        <translation>Inserisci schiuma protettiva</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="46"/>
        <source>Packing done.</source>
        <translation>Imballaggio completato.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="32"/>
        <source>Packing Wizard</source>
        <translation>Procedura guidata per l&apos;imballaggio</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="7"/>
        <source>Do you really want to turn off the printer?</source>
        <translation>Vuoi davvero spegnere la stampante?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="10"/>
        <source>Powering Off...</source>
        <translation>Spegnimento...</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Power Off?</source>
        <translation>Spegnere?</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="121"/>
        <source>Cover</source>
        <translation>Coperchio</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="125"/>
        <source>Delayed Start</source>
        <translation>Avvio ritardato</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="151"/>
        <source>Disabled</source>
        <translation>Disabilitato</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="140"/>
        <source>Do not touch the printer!</source>
        <translation>Non toccare la stampante!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="120"/>
        <source>Fan</source>
        <translation>Ventola</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation>Attendere</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Project</source>
        <translation>Progetto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="122"/>
        <source>Resin</source>
        <translation>Resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="123"/>
        <source>Starting Positions</source>
        <translation>Posizioni di partenza</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="124"/>
        <source>Stirring</source>
        <translation>Mescolando</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="150"/>
        <source>With Warning</source>
        <translation>Con preavviso</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Action Pending</source>
        <translation>Azione in sospeso</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="117"/>
        <source>Check Warning</source>
        <translation>Controllo avvisi</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="151"/>
        <source>Close Cover!</source>
        <translation>Chiudi il coperchio!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="113"/>
        <source>Continue?</source>
        <translation>Continuare?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Cover Open</source>
        <translation>Coperchio aperto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation>Stampante in preparazione per l’aggiunta della resina.&lt;br/&gt;Per favore attendere.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Going down</source>
        <translation>Abbassamento</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Going up</source>
        <translation>Sollevamento</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="130"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation>Se non vuoi continuare, premi il pulsante Indietro nella parte superiore dello schermo e il lavoro corrente sarà annullato.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Moving platform to the bottom position</source>
        <translation>Spostamento della piattaforma nella posizione inferiore</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Moving platform to the top position</source>
        <translation>Spostamento della piattaforma nella posizione superiore</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Moving the resin tank down...</source>
        <translation>Spostando il serbatoio resina verso il basso...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Paused.</source>
        <translation>In pausa.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Please close the cover to continue</source>
        <translation>Si prega di chiudere il coperchio per continuare</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="152"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation>Per favore, chiudi il coperchio! Le radiazioni UV sono dannose.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="220"/>
        <location filename="../qml/PagePrint.qml" line="277"/>
        <source>Please wait...</source>
        <translation>Attendere...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Project</source>
        <translation>Progetto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="243"/>
        <source>Reading data...</source>
        <translation>Lettura dei dati...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="129"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation>Rilasciare il meccanismo del serbatoio e premere Continua.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation>Le azioni richieste saranno eseguite non appena questo layer finirà, per favore attendi...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Setting start positions...</source>
        <translation>Impostazione delle posizioni di partenza...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring</source>
        <translation>Mescolando</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring resin</source>
        <translation>Mescolando la resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="127"/>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Stuck Recovery</source>
        <translation>Recupero dal blocco</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Tank Moving Down</source>
        <translation>Abbassamento Serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="128"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation>La stampante si è bloccata e necessita dell&apos;assistenza dell&apos;utente.</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="302"/>
        <source>Exposure Times</source>
        <translation>Tempi di Esposizione</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="348"/>
        <source>Last Modified</source>
        <translation>Ultima Modifica</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layer Height</source>
        <translation>Altezza Layer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layers</source>
        <translation>Layer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="447"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Riempi il serbatoio resina ad almeno %1 % e chiudi il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="493"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="483"/>
        <source>Print Settings</source>
        <translation>Impostazioni di Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="329"/>
        <source>Print Time Estimate</source>
        <translation>Tempo di Stampa Stimato</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation>Progetto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="374"/>
        <source>Swipe to continue</source>
        <translation>Scorri per continuare</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="166"/>
        <source>Swipe to project</source>
        <translation>Scorri al progetto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="444"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>Il progetto richiede&lt;br/&gt;%1 % di resina. Sarà necessario ricaricare la resina durante la stampa.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="208"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="357"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation>Sconosciuto</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="239"/>
        <source>Area fill:</source>
        <translation>Riempimento dell&apos;area:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="492"/>
        <location filename="../qml/PagePrintPrinting.qml" line="498"/>
        <source>Cancel Print</source>
        <translation>Annulla Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="431"/>
        <source>Consumed Resin</source>
        <translation>Resina utilizzata</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="516"/>
        <source>Enter Admin</source>
        <translation>Inserisci Admin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="373"/>
        <source>Estimated End</source>
        <translation>Fine Prevista</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="403"/>
        <source>Layer</source>
        <translation>Layer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="225"/>
        <source>Layer:</source>
        <translation>Layer:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Layer Height:</source>
        <translation>Altezza Layer:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="34"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="388"/>
        <source>Printing Time</source>
        <translation>Tempo di stampa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="473"/>
        <source>Print Settings</source>
        <translation>Impostazioni di stampa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="274"/>
        <source>Progress:</source>
        <translation>Progresso:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="479"/>
        <source>Refill Resin</source>
        <translation>Riempire la resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="417"/>
        <source>Remaining Resin</source>
        <translation>Resina residua</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="358"/>
        <source>Remaining Time</source>
        <translation>Tempo rimanente</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Today at</source>
        <translation>Oggi alle</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="499"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation>Per essere sicuri che la stampa non venga fermata accidentalmente, scorri sullo schermo per andare a prossimo passo, dove potrai fermare la stampa.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="110"/>
        <source>Tomorrow at</source>
        <translation>Domani alle</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="363"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Riempi il serbatoio resina ad almeno %1 % e chiudi il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation>Versa la resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>Il progetto richiede&lt;br/&gt;%1 % di resina. Sarà necessario ricaricare la resina durante la stampa.</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation>Codice QR della pagina</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation>Installa ora</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation>Più tardi</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation>Note di rilascio</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="102"/>
        <source>Can you hear the music?</source>
        <translation>Senti la musica?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="77"/>
        <location filename="../qml/PageSelftestWizard.qml" line="140"/>
        <location filename="../qml/PageSelftestWizard.qml" line="173"/>
        <source>Close the cover.</source>
        <translation>Chiudi il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Allenta la manopola nera e rimuovi la piattaforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="155"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation>Assicurati che il serbatoio della resina sia installato e che le viti siano serrate.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="164"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation>Installare la piattaforma con un angolo di 0° e serrarla con la manopola nera.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="122"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation>Si prega di installare la piattaforma con un angolo inferiore a 60° e di stringerla con la manopola nera.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="113"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation>Si prega di installare il serbatoio della resina e di stringere le viti in modo uniforme.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Ti preghiamo di svitare e rimuovere il serbatoio della resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="131"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation>Rilasciare la piattaforma dal cantilever e posizionarla sul centro del serbatoio della resina con un angolo di 90°.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation>Autotest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="51"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation>Questa procedura è obbligatoria e controllerà tutti i componenti della stampante.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="49"/>
        <source>Welcome to the selftest wizard.</source>
        <translation>Benvenuto nella procedura guidata di autotest.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="676"/>
        <source>Set</source>
        <translation>Imposta</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation>Imposta Data</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only a-z, 0-9 and  &quot;-&quot;. Must not begin or end with &quot;-&quot;.</source>
        <translation>Può contenere solo a-z, 0-9 e &quot;-&quot;. Non deve iniziare o finire con &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation>Nome Host</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation>Imposta</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaConnectRegistration</name>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="90"/>
        <source>Add Printer to Prusa Connect</source>
        <translation>Aggiungi la stampante a Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="79"/>
        <source>In Progress</source>
        <translation>In corso</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="78"/>
        <source>Not Registered</source>
        <translation>Non registrato</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="31"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="80"/>
        <source>Registered</source>
        <translation>Registrato</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="96"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="126"/>
        <source>Register Printer</source>
        <translation>Registra stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="75"/>
        <source>Registration Status</source>
        <translation>Stato della registrazione</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="123"/>
        <source>Review the Registration QR Code</source>
        <translation>Controlla il codice QR di registrazione</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="98"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="128"/>
        <source>Scan the QR Code or visit &lt;b&gt;prusa.io/add&lt;/b&gt;. Log into your Prusa Account to add this printer.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</source>
        <translation>Scansionare il codice QR o visitare il sito &lt;b&gt;prusa.io/add&lt;/b&gt;. Accedere al proprio Prusa Account per aggiungere la stampante.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Codice: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="81"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="111"/>
        <source>Unregister Printer from Prusa Connect</source>
        <translation>Cancella stampante da Prusa Connect</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaLinkWebLogin</name>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="131"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation>Può contenere solo caratteri a-z, A-Z, 0-9 e &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="117"/>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="125"/>
        <source>Must be at least 8 chars long</source>
        <translation>Deve essere lunga almeno 8 caratteri</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="92"/>
        <source>Printer Password</source>
        <translation>Password Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="32"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="144"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="55"/>
        <source>User Name</source>
        <translation>Nome Utente</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation>Minuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation>Imposta</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation>Imposta Ora</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="134"/>
        <source>City</source>
        <translation>Città</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="128"/>
        <source>Region</source>
        <translation>Regione</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="140"/>
        <source>Set</source>
        <translation>Imposta</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation>Imposta fuso orario</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="60"/>
        <source>Calibration</source>
        <translation>Calibrazione</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="150"/>
        <source>Enter Admin</source>
        <translation>Inserisci Admin</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="89"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="119"/>
        <source>Language &amp; Time</source>
        <translation>Lingua &amp; Ora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="69"/>
        <source>Network</source>
        <translation>Rete</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="79"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Piattaforma e serbatoio resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="29"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="99"/>
        <source>Settings &amp; Sensors</source>
        <translation>Impostazioni &amp; Sensori</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Support</source>
        <translation>Supporto</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>System Logs</source>
        <translation>Registri di sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="109"/>
        <source>Touchscreen</source>
        <translation>Touchscreen</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="32"/>
        <source>Calibration</source>
        <translation>Calibrazione</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="94"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation>Hai sostituito il DISPLAY DI ESPOSIZIONE?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="107"/>
        <source>Did you replace the UV LED SET?</source>
        <translation>Hai sostituito il SET LED UV?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="76"/>
        <source>Display Test</source>
        <translation>Test Display</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>New Display?</source>
        <translation>Nuovo Display?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>New UV LED SET?</source>
        <translation>Nuovo set LED UV?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="58"/>
        <source>Printer Calibration</source>
        <translation>Calibrazione Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="51"/>
        <source>Selftest</source>
        <translation>Autotest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="68"/>
        <source>UV Calibration</source>
        <translation>Calibrazione UV</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation>Dopo aver installato questo aggiornamento, la stampante può essere aggiornata a un altro FW ma &lt;b&gt;la stampa non funzionerà.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="414"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;non è compatibile con l&apos;attuale modello di hardware - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation>Controlla aggiornamenti</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation>Verifica aggiornamento non riuscito</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation>Verifica aggiornamenti...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="215"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>Continue anyway?</source>
        <translation>Continua comunque?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Downgrade?</source>
        <translation>Avviare il Downgrade?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="347"/>
        <source>Downgrade Firmware?</source>
        <translation>Effettuare il Downgrade del Firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation>Scarica ora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="182"/>
        <source>Do you really want to downgrade to FW</source>
        <translation>Vuoi davvero fare il downgrade al FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation>Vuoi davvero eseguire il ripristino dei dati di fabbrica?

Tutte le impostazioni verranno cancellate!
I progetti rimarranno intatti.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>Do you want to continue?</source>
        <translation>Vuoi continuare?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="248"/>
        <source>Factory Reset</source>
        <translation>Reset di Fabbrica</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="36"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="334"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation>Meta-informazioni del file FW non trovate. &lt;br/&gt;&lt;br/&gt;Vuoi installare&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;comunque?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="290"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation>Informazioni FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="213"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation>Se si scambia, è possibile aggiornare successivamente ad un&apos;altra versione FW ma &lt;b&gt;non sarà possibile stampare.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="413"/>
        <source>Incompatible FW!</source>
        <translation>FW non compatibile!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation>Versione installata</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="333"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="382"/>
        <source>Install Firmware?</source>
        <translation>Installare il firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation>Installa ora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>è inferiore o uguale alla corrente&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="291"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation>Caricamento delle metainformazioni del file FW
(potrebbe richiedere fino a 20 secondi) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="239"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Più recente di SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="235"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="425"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Nessuno</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="138"/>
        <source>Receive Beta Updates</source>
        <translation>Ricevi Aggiornamenti Beta</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Switch to beta?</source>
        <translation>Passare a beta?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="169"/>
        <source>Switch to version:</source>
        <translation>Passa alla versione:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation>Il sistema è aggiornato</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="211"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>La versione firmware alternativa &lt;b&gt;%1&lt;/b&gt; non è compatibile con il modello della stampante in uso: &lt;b&gt;%2&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="428"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation>Aggiornamento disponibile</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation>Download aggiornamento non riuscito</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation>L&apos;aggiornamento è in fase di download</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation>L&apos;aggiornamento è in fase di download</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation>Servizio di aggiornamento inattivo</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="350"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation>Versione del file FW selezionato&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="157"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation>Attenzione! Gli aggiornamenti beta possono essere instabili.&lt;br/&gt;&lt;br/&gt;Non consigliato per stampanti di produzione.&lt;br/&gt;&lt;br/&gt;Continuare?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>che ha la versione &lt;b&gt;%1&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="383"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation>Hai selezionato il pacchetto di aggiornamento &lt;b&gt;&quot;%1&quot;&lt;/b&gt; ,</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="29"/>
        <source>Language &amp; Time</source>
        <translation>Lingua &amp; Ora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="35"/>
        <source>Set Language</source>
        <translation>Imposta lingua</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="41"/>
        <source>Time Settings</source>
        <translation>Impostazioni Ora</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="37"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="74"/>
        <source>Hostname</source>
        <translation>Nome Host</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="29"/>
        <source>Network</source>
        <translation>Rete</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="89"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="81"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="53"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="109"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="48"/>
        <source>Disable Steppers</source>
        <translation>Disabilita Motori</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="102"/>
        <source>Limit for Fast Tilt</source>
        <translation>Limite per inclinazione veloce</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="34"/>
        <source>Move Platform</source>
        <translation>Sposta Piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="41"/>
        <source>Move Resin Tank</source>
        <translation>Sposta Serbatoio Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="56"/>
        <source>Platform Axis Sensitivity</source>
        <translation>Sensibilità Asse Piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="128"/>
        <source>Platform Offset</source>
        <translation>Offset Piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="28"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Piattaforma e serbatoio resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="159"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="79"/>
        <source>Tank Axis Sensitivity</source>
        <translation>Sensibilità Asse Serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="152"/>
        <source>Tank Surface Cleaning Exposure</source>
        <translation>Esposizione Pulizia della Superficie del Serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="135"/>
        <source>um</source>
        <translation>um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="112"/>
        <location filename="../qml/PageSettingsSensors.qml" line="148"/>
        <source>Are you sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="61"/>
        <source>Auto Power Off</source>
        <translation>Spegnimento Automatico</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="87"/>
        <source>Cover Check</source>
        <translation>Controllo Coperchio</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="36"/>
        <source>Device hash in QR</source>
        <translation>Hash del dispositivo in QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="113"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation>Disattivare il sensore del coperchio?&lt;br/&gt;&lt;br/&gt;ATTENZIONE: Questo può portare all&apos;esposizione indesiderata ai raggi UV o a lesioni personali a causa delle parti in movimento. Questa azione non è consigliata!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="149"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation>Disattivare il sensore di resina?&lt;br/&gt;&lt;br/&gt;ATTENZIONE: Questo può portare a stampe non riuscite o al trabocco del serbatoio di resina! Questa azione non è consigliata!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="180"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="161"/>
        <source>Rear Fan Speed</source>
        <translation>Velocità Ventola Posteriore</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="125"/>
        <source>Resin Sensor</source>
        <translation>Sensore Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="168"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="28"/>
        <source>Settings &amp; Sensors</source>
        <translation>Impostazioni &amp; Sensori</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="78"/>
        <source>About Us</source>
        <translation>Chi siamo</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="36"/>
        <source>Download Examples</source>
        <translation>Scarica Esempi</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="50"/>
        <source>Manual</source>
        <translation>Manuale</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="30"/>
        <source>Support</source>
        <translation>Supporto</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="69"/>
        <source>System Information</source>
        <translation>Informazioni di Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="60"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Nessun registro è stato ancora caricato.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>Last Seen Logs:</source>
        <translation>Ultimi Registri Visti:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="55"/>
        <source>Save to USB Drive</source>
        <translation>Salva sul dispositivo USB</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="29"/>
        <source>System Logs</source>
        <translation>Registri di Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="65"/>
        <source>Upload to Server</source>
        <translation>Carica sul server</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="98"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="63"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="58"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="36"/>
        <source>Screensaver timer</source>
        <translation>Timer screensaver</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="80"/>
        <source>Touch Screen Brightness</source>
        <translation>Luminosità Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="28"/>
        <source>Touchscreen</source>
        <translation>Touchscreen</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="81"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation>Token Temporaneo</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation>Nome Pacchetto</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation>Pacchetti software</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation>Controller A64 SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>Ambient Temperature</source>
        <translation>Temperatura ambiente</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <source>Blower Fan</source>
        <translation>Ventilatore</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation>Scheda Booster SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <location filename="../qml/PageSysinfo.qml" line="192"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Closed</source>
        <translation>Chiuso</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>Cover State</source>
        <translation>Stato Coperchio</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>CPU Temperature</source>
        <translation>Temperatura CPU</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation>Indirizzo IP Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation>Display di esposizione SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="199"/>
        <location filename="../qml/PageSysinfo.qml" line="209"/>
        <location filename="../qml/PageSysinfo.qml" line="219"/>
        <source>Fan Error!</source>
        <translation>Errore Ventola!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Finished Projects</source>
        <translation>Progetti Completati</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation>Versione GUI</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation>Stato Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation>Revisione HW del controller di movimento</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation>Controller di movimento SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation>Versione SW Controller di movimento</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation>Stato Rete</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Più recente di SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Nessuno</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Not triggered</source>
        <translation>Non attivato</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation>Versione OS Image</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation>Altre Componenti</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="240"/>
        <source>Power Supply Voltage</source>
        <translation>Voltaggio Alimentatore</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Print Display Time Counter</source>
        <translation>Contatore Display di Stampa</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation>Modello Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>Printer Password</source>
        <translation>Password Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>Rear Fan</source>
        <translation>Ventola Posteriore</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Resin Sensor State</source>
        <translation>Stato Sensore Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="298"/>
        <source>Show software packages &amp; licenses</source>
        <translation>Mostra pacchetti software e licenze</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Started Projects</source>
        <translation>Progetti Iniziati</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>Statistics</source>
        <translation>Statistiche</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation>Informazioni di Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation>Tempo dell&apos;inclinazione veloce</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Time of High Viscosity Tilt:</source>
        <translation>Tempo per Inclinazione Alta Viscosità:</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation>Tempo dell&apos;inclinazione lenta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Layers Printed</source>
        <translation>Totale Layer Stampati</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Print Time</source>
        <translation>Tempo di Stampa Totale</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="287"/>
        <source>Total Resin Consumed</source>
        <translation>Consumo Resina Totale</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Triggered</source>
        <translation>Attivato</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="226"/>
        <source>UV LED</source>
        <translation>LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <source>UV LED Fan</source>
        <translation>Ventola LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>UV LED Temperature</source>
        <translation>Temperatura LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>UV LED Time Counter</source>
        <translation>Contatore Tempo LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="241"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation>Indirizzo IP Wifi</translation>
    </message>
</context>
<context>
    <name>PageTankSurfaceCleanerWizard</name>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="160"/>
        <source>Cleaning Adaptor</source>
        <translation>Adattatore di Pulizia</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="149"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="134"/>
        <source>Exposure[s]:</source>
        <translation>Esposizione[s]:</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="81"/>
        <source>Here you can optionally adjust the exposure settings before cleaning starts.</source>
        <translation>Qui è possibile regolare in modo opzionale le impostazioni di esposizione prima dell&apos;inizio della pulizia.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="89"/>
        <source>Less</source>
        <translation>Meno</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="108"/>
        <source>More</source>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="172"/>
        <source>Now remove the cleaning adaptor along with the exposed film, careful not to tear it.</source>
        <translation>Ora, rimuovi l&apos;adattatore di pulizia insieme allo strato esposto, facendo attenzione a non strapparlo.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="161"/>
        <source>Please insert the cleaning adaptor as is ilustrated in the picture on the left.</source>
        <translation>Si prega di inserire l&apos;adattatore di pulizia come illustrato nell&apos;immagine a sinistra.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="171"/>
        <source>Remove Adaptor</source>
        <translation>Rimuovere l&apos;Adattatore</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="33"/>
        <source>Resin Tank Cleaning</source>
        <translation>Pulizia Serbatoio Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="61"/>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="69"/>
        <source>Tank Cleaning</source>
        <translation>Pulizia Serbatoio</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="62"/>
        <source>This wizard will help you clean your resin tank and remove resin debris.

You will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine&apos;s internal storage. Or you can download it at printables.com</source>
        <translation>Questa procedura guidata ti aiuterà a pulire il serbatoio della resina e a rimuovere i residui di resina.

È necessario uno strumento speciale - un adattatore di pulizia. Puoi stamparlo tu stesso - è incluso come esempio di stampa nella memoria interna di questa macchina. Oppure puoi scaricarlo da printables.com</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation>Abbassa</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation>Abbassa Veloce</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation>Solleva Veloce</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation>Sposta Serbatoio Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation>Solleva</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="139"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation>12-ore</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="140"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation>24-ore</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="163"/>
        <source>Automatic time settings using NTP ...</source>
        <translation>Impostazione ora automatica tramite NTP ...</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="94"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="138"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation>Nativo</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="73"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="133"/>
        <source>Time Format</source>
        <translation>Formato Ora</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="35"/>
        <source>Time Settings</source>
        <translation>Impostazioni Ora</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="115"/>
        <source>Timezone</source>
        <translation>Fuso orario</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="47"/>
        <source>Use NTP</source>
        <translation>Impostazione ora automatica</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation>Abbassa</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation>Abbassa Veloce</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation>Solleva Veloce</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation>Sposta Piattaforma</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation>Solleva</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="79"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Staccare con cura l&apos;adesivo protettivo dal display dell&apos;esposizione.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="69"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation>Rimuovi l&apos;adesivo di sicurezza e apri il coperchio.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="59"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation>Rimuovi la spugna nera da entrambi i lati della piattaforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Disimballaggio</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="89"/>
        <source>Unpacking done.</source>
        <translation>Disimballaggio completo.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="49"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation>Svita e rimuovi la tanica della resina e rimuovi la schiuma nera.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="46"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Staccare con cura l&apos;adesivo protettivo dal display dell&apos;esposizione.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Disimballaggio</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="56"/>
        <source>Unpacking done.</source>
        <translation>Disimballaggio completo.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="419"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation>Si è verificato un problema durante l&apos;aggiornamento, per favore facci sapere cosa è successo (inviaci i log). Non aver paura, la tua stampante funziona ancora come prima. Premi &quot;Indietro&quot; per continuare</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="296"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation>Non spegnere la stampante durante l&apos;aggiornamento!&lt;br/&gt;La stampante verrà riavviata dopo l&apos;aggiornamento corretto.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="229"/>
        <source>Download failed.</source>
        <translation>Scaricamento non riuscito.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation>Scaricamento</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation>Scaricamento firmware, l&apos;installazione inizierà subito dopo.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="255"/>
        <source>Installing Firmware</source>
        <translation>Installazione Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="360"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation>La stampante viene riavviata con il nuovo firmware(%1), attendere prego</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation>Aggiornamento Stampante</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="343"/>
        <source>Updated to %1 failed.</source>
        <translation>Aggiornamento a %1 non riuscito.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="386"/>
        <source>Update to %1 failed.</source>
        <translation>Caricamento su %1 non riuscito.</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="32"/>
        <source>Hardware Upgrade</source>
        <translation>Aggiornamento hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="90"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Utilizzare solo la piattaforma fornita. L&apos;utilizzo di una piattaforma diversa può causare la fuoriuscita della resina e danni alla stampante!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="100"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Si prega di notare che il downgrade non è supportato. 

Il downgrade della stampante cancellerà la calibrazione UV e la stampante non funzionerà correttamente. 

Sarà necessario ricalibrarla utilizzando un calibratore UV esterno.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="49"/>
        <source>Proceed?</source>
        <translation>Procedere?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="60"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation>Rimontare i componenti della SL1 e accendere la stampante. Questo ripristinerà lo stato originale.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation>Componenti SL1S rilevati (aggiornamento da SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="69"/>
        <source>The configuration is going to be cleared now.</source>
        <translation>La configurazione verrà ora cancellata.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="70"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>La stampante richiederà la configurazione iniziale dopo il riavvio.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="59"/>
        <source>The printer will power off now.</source>
        <translation>La stampante si spegnerà adesso.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="48"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>Per completare la procedura di upgrade, la stampante deve cancellare la configurazione e riavviare.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="109"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation>Aggiornamento completato. Nel prossimo passo, la stampante si riavvierà.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="80"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation>Usa solo il serbatoio resina in plastica in dotazione. L&apos;uso del vecchio serbatoio resina in metallo può causare la fuoriuscita di resina e danneggiare la stampante!</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation>1. Se il serbatoio della resina è nella stampante, rimuovilo insieme alle viti.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="71"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation>1. Posizionare il calibratore UV sul display di stampa e collegarlo all&apos;USB anteriore.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="52"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="73"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation>2. Chiudere il coperchio, non aprirlo! I raggi UV possono essere dannosi!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Intensity: center %1, edge %2</source>
        <translation>Intensità: centro %1, bordo %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="84"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation>Apri il coperchio, &lt;br&gt;rimuovi e scollega&lt;/b&gt; il calibratore UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="107"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation>La stampante è stata calibrata correttamente!
Vuoi applicare i risultati della calibrazione?</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="100"/>
        <source>The result of calibration:</source>
        <translation>Risultato della calibrazione:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="32"/>
        <source>UV Calibration</source>
        <translation>Calibrazione UV</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation>Intensità UV: %1, σ = %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="103"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation>Intensità UV min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="101"/>
        <source>UV PWM: %1</source>
        <translation>UV PWM: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="57"/>
        <source>Warm-up: %1 s</source>
        <translation>Riscaldamento: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>Welcome to the UV calibration.</source>
        <translation>Benvenuto nella calibrazione UV.</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation>Caricamento, attendere...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation>La scansione del codice QR porterà alla nostra playlist di YouTube con video su questo dispositivo.
In alternativa, utilizzare questo link:</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation>Attendere</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="193"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="203"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation>Configurando la connessione, attendere...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation>Connesso</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation>Disconnesso</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="233"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Vuoi veramente eliminare le impostazioni di questa rete?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="232"/>
        <source>Forget network?</source>
        <translation>Dimenticare rete?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="223"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation>Dimentica rete</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation>Informazioni rete</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="212"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Ripristinare</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation>Impostazioni Wireless</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="71"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="59"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="115"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation>Scorri per continuare</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="102"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="265"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="174"/>
        <source>Swipe for a picture</source>
        <translation>Scorri per una foto</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="253"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation>Collegato</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation>Scollegato</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="113"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="172"/>
        <source>On</source>
        <translation>On</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation>Attendere...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation>Scorri per confermare</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation>Non può essere vuoto, qui sono permessi solo 0-9, a-z, A-Z, _ e -!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="115"/>
        <source>back</source>
        <translation>indietro</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="130"/>
        <source>cancel</source>
        <translation>annulla</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="144"/>
        <source>close</source>
        <translation>chiudi</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation>Attivazione</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>ambient temperature</source>
        <translation>temperatura ambiente</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>blower fan</source>
        <translation>Ventilatore radiale</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="650"/>
        <source>Cancel?</source>
        <translation>Annulla?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="665"/>
        <source>Cancel the current print job?</source>
        <translation>Annullare il lavoro di stampa corrente?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation>Connesso</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>CPU temperature</source>
        <translation>Temperatura CPU</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation>Disattivato</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation>Disattivazione</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="699"/>
        <location filename="../qml/main.qml" line="723"/>
        <source>DEPRECATED PROJECTS</source>
        <translation>PROGETTI OBSOLETI</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="407"/>
        <source>Initializing...</source>
        <translation>Inizializzazione...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="417"/>
        <source>MC Update</source>
        <translation>Aggiornamento MC</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="310"/>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="356"/>
        <source>Now it&apos;s safe to remove USB drive</source>
        <translation>Ora è sicuro rimuovere l&apos;unità USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>Press &quot;cancel&quot; to abort.</source>
        <translation>Premere &quot;annulla&quot; per interrompere.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="688"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation>La stampante non può essere spenta in questo stato. Termina o annulla l&apos;azione corrente e riprova.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation>Interfaccia utente touchscreen Prusa SL1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>rear fan</source>
        <translation>ventilatore posteriore</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="355"/>
        <source>Remove USB</source>
        <translation>Rimuovi l&apos;USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="339"/>
        <source>Remove USB flash drive?</source>
        <translation>Rimuovere l&apos;unità USB?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="714"/>
        <location filename="../qml/main.qml" line="737"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="724"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation>Sono stati trovati alcuni progetti incompatibili, puoi scaricarli su

http://%1/old-projects.

Vuoi rimuoverli?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="700"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation>Trovati progetti non compatibili, puoi scaricarli da

http://%1/old-projects

Vuoi rimuoverli?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="418"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation>Il firmware del Motion Controller è in fase di aggiornamento.

Si prega di attendere...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="408"/>
        <source>The printer is initializing, please wait ...</source>
        <translation>La stampante è in fase di inizializzazione, attendere...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>The USB drive cannot be safely removed because it is now in use, please wait.</source>
        <translation>L&apos;unità USB non può essere rimossa in modo sicuro perché è in uso, attendere.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="637"/>
        <source>Turn Off?</source>
        <translation>Spegnere?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>unknown device</source>
        <translation>dispositivo sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation>Sconosciuto</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>UV LED fan</source>
        <translation>Ventola LED UV</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="101"/>
        <source>UV LED temperature</source>
        <translation>Temperatura LED UV</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation>Meno di un minuto</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n ora</numerusform>
            <numerusform>%n ore</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minuti</numerusform>
        </translation>
    </message>
</context>
</TS>
