<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en-US">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="90"/>
        <source>Add Hidden Network</source>
        <translation>Add Hidden Network</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Plugged in</source>
        <translation>Plugged in</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Unplugged</source>
        <translation>Unplugged</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="37"/>
        <source>Network Info</source>
        <translation>Network info</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="38"/>
        <source>Wi-Fi Client</source>
        <translation>Wi-Fi Client</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Do you really want to forget this network&apos;s settings?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="105"/>
        <source>Forget network?</source>
        <translation>Forget network?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="35"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="136"/>
        <source>Apply calibration results</source>
        <translation>Apply calibration results</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="134"/>
        <source>Calibrate center</source>
        <translation>Calibrate center</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="135"/>
        <source>Calibrate edge</source>
        <translation>Calibrate edge</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="174"/>
        <source>Canceled</source>
        <translation>Canceled</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="156"/>
        <source>Check ID:</source>
        <translation>Check ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="131"/>
        <source>Check for UV calibrator</source>
        <translation>Check for UV calibrator</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Clear UV calibration data</source>
        <translation>Clear UV calibration data</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation>Clear downloaded Slicer profiles</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Disable factory mode</source>
        <translation>Disable factory mode</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="130"/>
        <source>Disable ssh, serial</source>
        <translation>Disable ssh, serial</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Display test</source>
        <translation>Display test</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="140"/>
        <source>Erase UV PWM settings</source>
        <translation>Erase UV PWM settings</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Erase motion controller EEPROM</source>
        <translation>Erase motion controller EEPROM</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Erase projects</source>
        <translation>Erase projects</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="150"/>
        <source>Exposing debris</source>
        <translation>Exposing debris</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="171"/>
        <source>Failure</source>
        <translation>Failure</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Make tank accessible</source>
        <translation>Make tank accessible</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="148"/>
        <source>Moving platform down to safe distance</source>
        <translation>Moving platform down to safe distance</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="151"/>
        <source>Moving platform gently up</source>
        <translation>Moving platform gently up</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Moving printer to accept protective foam</source>
        <translation>Moving printer to accept the protective foam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Obtain calibration info</source>
        <translation>Obtain calibration info</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Obtain system info</source>
        <translation>Obtain system info</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="170"/>
        <source>Passed</source>
        <translation>Passed</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Platform calibration</source>
        <translation>Platform calibration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="153"/>
        <source>Platform home</source>
        <translation>Platform home</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Platform range</source>
        <translation>Platform range</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Pressing protective foam</source>
        <translation>Pressing protective foam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Printer calibration</source>
        <translation>Printer calibration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>Recording changes</source>
        <translation>Recording changes</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Release foam</source>
        <translation>Release foam</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Reset NTP state</source>
        <translation>Reset NTP state</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Reset Network settings</source>
        <translation>Reset Network settings</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Reset Prusa Connect</source>
        <translation>Reset Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>Reset PrusaLink</source>
        <translation>Reset PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="139"/>
        <source>Reset UI settings</source>
        <translation>Reset UI settings</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Reset homing profiles</source>
        <translation>Reset homing profiles</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>Reset hostname</source>
        <translation>Reset hostname</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset print configuration</source>
        <translation>Reset print configuration</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Reset printer calibration status</source>
        <translation>Reset printer calibration status</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Reset selftest status</source>
        <translation>Reset selftest status</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Reset system locale</source>
        <translation>Reset system locale</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Reset timezone</source>
        <translation>Reset timezone</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="154"/>
        <source>Reset update channel</source>
        <translation>Reset update channel</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Resetting hardware counters</source>
        <translation>Resetting hardware counters</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Resin sensor</source>
        <translation>Resin sensor</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="169"/>
        <source>Running</source>
        <translation>Running</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Send printer data to MQTT</source>
        <translation>Send printer data to MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Serial number</source>
        <translation>Serial number</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Set new printer model</source>
        <translation>Set new printer model</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Sound test</source>
        <translation>Sound test</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Tank calib. start</source>
        <translation>Tank calib. start</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Tank home</source>
        <translation>Tank home</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="138"/>
        <source>Tank level</source>
        <translation>Tank level</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Tank range</source>
        <translation>Tank range</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Tilt timming</source>
        <translation>Tilt timing</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="149"/>
        <source>Touching platform down</source>
        <translation>Touching platform down</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>UV LED</source>
        <translation>UV LED</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>UV LED and fans</source>
        <translation>UV LED and fans</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="132"/>
        <source>UV LED warmup</source>
        <translation>UV LED warmup</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="133"/>
        <source>UV calibrator placed</source>
        <translation>UV calibrator placed</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="173"/>
        <source>User action pending</source>
        <translation>User action pending</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="168"/>
        <source>Waiting</source>
        <translation>Waiting</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="137"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation>Waiting for UV calibrator to be removed</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="152"/>
        <source>Waiting for user</source>
        <translation>Waiting for user</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="172"/>
        <source>With Warning</source>
        <translation>With Warning</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="56"/>
        <source>Understood</source>
        <translation>Understood</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation>Unknown error</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>A part of the LED panel is disconnected.</source>
        <translation>A part of the LED panel is disconnected.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>A64 OVERHEAT</source>
        <translation>A64 OVERHEAT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation>ADMIN NOT AVAILABLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="234"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation>AMBIENT TEMP. TOO HIGH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="235"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation>AMBIENT TEMP. TOO LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>An unexpected error has occurred.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation>An unexpected error has occurred.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>Analysis of the project failed</source>
        <translation>Analysis of the project failed</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation>BOOSTER BOARD PROBLEM</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="231"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation>BOOTED SLOT CHANGED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>Broken UV LED panel</source>
        <translation>Broken UV LED panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>CALIBRATION ERROR</source>
        <translation>CALIBRATION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>CALIBRATION FAILED</source>
        <translation>CALIBRATION FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation>CALIBRATION LOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation>CALIBRATION PROJECT IS INVALID</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="236"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation>CAN&apos;T COPY PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>CANNOT READ PROJECT</source>
        <translation>CANNOT READ PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation>CANNOT REMOVE PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>CLEANING ADAPTOR MISSING</source>
        <translation>CLEANING ADAPTOR MISSING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation>CONFIG FILE READ ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>CONNECTION FAILED</source>
        <translation>CONNECTION FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>Calibration project is invalid</source>
        <translation>Calibration project is invalid</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation>Cannot connect to the UV LED calibrator. Check the connection and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Cannot export profile</source>
        <translation>Cannot export profile</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Cannot find the selected file!</source>
        <translation>Cannot find the selected file!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation>Cannot get the update channel. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>Cannot import profile</source>
        <translation>Cannot import profile</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation>Cannot set the update channel. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>Cleaning adaptor was not detected, it does not seem to be correctly attached to the print platform.
Attach it properly and try again.</source>
        <translation>Cleaning adaptor was not detected, it does not seem to be correctly attached to the print platform.
Attach it properly and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Communication with the Booster board failed.</source>
        <translation>Communication with the Booster board failed.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation>Communication with the UV LED calibrator has failed. Check the connection and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation>Connection to Prusa servers failed, please try again later.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation>DATA OVERWRITE FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation>DATA PREPARATION FAILURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation>DIRECTORY NOT EMPTY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>DISPLAY TEST ERROR</source>
        <translation>DISPLAY TEST ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>DISPLAY TEST FAILED</source>
        <translation>DISPLAY TEST FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>DOWNLOAD FAILED</source>
        <translation>DOWNLOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation>Data is from unknown UV LED sensor!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>Disconnected UV LED panel</source>
        <translation>Disconnected UV LED panel</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Display test failed.</source>
        <translation>Display test failed.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>Display usage error</source>
        <translation>Display usage error</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="246"/>
        <source>EXPECT OVERHEATING</source>
        <translation>EXPECT OVERHEATING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Error displaying test image.</source>
        <translation>Error displaying test image.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>Error, there is no file to reprint.</source>
        <translation>Error, there is no file to reprint.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation>The exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>FAN FAILURE</source>
        <translation>FAN FAILURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation>FAN RPM OUT OF TEST RANGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="245"/>
        <source>FAN WARNING</source>
        <translation>FAN WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>FILE ALREADY EXISTS</source>
        <translation>FILE ALREADY EXISTS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FILE NOT FOUND</source>
        <translation>FILE NOT FOUND</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="247"/>
        <source>FILL THE RESIN</source>
        <translation>FILL THE RESIN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation>FIRMWARE UPDATE FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation>Failed to change the log level (detail). Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation>Failed to load fans and LEDs factory calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Failed to reach the tilt endstop.</source>
        <translation>Failed to reach the tilt endstop.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tower endstop.</source>
        <translation>Failed to reach the tower endstop.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation>Failed to save Wizard data. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation>Failed to serialize Wizard data. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Failed to set hostname</source>
        <translation>Failed to set hostname</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation>File already exists! Delete it in the printer first and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation>File has an invalid extension! See the article for supported file extensions.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>HOSTNAME ERROR</source>
        <translation>HOSTNAME ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="237"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation>INCORRECT PRINTER MODEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>INTERNAL ERROR</source>
        <translation>INTERNAL ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation>INTERNAL MEMORY FULL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>INVALID API KEY</source>
        <translation>INVALID API KEY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>INVALID FILE EXTENSION</source>
        <translation>INVALID FILE EXTENSION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INVALID PASSWORD</source>
        <translation>INVALID PASSWORD</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>INVALID PROJECT</source>
        <translation>INVALID PROJECT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation>INVALID TILT ALIGN POSITION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>Image preloader did not finish successfully!</source>
        <translation>Image preloader did not finish successfully!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan.</source>
        <translation>Incorrect RPM reading of the %(failed_fans_text)s fan.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation>Incorrect RPM reading of the %(failed_fans_text)s fan. The print may continue, however, there&apos;s a risk of overheating.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>Incorrect RPM reading of the %(fan__map_HardwareDeviceId)s.</source>
        <translation>Incorrect RPM reading of the %(fan__map_HardwareDeviceId)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation>Internal memory is full. Delete some of your projects first.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>Invalid tilt alignment position.</source>
        <translation>Invalid tilt alignment position.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>LANGUAGE NOT SET</source>
        <translation>LANGUAGE NOT SET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="241"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation>MASK NOAVAIL WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>MC WRONG REVISION</source>
        <translation>MC WRONG REVISION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation>MQTT UPLOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation>Measured resin volume is too low. The print can continue, however, a refill might be required.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <source>Moving the tilt failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Moving the tilt failed. Make sure there is no obstacle in its path and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>NO FILE TO REPRINT</source>
        <translation>NO FILE TO REPRINT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>NO INTERNET CONNECTION</source>
        <translation>NO INTERNET CONNECTION</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation>NO UV CALIBRATION DATA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="232"/>
        <source>NO WARNING</source>
        <translation>NO WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="238"/>
        <source>NOT ENOUGH RESIN</source>
        <translation>NOT ENOUGH RESIN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation>NOT ENOUGH LAYERS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No USB storage present</source>
        <translation>No USB storage present</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>No calibration data to show!</source>
        <translation>No calibration data to show!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>No display usage data to show</source>
        <translation>No display usage data to show</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation>No problem detected. You can continue using the printer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="242"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation>OBJECT CROPPED WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>OLD EXPO PANEL</source>
        <translation>OLD EXPO PANEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>OPENING PROJECT FAILED</source>
        <translation>OPENING PROJECT FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation>Object was cropped because it does not fit the print area.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="239"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation>PARAMETERS OUT OF RANGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="240"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation>PERPARTES NOAVAIL WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>PRELOAD FAILED</source>
        <translation>PRELOAD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation>PRINT EXAMPLES MISSING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>PRINT JOB CANCELLED</source>
        <translation>PRINT JOB CANCELLED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>PRINTER IS BUSY</source>
        <translation>PRINTER IS BUSY</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>PRINTER IS OK</source>
        <translation>PRINTER IS OK</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation>PRINTER NOT UV CALIBRATED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="243"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation>PRINTER VARIANT MISMATCH WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation>PROFILE EXPORT ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation>PROFILE IMPORT ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation>PROJECT ANALYSIS FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation>PROJECT IS CORRUPTED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="118"/>
        <source>Per-partes print not available.</source>
        <translation>Per-partes print not available.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>Pour enough resin for the selected file into the tank and close the lid. The minimal amount of the resin is displayed on the touchscreen.</source>
        <translation>Pour enough resin for the selected file into the tank and close the lid. The minimal amount of the resin is displayed on the touchscreen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>Print mask is missing.</source>
        <translation>Print mask is missing.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>REMOTE API ERROR</source>
        <translation>REMOTE API ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="244"/>
        <source>RESIN LOW</source>
        <translation>RESIN LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>RESIN MEASURING FAILED</source>
        <translation>RESIN MEASURING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>RESIN SENSOR ERROR</source>
        <translation>RESIN SENSOR ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN TOO HIGH</source>
        <translation>RESIN LEVEL TOO HIGH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>RESIN TOO LOW</source>
        <translation>RESIN LEVEL TOO LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>RPM of %(fan)s not in range!
RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation>RPM of %(fan)s not in range!
RPM data: %(rpm)s
Average: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>RPM of %(fan__map_HardwareDeviceId)s not in range!

RPM data: %(min_rpm)d - %(max_rpm)d
Average: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</source>
        <translation>RPM of %(fan__map_HardwareDeviceId)s not in range!

RPM data: %(min_rpm)d - %(max_rpm)d
Average: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Reading of %(sensor__map_HardwareDeviceId)s not in range!

Measured temperature: %(temperature).1f °C, allowed range: %(min)d - %(max)d °C.

Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>Reading of %(sensor__map_HardwareDeviceId)s not in range!

Measured temperature: %(temperature).1f °C, allowed range: %(min)d - %(max)d °C.

Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation>Removing this project is not possible. The project is locked by a print job.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation>Requested intensity cannot be reached by max. allowed PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation>Requested intensity cannot be reached with min. allowed PWM.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation>SERIAL NUMBER ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation>SETTING LOG DETAIL FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation>SETTING UPDATE CHANNEL FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>SPEAKER TEST FAILED</source>
        <translation>SPEAKER TEST FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="230"/>
        <source>SYSTEM SERVICE CRASHED</source>
        <translation>SYSTEM SERVICE CRASHED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation>Saving the new factory default value failed. Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>Something went wrong with the printer firmware. Please contact our support and do not forget to attach the logs.
Crashed services are immediately started again. If, despite that, the printer does not behave correctly, try restarting it. </source>
        <translation>Something went wrong with the printer firmware. Please contact our support and do not forget to attach the logs.
Crashed services are immediately started again. If, despite that, the printer does not behave correctly, try restarting it. </translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Speaker test failed.</source>
        <translation>Speaker test failed.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation>TEMPERATURE OUT OF RANGE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation>TEMPERATURE SENSOR FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation>TILT AXIS CHECK FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation>TILT ENDSTOP NOT REACHED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TILT HOMING FAILED</source>
        <translation>TILT HOMING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>TILT MOVING FAILED</source>
        <translation>TILT MOVING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation>TOWER AXIS CHECK FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation>TOWER ENDSTOP NOT REACHED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TOWER HOMING FAILED</source>
        <translation>TOWER HOMING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>TOWER MOVING FAILED</source>
        <translation>TOWER MOVING FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>TOWER POSITION ERROR</source>
        <translation>TOWER POSITION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>The %(sensor)s sensor failed.</source>
        <translation>The %(sensor)s sensor failed.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>The %(sensor__map_HardwareDeviceId)s sensor failed.</source>
        <translation>The %(sensor__map_HardwareDeviceId)s sensor failed.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation>The UV LED calibrator is not detected. Check the connection and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The UV LED panel is not detected.</source>
        <translation>The UV LED panel is not detected.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>The admin menu is not available.</source>
        <translation>The admin menu is not available.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation>The ambient temperature is too high, the print can continue, but it might fail.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation>The ambient temperature is too low, the print can continue, but it might fail.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation>The automatic UV LED calibration did not finish successfully! Run the calibration again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation>The calibration did not finish successfully! Run the calibration again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The directory is not empty.</source>
        <translation>The directory is not empty.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation>The download failed. Check the connection to the internet and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick your preferred language.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation>The model was sliced for a different printer model. Reslice the model using the correct settings.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>The password is incorrect. Please check or update it in:

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

The password must be at least 8 characters long. Supported characters are letters, numbers, and dash.</source>
        <translation>The password is incorrect. Please check or update it in:

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

The password must be at least 8 characters long. Supported characters are letters, numbers, and dash.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>The print job cancelled by the user.</source>
        <translation>The print job cancelled by the user.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="117"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation>The printer is not calibrated. Please run the Wizard first.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation>The printer is not connected to the internet. Check the connection in the Settings.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer model was not detected.</source>
        <translation>The printer model was not detected.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>The project file is invalid!</source>
        <translation>The project file is invalid!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The project must have at least one layer</source>
        <translation>The project must have at least one layer</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured.</source>
        <translation>The resin sensor was not triggered. Check whether the tank and the platform are properly secured.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation>The update of the firmware failed! Restart the printer and try again.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>The wizard did not finish successfully!</source>
        <translation>The wizard did not finish successfully!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>There is no warning</source>
        <translation>There is no warning</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation>This Wizard cannot be canceled, finish the steps first.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>This project was prepared for a different printer</source>
        <translation>This project was prepared for a different printer</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation>This request is not compatible with the Prusa remote API. See our documentation for more details.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps</source>
        <translation>Tilt axis check failed!

Current position: %(position)d steps</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation>Tilt homing failed, check its surroundings and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UNAUTHORIZED</source>
        <translation>UNAUTHORIZED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>UNEXPECTED ERROR</source>
        <translation>UNEXPECTED ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation>UNEXPECTED MC ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="233"/>
        <source>UNKNOWN WARNING</source>
        <translation>UNKNOWN WARNING</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation>UPDATE CHANNEL FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation>USB DRIVE NOT DETECTED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>UV CALIBRATION ERROR</source>
        <translation>UV CALIBRATION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>UV CALIBRATION FAILED</source>
        <translation>UV CALIBRATION FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>UV DATA EROR</source>
        <translation>UV DATA ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>UV INTENSITY ERROR</source>
        <translation>UV INTENSITY ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation>UV INTENSITY TOO HIGH</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation>UV INTENSITY TOO LOW</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation>UV LED CALIBRATOR CONNECTION ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation>UV LED CALIBRATOR ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation>UV LED CALIBRATOR LINK ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation>UV LED CALIBRATOR NOT DETECTED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation>UV LED CALIBRATOR READINGS ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation>UV LED CALIBRATOR UNKNOWN ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV LED TEMP. ERROR</source>
        <translation>UV LED TEMP. ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>UV LED is overheating!</source>
        <translation>UV LED is overheating!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation>UV intensity not set. Please run the UV calibration before starting a print.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation>UVLED HEAT SINK FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation>UVLED VOLTAGE ERROR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>Unknown UV LED calibrator error code: %(nonprusa_code)d</source>
        <translation>Unknown UV LED calibrator error code: %(nonprusa_code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>Unknown printer model</source>
        <translation>Unknown printer model</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>WIZARD DATA FAILURE</source>
        <translation>WIZARD DATA FAILURE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>WIZARD FAILED</source>
        <translation>WIZARD FAILED</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>WRONG PRINTER MODEL</source>
        <translation>WRONG PRINTER MODEL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation>Wrong revision of the Motion Controller (MC). Contact our support.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>YOU SHALL NOT PASS</source>
        <translation>YOU SHALL NOT PASS</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="110"/>
        <source>Already exists</source>
        <translation>Already exists</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="114"/>
        <source>Can&apos;t read</source>
        <translation>Can&apos;t read</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="96"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="108"/>
        <source>File not found</source>
        <translation>File not found</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="112"/>
        <source>Invalid extension</source>
        <translation>Invalid extension</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="106"/>
        <source>Storage full</source>
        <translation>Storage full</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation>(unchanged)</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="149"/>
        <source>Inactive</source>
        <translation>Inactive</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="247"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="175"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="153"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="253"/>
        <source>SSID must not be empty</source>
        <translation>SSID must not be empty</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="232"/>
        <source>Security</source>
        <translation>Security</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="216"/>
        <source>Show Password</source>
        <translation>Show Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="265"/>
        <source>Start AP</source>
        <translation>Start AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="138"/>
        <source>State:</source>
        <translation>State:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation>Stop AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="296"/>
        <source>Swipe for QRCode</source>
        <translation>Swipe for QR Code</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="377"/>
        <source>Swipe for settings</source>
        <translation>Swipe for settings</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="23"/>
        <source>About Us</source>
        <translation>About Us</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="52"/>
        <source>Prusa Research a.s.</source>
        <translation>Prusa Research a.s.</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="48"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation>To find out more about us please scan the QR code or use the link below:</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation>Add Hidden Network</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation>PASS</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation>SSID must not be empty.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation>Security</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation>Show Password</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="40"/>
        <source>Admin API</source>
        <translation>Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation>(unchanged)</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="156"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="168"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="194"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="64"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation>Please, enter the correct password for network &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation>Show Password</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Are you sure?</source>
        <translation>Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation>Can you see the company logo on the exposure display through the lid?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="126"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Carefully peel off the protective sticker from the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="174"/>
        <source>Close the cover.</source>
        <translation>Close the lid.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="173"/>
        <source>Cover</source>
        <translation>Lid</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="57"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation>Do you really want to cancel the wizard?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="114"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation>Once you place the paper inside the printer, do not forget to close the lid!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="59"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation>The machine will not work without a completed Wizard procedure.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="112"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation>Wizard</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="154"/>
        <source>Wizard canceled</source>
        <translation>Wizard canceled</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard failed</source>
        <translation>Wizard failed</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="136"/>
        <source>Wizard finished sucessfuly!</source>
        <translation>Wizard finished successfully!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation>Wizard stopped due to a problem, retry?</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="121"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="92"/>
        <source>Move Down</source>
        <translation>Move Down</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="83"/>
        <source>Move Up</source>
        <translation>Move Up</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation>Tank Movement</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="109"/>
        <source>Tilt position:</source>
        <translation>Tilt position:</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="160"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation>Adjust the platform so it is aligned with the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="195"/>
        <source>All done, happy printing!</source>
        <translation>All done, happy printing!</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="129"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="145"/>
        <source>Close the cover.</source>
        <translation>Close the lid.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="131"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation>Do not rotate the platform. It should be positioned according to the picture.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="162"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation>Front edges of the platform and exposition display need to be parallel.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="50"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation>If the platform is not yet inserted, insert it according to the picture at a 0° degree angle and secure it with the black knob.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="86"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="109"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation>Make sure that the platform, tank and tilt bed are PERFECTLY clean.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation>Printer Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="120"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="99"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation>Set the tilt bed against the resin tank</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="64"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation>Some SL1 printers may have two screws - see the handbook for more information.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="180"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="111"/>
        <source>The image is for illustration purposes only.</source>
        <translation>The image is for illustration purposes only.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="178"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation>Tighten the small screw on the cantilever with an allen key.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="77"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="149"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="177"/>
        <source>Above Area Fill</source>
        <translation>Above Area Fill</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="171"/>
        <source>Above Area Fill Threshold Settings</source>
        <translation>Above Area Fill Threshold Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="148"/>
        <source>Area Fill Threshold</source>
        <translation>Area Fill Threshold</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="197"/>
        <source>Below Area Fill</source>
        <translation>Below Area Fill</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="191"/>
        <source>Below Area Fill Threshold Settings</source>
        <translation>Below Area Fill Threshold Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="72"/>
        <source>Exposure</source>
        <translation>Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="97"/>
        <source>Exposure Time Incr.</source>
        <translation>Exposure time incr.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="124"/>
        <source>First Layer Expo.</source>
        <translation>First Layer Expo.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="41"/>
        <source>Print Settings</source>
        <translation>Print Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="90"/>
        <location filename="../qml/PageChange.qml" line="118"/>
        <location filename="../qml/PageChange.qml" line="142"/>
        <source>s</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="194"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation>Swipe for a picture</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="207"/>
        <source>Connected to %1</source>
        <translation>Connected to %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="196"/>
        <source>Connection to %1 failed.</source>
        <translation>Connection to %1 failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation>Hidden Network</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="139"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="147"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK must be at least 8 characters long.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="122"/>
        <source>Security</source>
        <translation>Security</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="115"/>
        <source>Show Password</source>
        <translation>Show Password</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="235"/>
        <source>Start AP</source>
        <translation>Start AP</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="185"/>
        <source>Working...</source>
        <translation>Working...</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation>Cooling down</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation>Temperature is %1 C</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation>UV LED OVERHEAT!</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation>Display Test</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="91"/>
        <source>All done, happy printing!</source>
        <translation>All done, happy printing!</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="77"/>
        <source>Close the cover.</source>
        <translation>Close the lid.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="32"/>
        <source>Display Test</source>
        <translation>Display Test</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Loosen the black knob and remove the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Please unscrew and remove the resin tank.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="51"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation>This procedure will help you make sure that your exposure display is working correctly.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>Welcome to the display wizard.</source>
        <translation>Welcome to the display wizard.</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="78"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation>The current configuration is going to be cleared now.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="108"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation>Downgrade done. In the next step, the printer will be restarted.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation>Hardware Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="99"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Only use the platform supplied. Using a different platform may cause the resin to spill and damage your printer!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="69"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="48"/>
        <source>Proceed?</source>
        <translation>Proceed?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="59"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation>Reassemble SL1S components and power on the printer. This will restore the original state.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation>SL1 components detected (downgrade from SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="79"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>The printer will ask for the inital setup after reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="58"/>
        <source>The printer will power off now.</source>
        <translation>The printer will power off now.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>To complete the downgrade procedure, the printer needs to clear the configuration and reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="89"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation>Use only the metal resin tank supplied. Using the different resin tanks may cause the resin to spill and damage your printer!</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Cleanup...</source>
        <translation>Cleanup...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Copying...</source>
        <translation>Copying...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Done.</source>
        <translation>Done.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Downloading ...</source>
        <translation>Downloading ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="55"/>
        <source>Downloading examples...</source>
        <translation>Downloading examples...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation>Examples</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="129"/>
        <source>Failure.</source>
        <translation>Failure.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Initializing...</source>
        <translation>Initializing...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="132"/>
        <source>Unknown state.</source>
        <translation>Unknown state.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Unpacking...</source>
        <translation>Unpacking...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="141"/>
        <source>View Examples</source>
        <translation>View Examples</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="44"/>
        <source>Error code:</source>
        <translation>Error code:</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="48"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>For further information, please scan the QR code or contact your reseller.</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="202"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>Configuring the connection,</source>
        <translation>Configuring the connection,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="123"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="35"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="148"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="136"/>
        <source>IP Address</source>
        <translation>IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="117"/>
        <source>Network Info</source>
        <translation>Network info</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="219"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Revert</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>please wait...</source>
        <translation>please wait...</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="61"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="68"/>
        <source>Error code:</source>
        <translation>Error code:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="72"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>For further information, please scan the QR code or contact your reseller.</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="106"/>
        <source>Save Logs to USB</source>
        <translation>Save Logs to USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="119"/>
        <source>Send Logs to Cloud</source>
        <translation>Send Logs to Cloud</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="80"/>
        <source>Swipe to proceed</source>
        <translation>Swipe to proceed</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation>System Error</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="140"/>
        <source>Turn Off</source>
        <translation>Turn off</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="133"/>
        <source>Update Firmware</source>
        <translation>Update Firmware</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="32"/>
        <source>Factory Reset</source>
        <translation>Factory Reset</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="45"/>
        <source>Factory Reset done.</source>
        <translation>Factory reset done.</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation>Feed Me</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation>If you do not want to refill, press the Back button at top of the screen.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation>Manual resin refill.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation>Refill the tank up to the 100% mark and press Done.</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation>
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation>Before printing, the following steps are required to pass:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation>Calibrate?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation>Do you really want to install %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation>Do you want to start now?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation>Install?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="314"/>
        <source>No usable projects were found,</source>
        <translation>No usable projects were found,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation>Previous Prints</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation>Printer Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation>Projects</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation>Remote</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation>Remote</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation>The printer is not fully calibrated.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation>UV calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation>Update Bundles</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation>Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation>insert a USB drive or download examples in Settings -&gt; Support.</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="127"/>
        <source>CANCELED</source>
        <translation>CANCELED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="207"/>
        <source>Consumed Resin</source>
        <translation>Consumed resin</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="126"/>
        <source>FAILED</source>
        <translation>FAILED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="125"/>
        <location filename="../qml/PageFinished.qml" line="128"/>
        <source>FINISHED</source>
        <translation>FINISHED</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation>Finished</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="277"/>
        <source>First Layer Exposure</source>
        <translation>First Layer Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="328"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="253"/>
        <source>Layer Exposure</source>
        <translation>Layer Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>Layer height</source>
        <translation>Layer height</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="183"/>
        <source>Layers</source>
        <translation>Layers</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="405"/>
        <source>Loading, please wait...</source>
        <translation>Loading, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="156"/>
        <source>Print Time</source>
        <translation>Print Time</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="348"/>
        <source>Reprint</source>
        <translation>Reprint</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="338"/>
        <source>Resin Tank Cleaning</source>
        <translation>Resin Tank Cleaning</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="300"/>
        <source>Swipe to continue</source>
        <translation>Swipe to continue</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="368"/>
        <source>Turn Off</source>
        <translation>Turn off</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="261"/>
        <location filename="../qml/PageFinished.qml" line="285"/>
        <source>s</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation>Fullscreen Image</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="51"/>
        <source>Control</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="58"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="65"/>
        <source>Turn Off</source>
        <translation>Turn off</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="81"/>
        <source>Set</source>
        <translation>Set</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation>Set Language</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation>Extracting log data</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation>Log upload finished</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation>Logs export</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation>Logs export canceled</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation>Log export failed.

Please check your flash drive / internet connection.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation>Logs export finished</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation>Saving data to USB drive</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation>Uploading data to server</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</translation>
    </message>
</context>
<context>
    <name>PageModifyLayerProfile</name>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="144"/>
        <source>Delay After Exposure</source>
        <translation>Delay After Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="130"/>
        <source>Delay Before Exposure</source>
        <translation>Delay Before Exposure</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="375"/>
        <source>Disabling the &apos;Use tilt&apos; causes the object to separate away from the film in the vertical direction only.

&apos;Tower hop height&apos; has been set to recommended value of 5 mm.</source>
        <translation>Disabling the &apos;Use tilt&apos; causes the object to separate away from the film in the vertical direction only.

&apos;Tower hop height&apos; has been set to recommended value of 5 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="29"/>
        <source>Edit Layer Profile</source>
        <translation>Edit Layer Profile</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="385"/>
        <source>Enabling the &apos;Use tilt&apos; causes the object to separate away from the film mainly by tilt.

&apos;Tower hop height&apos; has been set to 0 mm.</source>
        <translation>Enabling the &apos;Use tilt&apos; causes the object to separate away from the film mainly by tilt.

&apos;Tower hop height&apos; has been set to 0 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="266"/>
        <source>Tilt Down Cycles</source>
        <translation>Tilt Down Cycles</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="278"/>
        <source>Tilt Down Delay</source>
        <translation>Tilt Down Delay</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="254"/>
        <source>Tilt Down Finish Speed</source>
        <translation>Tilt Down Finish Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="214"/>
        <source>Tilt Down Initial Speed</source>
        <translation>Tilt Down Initial Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="226"/>
        <source>Tilt Down Offset</source>
        <translation>Tilt Down Offset</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="239"/>
        <source>Tilt Down Offset Delay</source>
        <translation>Tilt Down Offset Delay</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="345"/>
        <source>Tilt Up Cycles</source>
        <translation>Tilt Up Cycles</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="357"/>
        <source>Tilt Up Delay</source>
        <translation>Tilt Up Delay</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="333"/>
        <source>Tilt Up Finish Speed</source>
        <translation>Tilt Up Finish Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="293"/>
        <source>Tilt Up Initial Speed</source>
        <translation>Tilt Up Initial Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="305"/>
        <source>Tilt Up Offset</source>
        <translation>Tilt Up Offset</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="318"/>
        <source>Tilt Up Offset Delay</source>
        <translation>Tilt Up Offset Delay</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="159"/>
        <source>Tower Hop Height</source>
        <translation>Tower Hop Height</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="173"/>
        <source>Tower Speed</source>
        <translation>Tower Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="185"/>
        <source>Use Tilt</source>
        <translation>Use Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="374"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="384"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="160"/>
        <source>mm</source>
        <comment>millimeters</comment>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="174"/>
        <source>mm/s</source>
        <comment>millimeters per second</comment>
        <translation>mm/s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="131"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="145"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="240"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="279"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="319"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="358"/>
        <source>s</source>
        <comment>seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="227"/>
        <source>μ-step</source>
        <comment>tilt microsteps</comment>
        <translation>μ-step</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="306"/>
        <source>μ-step</source>
        <comment>tilt micro steps</comment>
        <translation>μ-step</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="216"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="256"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="295"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="335"/>
        <source>μ-step/s</source>
        <comment>microsteps per second</comment>
        <translation>μ-step/s</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation>Disable
Steppers</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation>Home
Platform</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation>Home
Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tank, please wait...</source>
        <translation>Homing the tank, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tower, please wait...</source>
        <translation>Homing the tower, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="68"/>
        <source>Resin Tank Cleaning</source>
        <translation>Resin Tank Cleaning</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation>Network</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="32"/>
        <source>New Exposure Panel</source>
        <translation>New Exposure Panel</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="45"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation>A new exposure screen has been detected.

The printer will ask for the initial setup (selftest and calibration) to make sure everything works correctly.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation>Available update to %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation>Print canceled: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation>Print failed: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation>Print finished: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="55"/>
        <source>Insert protective foam</source>
        <translation>Insert protective foam</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="32"/>
        <source>Packing Wizard</source>
        <translation>Packing Wizard</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="46"/>
        <source>Packing done.</source>
        <translation>Packing done.</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="7"/>
        <source>Do you really want to turn off the printer?</source>
        <translation>Do you really want to turn off the printer?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Power Off?</source>
        <translation>Power Off?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="10"/>
        <source>Powering Off...</source>
        <translation>Powering Off...</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="121"/>
        <source>Cover</source>
        <translation>Lid</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="125"/>
        <source>Delayed Start</source>
        <translation>Delayed Start</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="151"/>
        <source>Disabled</source>
        <translation>Disabled</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="140"/>
        <source>Do not touch the printer!</source>
        <translation>Do not touch the printer!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="120"/>
        <source>Fan</source>
        <translation>Fan</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation>Please Wait</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Project</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="122"/>
        <source>Resin</source>
        <translation>Resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="123"/>
        <source>Starting Positions</source>
        <translation>Starting Positions</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="124"/>
        <source>Stirring</source>
        <translation>Stirring</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Temperature</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="150"/>
        <source>With Warning</source>
        <translation>With Warning</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Action Pending</source>
        <translation>Action Pending</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="117"/>
        <source>Check Warning</source>
        <translation>Check Warning</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="151"/>
        <source>Close Cover!</source>
        <translation>Close the lid!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="113"/>
        <source>Continue?</source>
        <translation>Continue?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Cover Open</source>
        <translation>Cover Open</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation>Getting the printer ready to add resin. Please wait.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Going down</source>
        <translation>Going down</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Going up</source>
        <translation>Going up</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="130"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Moving platform to the bottom position</source>
        <translation>Moving platform to the bottom position</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Moving platform to the top position</source>
        <translation>Moving platform to the top position</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Moving the resin tank down...</source>
        <translation>Moving the resin tank down...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Paused.</source>
        <translation>Paused.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Please close the cover to continue</source>
        <translation>Please close the lid to continue</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="220"/>
        <location filename="../qml/PagePrint.qml" line="277"/>
        <source>Please wait...</source>
        <translation>Please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="152"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation>Please, close the lid! UV radiation is harmful.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Project</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="243"/>
        <source>Reading data...</source>
        <translation>Reading data...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="129"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation>Release the tank mechanism and press Continue.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation>Requested actions will be executed once this layer finishes, please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Setting start positions...</source>
        <translation>Setting start positions...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring</source>
        <translation>Stirring</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring resin</source>
        <translation>Stirring resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="127"/>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Stuck Recovery</source>
        <translation>Stuck Recovery</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Tank Moving Down</source>
        <translation>Tank Moving Down</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="128"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation>The printer got stuck and needs user assistance.</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="302"/>
        <source>Exposure Times</source>
        <translation>Exposure Times</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="348"/>
        <source>Last Modified</source>
        <translation>Last Modified</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layer Height</source>
        <translation>Layer Height</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layers</source>
        <translation>Layers</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="447"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Please fill the resin tank to at least %1 % and close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="493"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="483"/>
        <source>Print Settings</source>
        <translation>Print Settings</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="329"/>
        <source>Print Time Estimate</source>
        <translation>Print Time Estimate</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation>Project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="374"/>
        <source>Swipe to continue</source>
        <translation>Swipe to continue</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="166"/>
        <source>Swipe to project</source>
        <translation>Swipe to project</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="444"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>The project requires&lt;br/&gt;%1 % of the resin. It will be necessary to refill the resin during print.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="208"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="357"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="239"/>
        <source>Area fill:</source>
        <translation>Area fill:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="492"/>
        <location filename="../qml/PagePrintPrinting.qml" line="498"/>
        <source>Cancel Print</source>
        <translation>Cancel Print</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="431"/>
        <source>Consumed Resin</source>
        <translation>Consumed resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="516"/>
        <source>Enter Admin</source>
        <translation>Enter Admin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="373"/>
        <source>Estimated End</source>
        <translation>Estimated End</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="403"/>
        <source>Layer</source>
        <translation>Layer</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Layer Height:</source>
        <translation>Layer Height:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="225"/>
        <source>Layer:</source>
        <translation>Layer:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="34"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="473"/>
        <source>Print Settings</source>
        <translation>Print Settings</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="388"/>
        <source>Printing Time</source>
        <translation>Printing time</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="274"/>
        <source>Progress:</source>
        <translation>Progress:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="479"/>
        <source>Refill Resin</source>
        <translation>Refill Resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="417"/>
        <source>Remaining Resin</source>
        <translation>Remaining resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="358"/>
        <source>Remaining Time</source>
        <translation>Remaining time</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="499"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Today at</source>
        <translation>Today at</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="110"/>
        <source>Tomorrow at</source>
        <translation>Tomorrow at</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="363"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Please fill the resin tank to at least %1 % and close the cover.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation>Pour in resin</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>The project requires&lt;br/&gt;%1 % of the resin. It will be necessary to refill the resin during print.</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation>Page QR code</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation>Install Now</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation>Later</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation>Release Notes</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="102"/>
        <source>Can you hear the music?</source>
        <translation>Can you hear the music?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="77"/>
        <location filename="../qml/PageSelftestWizard.qml" line="140"/>
        <location filename="../qml/PageSelftestWizard.qml" line="173"/>
        <source>Close the cover.</source>
        <translation>Close the lid.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Loosen the black knob and remove the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="155"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation>Make sure that the resin tank is installed and the screws are tight.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="164"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation>Please install the platform under 0° angle and secure it by tightening the black knob.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="122"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation>Please install the platform under 60° angle and secure it by tightening the black knob.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="113"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation>Please install the resin tank and tighten the screws evenly.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Please unscrew and remove the resin tank.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="131"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="51"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation>This procedure is mandatory and it will check all components of the printer.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="49"/>
        <source>Welcome to the selftest wizard.</source>
        <translation>Welcome to the selftest wizard.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="676"/>
        <source>Set</source>
        <translation>Set</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation>Set Date</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only a-z, 0-9 and  &quot;-&quot;. Must not begin or end with &quot;-&quot;.</source>
        <translation>Can contain only a-z, 0-9 and  &quot;-&quot;. Must not begin or end with &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation>Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation>Set</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaConnectRegistration</name>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="90"/>
        <source>Add Printer to Prusa Connect</source>
        <translation>Add Printer to Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="79"/>
        <source>In Progress</source>
        <translation>In Progress</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="78"/>
        <source>Not Registered</source>
        <translation>Not Registered</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="31"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="96"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="126"/>
        <source>Register Printer</source>
        <translation>Register Printer</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="80"/>
        <source>Registered</source>
        <translation>Registered</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="75"/>
        <source>Registration Status</source>
        <translation>Registration Status</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="123"/>
        <source>Review the Registration QR Code</source>
        <translation>Review the Registration QR Code</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="98"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="128"/>
        <source>Scan the QR Code or visit &lt;b&gt;prusa.io/add&lt;/b&gt;. Log into your Prusa Account to add this printer.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</source>
        <translation>Scan the QR Code or visit &lt;b&gt;prusa.io/add&lt;/b&gt;. Log into your Prusa Account to add this printer.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="81"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="111"/>
        <source>Unregister Printer from Prusa Connect</source>
        <translation>Unregister Printer from Prusa Connect</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaLinkWebLogin</name>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="131"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="117"/>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="125"/>
        <source>Must be at least 8 chars long</source>
        <translation>Must be at least 8 chars long</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="92"/>
        <source>Printer Password</source>
        <translation>Printer Password</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="32"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="144"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="55"/>
        <source>User Name</source>
        <translation>User Name</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation>Hour</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation>Set</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation>Set Time</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="134"/>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="128"/>
        <source>Region</source>
        <translation>Region</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="140"/>
        <source>Set</source>
        <translation>Set</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation>Set Timezone</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="60"/>
        <source>Calibration</source>
        <translation>Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="150"/>
        <source>Enter Admin</source>
        <translation>Enter Admin</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="89"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="119"/>
        <source>Language &amp; Time</source>
        <translation>Language &amp; Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="69"/>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="79"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Platform &amp; Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="29"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="99"/>
        <source>Settings &amp; Sensors</source>
        <translation>Settings &amp; Sensors</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Support</source>
        <translation>Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>System Logs</source>
        <translation>System Logs</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="109"/>
        <source>Touchscreen</source>
        <translation>Touchscreen</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="32"/>
        <source>Calibration</source>
        <translation>Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="94"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation>Did you replace the EXPOSITION DISPLAY?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="107"/>
        <source>Did you replace the UV LED SET?</source>
        <translation>Did you replace the UV LED SET?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="76"/>
        <source>Display Test</source>
        <translation>Display Test</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>New Display?</source>
        <translation>New Display?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>New UV LED SET?</source>
        <translation>New UV LED SET?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="58"/>
        <source>Printer Calibration</source>
        <translation>Printer Calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="51"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="68"/>
        <source>UV Calibration</source>
        <translation>UV calibration</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="414"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation>Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation>Check for Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation>Check for update failed</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation>Checking for updates...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="215"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>Continue anyway?</source>
        <translation>Continue anyway?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="182"/>
        <source>Do you really want to downgrade to FW</source>
        <translation>Do you really want to downgrade to FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>Do you want to continue?</source>
        <translation>Do you want to continue?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="347"/>
        <source>Downgrade Firmware?</source>
        <translation>Downgrade Firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Downgrade?</source>
        <translation>Downgrade?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation>Download Now</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="290"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation>FW Info</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="334"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="248"/>
        <source>Factory Reset</source>
        <translation>Factory Reset</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="36"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="213"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="413"/>
        <source>Incompatible FW!</source>
        <translation>Incompatible FW!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="333"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="382"/>
        <source>Install Firmware?</source>
        <translation>Install Firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation>Install Now</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation>Installed version</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="291"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation>Loading FW file meta information
(may take up to 20 seconds) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="239"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Newer than SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="235"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="425"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="138"/>
        <source>Receive Beta Updates</source>
        <translation>Receive Beta Updates</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Switch to beta?</source>
        <translation>Switch to beta?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="169"/>
        <source>Switch to version:</source>
        <translation>Switch to version:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation>System is up-to-date</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="211"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="428"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation>Update available</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation>Update download failed</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation>Update is being downloaded</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation>Update is downloading</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation>Updater service idle</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="350"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="157"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="383"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>which has version &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="29"/>
        <source>Language &amp; Time</source>
        <translation>Language &amp; Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="35"/>
        <source>Set Language</source>
        <translation>Set Language</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="41"/>
        <source>Time Settings</source>
        <translation>Time Settings</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="37"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="74"/>
        <source>Hostname</source>
        <translation>Hostname</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="29"/>
        <source>Network</source>
        <translation>Network</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="89"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="81"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="53"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="109"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="48"/>
        <source>Disable Steppers</source>
        <translation>Disable Steppers</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="102"/>
        <source>Limit for Fast Tilt</source>
        <translation>Limit for Fast Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="34"/>
        <source>Move Platform</source>
        <translation>Move Platform</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="41"/>
        <source>Move Resin Tank</source>
        <translation>Move Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="28"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Platform &amp; Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="56"/>
        <source>Platform Axis Sensitivity</source>
        <translation>Platform Axis Sensitivity</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="128"/>
        <source>Platform Offset</source>
        <translation>Platform Offset</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="79"/>
        <source>Tank Axis Sensitivity</source>
        <translation>Tank Axis Sensitivity</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="152"/>
        <source>Tank Surface Cleaning Exposure</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="159"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="135"/>
        <source>um</source>
        <translation>um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="112"/>
        <location filename="../qml/PageSettingsSensors.qml" line="148"/>
        <source>Are you sure?</source>
        <translation>Are you sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="61"/>
        <source>Auto Power Off</source>
        <translation>Auto Power Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="87"/>
        <source>Cover Check</source>
        <translation>Cover Check</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="36"/>
        <source>Device hash in QR</source>
        <translation>Device hash in QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="113"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="149"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="180"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="168"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="161"/>
        <source>Rear Fan Speed</source>
        <translation>Rear Fan Speed</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="125"/>
        <source>Resin Sensor</source>
        <translation>Resin Sensor</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="28"/>
        <source>Settings &amp; Sensors</source>
        <translation>Settings &amp; Sensors</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="78"/>
        <source>About Us</source>
        <translation>About Us</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="36"/>
        <source>Download Examples</source>
        <translation>Download Examples</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="50"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="30"/>
        <source>Support</source>
        <translation>Support</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="69"/>
        <source>System Information</source>
        <translation>System Information</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="60"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>Last Seen Logs:</source>
        <translation>Last Seen Logs:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="55"/>
        <source>Save to USB Drive</source>
        <translation>Save to USB Drive</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="29"/>
        <source>System Logs</source>
        <translation>System Logs</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="65"/>
        <source>Upload to Server</source>
        <translation>Upload to Server</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="98"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="63"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="36"/>
        <source>Screensaver timer</source>
        <translation>Screensaver timer</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="80"/>
        <source>Touch Screen Brightness</source>
        <translation>Touch Screen Brightness</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="28"/>
        <source>Touchscreen</source>
        <translation>Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="58"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="81"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation>Temporary Token</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation>Package Name</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation>Software Packages</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation>A64 Controller SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>Ambient Temperature</source>
        <translation>Ambient Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <source>Blower Fan</source>
        <translation>Blower Fan</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation>Booster Board SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>CPU Temperature</source>
        <translation>CPU Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Closed</source>
        <translation>Closed</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>Cover State</source>
        <translation>Lid State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation>Ethernet IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation>Exposure display SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="199"/>
        <location filename="../qml/PageSysinfo.qml" line="209"/>
        <location filename="../qml/PageSysinfo.qml" line="219"/>
        <source>Fan Error!</source>
        <translation>Fan Error!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Finished Projects</source>
        <translation>Finished Projects</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation>GUI Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation>Hardware State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation>Motion Controller HW Revision</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation>Motion Controller SN</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation>Motion Controller SW version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation>Network State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Newer than SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Not triggered</source>
        <translation>Not triggered</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation>OS Image Version</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation>Other Components</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="240"/>
        <source>Power Supply Voltage</source>
        <translation>Power Supply Voltage</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Print Display Time Counter</source>
        <translation>Print Display Time Counter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation>Printer Model</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>Printer Password</source>
        <translation>Printer Password</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>Rear Fan</source>
        <translation>Rear Fan</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Resin Sensor State</source>
        <translation>Resin Sensor State</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="298"/>
        <source>Show software packages &amp; licenses</source>
        <translation>Show software packages &amp; licenses</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Started Projects</source>
        <translation>Started Projects</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>Statistics</source>
        <translation>Statistics</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation>System Information</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation>Time of Fast Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Time of High Viscosity Tilt:</source>
        <translation>Time of High Viscosity Tilt:</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation>Time of Slow Tilt</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Layers Printed</source>
        <translation>Total Layers Printed</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Print Time</source>
        <translation>Total Print Time</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="287"/>
        <source>Total Resin Consumed</source>
        <translation>Total Resin Consumed</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Triggered</source>
        <translation>Triggered</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="226"/>
        <source>UV LED</source>
        <translation>UV LED</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <source>UV LED Fan</source>
        <translation>UV LED Fan</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>UV LED Temperature</source>
        <translation>UV LED Temperature</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>UV LED Time Counter</source>
        <translation>UV LED Time Counter</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="241"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation>Wifi IP Address</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>seconds</source>
        <translation>seconds</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <location filename="../qml/PageSysinfo.qml" line="192"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
</context>
<context>
    <name>PageTankSurfaceCleanerWizard</name>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="160"/>
        <source>Cleaning Adaptor</source>
        <translation>Cleaning Adaptor</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="149"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="134"/>
        <source>Exposure[s]:</source>
        <translation>Exposure[s]:</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="81"/>
        <source>Here you can optionally adjust the exposure settings before cleaning starts.</source>
        <translation>Here you can optionally adjust the exposure settings before cleaning starts.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="89"/>
        <source>Less</source>
        <translation>Less</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="108"/>
        <source>More</source>
        <translation>More</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="172"/>
        <source>Now remove the cleaning adaptor along with the exposed film, careful not to tear it.</source>
        <translation>Now remove the cleaning adaptor along with the exposed film, be careful not to tear it.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="161"/>
        <source>Please insert the cleaning adaptor as is ilustrated in the picture on the left.</source>
        <translation>Please insert the cleaning adaptor as is ilustrated in the picture on the left.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="171"/>
        <source>Remove Adaptor</source>
        <translation>Remove Adaptor</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="33"/>
        <source>Resin Tank Cleaning</source>
        <translation>Resin Tank Cleaning</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="61"/>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="69"/>
        <source>Tank Cleaning</source>
        <translation>Tank Cleaning</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="62"/>
        <source>This wizard will help you clean your resin tank and remove resin debris.

You will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine&apos;s internal storage. Or you can download it at printables.com</source>
        <translation>This wizard will help you clean your resin tank and remove resin debris.

You will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine&apos;s internal storage. Or you can download it at printables.com</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation>Down</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation>Fast Down</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation>Fast Up</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation>Move Resin Tank</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation>Up</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="139"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation>12-hour</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="140"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation>24-hour</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="163"/>
        <source>Automatic time settings using NTP ...</source>
        <translation>Automatic time settings using NTP ...</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="94"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="138"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation>Native</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="73"/>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="133"/>
        <source>Time Format</source>
        <translation>Time Format</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="35"/>
        <source>Time Settings</source>
        <translation>Time Settings</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="115"/>
        <source>Timezone</source>
        <translation>Timezone</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="47"/>
        <source>Use NTP</source>
        <translation>Automatic time setting</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation>Down</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation>Fast Down</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation>Fast Up</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation>Move Platform</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation>Up</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="79"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Carefully peel off the protective sticker from the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="69"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation>Remove the safety sticker and open the lid.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="59"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation>Remove the black foam from both sides of the platform.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Unpacking</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="89"/>
        <source>Unpacking done.</source>
        <translation>Unpacking done.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="49"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation>Unscrew and remove the resin tank and remove the black foam underneath it.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="46"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Carefully peel off the protective sticker from the exposition display.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Unpacking</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="56"/>
        <source>Unpacking done.</source>
        <translation>Unpacking done.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="419"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="296"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation>Do not power off the printer while updating!&lt;br/&gt;The printer will be rebooted after a successful update.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="229"/>
        <source>Download failed.</source>
        <translation>Download failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation>Downloading</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation>Downloading firmware, installation will begin immediately after.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="255"/>
        <source>Installing Firmware</source>
        <translation>Installing Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation>Printer Update</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="360"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation>Printer is being restarted into the new firmware(%1), please wait</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="386"/>
        <source>Update to %1 failed.</source>
        <translation>Update to %1 failed.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="343"/>
        <source>Updated to %1 failed.</source>
        <translation>Updated to %1 failed.</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="32"/>
        <source>Hardware Upgrade</source>
        <translation>Hardware Upgrade</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="90"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Only use the platform supplied. Using a different platform may cause the resin to spill and damage your printer!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="100"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="49"/>
        <source>Proceed?</source>
        <translation>Proceed?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="60"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation>Reassemble SL1 components and power on the printer. This will restore the original state.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation>SL1S components detected (upgrade from SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="69"/>
        <source>The configuration is going to be cleared now.</source>
        <translation>The configuration is going to be cleared now.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="70"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>The printer will ask for the inital setup after reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="59"/>
        <source>The printer will power off now.</source>
        <translation>The printer will power off now.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="48"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="109"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation>Upgrade done. In the next step, the printer will be restarted.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="80"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation>1. If the resin tank is in the printer, remove it along with the screws.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="71"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation>1. Place the UV calibrator on the print display and connect it to the front USB.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="52"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="73"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation>2. Close the lid, don&apos;t open it! UV radiation may be harmful!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Intensity: center %1, edge %2</source>
        <translation>Intensity: center %1, edge %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="84"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation>Open the lid, &lt;br&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="107"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation>The printer has been successfully calibrated!
Would you like to apply the calibration results?</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="100"/>
        <source>The result of calibration:</source>
        <translation>The result of calibration:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="32"/>
        <source>UV Calibration</source>
        <translation>UV calibration</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="103"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation>UV Intensity min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation>UV Intensity: %1, σ = %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="101"/>
        <source>UV PWM: %1</source>
        <translation>UV PWM: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="57"/>
        <source>Warm-up: %1 s</source>
        <translation>Warm-up: %1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>Welcome to the UV calibration.</source>
        <translation>Welcome to the UV calibration.</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation>Loading, please wait...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation>Please Wait</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="193"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="203"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation>Configuring the connection,
please wait...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation>Connected</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation>Disconnected</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="233"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>Do you really want to forget this network&apos;s settings?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="223"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation>Forget network</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="232"/>
        <source>Forget network?</source>
        <translation>Forget network?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation>Network info</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="212"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Revert</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation>Wireless Settings</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Are You Sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="71"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="59"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation>Are You Sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="115"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation>Swipe to proceed</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="102"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>Are You Sure?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="265"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="174"/>
        <source>Swipe for a picture</source>
        <translation>Swipe for a picture</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="253"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation>Plugged in</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation>Unplugged</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="113"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="172"/>
        <source>On</source>
        <translation>On</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation>Please wait...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation>Swipe to confirm</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="115"/>
        <source>back</source>
        <translation>back</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="130"/>
        <source>cancel</source>
        <translation>cancel</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="144"/>
        <source>close</source>
        <translation>close</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="714"/>
        <location filename="../qml/main.qml" line="737"/>
        <source>&lt;printer IP&gt;</source>
        <translation>&lt;printer IP&gt;</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation>Activating</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>CPU temperature</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="665"/>
        <source>Cancel the current print job?</source>
        <translation>Cancel the current print job?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="650"/>
        <source>Cancel?</source>
        <translation>Cancel?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation>Connected</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="699"/>
        <location filename="../qml/main.qml" line="723"/>
        <source>DEPRECATED PROJECTS</source>
        <translation>DEPRECATED PROJECTS</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation>Deactivated</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation>Deactivating</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="407"/>
        <source>Initializing...</source>
        <translation>Initializing...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="417"/>
        <source>MC Update</source>
        <translation>MC Update</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="310"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="356"/>
        <source>Now it&apos;s safe to remove USB drive</source>
        <translation>Now it&apos;s safe to remove the USB drive</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>Press &quot;cancel&quot; to abort.</source>
        <translation>Press &quot;Cancel&quot; to abort.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="688"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation>Printer cannot be turned off in this state. Finish or cancel the current action and try again.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation>Prusa SL1 Touchscreen User Interface</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="355"/>
        <source>Remove USB</source>
        <translation>Remove the USB drive</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="339"/>
        <source>Remove USB flash drive?</source>
        <translation>Remove USB flash drive?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="724"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation>Some incompatible projects were found, you can download them at 

http://%1/old-projects.

Do you want to remove them?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="700"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="418"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation>The Motion Controller firmware is being updated.

Please wait...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>The USB drive cannot be safely removed because it is now in use, please wait.</source>
        <translation>The USB drive cannot be safely removed because it is now in use, please wait.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="408"/>
        <source>The printer is initializing, please wait ...</source>
        <translation>The printer is initializing, please wait ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="637"/>
        <source>Turn Off?</source>
        <translation>Turn off?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>UV LED fan</source>
        <translation>UV LED fan</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="101"/>
        <source>UV LED temperature</source>
        <translation>UV LED temperature</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>ambient temperature</source>
        <translation>Ambient Temperature</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>blower fan</source>
        <translation>Blower Fan</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>rear fan</source>
        <translation>Rear Fan</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>unknown device</source>
        <translation>Unknown Device</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n hour</numerusform>
            <numerusform>%n hours</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n minute</numerusform>
            <numerusform>%n minutes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation>Less than a minute</translation>
    </message>
</context>
</TS>
