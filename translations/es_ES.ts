<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es-ES">
<context>
    <name>DelegateAddHiddenNetwork</name>
    <message>
        <location filename="../qml/DelegateAddHiddenNetwork.qml" line="90"/>
        <source>Add Hidden Network</source>
        <translation>Añadir Red Oculta</translation>
    </message>
</context>
<context>
    <name>DelegateEthNetwork</name>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Plugged in</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateEthNetwork.qml" line="109"/>
        <source>Unplugged</source>
        <translation>Desconectado</translation>
    </message>
</context>
<context>
    <name>DelegateState</name>
    <message>
        <location filename="../qml/DelegateState.qml" line="37"/>
        <source>Network Info</source>
        <translation>Información de Red</translation>
    </message>
</context>
<context>
    <name>DelegateWifiClientOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiClientOnOff.qml" line="38"/>
        <source>Wi-Fi Client</source>
        <translation>Cliente Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWifiNetwork</name>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="106"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>¿Realmente quieres olvidar la configuración de esta red?</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWifiNetwork.qml" line="105"/>
        <source>Forget network?</source>
        <translation>¿Olvidar red?</translation>
    </message>
</context>
<context>
    <name>DelegateWifiOnOff</name>
    <message>
        <location filename="../qml/DelegateWifiOnOff.qml" line="35"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>DelegateWizardCheck</name>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="136"/>
        <source>Apply calibration results</source>
        <translation>Aplicar los resultados de la calibración</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="134"/>
        <source>Calibrate center</source>
        <translation>Calibrar el centro</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="135"/>
        <source>Calibrate edge</source>
        <translation>Calibrar borde</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="174"/>
        <source>Canceled</source>
        <translation>Cancelado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="131"/>
        <source>Check for UV calibrator</source>
        <translation>Comprueba el calibrador UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="156"/>
        <source>Check ID:</source>
        <translation>Verificar ID:</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="122"/>
        <source>Clear downloaded Slicer profiles</source>
        <translation>Borrar perfiles de Slicer descargados</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="121"/>
        <source>Clear UV calibration data</source>
        <translation>Borrar datos de calibración UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="127"/>
        <source>Disable factory mode</source>
        <translation>Desactivar modo de fábrica</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="130"/>
        <source>Disable ssh, serial</source>
        <translation>Deshabilitar ssh, serie</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="97"/>
        <source>Display test</source>
        <translation>Test de pantalla</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="124"/>
        <source>Erase motion controller EEPROM</source>
        <translation>Borrar la EEPROM del controlador de movimiento</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="113"/>
        <source>Erase projects</source>
        <translation>Borrar proyectos</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="140"/>
        <source>Erase UV PWM settings</source>
        <translation>Borrar la configuración PWM de los UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="150"/>
        <source>Exposing debris</source>
        <translation>Exponiendo los restos</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="171"/>
        <source>Failure</source>
        <translation>Fallo</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="103"/>
        <source>Make tank accessible</source>
        <translation>Ten el tanque a mano</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="148"/>
        <source>Moving platform down to safe distance</source>
        <translation>Bajando la plataforma a una distancia segura</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="151"/>
        <source>Moving platform gently up</source>
        <translation>Moviendo la plataforma suavemente hacia arriba</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="128"/>
        <source>Moving printer to accept protective foam</source>
        <translation>Mueve la impresora para colocar la espuma protectora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="112"/>
        <source>Obtain calibration info</source>
        <translation>Obtener información de calibración</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="111"/>
        <source>Obtain system info</source>
        <translation>Obtener información del sistema</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="170"/>
        <source>Passed</source>
        <translation>Aprobado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="109"/>
        <source>Platform calibration</source>
        <translation>Calibración de la plataforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="94"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="153"/>
        <source>Platform home</source>
        <translation>Plataforma al origen</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="93"/>
        <source>Platform range</source>
        <translation>Recorrido de la plataforma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="129"/>
        <source>Pressing protective foam</source>
        <translation>Apretando la espuma protectora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="98"/>
        <source>Printer calibration</source>
        <translation>Calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="145"/>
        <source>Recording changes</source>
        <translation>Grabando cambios</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="102"/>
        <source>Release foam</source>
        <translation>Liberar la espuma</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="125"/>
        <source>Reset homing profiles</source>
        <translation>Restablecer perfiles de posicionamiento inicial</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="114"/>
        <source>Reset hostname</source>
        <translation>Restablecer nombre de host</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="117"/>
        <source>Reset Network settings</source>
        <translation>Restablecer Ajustes de red</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="119"/>
        <source>Reset NTP state</source>
        <translation>Restablecer el estado de NTP</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="123"/>
        <source>Reset print configuration</source>
        <translation>Restablecer la configuración de impresión</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="142"/>
        <source>Reset printer calibration status</source>
        <translation>Restablecer el estado de calibración de la impresora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="116"/>
        <source>Reset Prusa Connect</source>
        <translation>Reiniciar Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="115"/>
        <source>Reset PrusaLink</source>
        <translation>Reiniciar PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="141"/>
        <source>Reset selftest status</source>
        <translation>Restablecer el estado del autotest</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="120"/>
        <source>Reset system locale</source>
        <translation>Restablecer la configuración regional del sistema</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="118"/>
        <source>Reset timezone</source>
        <translation>Reiniciar zona horaria</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="144"/>
        <source>Resetting hardware counters</source>
        <translation>Reseteando contadores de hardware</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="139"/>
        <source>Reset UI settings</source>
        <translation>Restablecer la configuración de la interfaz de usuario</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="154"/>
        <source>Reset update channel</source>
        <translation>Restablecer canal de actualización</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="104"/>
        <source>Resin sensor</source>
        <translation>Sensor de resina</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="169"/>
        <source>Running</source>
        <translation>En ejecución</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="126"/>
        <source>Send printer data to MQTT</source>
        <translation>Enviar datos de la impresora a MQTT</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="105"/>
        <source>Serial number</source>
        <translation>Número de serie</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="143"/>
        <source>Set new printer model</source>
        <translation>Ajustar nuevo modelo de impresora</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="99"/>
        <source>Sound test</source>
        <translation>Prueba de sonido</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="107"/>
        <source>Tank calib. start</source>
        <translation>Inicio Cal. tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="96"/>
        <source>Tank home</source>
        <translation>Home tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="108"/>
        <location filename="../qml/DelegateWizardCheck.qml" line="138"/>
        <source>Tank level</source>
        <translation>Nivel del tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="95"/>
        <source>Tank range</source>
        <translation>Rango del tanque</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="106"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="110"/>
        <source>Tilt timming</source>
        <translation>Tiempo de inclinación</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="149"/>
        <source>Touching platform down</source>
        <translation>Tocando la plataforma abajo</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="146"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="173"/>
        <source>User action pending</source>
        <translation>Acción del usuario pendiente</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="133"/>
        <source>UV calibrator placed</source>
        <translation>Calibrador UV colocado</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="100"/>
        <source>UV LED</source>
        <translation>LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="101"/>
        <source>UV LED and fans</source>
        <translation>LED UV y ventiladores</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="132"/>
        <source>UV LED warmup</source>
        <translation>Calentamiento LED UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="168"/>
        <source>Waiting</source>
        <translation>Esperando</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="152"/>
        <source>Waiting for user</source>
        <translation>Esperando al usuario</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="137"/>
        <source>Waiting for UV calibrator to be removed</source>
        <translation>Esperando que se retire el calibrador UV</translation>
    </message>
    <message>
        <location filename="../qml/DelegateWizardCheck.qml" line="172"/>
        <source>With Warning</source>
        <translation>Con Advertencia</translation>
    </message>
</context>
<context>
    <name>ErrorPopup</name>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="56"/>
        <source>Understood</source>
        <translation>Entendido</translation>
    </message>
    <message>
        <location filename="../qml/ErrorPopup.qml" line="40"/>
        <source>Unknown error</source>
        <translation>Error desconocido</translation>
    </message>
</context>
<context>
    <name>ErrorcodesText</name>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="150"/>
        <source>A64 OVERHEAT</source>
        <translation>SOBRECALENTAMIENTO A64</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="28"/>
        <source>A64 temperature is too high. Measured: %(temperature).1f °C! Shutting down in 10 seconds...</source>
        <translation>Temperatura del A64 es muy alta. Medida: %(temperature).1f °C! Apagando en 10 segundos...</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="198"/>
        <source>ADMIN NOT AVAILABLE</source>
        <translation>ADMIN NO DISPONIBLE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="234"/>
        <source>AMBIENT TEMP. TOO HIGH</source>
        <translation>TEMP. AMBIENTE DEMASIADO ALTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="235"/>
        <source>AMBIENT TEMP. TOO LOW</source>
        <translation>TEMP. AMBIENTE DEMASIADO BAJA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="101"/>
        <source>Analysis of the project failed</source>
        <translation>Análisis del proyecto fallido</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="65"/>
        <source>Another action is already running. Finish this action directly using the printer&apos;s touchscreen.</source>
        <translation>Ya se está ejecutando otra acción. Termina esta acción directamente usando la pantalla táctil de la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="61"/>
        <source>An unexpected error has occurred.
If print job is in progress, it should be finished.
You can turn the printer off by pressing the front power button.
See the handbook to learn how to save a log file and send it to us.</source>
        <translation>Se ha producido un error inesperado.
Si hay un trabajo de impresión en curso, éste debería haber terminado.
Puedes apagar la impresora pulsando el botón frontal de encendido.
Consulta el manual para saber cómo guardar un archivo de registro y enviárnoslo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="111"/>
        <source>An unknown warning has occured. Restart the printer and try again. Contact our tech support if the problem persists.</source>
        <translation>Se ha producido una advertencia desconocida. Reinicia la impresora y vuelve a intentarlo. Pónte en contacto con nuestro soporte técnico si el problema persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="50"/>
        <source>A part of the LED panel is disconnected.</source>
        <translation>Una parte del panel LED está desconectada.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="170"/>
        <source>BOOSTER BOARD PROBLEM</source>
        <translation>PROBLEMA EN LA PLACA BOOSTER</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="231"/>
        <source>BOOTED SLOT CHANGED</source>
        <translation>RANURA DE ARRANQUE CAMBIADA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="172"/>
        <source>Broken UV LED panel</source>
        <translation>Panel LED UV roto</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="135"/>
        <source>CALIBRATION ERROR</source>
        <translation>ERROR DE CALIBRACIÓN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="191"/>
        <source>CALIBRATION FAILED</source>
        <translation>CALIBRACIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="205"/>
        <source>CALIBRATION LOAD FAILED</source>
        <translation>CARGA DE CALIBRACIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="102"/>
        <source>Calibration project is invalid</source>
        <translation>El proyecto de calibración no es válido</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="224"/>
        <source>CALIBRATION PROJECT IS INVALID</source>
        <translation>EL PROYECTO DE CALIBRACIÓN NO ES VÁLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="40"/>
        <source>Cannot connect to the UV LED calibrator. Check the connection and try again.</source>
        <translation>No se puede conectar al calibrador de LED UV. Verifica la conexión y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="97"/>
        <source>Cannot export profile</source>
        <translation>No se puede exportar el perfil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="77"/>
        <source>Cannot find the selected file!</source>
        <translation>¡No se puede encontrar el archivo seleccionado!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="73"/>
        <source>Cannot get the update channel. Restart the printer and try again.</source>
        <translation>No se puede obtener el canal de actualización. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="96"/>
        <source>Cannot import profile</source>
        <translation>No se puede importar el perfil</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="220"/>
        <source>CANNOT READ PROJECT</source>
        <translation>NO SE PUEDE LEER EL PROYECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="226"/>
        <source>CANNOT REMOVE PROJECT</source>
        <translation>NO SE PUEDE ELIMINAR EL PROYECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="52"/>
        <source>Cannot send factory config to the database (MQTT)! Check the network connection. Please, contact support.</source>
        <translation>No se puede enviar la configuración de fábrica a la base de datos (MQTT)! Verifica la conexión de red. Por favor, contacta con el soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="72"/>
        <source>Cannot set the update channel. Restart the printer and try again.</source>
        <translation>No se puede establecer el canal de actualización. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="236"/>
        <source>CAN&apos;T COPY PROJECT</source>
        <translation>NO SE PUEDE COPIAR EL PROYECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="145"/>
        <source>CLEANING ADAPTOR MISSING</source>
        <translation>FALTA ADAPTADOR LIMPIEZA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="23"/>
        <source>Cleaning adaptor was not detected, it does not seem to be correctly attached to the print platform.
Attach it properly and try again.</source>
        <translation>Adaptador de limpieza no detectado, no parece estar correctamente acoplado a la plataforma de impresión.
Colócalo correctamente e inténtalo de nuevo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="48"/>
        <source>Communication with the Booster board failed.</source>
        <translation>Comunicación con la placa Booster fallida.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="41"/>
        <source>Communication with the UV LED calibrator has failed. Check the connection and try again.</source>
        <translation>Ha fallado la comunicación con el calibrador de LED UV. Verifica la conexión y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="186"/>
        <source>CONFIG FILE READ ERROR</source>
        <translation>ERROR AL LEER ARCHIVO CONFIG</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="176"/>
        <source>CONNECTION FAILED</source>
        <translation>CONEXIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="54"/>
        <source>Connection to Prusa servers failed, please try again later.</source>
        <translation>Falló la conexión con los servidores de Prusa. Vuelve a intentarlo más tarde.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="47"/>
        <source>Correct settings were found, but the standard deviation
(%(found).1f) is greater than the allowed value (%(allowed).1f).
Verify the UV LED calibrator&apos;s position and calibration, then try again.</source>
        <translation>Se encontraron ajustes correctos, pero la desviación estándar
(%(found).1f) es mayor que el valor permitido (%(allowed).1f).
Verifica la posición y la calibración del calibrador de LED UV y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="92"/>
        <source>Data is from unknown UV LED sensor!</source>
        <translation>¡Datos de un sensor UV desconocido!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="211"/>
        <source>DATA OVERWRITE FAILED</source>
        <translation>SOBRESCRITURA DE DATOS FALLLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="206"/>
        <source>DATA PREPARATION FAILURE</source>
        <translation>FALLO EN LA PREPARACIÓN DE DATOS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="227"/>
        <source>DIRECTORY NOT EMPTY</source>
        <translation>DIRECTORIO NO VACÍO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="171"/>
        <source>Disconnected UV LED panel</source>
        <translation>Panel LED UV desconectado</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="212"/>
        <source>DISPLAY TEST ERROR</source>
        <translation>ERROR TEST PANTALLA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="18"/>
        <source>Display test failed.</source>
        <translation>Test de pantalla fallido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="140"/>
        <source>DISPLAY TEST FAILED</source>
        <translation>TEST DE PANTALLA FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="216"/>
        <source>Display usage error</source>
        <translation>Error uso de pantalla</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="177"/>
        <source>DOWNLOAD FAILED</source>
        <translation>DESCARGA FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="90"/>
        <source>Error displaying test image.</source>
        <translation>Error al mostrar la imagen de prueba.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="67"/>
        <source>Error, there is no file to reprint.</source>
        <translation>Error, no hay archivo para reimprimir.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="82"/>
        <source>Examples (any projects) are missing in the user storage. Redownload them from the &apos;Settings&apos; menu.</source>
        <translation>Faltan ejemplos (cualquier proyecto) en el almacenamiento del usuario. Vuelve a descargarlos desde el menú &apos;Ajustes&apos;.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="246"/>
        <source>EXPECT OVERHEATING</source>
        <translation>SE ESPERA SOBRECALENTAMIENTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="107"/>
        <source>Exposure screen that is currently connected has already been used on this printer. This screen was last used for approximately %(counter_h)d hours.

If you do not want to use this screen: turn the printer off, replace the screen and turn the printer back on.</source>
        <translation>La pantalla de exposición que está actualmente conectada ya se ha utilizado en esta impresora. Esta pantalla se utilizó por última vez durante aproximadamente %(counter_h)d horas.

Si no desea utilizar esta pantalla: apaga la impresora, cambia la pantalla y vuelve a encender la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="88"/>
        <source>Failed to change the log level (detail). Restart the printer and try again.</source>
        <translation>No se pudo cambiar el nivel de registro (detalle). Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="83"/>
        <source>Failed to load fans and LEDs factory calibration.</source>
        <translation>Falló la carga de la calibración de fábrica de ventiladores y  de LEDS.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="15"/>
        <source>Failed to reach the tilt endstop.</source>
        <translation>Fallo al alcanzar el endstop de inclinación.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="14"/>
        <source>Failed to reach the tower endstop.</source>
        <translation>Fallo al alcanzar el endstop de la torre.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="64"/>
        <source>Failed to read the configuration file. Try to reset the printer. If the problem persists, contact our support.</source>
        <translation>No se pudo leer el archivo de configuración. Intenta reiniciar la impresora. Si el problema persiste, contacta con nuestro soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="85"/>
        <source>Failed to save Wizard data. Restart the printer and try again.</source>
        <translation>No se pudieron guardar los datos del Asistente. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="84"/>
        <source>Failed to serialize Wizard data. Restart the printer and try again.</source>
        <translation>Error al serializar los datos del Asistente. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="95"/>
        <source>Failed to set hostname</source>
        <translation>Fallo al establecer nombre de host</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="132"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="146"/>
        <source>FAN FAILURE</source>
        <translation>FALLO DEL VENTILADOR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="142"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="147"/>
        <source>FAN RPM OUT OF TEST RANGE</source>
        <translation>RPM DEL VENTILADOR FUERA DEL RANGO DE PRUEBA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="245"/>
        <source>FAN WARNING</source>
        <translation>AVISO DEL VENTILADOR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="201"/>
        <source>FILE ALREADY EXISTS</source>
        <translation>EL ARCHIVO YA EXISTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="79"/>
        <source>File already exists! Delete it in the printer first and try again.</source>
        <translation>¡El archivo ya existe! Bórralo en la impresora primero y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="78"/>
        <source>File has an invalid extension! See the article for supported file extensions.</source>
        <translation>¡El archivo tiene una extensión no válida! Consulta el artículo para conocer las extensiones de archivo compatibles.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="199"/>
        <source>FILE NOT FOUND</source>
        <translation>ARCHIVO NO ENCOTRADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="247"/>
        <source>FILL THE RESIN</source>
        <translation>RELLENAR LA RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="215"/>
        <source>FIRMWARE UPDATE FAILED</source>
        <translation>ACTUALIZACIÓN DE FIRMWARE FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="217"/>
        <source>HOSTNAME ERROR</source>
        <translation>ERROR DE NOMBRE DE HOST</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="62"/>
        <source>Image preloader did not finish successfully!</source>
        <translation>¡La precarga de imágenes no se completó correctamente!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="237"/>
        <source>INCORRECT PRINTER MODEL</source>
        <translation>MODELO DE IMPRESORA INCORRECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="10"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="123"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan.</source>
        <translation>Lectura incorrecta de las RPM del ventilador %(failed_fans_text)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="124"/>
        <source>Incorrect RPM reading of the %(failed_fans_text)s fan. The print may continue, however, there&apos;s a risk of overheating.</source>
        <translation>Lectura incorrecta de las RPM del ventilador %(failed_fans_text)s. La impresión puede continuar, sin embargo, hay riesgo de sobrecalentamiento.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="24"/>
        <source>Incorrect RPM reading of the %(fan__map_HardwareDeviceId)s.</source>
        <translation>Lectura incorrecta de las RPM de %(fan__map_HardwareDeviceId)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="188"/>
        <source>INTERNAL ERROR</source>
        <translation>ERROR INTERNO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="66"/>
        <source>Internal error (DBUS mapping failed), restart the printer. Contact support if the problem persists.</source>
        <translation>Error interno (Error en el mapeo DBUS), reinicia la impresora. Pónte en contacto con nuestro soporte técnico si el problema persiste.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="197"/>
        <source>INTERNAL MEMORY FULL</source>
        <translation>MEMORIA INTERNA LLENA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="75"/>
        <source>Internal memory is full. Delete some of your projects first.</source>
        <translation>La memoria interna está llena. Primero elimina algunos de sus proyectos.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="178"/>
        <source>INVALID API KEY</source>
        <translation>CLAVE API INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="200"/>
        <source>INVALID FILE EXTENSION</source>
        <translation>EXTENSIÓN DE ARCHIVO INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="181"/>
        <source>INVALID PASSWORD</source>
        <translation>CONTRASEÑA INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="202"/>
        <source>INVALID PROJECT</source>
        <translation>PROYECTO INVÁLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="19"/>
        <source>Invalid tilt alignment position.</source>
        <translation>Posición de alineación de inclinación inválida.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="141"/>
        <source>INVALID TILT ALIGN POSITION</source>
        <translation>POSICIÓN DE ALINEACIÓN DE INCLINACIÓN INVÁLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="228"/>
        <source>LANGUAGE NOT SET</source>
        <translation>IDIOMA NO ESTABLECIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="241"/>
        <source>MASK NOAVAIL WARNING</source>
        <translation>AVISO MASK NOAVAIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="155"/>
        <source>MC WRONG REVISION</source>
        <translation>REVISIÓN INCORRECTA MC</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="122"/>
        <source>Measured resin volume is too low. The print can continue, however, a refill might be required.</source>
        <translation>El volumen de resina medido es demasiado bajo. La impresión puede continuar, sin embargo, es posible que se requiera una recarga.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="12"/>
        <source>Measured resin volume %(volume_ml)d ml is higher than required for this print. Make sure that the resin level does not exceed the 100% mark and restart the print.</source>
        <translation>El volumen de resina medido %(volume_ml)d ml es mayor de lo necesario para esta impresión. Asegúrate de que el nivel de resina no supere la marca del 100% y reinicia la impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="11"/>
        <source>Measured resin volume %(volume_ml)d ml is lower than required for this print. Refill the tank and restart the print.</source>
        <translation>El volumen de resina medido %(volume_ml)d ml es menor de lo necesario para esta impresión. Rellena el tanque y reinicia la impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="22"/>
        <source>Measuring the resin failed. Check the presence of the platform and the amount of resin in the tank.</source>
        <translation>La medición de la resina ha fallado. Verifica la presencia de la plataforma y la cantidad de resina en el tanque.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="9"/>
        <source>Moving the tilt failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Fallo al mover la inclinación. Asegúrate de que no hay ningún obstáculo en su camino y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="8"/>
        <source>Moving the tower failed. Make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Fallo al mover la torre. Asegúrate de que no hay ningún obstáculo en su camino y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="174"/>
        <source>MQTT UPLOAD FAILED</source>
        <translation>FALLO EN LA CARGA DE MQTT</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="91"/>
        <source>No calibration data to show!</source>
        <translation>No hay datos de calibración para mostrar!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="94"/>
        <source>No display usage data to show</source>
        <translation>No hay datos de uso de la pantalla para mostrar</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="189"/>
        <source>NO FILE TO REPRINT</source>
        <translation>NO HAY ARCHIVO PARA REIMPRIMIR</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="175"/>
        <source>NO INTERNET CONNECTION</source>
        <translation>SIN CONEXIÓN A INTERNET</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="60"/>
        <source>No problem detected. You can continue using the printer.</source>
        <translation>No se detectó ningún problema. Puedes seguir usando la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="238"/>
        <source>NOT ENOUGH RESIN</source>
        <translation>NO HAY SUFICIENTE RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="221"/>
        <source>NOT ENOUGHT LAYERS</source>
        <translation>NO HAY SUFICIENTES CAPAS</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="87"/>
        <source>No USB storage present</source>
        <translation>Almac. USB no presente</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="213"/>
        <source>NO UV CALIBRATION DATA</source>
        <translation>SIN DATOS DE CALIBRACIÓN UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="232"/>
        <source>NO WARNING</source>
        <translation>SIN ADVERTENCIA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="242"/>
        <source>OBJECT CROPPED WARNING</source>
        <translation>ADVERTENCIA DE OBJETO RECORTADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="120"/>
        <source>Object was cropped because it does not fit the print area.</source>
        <translation>La pieza se recortó porque no se ajusta al área de impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="229"/>
        <source>OLD EXPO PANEL</source>
        <translation>PANEL EXPO ANTIGUO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="185"/>
        <source>OPENING PROJECT FAILED</source>
        <translation>APERTURA DEL PROYECTO FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="100"/>
        <source>Opening the project failed. The file is corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>No se puede abrir el proyecto. El archivo está dañado. Vuelve a laminar o exportar el proyecto y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="98"/>
        <source>Opening the project failed. The file is possibly corrupted. Please re-slice or re-export the project and try again.</source>
        <translation>No se pudo abrir el proyecto. Es posible que el archivo esté dañado. Relamina o reexporta el proyecto, vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="63"/>
        <source>Opening the project failed, the file may be corrupted. Re-slice or re-export the project and try again.</source>
        <translation>No se pudo abrir el proyecto, el archivo puede estar corrupto. Relamina o reexporta el proyecto, vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="239"/>
        <source>PARAMETERS OUT OF RANGE</source>
        <translation>PARÁMETROS FUERA DE RANGO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="240"/>
        <source>PERPARTES NOAVAIL WARNING</source>
        <translation>AVISO NO PERMITIDO PERPARTES</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="118"/>
        <source>Per-partes print not available.</source>
        <translation>Impresión Per Partes no disponible.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="56"/>
        <source>Please turn on the HTTP digest (which is the recommended security option) or update the API key. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>Activa el HTTP Digest (que es la opción de seguridad recomendada) o actualiza la clave API. Puedes encontrarlo en Configuración&gt; Red&gt; Credenciales de inicio de sesión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="125"/>
        <source>Pour enough resin for the selected file into the tank and close the lid. The minimal amount of the resin is displayed on the touchscreen.</source>
        <translation>Vierte suficiente resina para el archivo seleccionado en el depósito y cierra la tapa. La cantidad mínima de resina se muestra en la pantalla táctil.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="184"/>
        <source>PRELOAD FAILED</source>
        <translation>PRECARGA FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="187"/>
        <source>PRINTER IS BUSY</source>
        <translation>LA IMPRESORA ESTÁ OCUPADA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="182"/>
        <source>PRINTER IS OK</source>
        <translation>LA IMPRESORA ESTÁ BIEN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="158"/>
        <source>PRINTER NOT UV CALIBRATED</source>
        <translation>UV DE IMPRESORA NO CALIBRADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="243"/>
        <source>PRINTER VARIANT MISMATCH WARNING</source>
        <translation>ADVERTENCIA DE DISCAPACIDAD DE VARIANTE DE IMPRESORA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="204"/>
        <source>PRINT EXAMPLES MISSING</source>
        <translation>FALTAN LOS EJEMPLOS DE IMPRESIÓN</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="196"/>
        <source>PRINT JOB CANCELLED</source>
        <translation>TRABAJO DE IMPRESIÓN CANCELADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="119"/>
        <source>Print mask is missing.</source>
        <translation>Falta la máscara de impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="219"/>
        <source>PROFILE EXPORT ERROR</source>
        <translation>ERROR DE EXPORTACIÓN DE PERFIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="218"/>
        <source>PROFILE IMPORT ERROR</source>
        <translation>ERROR DE IMPORTACIÓN DE PERFIL</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="223"/>
        <source>PROJECT ANALYSIS FAILED</source>
        <translation>ANÁLISIS DE PROYECTO FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="222"/>
        <source>PROJECT IS CORRUPTED</source>
        <translation>PROYECTO CORRUPTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="32"/>
        <source>Reading of %(sensor__map_HardwareDeviceId)s not in range!

Measured temperature: %(temperature).1f °C, allowed range: %(min)d - %(max)d °C.

Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>¡La lectura de %(sensor__map_HardwareDeviceId)s no está en rango!

Temperatura medida: %(temperature).1f °C, rango permitido: %(min)d - %(max)d °C.

Mantén la impresora alejada de la luz solar directa y a temperatura ambiente. (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="30"/>
        <source>Reading of UV LED temperature has failed! This value is essential for the UV LED lifespan and printer safety. Please contact tech support! Current print job will be canceled.</source>
        <translation>¡La lectura de la temperatura del LED UV ha fallado! Este valor es esencial para la vida útil de los LED UV y la seguridad de la impresora. Ponte en contacto con el soporte técnico. Se cancelará el trabajo de impresión actual.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="180"/>
        <source>REMOTE API ERROR</source>
        <translation>ERROR API REMOTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="104"/>
        <source>Removing this project is not possible. The project is locked by a print job.</source>
        <translation>No es posible eliminar este proyecto. El proyecto está bloqueado por un trabajo de impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="46"/>
        <source>Requested intensity cannot be reached by max. allowed PWM.</source>
        <translation>La intensidad solicitada no se puede alcanzar con el PWM max. permitido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="45"/>
        <source>Requested intensity cannot be reached by min. allowed PWM.</source>
        <translation>La intensidad solicitada no se puede alcanzar con el PWM min. permitido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="244"/>
        <source>RESIN LOW</source>
        <translation>POCA RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="144"/>
        <source>RESIN MEASURING FAILED</source>
        <translation>MEDICIÓN DE RESINA FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="157"/>
        <source>RESIN SENSOR ERROR</source>
        <translation>ERROR SENSOR DE RESINA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="134"/>
        <source>RESIN TOO HIGH</source>
        <translation>NIVEL DE RESINA MUY ALTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="133"/>
        <source>RESIN TOO LOW</source>
        <translation>NVEL DE RESINA MUY BAJO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="25"/>
        <source>RPM of %(fan__map_HardwareDeviceId)s not in range!

RPM data: %(min_rpm)d - %(max_rpm)d
Average: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</source>
        <translation>¡RPM de %(fan__map_HardwareDeviceId)s no está en rango!

Datos RPM: %(min_rpm)d - %(max_rpm)d
Media: %(avg_rpm)d (%(lower_bound_rpm)d - %(upper_bound_rpm)d), error: %(error)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="20"/>
        <source>RPM of %(fan)s not in range!
RPM data: %(rpm)s
Average: %(avg)s</source>
        <translation>¡No están en rango las RPM del %(fan)s!
Datos de RPM: %(rpm)s
Media: %(avg)s</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="89"/>
        <source>Saving the new factory default value failed. Restart the printer and try again.</source>
        <translation>No se pudo guardar el nuevo valor predeterminado de fábrica. Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="29"/>
        <source>%(sensor)s not in range! Measured temperature: %(temperature).1f °C. Keep the printer out of direct sunlight at room temperature (18 - 32 °C).</source>
        <translation>¡%(sensor)s no está en rango! Temperatura medida: %(temperature).1f °C. Mantén la impresora alejada de la luz solar directa a temperatura ambiente. (18 - 32 °C).</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="208"/>
        <source>SERIAL NUMBER ERROR</source>
        <translation>ERROR NÚMERO DE SERIE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="86"/>
        <source>Serial numbers in wrong format! A64: %(a64)s MC: %(mc)s Please contact tech support!</source>
        <translation>¡Números de serie en formato incorrecto! A64: %(a64)s MC: %(mc)s ¡Ponte en contacto con el soporte técnico!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="210"/>
        <source>SETTING LOG DETAIL FAILED</source>
        <translation>AJUSTE DEL REGISTRO DETALLADO FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="194"/>
        <source>SETTING UPDATE CHANNEL FAILED</source>
        <translation>AJUSTE DEL CANAL ACTUALIZACIÓN FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="108"/>
        <source>Something went wrong with the printer firmware. Please contact our support and do not forget to attach the logs.
Crashed services are immediately started again. If, despite that, the printer does not behave correctly, try restarting it. </source>
        <translation>Algo ha ido mal con el firmware de la impresora. Ponte en contacto con nuestro soporte técnico y no olvides adjuntar los registros.
Los servicios caídos se reinician inmediatamente. Si, a pesar de ello, la impresora no se comporta correctamente, prueba a reiniciarla. </translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="38"/>
        <source>Speaker test failed.</source>
        <translation>Test altavoz fallido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="160"/>
        <source>SPEAKER TEST FAILED</source>
        <translation>TEST ALTAVOZ FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="230"/>
        <source>SYSTEM SERVICE CRASHED</source>
        <translation>SERVICIO DEL SISTEMA CAÍDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="151"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="154"/>
        <source>TEMPERATURE OUT OF RANGE</source>
        <translation>TEMPERATURA FUERA DE RANGO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="148"/>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="153"/>
        <source>TEMPERATURE SENSOR FAILED</source>
        <translation>SENSOR DE TEMPERATURA FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="76"/>
        <source>The admin menu is not available.</source>
        <translation>El menú admin no está disponible.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="112"/>
        <source>The ambient temperature is too high, the print can continue, but it might fail.</source>
        <translation>La temperatura ambiente es demasiado alta, la impresión puede continuar, pero puede fallar.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="113"/>
        <source>The ambient temperature is too low, the print can continue, but it might fail.</source>
        <translation>La temperatura ambiente es demasiado baja, la impresión puede continuar, pero puede fallar.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="116"/>
        <source>The amount of resin in the tank is not enough for the current project. Adding more resin will be required during the print.</source>
        <translation>La cantidad de resina en el tanque no es suficiente para el proyecto actual. Será necesario añadir más resina durante la impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="70"/>
        <source>The automatic UV LED calibration did not finish successfully! Run the calibration again.</source>
        <translation>¡La calibración automática de los LED UV no se completó correctamente! Ejecuta la calibración nuevamente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="69"/>
        <source>The calibration did not finish successfully! Run the calibration again.</source>
        <translation>¡La calibración no finalizó correctamente! Ejecuta la calibración nuevamente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="105"/>
        <source>The directory is not empty.</source>
        <translation>El directorio no está vacío.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="55"/>
        <source>The download failed. Check the connection to the internet and try again.</source>
        <translation>La descarga falló. Verifica la conexión a Internet y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="114"/>
        <source>The internal memory is full, project cannot be copied. You can continue printing. However, you must not remove the USB drive during the print, otherwise the process will fail.</source>
        <translation>La memoria interna está llena, el proyecto no se puede copiar. Puedes seguir imprimiendo. Sin embargo, no debes quitar la unidad USB durante la impresión, de lo contrario, el proceso fallará.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="106"/>
        <source>The language is not set. Go to Settings -&gt; Language &amp; Time -&gt; Set Language and pick preferred language.</source>
        <translation>No se ha establecido el idioma. Ve a Ajustes -&gt; Idioma &amp; Hora -&gt; Establecer Idioma y escoje tu idioma preferido.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="115"/>
        <source>The model was sliced for a different printer model. Reslice the model using the correct settings.</source>
        <translation>El modelo fue laminado para un modelo de impresora diferente. Relamina el modelo usando la configuración correcta.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="121"/>
        <source>The model was sliced for a different printer variant %(project_variant)s. Your printer variant is %(printer_variant)s.</source>
        <translation>El modelo fue lamiando para una variante de impresora diferente %(project_variant)s. Tu variante de impresora es %(printer_variant)s.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="34"/>
        <source>The Motion Controller (MC) has encountered an unexpected error. Restart the printer.</source>
        <translation>El Motion Controller (MC) ha encontrado un error inesperado. Reinicia la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="59"/>
        <source>The password is incorrect. Please check or update it in:

Settings &gt; Network &gt; PrusaLink.
Prusa Slicer must use HTTP digest authorization type.

The password must be at least 8 characters long. Supported characters are letters, numbers, and dash.</source>
        <translation>La contraseña es incorrecta. Por favor, compruébala o actualízala en:

Configuración &gt; Red &gt; PrusaLink.
Prusa Slicer debe usar el tipo de autorización HTTP digest.

La contraseña debe tener al menos 8 caracteres. Los caracteres admitidos son letras, números y guiones.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="109"/>
        <source>The printer has booted from an alternative slot due to failed boot attempts using the primary slot.
Update the printer with up-to-date firmware ASAP to recover the primary slot.
This usually happens after a failed update, or due to a hardware failure. Printer settings may have been reset.</source>
        <translation>La impresora se ha iniciado desde una ranura alternativa debido a intentos fallidos de inicio utilizando la ranura principal.
Actualiza la impresora con firmware actualizado lo antes posible para recuperar la ranura principal.
Esto suele ocurrir después de una actualización fallida o debido a un fallo de hardware. Es posible que se hayan restablecido los ajustes de la impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="13"/>
        <source>The printer is not calibrated. Please run the Wizard first.</source>
        <translation>La impresora no está calibrada. Primero ejecuta el Asistente.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="53"/>
        <source>The printer is not connected to the internet. Check the connection in the Settings.</source>
        <translation>La impresora no está conectada a Internet. Verifica la conexión en los Ajustes.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="36"/>
        <source>The printer is not UV calibrated. Connect the UV calibrator and complete the calibration.</source>
        <translation>La impresora no está calibrada con UV. Conecta el calibrador de UV y completa la calibración.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="51"/>
        <source>The printer model was not detected.</source>
        <translation>No se detectó el modelo de impresora.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="57"/>
        <source>The printer uses HTTP digest security. Please enable it also in your slicer (recommended), or turn off this security option in the printer. You can find it in Settings &gt; Network &gt; Login credentials.</source>
        <translation>La impresora utiliza la seguridad de HTTP Digest. Habilítala también en tu programa de laminado (recomendado) o desactiva esta opción de seguridad en la impresora. Puedes encontrarla en Configuración&gt; Red&gt; Credenciales de inicio de sesión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="74"/>
        <source>The print job cancelled by the user.</source>
        <translation>El trabajo de impresión cancelado por el usuario.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="117"/>
        <source>The print parameters are out of range of the printer, the system can try to fix the project. Proceed?</source>
        <translation>Los parámetros de impresión están fuera del rango de la impresora, el sistema puede intentar arreglar el proyecto. ¿Continuar?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="80"/>
        <source>The project file is invalid!</source>
        <translation>¡El archivo del proyecto no es válido!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="99"/>
        <source>The project must have at least one layer</source>
        <translation>El proyecto debe tener al menos una capa</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="110"/>
        <source>There is no warning</source>
        <translation>No hay ninguna advertencia</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="35"/>
        <source>The resin sensor was not triggered. Check whether the tank and the platform are properly secured.</source>
        <translation>El sensor de resina no se activó. Comprueba que el tanque y la plataforma están bien asegurados.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="31"/>
        <source>The %(sensor__map_HardwareDeviceId)s sensor failed.</source>
        <translation>El sensor %(sensor__map_HardwareDeviceId)s ha fallado.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="26"/>
        <source>The %(sensor)s sensor failed.</source>
        <translation>El sensor %(sensor)s ha fallado.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="93"/>
        <source>The update of the firmware failed! Restart the printer and try again.</source>
        <translation>¡Falló la actualización del firmware! Reinicia la impresora y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="42"/>
        <source>The UV LED calibrator detected some light on a dark display. This means there is a light &apos;leak&apos; under the UV calibrator, or your display does not block the UV light enough. Check the UV calibrator placement on the screen or replace the exposure display.</source>
        <translation>El calibrador de LED UV detectó algo de luz en una pantalla oscura. Esto significa que hay una &apos;fuga&apos; de luz debajo del calibrador UV o que tu pantalla no bloquea la luz UV lo suficiente. Verifica la ubicación del calibrador UV en la pantalla o reemplaza la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="43"/>
        <source>The UV LED calibrator failed to read expected UV light intensity. Check the UV calibrator placement on the screen.</source>
        <translation>El calibrador de LED UV no pudo leer la intensidad de luz UV esperada. Verifica la ubicación del calibrador UV en la pantalla.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="39"/>
        <source>The UV LED calibrator is not detected. Check the connection and try again.</source>
        <translation>No se detecta el calibrador de LED UV. Verifica la conexión y vuelve a intentarlo.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="49"/>
        <source>The UV LED panel is not detected.</source>
        <translation>No se detecta el panel LED UV.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="68"/>
        <source>The wizard did not finish successfully!</source>
        <translation>¡El Asistente no terminó correctamente!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="103"/>
        <source>This project was prepared for a different printer</source>
        <translation>Este proyecto se preparó para una impresora diferente</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="58"/>
        <source>This request is not compatible with the Prusa remote API. See our documentation for more details.</source>
        <translation>Esta solicitud no es compatible con la API remota de Prusa. Consulta nuestra documentación para obtener más detalles.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="81"/>
        <source>This Wizard cannot be canceled, finish the steps first.</source>
        <translation>Este Asistente no puede ser cancelado, finaliza los pasos primero.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="139"/>
        <source>TILT AXIS CHECK FAILED</source>
        <translation>COMPROBACIÓN EJE INCLINACIÓN FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="17"/>
        <source>Tilt axis check failed!

Current position: %(position)d steps</source>
        <translation>¡La comprobación del eje de inclinación falló!

Posición actual: %(position)d pasos</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="137"/>
        <source>TILT ENDSTOP NOT REACHED</source>
        <translation>ENDSTOP INCLINACIÓN NO ALCANZADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="128"/>
        <source>TILT HOMING FAILED</source>
        <translation>HOMING INCLINACIÓN FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="6"/>
        <source>Tilt homing failed, check its surroundings and repeat the action.</source>
        <translation>Homing de la inclinación fallido, verifica sus alrededores y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="131"/>
        <source>TILT MOVING FAILED</source>
        <translation>MOVIMIENTO INCLINACIÓN FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="138"/>
        <source>TOWER AXIS CHECK FAILED</source>
        <translation>COMPROBACIÓN EJE TORRE FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="16"/>
        <source>Tower axis check failed!

Current position: %(position_nm)d nm

Check if the ballscrew can move smoothly in its entire range.</source>
        <translation>¡La comprobación del eje de la torre falló!

Posición actual: %(position_nm)d nm

Comprueba si el husillo de bolas puede moverse suavemente en todo su rango.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="136"/>
        <source>TOWER ENDSTOP NOT REACHED</source>
        <translation>ENDSTOP TORRE NO ALCANZADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="129"/>
        <source>TOWER HOMING FAILED</source>
        <translation>HOMING TORRE FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="7"/>
        <source>Tower homing failed, make sure there is no obstacle in its path and repeat the action.</source>
        <translation>Homing de la torre fallido, asegúrate de que no hay ningún obstáculo en su camino y repite la acción.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="130"/>
        <source>TOWER MOVING FAILED</source>
        <translation>MOVIMIENTO TORRE FALLIDO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="21"/>
        <source>Tower not at the expected position.

Are the platform and tank mounted and secured correctly?</source>
        <translation>La torre no está en la posición esperada.

¿Están la plataforma y el tanque montados y asegurados correctamente?</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="143"/>
        <source>TOWER POSITION ERROR</source>
        <translation>ERROR POSICIÓN TORRE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="179"/>
        <source>UNAUTHORIZED</source>
        <translation>NO AUTORIZADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="183"/>
        <source>UNEXPECTED ERROR</source>
        <translation>ERROR INESPERADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="156"/>
        <source>UNEXPECTED MC ERROR</source>
        <translation>ERROR MC INESPERADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="173"/>
        <source>Unknown printer model</source>
        <translation>Modelo de impresora desconocido</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="44"/>
        <source>Unknown UV LED calibrator error code: %(nonprusa_code)d</source>
        <translation>Código de error desconocido del calibrador de LED UV: %(nonprusa_code)d</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="233"/>
        <source>UNKNOWN WARNING</source>
        <translation>ADVERTENCIA DESCONOCIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="195"/>
        <source>UPDATE CHANNEL FAILED</source>
        <translation>ACTUALIZACIÓN DE CANAL FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="209"/>
        <source>USB DRIVE NOT DETECTED</source>
        <translation>UNIDAD USB NO DETECTADA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="169"/>
        <source>UV CALIBRATION ERROR</source>
        <translation>ERROR DE CALIBRACIÓN UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="192"/>
        <source>UV CALIBRATION FAILED</source>
        <translation>CALIBRACIÓN UV FALLIDA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="214"/>
        <source>UV DATA EROR</source>
        <translation>ERROR DE DATOS UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="193"/>
        <source>UV INTENSITY ERROR</source>
        <translation>ERROR DE INTENSIDAD UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="71"/>
        <source>UV intensity not set. Please run the UV calibration before starting a print.</source>
        <translation>Intensidad UV no ajustada. Ejecuta la calibración UV antes de comenzar una impresión.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="167"/>
        <source>UV INTENSITY TOO HIGH</source>
        <translation>INTENSIDAD UV DEMASIADO ALTA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="168"/>
        <source>UV INTENSITY TOO LOW</source>
        <translation>INTENSIDAD UV DEMASIADO BAJA</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="162"/>
        <source>UV LED CALIBRATOR CONNECTION ERROR</source>
        <translation>ERROR DE CONEXIÓN CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="164"/>
        <source>UV LED CALIBRATOR ERROR</source>
        <translation>ERROR DE CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="163"/>
        <source>UV LED CALIBRATOR LINK ERROR</source>
        <translation>ERROR DE ENLACE DEL CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="161"/>
        <source>UV LED CALIBRATOR NOT DETECTED</source>
        <translation>CALIBRADOR LED UV NO DETECTADO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="165"/>
        <source>UV LED CALIBRATOR READINGS ERROR</source>
        <translation>ERROR DE LECTURAS DEL CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="166"/>
        <source>UV LED CALIBRATOR UNKNONW ERROR</source>
        <translation>ERROR DESCONOCIDO DEL CALIBRADOR LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="149"/>
        <source>UVLED HEAT SINK FAILED</source>
        <translation>FALLO DISIPADOR DE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="27"/>
        <source>UV LED is overheating!</source>
        <translation>¡El LED UV se está sobrecalentando!</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="152"/>
        <source>UV LED TEMP. ERROR</source>
        <translation>ERROR TEMP. LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="159"/>
        <source>UVLED VOLTAGE ERROR</source>
        <translation>ERROR DE VOLTAJE DE LED UV</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="37"/>
        <source>UV LED voltages differ too much. The LED module might be faulty. Contact our support.</source>
        <translation>Los voltajes de los LED UV difieren demasiado. El módulo LED puede estar defectuoso. Ponte en contacto con nuestro soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="207"/>
        <source>WIZARD DATA FAILURE</source>
        <translation>FALLO DE DATOS DEL ASISTENTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="190"/>
        <source>WIZARD FAILED</source>
        <translation>FALLO DEL ASISTENTE</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="225"/>
        <source>WRONG PRINTER MODEL</source>
        <translation>MODELO DE IMPRESORA INCORRECTO</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="33"/>
        <source>Wrong revision of the Motion Controller (MC). Contact our support.</source>
        <translation>Revisión incorrecta del controlador de movimiento (MC). Ponte en contacto con nuestro soporte.</translation>
    </message>
    <message>
        <location filename="../build/3rdparty/sl1-errors/ErrorcodesText.qml" line="203"/>
        <source>YOU SHALL NOT PASS</source>
        <translation>NO PASARÁS</translation>
    </message>
</context>
<context>
    <name>NotificationProgressDelegate</name>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="110"/>
        <source>Already exists</source>
        <translation>Ya existe</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="114"/>
        <source>Can&apos;t read</source>
        <translation>No se puede leer</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="96"/>
        <source>Error: %1</source>
        <translation>Error: %1</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="108"/>
        <source>File not found</source>
        <translation>Archivo no encontrado</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="112"/>
        <source>Invalid extension</source>
        <translation>Extensión inválida</translation>
    </message>
    <message>
        <location filename="../qml/NotificationProgressDelegate.qml" line="106"/>
        <source>Storage full</source>
        <translation>Almacenamiento lleno</translation>
    </message>
</context>
<context>
    <name>PageAPSettings</name>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="34"/>
        <source>Hotspot</source>
        <translation>Punto de acceso</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="149"/>
        <source>Inactive</source>
        <translation>Inactivo</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="175"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="247"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="232"/>
        <source>Security</source>
        <translation>Seguridad</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="216"/>
        <source>Show Password</source>
        <translation>Ver Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="153"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="253"/>
        <source>SSID must not be empty</source>
        <translation>El SSID no debe estar vacío</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="265"/>
        <source>Start AP</source>
        <translation>Comenzar AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="138"/>
        <source>State:</source>
        <translation>Estado:</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="68"/>
        <source>Stop AP</source>
        <translation>Paro AP</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="296"/>
        <source>Swipe for QRCode</source>
        <translation>Desliza para Código QR</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="377"/>
        <source>Swipe for settings</source>
        <translation>Desliza para ajustes</translation>
    </message>
    <message>
        <location filename="../qml/PageAPSettings.qml" line="51"/>
        <source>(unchanged)</source>
        <translation>(sin cambios)</translation>
    </message>
</context>
<context>
    <name>PageAbout</name>
    <message>
        <location filename="../qml/PageAbout.qml" line="23"/>
        <source>About Us</source>
        <translation>Sobre Nosotros</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="52"/>
        <source>Prusa Research a.s.</source>
        <translation>Prusa Research a.s.</translation>
    </message>
    <message>
        <location filename="../qml/PageAbout.qml" line="48"/>
        <source>To find out more about us please scan the QR code or use the link below:</source>
        <translation>Para obtener más información sobre nosotros, escanea el código QR o utiliza el siguiente enlace:</translation>
    </message>
</context>
<context>
    <name>PageAddWifiNetwork</name>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="31"/>
        <source>Add Hidden Network</source>
        <translation>Añadir Red Oculta</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="182"/>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="84"/>
        <source>PASS</source>
        <translation>CORRECTO</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="163"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="147"/>
        <source>Security</source>
        <translation>Seguridad</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="129"/>
        <source>Show Password</source>
        <translation>Ver Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageAddWifiNetwork.qml" line="169"/>
        <source>SSID must not be empty.</source>
        <translation>El SSID no debe estar vacío.</translation>
    </message>
</context>
<context>
    <name>PageAdminAPI</name>
    <message>
        <location filename="../qml/PageAdminAPI.qml" line="40"/>
        <source>Admin API</source>
        <translation>Admin API</translation>
    </message>
</context>
<context>
    <name>PageAskForPassword</name>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="156"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="168"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="82"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="64"/>
        <source>Please, enter the correct password for network &quot;%1&quot;</source>
        <translation>Por favor, introduce la contraseña correcta para la red &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="194"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="128"/>
        <source>Show Password</source>
        <translation>Ver Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageAskForPassword.qml" line="92"/>
        <source>(unchanged)</source>
        <translation>(sin cambios)</translation>
    </message>
</context>
<context>
    <name>PageBasicWizard</name>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="56"/>
        <source>Are you sure?</source>
        <translation>¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="110"/>
        <source>Can you see the company logo on the exposure display through the cover?</source>
        <translation>¿Puedes ver el logotipo de la empresa en la pantalla de exposición a través de la tapa naranja?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="126"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Despega con cuidado el adhesivo de protección de la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="174"/>
        <source>Close the cover.</source>
        <translation>Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="173"/>
        <source>Cover</source>
        <translation>Tapa</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="57"/>
        <source>Do you really want to cancel the wizard?</source>
        <translation>¿Realmente deseas cancelar el asistente?</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="114"/>
        <source>Once you place the paper inside the printer, do not forget to CLOSE THE COVER!</source>
        <translation>Una vez que coloques el papel dentro de la impresora, ¡no olvides cerrar la tapa!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="59"/>
        <source>The machine will not work without a completed wizard procedure.</source>
        <translation>La impresora no funcionará sin completar el procedimiento del Asistente.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="112"/>
        <source>Tip: If you can&apos;t see the logo clearly, try placing a sheet of paper onto the screen.</source>
        <translation>Consejo: si no puedes ver el logotipo con claridad, intenta colocar una hoja de papel en la pantalla.</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="34"/>
        <source>Wizard</source>
        <translation>Asistente</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="154"/>
        <source>Wizard canceled</source>
        <translation>Asistente cancelado</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="145"/>
        <source>Wizard failed</source>
        <translation>Fallo del asistente</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="136"/>
        <source>Wizard finished sucessfuly!</source>
        <translation>El asistente finalizó con éxito!</translation>
    </message>
    <message>
        <location filename="../qml/PageBasicWizard.qml" line="163"/>
        <source>Wizard stopped due to a problem, retry?</source>
        <translation>El asistente se detuvo debido a un problema, ¿repetimos?</translation>
    </message>
</context>
<context>
    <name>PageCalibrationTilt</name>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="121"/>
        <source>Done</source>
        <translation>Hecho</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="92"/>
        <source>Move Down</source>
        <translation>Mover Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="83"/>
        <source>Move Up</source>
        <translation>Mover Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="28"/>
        <source>Tank Movement</source>
        <translation>Movimiento del Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationTilt.qml" line="109"/>
        <source>Tilt position:</source>
        <translation>Posición de inclinación:</translation>
    </message>
</context>
<context>
    <name>PageCalibrationWizard</name>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="160"/>
        <source>Adjust the platform so it is aligned with the exposition display.</source>
        <translation>Ajuste la plataforma para que esté alineada con la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="195"/>
        <source>All done, happy printing!</source>
        <translation>¡Todo terminado, felices impresiones!</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="129"/>
        <source>Check whether the platform is properly secured with the black knob(hold it in place and tighten the knob if needed).</source>
        <translation>Comprueba si la plataforma está asegurada correctamente con el pomo negro (mantenla en su lugar y aprieta el pomo si es necesario).</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="145"/>
        <source>Close the cover.</source>
        <translation>Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="131"/>
        <source>Do not rotate the platform. It should be positioned according to the picture.</source>
        <translation>No gires la plataforma. Debería estar colocada como en la imagen.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="162"/>
        <source>Front edges of the platform and exposition display need to be parallel.</source>
        <translation>Los bordes frontales de la plataforma y de la pantalla de exposición deben estar paralelos.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="175"/>
        <source>Hold the platform still with one hand and apply a slight downward force on it, so it maintains good contact with the screen.


Next, use an Allen key to tighten the screw on the cantilever.

Then release the platform.</source>
        <translation>Sostén la plataforma quieta con una mano y aplica una ligera fuerza hacia abajo para que mantenga ejerza un buen contacto con la pantalla.


Luego, usa una llave Allen para apretar el tornillo en el cantilever.

Después suelta la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="50"/>
        <source>If the platform is not yet inserted, insert it according to the picture at 0° degrees angle and secure it with the black knob.</source>
        <translation>Si la plataforma aún no está colocada, colócala de acuerdo con la imagen en un ángulo de 0° y fíjala con el pomo negro.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="86"/>
        <source>In the next step, move the tilt bed up/down until it is in direct contact with the resin tank. The tilt bed and tank have to be aligned in a perfect line.</source>
        <translation>El el siguiente paso, inclina la base hasta que toque el tanque de resina. La base y el tanque deberían de estar perfectamente alineados.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="62"/>
        <source>Loosen the small screw on the cantilever with an allen key. Be careful not to unscrew it completely.</source>
        <translation>Afloja el tornillo pequeño del soporte de la plataforma de impresión con una llave Allen. Ten cuidado de no desenroscarlo completamente.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="109"/>
        <source>Make sure that the platfom, tank and tilt bed are PERFECTLY clean.</source>
        <translation>Comprueba que la plataforma, el tanque y la plataforma basculante están PERFECTAMENTE limpios.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="31"/>
        <source>Printer Calibration</source>
        <translation>Calibración de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="120"/>
        <source>Return the tank to the original position and secure it with tank screws. Make sure that you tighten both screws evenly and with the same amount of force.</source>
        <translation>Coloca el tanque en la posición original y sujétalo con los tornillos. Comprueba que aprietas ambos tornillos a la vez y con la misma fuerza.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="99"/>
        <source>Set the tilt bed against the resin tank</source>
        <translation>Ajusta la base basculante con las flechas hasta que toque el tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="64"/>
        <source>Some SL1 printers may have two screws - see the handbook for more information.</source>
        <translation>Algunas impresoras SL1 pueden tener dos tornillos - consulta el manual para obtener más información.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="180"/>
        <source>Some SL1 printers may have two screws - tighten them evenly, little by little. See the handbook for more information.</source>
        <translation>Algunas impresoras SL1 pueden tener dos tornillos; apriétalos de manera uniforme, poco a poco. Consulta el manual para obtener más información.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="111"/>
        <source>The image is for illustration purposes only.</source>
        <translation>La imagen es solo para fines ilustrativos.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="178"/>
        <source>Tighten the small screw on the cantilever with an allen key.</source>
        <translation>Aprieta el tornillo pequeño del soporte de la plataforma de impresión con una llave Allen.</translation>
    </message>
    <message>
        <location filename="../qml/PageCalibrationWizard.qml" line="77"/>
        <source>Unscrew the tank, rotate it by 90° and place it flat across the tilt bed. Remove the tank screws completely.</source>
        <translation>Afloja los tornillos del tanque, gíralo 90° y colócalo plano sobre la plataforma basculante. Quita los tornillos del tanque por completo.</translation>
    </message>
</context>
<context>
    <name>PageChange</name>
    <message>
        <location filename="../qml/PageChange.qml" line="149"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="177"/>
        <source>Above Area Fill</source>
        <translation>Relleno por Encima del Área</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="171"/>
        <source>Above Area Fill Threshold Settings</source>
        <translation>Configuración del Umbral de Relleno por Encima del Área</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="148"/>
        <source>Area Fill Threshold</source>
        <translation>Umbral del Área de Relleno</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="197"/>
        <source>Below Area Fill</source>
        <translation>Relleno por Debajo del Área</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="191"/>
        <source>Below Area Fill Threshold Settings</source>
        <translation>Configuración del Umbral de Relleno por Debajo del Área</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="72"/>
        <source>Exposure</source>
        <translation>Exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="97"/>
        <source>Exposure Time Incr.</source>
        <translation>Incremento tiempo exp.</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="124"/>
        <source>First Layer Expo.</source>
        <translation>Expo. Primera Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="41"/>
        <source>Print Settings</source>
        <translation>Ajustes de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageChange.qml" line="90"/>
        <location filename="../qml/PageChange.qml" line="118"/>
        <location filename="../qml/PageChange.qml" line="142"/>
        <source>s</source>
        <translation>s</translation>
    </message>
</context>
<context>
    <name>PageConfirm</name>
    <message>
        <location filename="../qml/PageConfirm.qml" line="194"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageConfirm.qml" line="120"/>
        <source>Swipe for a picture</source>
        <translation>Desliza para ver imagen</translation>
    </message>
</context>
<context>
    <name>PageConnectHiddenNetwork</name>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="207"/>
        <source>Connected to %1</source>
        <translation>Conectado a %1</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="196"/>
        <source>Connection to %1 failed.</source>
        <translation>Conexión a %1 fallida.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="34"/>
        <source>Hidden Network</source>
        <translation>Red Oculta</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="78"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="139"/>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="147"/>
        <source>PSK must be at least 8 characters long.</source>
        <translation>PSK debe tener al menos 8 caracteres.</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="122"/>
        <source>Security</source>
        <translation>Seguridad</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="115"/>
        <source>Show Password</source>
        <translation>Ver Contraseña</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="62"/>
        <source>SSID</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="235"/>
        <source>Start AP</source>
        <translation>Comenzar AP</translation>
    </message>
    <message>
        <location filename="../qml/PageConnectHiddenNetwork.qml" line="185"/>
        <source>Working...</source>
        <translation>Trabajando...</translation>
    </message>
</context>
<context>
    <name>PageContinue</name>
    <message>
        <location filename="../qml/PageContinue.qml" line="27"/>
        <location filename="../qml/PageContinue.qml" line="59"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
</context>
<context>
    <name>PageCoolingDown</name>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Cooling down</source>
        <translation>Enfriando</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="40"/>
        <source>Temperature is %1 C</source>
        <translation>La temperatura es %1 C</translation>
    </message>
    <message>
        <location filename="../qml/PageCoolingDown.qml" line="27"/>
        <location filename="../qml/PageCoolingDown.qml" line="37"/>
        <source>UV LED OVERHEAT!</source>
        <translation>¡SOBRECALENTAMIENTO DE LOS LED UV!</translation>
    </message>
</context>
<context>
    <name>PageDisplayTest</name>
    <message>
        <location filename="../qml/PageDisplayTest.qml" line="31"/>
        <source>Display Test</source>
        <translation>Test de Pantalla</translation>
    </message>
</context>
<context>
    <name>PageDisplaytestWizard</name>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="91"/>
        <source>All done, happy printing!</source>
        <translation>¡Todo terminado, felices impresiones!</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="77"/>
        <source>Close the cover.</source>
        <translation>Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="32"/>
        <source>Display Test</source>
        <translation>Test de Pantalla</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Afloja el pomo negro  y retira la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Por favor desatornilla y retira el tanque de resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="51"/>
        <source>This procedure will help you make sure that your exposure display is working correctly.</source>
        <translation>Este procedimiento te ayudará a asegurarte de que tu pantalla de exposición funciona correctamente.</translation>
    </message>
    <message>
        <location filename="../qml/PageDisplaytestWizard.qml" line="49"/>
        <source>Welcome to the display wizard.</source>
        <translation>Bienvenido al asistente de la pantalla.</translation>
    </message>
</context>
<context>
    <name>PageDowngradeWizard</name>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="78"/>
        <source>Current configuration is going to be cleared now.</source>
        <translation>La configuración actual se borrará ahora.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="108"/>
        <source>Downgrade done. In the next step, the printer will be restarted.</source>
        <translation>Degradado de firmware terminado. En el siguiente paso se reiniciará la impresora.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="31"/>
        <source>Hardware Downgrade</source>
        <translation>Degradar Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="99"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Emplea solamente la plataforma suministrada. Si usas otra, ¡podrías causar vertidos de resina y daños a tu impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="69"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Tenga en cuenta que no se admite la degradación.

La degradación de su impresora borrará su calibración UV y su impresora no funcionará correctamente.

Deberá volver a calibrarla con un calibrador UV externo.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="48"/>
        <source>Proceed?</source>
        <translation>¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="59"/>
        <source>Reassemble SL1S components and power on the printer. This will restore the original state.</source>
        <translation>Vuelve a montar las piezas de la SL1S y enciende la impresora. Esto la devolverá al estado original.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="46"/>
        <source>SL1 components detected (downgrade from SL1S).</source>
        <translation>Componentes de SL1 detectados (degradación de SL1S).</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="79"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>La impresora solicitará la configuración inicial después de reiniciar.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="58"/>
        <source>The printer will power off now.</source>
        <translation>Ahora la impresora se apagará.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="47"/>
        <source>To complete the downgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>Para completar el procedimiento de degradación, la impresora debe borrar la configuración y reiniciarse.</translation>
    </message>
    <message>
        <location filename="../qml/PageDowngradeWizard.qml" line="89"/>
        <source>Use only the metal resin tank supplied. Using the different resin tank may cause resin to spill and damage your printer!</source>
        <translation>Usa únicamente el tanque de resina metálica suministrado. ¡El uso de otros tanques de resina puede hacer que la resina se derrame y dañe tu impresora!</translation>
    </message>
</context>
<context>
    <name>PageDownloadingExamples</name>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="128"/>
        <source>Cleanup...</source>
        <translation>Limpiar...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="125"/>
        <source>Copying...</source>
        <translation>Copiando...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="127"/>
        <source>Done.</source>
        <translation>Hecho.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="124"/>
        <source>Downloading ...</source>
        <translation>Descargando ...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="55"/>
        <source>Downloading examples...</source>
        <translation>Descargando ejemplos...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="10"/>
        <source>Examples</source>
        <translation>Ejemplos</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="129"/>
        <source>Failure.</source>
        <translation>Fallo.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="123"/>
        <source>Initializing...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="132"/>
        <source>Unknown state.</source>
        <translation>Estado desconocido.</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="126"/>
        <source>Unpacking...</source>
        <translation>Desembalando...</translation>
    </message>
    <message>
        <location filename="../qml/PageDownloadingExamples.qml" line="141"/>
        <source>View Examples</source>
        <translation>Ver Ejemplos</translation>
    </message>
</context>
<context>
    <name>PageError</name>
    <message>
        <location filename="../qml/PageError.qml" line="33"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="44"/>
        <source>Error code:</source>
        <translation>Código de error:</translation>
    </message>
    <message>
        <location filename="../qml/PageError.qml" line="48"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Para más información, escanea el código QR o ponte en contacto con tu distribuidor.</translation>
    </message>
</context>
<context>
    <name>PageEthernetSettings</name>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="202"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>Configuring the connection,</source>
        <translation>Configurando la conexión,</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="123"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="35"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="148"/>
        <source>Gateway</source>
        <comment>default gateway address</comment>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="136"/>
        <source>IP Address</source>
        <translation>Dirección IP</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="117"/>
        <source>Network Info</source>
        <translation>Información de Red</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="128"/>
        <location filename="../qml/PageEthernetSettings.qml" line="212"/>
        <source>please wait...</source>
        <translation>espera por favor...</translation>
    </message>
    <message>
        <location filename="../qml/PageEthernetSettings.qml" line="219"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Revertir</translation>
    </message>
</context>
<context>
    <name>PageException</name>
    <message>
        <location filename="../qml/PageException.qml" line="61"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="68"/>
        <source>Error code:</source>
        <translation>Código de error:</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="72"/>
        <source>For further information, please scan the QR code or contact your reseller.</source>
        <translation>Para más información, escanea el código QR o ponte en contacto con tu distribuidor.</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="106"/>
        <source>Save Logs to USB</source>
        <translation>Guardar Registros al USB</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="119"/>
        <source>Send Logs to Cloud</source>
        <translation>Enviar Registros a la Nube</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="80"/>
        <source>Swipe to proceed</source>
        <translation>Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="36"/>
        <source>System Error</source>
        <translation>Error del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="140"/>
        <source>Turn Off</source>
        <translation>Apagar</translation>
    </message>
    <message>
        <location filename="../qml/PageException.qml" line="133"/>
        <source>Update Firmware</source>
        <translation>Actualización de Firmware</translation>
    </message>
</context>
<context>
    <name>PageFactoryResetWizard</name>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="45"/>
        <source>Factory Reset done.</source>
        <translation>Restablecimiento de fábrica realizado.</translation>
    </message>
    <message>
        <location filename="../qml/PageFactoryResetWizard.qml" line="32"/>
        <source>Factory Reset</source>
        <translation>Restaurar Valores de Fábrica</translation>
    </message>
</context>
<context>
    <name>PageFeedme</name>
    <message>
        <location filename="../qml/PageFeedme.qml" line="74"/>
        <source>Done</source>
        <translation>Listo</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="34"/>
        <source>Feed Me</source>
        <translation>Alimentame</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="44"/>
        <source>If you do not want to refill, press the Back button at top of the screen.</source>
        <translation>Si no deseas volver a llenar, pulsa el botón Atrás en la parte superior de la pantalla.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="42"/>
        <source>Manual resin refill.</source>
        <translation>Recarga de resina manual.</translation>
    </message>
    <message>
        <location filename="../qml/PageFeedme.qml" line="43"/>
        <source>Refill the tank up to the 100% mark and press Done.</source>
        <translation>Vuelve a llenar el tanque hasta la marca del 100% y presiona Hecho.</translation>
    </message>
</context>
<context>
    <name>PageFileBrowser</name>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="169"/>
        <source>Before printing, the following steps are required to pass:</source>
        <translation>Antes de imprimir, se tienen que realizar los siguientes pasos:</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="164"/>
        <source>Calibrate?</source>
        <translation>¿Calibrar?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="152"/>
        <source>Current system will still be available via Settings -&gt; Firmware -&gt; Downgrade</source>
        <translation>El sistema actual seguirá estando disponible a través de Configuración -&gt; Firmware -&gt; Degradar</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="150"/>
        <source>Do you really want to install %1?</source>
        <translation>¿Realmente deseas instalar %1?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="173"/>
        <source>Do you want to start now?</source>
        <translation>¿Quieres empezar ahora?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="316"/>
        <source>insert a USB drive or download examples in Settings -&gt; Support.</source>
        <translation>Conecta una unidad USB o descarga ejemplos en Configuración -&gt; Soporte.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="148"/>
        <source>Install?</source>
        <translation>¿Instalar?</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="94"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="242"/>
        <location filename="../qml/PageFileBrowser.qml" line="244"/>
        <source>Local</source>
        <comment>File is stored in a local storage</comment>
        <translation>Local</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/PageFileBrowser.qml" line="229"/>
        <source>%n item(s)</source>
        <comment>number of items in a directory</comment>
        <translation>
            <numerusform>%n ítem</numerusform>
            <numerusform>%n ítems</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="314"/>
        <source>No usable projects were found,</source>
        <translation>No se encontraron proyectos utilizables,</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="97"/>
        <source>Previous Prints</source>
        <comment>a directory with previously printed projects</comment>
        <translation>Impresiones Anteriores</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="44"/>
        <source>Printer Calibration</source>
        <translation>Calibración de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="33"/>
        <source>Projects</source>
        <translation>Proyectos</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="241"/>
        <source>Remote</source>
        <comment>File is stored in remote storage, i.e. a cloud</comment>
        <translation>Remoto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="96"/>
        <source>Remote</source>
        <translation>Remoto</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="246"/>
        <source>Root</source>
        <comment>Directory is a root of the directory tree, its subdirectories are different sources of projects</comment>
        <translation>Raíz</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="43"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="167"/>
        <source>The printer is not fully calibrated.</source>
        <translation>La impresora no está completamente calibrada.</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="267"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="98"/>
        <source>Update Bundles</source>
        <comment>a directory containing firmware update bundles</comment>
        <translation>Paquetes de Actualización</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="245"/>
        <source>Updates</source>
        <comment>File is in a repository of raucb files(update bundles)</comment>
        <translation>Actualizaciones</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="95"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="243"/>
        <source>USB</source>
        <comment>File is stored on USB flash disk</comment>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../qml/PageFileBrowser.qml" line="45"/>
        <source>UV Calibration</source>
        <translation>Calibración UV</translation>
    </message>
</context>
<context>
    <name>PageFinished</name>
    <message>
        <location filename="../qml/PageFinished.qml" line="127"/>
        <source>CANCELED</source>
        <translation>CANCELADA</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="207"/>
        <source>Consumed Resin</source>
        <translation>Resina consumida</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="126"/>
        <source>FAILED</source>
        <translation>FALLIDA</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="33"/>
        <source>Finished</source>
        <translation>Terminado</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="125"/>
        <location filename="../qml/PageFinished.qml" line="128"/>
        <source>FINISHED</source>
        <translation>FINALIZADA</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="277"/>
        <source>First Layer Exposure</source>
        <translation>Exposición Primera Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="328"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="253"/>
        <source>Layer Exposure</source>
        <translation>Exposición Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="229"/>
        <source>Layer height</source>
        <translation>Altura de capa</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="183"/>
        <source>Layers</source>
        <translation>Capas</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="405"/>
        <source>Loading, please wait...</source>
        <translation>Cargando, por favor espera...</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="156"/>
        <source>Print Time</source>
        <translation>Tiempo de impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="348"/>
        <source>Reprint</source>
        <translation>Reimprimir</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="338"/>
        <source>Resin Tank Cleaning</source>
        <translation>Limpieza Tanque Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="261"/>
        <location filename="../qml/PageFinished.qml" line="285"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="300"/>
        <source>Swipe to continue</source>
        <translation>Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageFinished.qml" line="368"/>
        <source>Turn Off</source>
        <translation>Apagar</translation>
    </message>
</context>
<context>
    <name>PageFullscreenImage</name>
    <message>
        <location filename="../qml/PageFullscreenImage.qml" line="27"/>
        <source>Fullscreen Image</source>
        <translation>Imagen a Pantalla Completa</translation>
    </message>
</context>
<context>
    <name>PageHome</name>
    <message>
        <location filename="../qml/PageHome.qml" line="51"/>
        <source>Control</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="29"/>
        <source>Home</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="37"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="58"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/PageHome.qml" line="65"/>
        <source>Turn Off</source>
        <translation>Apagar</translation>
    </message>
</context>
<context>
    <name>PageLanguage</name>
    <message>
        <location filename="../qml/PageLanguage.qml" line="81"/>
        <source>Set</source>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../qml/PageLanguage.qml" line="31"/>
        <source>Set Language</source>
        <translation>Establecer Idioma</translation>
    </message>
</context>
<context>
    <name>PageLogs</name>
    <message>
        <location filename="../qml/PageLogs.qml" line="88"/>
        <source>Extracting log data</source>
        <translation>Extrayendo datos de registro</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="29"/>
        <source>Logs export</source>
        <translation>Exportación de registros</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="61"/>
        <source>Logs export canceled</source>
        <translation>Exportación de registros cancelada</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="64"/>
        <source>Logs export failed.

Please check your flash drive / internet connection.</source>
        <translation>La exportación del registro ha fallado. Por favor, comprueba tu unidad flash / conexión a Internet.</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="57"/>
        <source>Logs export finished</source>
        <translation>Exportación de registros finalizada</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="52"/>
        <source>Logs has been successfully uploaded to the Prusa server.&lt;br /&gt;&lt;br /&gt;Please contact the Prusa support and share the following code with them:</source>
        <translation>Los registros se han cargado correctamente en el servidor de Prusa.&lt;br /&gt;&lt;br /&gt;Contacta con el soporte de Prusa y comparte el siguiente código con ellos:</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="50"/>
        <source>Log upload finished</source>
        <translation>Carga de registro finalizada</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="91"/>
        <source>Saving data to USB drive</source>
        <translation>Guardando datos en la memoria USB</translation>
    </message>
    <message>
        <location filename="../qml/PageLogs.qml" line="92"/>
        <source>Uploading data to server</source>
        <translation>Subiendo datos al servidor</translation>
    </message>
</context>
<context>
    <name>PageManual</name>
    <message>
        <location filename="../qml/PageManual.qml" line="25"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageManual.qml" line="43"/>
        <source>Scanning the QR code will load the handbook for this device.

Alternatively, use this link:</source>
        <translation>Al escanear el código QR, se cargará el manual de este dispositivo.

Alternativamente, use este enlace:</translation>
    </message>
</context>
<context>
    <name>PageModifyLayerProfile</name>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="144"/>
        <source>Delay After Exposure</source>
        <translation>Demora Tras la Exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="130"/>
        <source>Delay Before Exposure</source>
        <translation>Demora Previo a la Exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="375"/>
        <source>Disabling the &apos;Use tilt&apos; causes the object to separate away from the film in the vertical direction only.

&apos;Tower hop height&apos; has been set to recommended value of 5 mm.</source>
        <translation>Si se desactiva la opción &apos;Usar inclinación&apos;, el objeto se separará de la película solo en dirección vertical.

&apos;Altura del Salto de la Torre&apos; se ha ajustado al valor recomendado de 5 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="29"/>
        <source>Edit Layer Profile</source>
        <translation>Editar Perfil de Capa</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="385"/>
        <source>Enabling the &apos;Use tilt&apos; causes the object to separate away from the film mainly by tilt.

&apos;Tower hop height&apos; has been set to 0 mm.</source>
        <translation>La activación de la opción &apos;Usar inclinación&apos; hace que el objeto se separe de la película principalmente por inclinación.

&apos;Altura del Salto de la Torre&apos; se ha ajustado a 0 mm.</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="160"/>
        <source>mm</source>
        <comment>millimeters</comment>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="174"/>
        <source>mm/s</source>
        <comment>millimeters per second</comment>
        <translation>mm/s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="131"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="145"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="240"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="279"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="319"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="358"/>
        <source>s</source>
        <comment>seconds</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="266"/>
        <source>Tilt Down Cycles</source>
        <translation>Ciclos de Inclinación hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="278"/>
        <source>Tilt Down Delay</source>
        <translation>Demora Inclinación hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="254"/>
        <source>Tilt Down Finish Speed</source>
        <translation>Velocidad Final de la Inclinación hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="214"/>
        <source>Tilt Down Initial Speed</source>
        <translation>Velocidad Inicial de la Inclinación hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="226"/>
        <source>Tilt Down Offset</source>
        <translation>Offset Inclinación hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="239"/>
        <source>Tilt Down Offset Delay</source>
        <translation>Offset Demora de la Inclinación hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="345"/>
        <source>Tilt Up Cycles</source>
        <translation>Ciclos de Inclinación hacia Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="357"/>
        <source>Tilt Up Delay</source>
        <translation>Demora Inclinación hacia Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="333"/>
        <source>Tilt Up Finish Speed</source>
        <translation>Velocidad Final de la Inclinación hacia Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="293"/>
        <source>Tilt Up Initial Speed</source>
        <translation>Velocidad Inicial de la Inclinación hacia Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="305"/>
        <source>Tilt Up Offset</source>
        <translation>Offset Inclinación hacia Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="318"/>
        <source>Tilt Up Offset Delay</source>
        <translation>Offset Demora de la Inclinación hacia Arriba</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="159"/>
        <source>Tower Hop Height</source>
        <translation>Altura de Salto de la Torre</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="173"/>
        <source>Tower Speed</source>
        <translation>Velocidad de la Torre</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="185"/>
        <source>Use Tilt</source>
        <translation>Usar Inclinación</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="374"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="384"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="227"/>
        <source>μ-step</source>
        <comment>tilt microsteps</comment>
        <translation>μ-paso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="306"/>
        <source>μ-step</source>
        <comment>tilt micro steps</comment>
        <translation>μ-paso</translation>
    </message>
    <message>
        <location filename="../qml/PageModifyLayerProfile.qml" line="216"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="256"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="295"/>
        <location filename="../qml/PageModifyLayerProfile.qml" line="335"/>
        <source>μ-step/s</source>
        <comment>microsteps per second</comment>
        <translation>μ-paso/s</translation>
    </message>
</context>
<context>
    <name>PageMovementControl</name>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="27"/>
        <source>Control</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="60"/>
        <source>Disable
Steppers</source>
        <translation>Desactivar Motores</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="38"/>
        <source>Home
Platform</source>
        <translation>Home Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="49"/>
        <source>Home
Tank</source>
        <translation>Home
Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tank, please wait...</source>
        <translation>Homing de tanque, por favor espere...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="76"/>
        <source>Homing the tower, please wait...</source>
        <translation>Homing de torre, por favor espere...</translation>
    </message>
    <message>
        <location filename="../qml/PageMovementControl.qml" line="68"/>
        <source>Resin Tank Cleaning</source>
        <translation>Limpieza Tanque Resina</translation>
    </message>
</context>
<context>
    <name>PageNetworkEthernetList</name>
    <message>
        <location filename="../qml/PageNetworkEthernetList.qml" line="35"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
</context>
<context>
    <name>PageNetworkWifiList</name>
    <message>
        <location filename="../qml/PageNetworkWifiList.qml" line="29"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageNewExpoPanelWizard</name>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="32"/>
        <source>New Exposure Panel</source>
        <translation>Nuevo panel exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageNewExpoPanelWizard.qml" line="45"/>
        <source>New exposure screen has been detected.

The printer will ask for the inital setup (selftest and calibration) to make sure everything works correctly.</source>
        <translation>Se ha detectado una nueva pantalla de exposición.

La impresora solicitará la configuración inicial (selftest y calibración) para asegurarse de que todo funciona correctamente.</translation>
    </message>
</context>
<context>
    <name>PageNotificationList</name>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="61"/>
        <source>Available update to %1</source>
        <translation>Actualización disponible para %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="37"/>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="80"/>
        <source>Print canceled: %1</source>
        <translation>Impresión cancelada: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="79"/>
        <source>Print failed: %1</source>
        <translation>Impresión fallida: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageNotificationList.qml" line="81"/>
        <source>Print finished: %1</source>
        <translation>Impresión finalizada: %1</translation>
    </message>
</context>
<context>
    <name>PagePackingWizard</name>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="55"/>
        <source>Insert protective foam</source>
        <translation>Introduce la espuma protectora</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="46"/>
        <source>Packing done.</source>
        <translation>Embalaje completado.</translation>
    </message>
    <message>
        <location filename="../qml/PagePackingWizard.qml" line="32"/>
        <source>Packing Wizard</source>
        <translation>Asistente de embalaje</translation>
    </message>
</context>
<context>
    <name>PagePowerOffDialog</name>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="7"/>
        <source>Do you really want to turn off the printer?</source>
        <translation>¿Realmente quieres apagar la impresora?</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="10"/>
        <source>Powering Off...</source>
        <translation>Apagando...</translation>
    </message>
    <message>
        <location filename="../qml/PagePowerOffDialog.qml" line="6"/>
        <source>Power Off?</source>
        <translation>¿Apagar Impresora?</translation>
    </message>
</context>
<context>
    <name>PagePrePrintChecks</name>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="121"/>
        <source>Cover</source>
        <translation>Tapa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="125"/>
        <source>Delayed Start</source>
        <translation>Inicio Retrasado</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="151"/>
        <source>Disabled</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="140"/>
        <source>Do not touch the printer!</source>
        <translation>¡No toques la impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="120"/>
        <source>Fan</source>
        <translation>Ventilador</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="32"/>
        <source>Please Wait</source>
        <translation>Por favor Espere</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="118"/>
        <source>Project</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="122"/>
        <source>Resin</source>
        <translation>Resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="123"/>
        <source>Starting Positions</source>
        <translation>Posiciones de inicio</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="124"/>
        <source>Stirring</source>
        <translation>Removiendo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="117"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../qml/PagePrePrintChecks.qml" line="150"/>
        <source>With Warning</source>
        <translation>Con Advertencia</translation>
    </message>
</context>
<context>
    <name>PagePrint</name>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Action Pending</source>
        <translation>Acción Pendiente</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="117"/>
        <source>Check Warning</source>
        <translation>Comprueba la advertencia</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="151"/>
        <source>Close Cover!</source>
        <translation>¡Cierra la tapa!</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="113"/>
        <source>Continue?</source>
        <translation>¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Cover Open</source>
        <translation>Tapa Abierta</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Getting the printer ready to add resin. Please wait.</source>
        <translation>Preparando la impresora para añadir resina. Por favor, espere.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Going down</source>
        <translation>Bajando</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Going up</source>
        <translation>Subiendo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="130"/>
        <source>If you do not want to continue, press the Back button on top of the screen and the current job will be canceled.</source>
        <translation>Si no deseas continuar, pulsa el botón Atrás en la parte superior de la pantalla y se cancelará el trabajo actual.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="214"/>
        <source>Moving platform to the bottom position</source>
        <translation>Moviendo la plataforma a la posición inferior</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="211"/>
        <source>Moving platform to the top position</source>
        <translation>Moviendo la plataforma a la posición superior</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Moving the resin tank down...</source>
        <translation>Moviendo el tanque de resina hacia abajo ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Paused.</source>
        <translation>Pausada.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="223"/>
        <source>Please close the cover to continue</source>
        <translation>Cierra la tapa para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="152"/>
        <source>Please, close the cover! UV radiation is harmful.</source>
        <translation>¡Por favor, cierra la tapa! La radiación ultravioleta es perjudicial.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="220"/>
        <location filename="../qml/PagePrint.qml" line="277"/>
        <source>Please wait...</source>
        <translation>Por favor espere...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="217"/>
        <source>Project</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="243"/>
        <source>Reading data...</source>
        <translation>Leyendo datos...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="129"/>
        <source>Release the tank mechanism and press Continue.</source>
        <translation>Suelta el mecanismo del tanque y pulsa Continuar.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="240"/>
        <source>Requested actions will be executed after layer finish, please wait...</source>
        <translation>Las acciones solicitadas se ejecutarán una vez que finalice esta capa, espera ...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Setting start positions...</source>
        <translation>Fijando las posiciones iniciales...</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring</source>
        <translation>Agitando</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="237"/>
        <source>Stirring resin</source>
        <translation>Agitando la resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="127"/>
        <location filename="../qml/PagePrint.qml" line="250"/>
        <source>Stuck Recovery</source>
        <translation>Recuperación atascada</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="253"/>
        <source>Tank Moving Down</source>
        <translation>Tanque Moviéndose Hacia Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PagePrint.qml" line="128"/>
        <source>The printer got stuck and needs user assistance.</source>
        <translation>La impresora se atascó y necesita ayuda del usuario.</translation>
    </message>
</context>
<context>
    <name>PagePrintPreviewSwipe</name>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="302"/>
        <source>Exposure Times</source>
        <translation>Tiempos de Exposición</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="348"/>
        <source>Last Modified</source>
        <translation>Última Modificación</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layer Height</source>
        <translation>Altura de Capa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="281"/>
        <source>Layers</source>
        <translation>Capas</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="447"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Por favor, llena el tanque de resina al menos al&lt;br/&gt;%1 % y cierra la cubierta.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="493"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="483"/>
        <source>Print Settings</source>
        <translation>Ajustes de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="329"/>
        <source>Print Time Estimate</source>
        <translation>Tiempo Estimado de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="33"/>
        <source>Project</source>
        <translation>Proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="374"/>
        <source>Swipe to continue</source>
        <translation>Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="166"/>
        <source>Swipe to project</source>
        <translation>Desliza al proyecto</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="444"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>El proyecto requiere %1 % de la resina. Será necesario añadir resina durante la impresión.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="208"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPreviewSwipe.qml" line="357"/>
        <source>Unknown</source>
        <comment>Unknow time of last modification of a file</comment>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>PagePrintPrinting</name>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="239"/>
        <source>Area fill:</source>
        <translation>Relleno de área:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="492"/>
        <location filename="../qml/PagePrintPrinting.qml" line="498"/>
        <source>Cancel Print</source>
        <translation>Cancelar Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="431"/>
        <source>Consumed Resin</source>
        <translation>Resina consumida</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="516"/>
        <source>Enter Admin</source>
        <translation>Iniciar modo Administrador</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="373"/>
        <source>Estimated End</source>
        <translation>Fin Estimado</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="403"/>
        <source>Layer</source>
        <translation>Capa</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="225"/>
        <source>Layer:</source>
        <translation>Capa:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>Layer Height:</source>
        <translation>Altura de Capa:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="251"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="34"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="388"/>
        <source>Printing Time</source>
        <translation>Tiempo de impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="473"/>
        <source>Print Settings</source>
        <translation>Ajustes de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="274"/>
        <source>Progress:</source>
        <translation>Progreso:</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="479"/>
        <source>Refill Resin</source>
        <translation>Rellenar Resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="417"/>
        <source>Remaining Resin</source>
        <translation>Resina restante</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="358"/>
        <source>Remaining Time</source>
        <translation>Tiempo restante</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="107"/>
        <source>Today at</source>
        <translation>Hoy a las</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="499"/>
        <source>To make sure the print is not stopped accidentally,
please swipe the screen to move to the next step,
where you can cancel the print.</source>
        <translation>Para estar seguro de que la impresión no se detiene accidentalmente, desliza sobre la pantalla para pasar al siguiente paso, donde podrás cancelar la impresión.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="110"/>
        <source>Tomorrow at</source>
        <translation>Mañana a las</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="363"/>
        <source>Unknown</source>
        <comment>Remaining time is unknown</comment>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintPrinting.qml" line="421"/>
        <location filename="../qml/PagePrintPrinting.qml" line="435"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>PagePrintResinIn</name>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="74"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="64"/>
        <source>Please fill the resin tank to at least %1 % and close the cover.</source>
        <translation>Por favor, llena el tanque de resina al menos al&lt;br/&gt;%1 % y cierra la cubierta.</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="30"/>
        <source>Pour in resin</source>
        <translation>Vierte resina</translation>
    </message>
    <message>
        <location filename="../qml/PagePrintResinIn.qml" line="61"/>
        <source>The project requires %1 % of the resin. It will be necessary to refill the resin during print.</source>
        <translation>El proyecto requiere %1 % de la resina. Será necesario añadir resina durante la impresión.</translation>
    </message>
</context>
<context>
    <name>PageQrCode</name>
    <message>
        <location filename="../qml/PageQrCode.qml" line="30"/>
        <source>Page QR code</source>
        <translation>Código QR de la página</translation>
    </message>
</context>
<context>
    <name>PageReleaseNotes</name>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="120"/>
        <source>Install Now</source>
        <translation>Instalar Ahora</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="111"/>
        <source>Later</source>
        <translation>Más Tarde</translation>
    </message>
    <message>
        <location filename="../qml/PageReleaseNotes.qml" line="9"/>
        <source>Release Notes</source>
        <translation>Notas de lanzamiento</translation>
    </message>
</context>
<context>
    <name>PageSelftestWizard</name>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="102"/>
        <source>Can you hear the music?</source>
        <translation>¿Puedes escuchar la música?</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="77"/>
        <location filename="../qml/PageSelftestWizard.qml" line="140"/>
        <location filename="../qml/PageSelftestWizard.qml" line="173"/>
        <source>Close the cover.</source>
        <translation>Cierra la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="68"/>
        <source>Loosen the black knob and remove the platform.</source>
        <translation>Afloja el pomo negro  y retira la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="155"/>
        <source>Make sure that the resin tank is installed and the screws are tight.</source>
        <translation>Comprueba que el tanque de resina está instalado y que los tornillos están apretados.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="164"/>
        <source>Please install the platform under 0° angle and tighten it with the black knob.</source>
        <translation>Instala la plataforma en un ángulo de 0° y apriétala con el pomo negro.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="122"/>
        <source>Please install the platform under 60° angle and tighten it with the black knob.</source>
        <translation>Instala la plataforma en un ángulo de 60 ° y apriétala con el pomo negro.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="113"/>
        <source>Please install the resin tank and tighten the screws evenly.</source>
        <translation>Coloca el tanque de resina y aprieta los tornillos uniformemente.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="59"/>
        <source>Please unscrew and remove the resin tank.</source>
        <translation>Por favor desatornilla y retira el tanque de resina.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="131"/>
        <source>Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.</source>
        <translation>Suelta la plataforma del cantilever y colócala en el centro del tanque de resina en un ángulo de 90°.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="32"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="51"/>
        <source>This procedure is mandatory and it will check all components of the printer.</source>
        <translation>Este procedimiento es obligatorio y comprobará todos los componentes de la impresora.</translation>
    </message>
    <message>
        <location filename="../qml/PageSelftestWizard.qml" line="49"/>
        <source>Welcome to the selftest wizard.</source>
        <translation>Bienvenido al asistente del autotest.</translation>
    </message>
</context>
<context>
    <name>PageSetDate</name>
    <message>
        <location filename="../qml/PageSetDate.qml" line="676"/>
        <source>Set</source>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../qml/PageSetDate.qml" line="37"/>
        <source>Set Date</source>
        <translation>Ajustar Fecha</translation>
    </message>
</context>
<context>
    <name>PageSetHostname</name>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="73"/>
        <source>Can contain only a-z, 0-9 and  &quot;-&quot;. Must not begin or end with &quot;-&quot;.</source>
        <translation>Puede contener solo a-z, 0-9 y &quot;-&quot;. No debe empezar o terminar con &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="30"/>
        <location filename="../qml/PageSetHostname.qml" line="47"/>
        <source>Hostname</source>
        <translation>Nombre del host</translation>
    </message>
    <message>
        <location filename="../qml/PageSetHostname.qml" line="84"/>
        <source>Set</source>
        <translation>Ajustar</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaConnectRegistration</name>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="90"/>
        <source>Add Printer to Prusa Connect</source>
        <translation>Añadir Impresora a Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="79"/>
        <source>In Progress</source>
        <translation>En curso</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="78"/>
        <source>Not Registered</source>
        <translation>No Registrada</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="31"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="80"/>
        <source>Registered</source>
        <translation>Registrada</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="96"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="126"/>
        <source>Register Printer</source>
        <translation>Registrar Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="75"/>
        <source>Registration Status</source>
        <translation>Estado del Registro</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="123"/>
        <source>Review the Registration QR Code</source>
        <translation>Revisa el código QR de Registro</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="98"/>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="128"/>
        <source>Scan the QR Code or visit &lt;b&gt;prusa.io/add&lt;/b&gt;. Log into your Prusa Account to add this printer.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</source>
        <translation>Escanea el código QR o visita &lt;b&gt;prusa.io/add&lt;/b&gt;. Inicia sesión en tu Prusa Account para añadir esta impresora.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;Code: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="81"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaConnectRegistration.qml" line="111"/>
        <source>Unregister Printer from Prusa Connect</source>
        <translation>Dar de Baja la Impresora de Prusa Connect</translation>
    </message>
</context>
<context>
    <name>PageSetPrusaLinkWebLogin</name>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="131"/>
        <source>Can contain only characters a-z, A-Z, 0-9 and  &quot;-&quot;.</source>
        <translation>Solo puede contener caracteres a-z, A-Z, 0-9 e &quot;-&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="117"/>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="125"/>
        <source>Must be at least 8 chars long</source>
        <translation>Debe tener al menos 8 caracteres</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="92"/>
        <source>Printer Password</source>
        <translation>Contraseña de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="32"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="144"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../qml/PageSetPrusaLinkWebLogin.qml" line="55"/>
        <source>User Name</source>
        <translation>Nombre de Usuario</translation>
    </message>
</context>
<context>
    <name>PageSetTime</name>
    <message>
        <location filename="../qml/PageSetTime.qml" line="98"/>
        <source>Hour</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="105"/>
        <source>Minute</source>
        <translation>Minuto</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="112"/>
        <source>Set</source>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTime.qml" line="32"/>
        <source>Set Time</source>
        <translation>Ajustar Hora</translation>
    </message>
</context>
<context>
    <name>PageSetTimezone</name>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="134"/>
        <source>City</source>
        <translation>Ciudad</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="128"/>
        <source>Region</source>
        <translation>Región</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="140"/>
        <source>Set</source>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../qml/PageSetTimezone.qml" line="29"/>
        <source>Set Timezone</source>
        <translation>Ajustar Huso horario</translation>
    </message>
</context>
<context>
    <name>PageSettings</name>
    <message>
        <location filename="../qml/PageSettings.qml" line="60"/>
        <source>Calibration</source>
        <translation>Calibración</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="150"/>
        <source>Enter Admin</source>
        <translation>Iniciar modo Administrador</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="89"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="119"/>
        <source>Language &amp; Time</source>
        <translation>Idioma y Fecha</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="69"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="79"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Plataforma &amp; Tanque de Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="29"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="99"/>
        <source>Settings &amp; Sensors</source>
        <translation>Ajustes y Sensores</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="139"/>
        <source>Support</source>
        <translation>Soporte</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="129"/>
        <source>System Logs</source>
        <translation>Registros del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettings.qml" line="109"/>
        <source>Touchscreen</source>
        <translation>Pantalla Táctil</translation>
    </message>
</context>
<context>
    <name>PageSettingsCalibration</name>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="32"/>
        <source>Calibration</source>
        <translation>Calibración</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="94"/>
        <source>Did you replace the EXPOSITION DISPLAY?</source>
        <translation>¿Has cambiado la PANTALLA DE EXPOSICIÓN?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="107"/>
        <source>Did you replace the UV LED SET?</source>
        <translation>¿Reemplazaste el conjunto de LEDs UV?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="76"/>
        <source>Display Test</source>
        <translation>Test de Pantalla</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="93"/>
        <source>New Display?</source>
        <translation>¿Pantalla Nueva?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="106"/>
        <source>New UV LED SET?</source>
        <translation>¿Nuevo conjunto de LED UV?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="58"/>
        <source>Printer Calibration</source>
        <translation>Calibración de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="51"/>
        <source>Selftest</source>
        <translation>Selftest</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsCalibration.qml" line="68"/>
        <source>UV Calibration</source>
        <translation>Calibración UV</translation>
    </message>
</context>
<context>
    <name>PageSettingsFirmware</name>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="416"/>
        <source>After installing this update, the printer can be updated to another FW but &lt;b&gt;printing won&apos;t work.&lt;/b&gt;</source>
        <translation>Después de instalar esta actualización, la impresora se puede actualizar a otro FW pero &lt;b&gt;la impresión no funcionará.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="256"/>
        <source>Are you sure?</source>
        <translation>¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="414"/>
        <source>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;is not compatible with your current hardware model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;no es compatible con su modelo de hardware actual - &lt;b&gt;%2&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="78"/>
        <source>Check for Update</source>
        <translation>Buscar Actualizaciones</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="85"/>
        <source>Check for update failed</source>
        <translation>La búsqueda de actualizaciones falló</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="87"/>
        <source>Checking for updates...</source>
        <translation>Buscando actualizaciones...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="215"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="418"/>
        <source>Continue anyway?</source>
        <translation>¿Continuar de todos modos?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="181"/>
        <source>Downgrade?</source>
        <translation>¿Degradar?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="347"/>
        <source>Downgrade Firmware?</source>
        <translation>¿Degradar el firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="74"/>
        <source>Download Now</source>
        <translation>Descargar ahora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="182"/>
        <source>Do you really want to downgrade to FW</source>
        <translation>De verdad quieres cambiar a un FW anterior</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="257"/>
        <source>Do you really want to perform the factory reset?

All settings will be erased!
Projects will stay untouched.</source>
        <translation>¿Realmente deseas realizar el restablecimiento de fábrica?

¡Se borrarán todos los ajustes!
Los proyectos permanecerán intactos.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="354"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="387"/>
        <source>Do you want to continue?</source>
        <translation>¿Quieres continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="248"/>
        <source>Factory Reset</source>
        <translation>Restaurar Valores de Fábrica</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="36"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="334"/>
        <source>FW file meta information not found.&lt;br/&gt;&lt;br/&gt;Do you want to install&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;anyway?</source>
        <translation>No se encontraron los metadatos del archivo FW.&lt;br/&gt;&lt;br/&gt;¿Quieres instalar&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;&lt;br/&gt; de todas formas?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="290"/>
        <source>FW Info</source>
        <comment>page title, information about the selected update bundle</comment>
        <translation>Información de FW</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="213"/>
        <source>If you switch, you can update to another FW version afterwards but &lt;b&gt;you will not be able to print.&lt;/b&gt;</source>
        <translation>Si cambias, puedes actualizar a otra versión de FW posteriormente, pero &lt;b&gt;no podrás imprimir.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="210"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="413"/>
        <source>Incompatible FW!</source>
        <translation>¡FW incompatible!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="111"/>
        <source>Installed version</source>
        <translation>Versión instalada</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="333"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="382"/>
        <source>Install Firmware?</source>
        <translation>¿Instalar firmware?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="76"/>
        <source>Install Now</source>
        <translation>Instalar Ahora</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="352"/>
        <source>is lower or equal than current&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>es menor o igual que el actual&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="291"/>
        <source>Loading FW file meta information
(may take up to 20 seconds) ...</source>
        <translation>Cargando metadatos del archivo de FW
(puede tardar hasta 20 segundos) ...</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="239"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Más nueva que la SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="235"/>
        <location filename="../qml/PageSettingsFirmware.qml" line="425"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="138"/>
        <source>Receive Beta Updates</source>
        <translation>Recibir Actualizaciones de Betas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="156"/>
        <source>Switch to beta?</source>
        <translation>¿Cambiar a la beta?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="169"/>
        <source>Switch to version:</source>
        <translation>Cambiar a versión:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="89"/>
        <source>System is up-to-date</source>
        <translation>El sistema está actualizado</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="211"/>
        <source>The alternative FW version &lt;b&gt;%1&lt;/b&gt; is not compatible with your printer model - &lt;b&gt;%2&lt;/b&gt;.</source>
        <translation>La versión de FW alternativa &lt;b&gt;%1&lt;/b&gt; no es compatible con tu modelo de impresora - &lt;b&gt;%2&lt;/b&gt; .</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="428"/>
        <source>Unknown</source>
        <comment>Printer model is unknown, but likely better than SL1S</comment>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="45"/>
        <source>Unknown</source>
        <comment>Unknown operating system version on alternative slot</comment>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="91"/>
        <source>Update available</source>
        <translation>Actualización disponible</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="97"/>
        <source>Update download failed</source>
        <translation>La descarga de la actualización ha fallado</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="95"/>
        <source>Update is downloaded</source>
        <translation>Se está descargando la actualización</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="93"/>
        <source>Update is downloading</source>
        <translation>La actualización se está descargando</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="99"/>
        <source>Updater service idle</source>
        <translation>Servicio de actualización en espera</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="350"/>
        <source>Version of selected FW file&lt;br/&gt;&lt;b&gt;%1&lt;/b&gt;,</source>
        <translation>Versión del archivo FW seleccionado&lt;br/&gt; &lt;b&gt;%1&lt;/b&gt; ,</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="157"/>
        <source>Warning! The beta updates can be unstable.&lt;br/&gt;&lt;br/&gt;Not recommended for production printers.&lt;br/&gt;&lt;br/&gt;Continue?</source>
        <translation>¡Advertencia! Las actualizaciones beta pueden ser inestables.&lt;br/&gt;&lt;br/&gt; No recomendado para impresoras de producción.&lt;br/&gt;&lt;br/&gt; ¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="385"/>
        <source>which has version &lt;b&gt;%1&lt;/b&gt;.</source>
        <translation>que tiene la versión &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsFirmware.qml" line="383"/>
        <source>You have selected update bundle &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</source>
        <translation>Has seleccionado el paquete de actualización &lt;b&gt;&quot;%1&quot;&lt;/b&gt;,</translation>
    </message>
</context>
<context>
    <name>PageSettingsLanguageTime</name>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="29"/>
        <source>Language &amp; Time</source>
        <translation>Idioma y Fecha</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="35"/>
        <source>Set Language</source>
        <translation>Establecer Idioma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsLanguageTime.qml" line="41"/>
        <source>Time Settings</source>
        <translation>Ajustes de Hora</translation>
    </message>
</context>
<context>
    <name>PageSettingsNetwork</name>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="37"/>
        <source>Ethernet</source>
        <translation>Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="74"/>
        <source>Hostname</source>
        <translation>Nombre del host</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="67"/>
        <source>Hotspot</source>
        <translation>Punto de acceso</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="29"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="89"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="81"/>
        <source>PrusaLink</source>
        <translation>PrusaLink</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsNetwork.qml" line="53"/>
        <source>Wi-Fi</source>
        <translation>Wi-Fi</translation>
    </message>
</context>
<context>
    <name>PageSettingsPlatformResinTank</name>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="109"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="48"/>
        <source>Disable Steppers</source>
        <translation>Desactivar Motores</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="102"/>
        <source>Limit for Fast Tilt</source>
        <translation>Límite para Inclinación Rápida</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="34"/>
        <source>Move Platform</source>
        <translation>Mover Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="41"/>
        <source>Move Resin Tank</source>
        <translation>Mover Tanque Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="56"/>
        <source>Platform Axis Sensitivity</source>
        <translation>Sensibilidad Eje Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="128"/>
        <source>Platform Offset</source>
        <translation>Desplazamiento de la plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="28"/>
        <source>Platform &amp; Resin Tank</source>
        <translation>Plataforma &amp; Tanque de Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="159"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="79"/>
        <source>Tank Axis Sensitivity</source>
        <translation>Sensibilidad Eje Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="152"/>
        <source>Tank Surface Cleaning Exposure</source>
        <translation>Exposición Limpieza Superficie Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsPlatformResinTank.qml" line="135"/>
        <source>um</source>
        <translation>um</translation>
    </message>
</context>
<context>
    <name>PageSettingsSensors</name>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="112"/>
        <location filename="../qml/PageSettingsSensors.qml" line="148"/>
        <source>Are you sure?</source>
        <translation>¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="61"/>
        <source>Auto Power Off</source>
        <translation>Apagado Automático</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="87"/>
        <source>Cover Check</source>
        <translation>Comprobación Cubierta</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="36"/>
        <source>Device hash in QR</source>
        <translation>Hash del dispositivo en QR</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="113"/>
        <source>Disable the cover sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to unwanted exposure to UV light or personal injury due to moving parts. This action is not recommended!</source>
        <translation>¿Deshabilitar el sensor de la tapa?&lt;br/&gt;&lt;br/&gt; PRECAUCIÓN: Esto puede provocar una exposición no deseada a la luz ultravioleta o lesiones personales debido a las piezas móviles. ¡No se recomienda esta acción!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="149"/>
        <source>Disable the resin sensor?&lt;br/&gt;&lt;br/&gt;CAUTION: This may lead to failed prints or resin tank overflow! This action is not recommended!</source>
        <translation>¿Desactivar el sensor de resina?&lt;br/&gt;&lt;br/&gt;PRECAUCIÓN: ¡Esto puede crear fallos en impresiones o que el tanque se desborde! ¡No se recomienda esta acción!</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="180"/>
        <source>Off</source>
        <translation>Apagado</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="161"/>
        <source>Rear Fan Speed</source>
        <translation>Velocidad Ventilador Trasero</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="125"/>
        <source>Resin Sensor</source>
        <translation>Sensor de Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="168"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSensors.qml" line="28"/>
        <source>Settings &amp; Sensors</source>
        <translation>Ajustes y Sensores</translation>
    </message>
</context>
<context>
    <name>PageSettingsSupport</name>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="78"/>
        <source>About Us</source>
        <translation>Sobre nosotros</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="36"/>
        <source>Download Examples</source>
        <translation>Descargar Ejemplos</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="50"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="30"/>
        <source>Support</source>
        <translation>Soporte</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="69"/>
        <source>System Information</source>
        <translation>Info del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSupport.qml" line="60"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
</context>
<context>
    <name>PageSettingsSystemLogs</name>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>&lt;b&gt;No logs have been uploaded yet.&lt;/b&gt;</source>
        <translation>&lt;b&gt;Aún no se han subido registros.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="40"/>
        <source>Last Seen Logs:</source>
        <translation>Últimos Registros Vistos:</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="55"/>
        <source>Save to USB Drive</source>
        <translation>Guardar en la Memoria USB</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="29"/>
        <source>System Logs</source>
        <translation>Registros del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsSystemLogs.qml" line="65"/>
        <source>Upload to Server</source>
        <translation>Subir al Servidor</translation>
    </message>
</context>
<context>
    <name>PageSettingsTouchscreen</name>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="56"/>
        <source>h</source>
        <comment>hours short</comment>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="57"/>
        <source>min</source>
        <comment>minutes short</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="98"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="63"/>
        <source>Off</source>
        <translation>Apagada</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="58"/>
        <source>s</source>
        <comment>seconds short</comment>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="36"/>
        <source>Screensaver timer</source>
        <translation>Temporizador de Salvapantallas</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="80"/>
        <source>Touch Screen Brightness</source>
        <translation>Brillo Pantalla Táctil</translation>
    </message>
    <message>
        <location filename="../qml/PageSettingsTouchscreen.qml" line="28"/>
        <source>Touchscreen</source>
        <translation>Pantalla Táctil</translation>
    </message>
</context>
<context>
    <name>PageShowToken</name>
    <message>
        <location filename="../qml/PageShowToken.qml" line="81"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="61"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="27"/>
        <source>Prusa Connect</source>
        <translation>Prusa Connect</translation>
    </message>
    <message>
        <location filename="../qml/PageShowToken.qml" line="31"/>
        <source>Temporary Token</source>
        <translation>Token Temporal</translation>
    </message>
</context>
<context>
    <name>PageSoftwareLicenses</name>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="250"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="112"/>
        <location filename="../qml/PageSoftwareLicenses.qml" line="233"/>
        <source>Package Name</source>
        <translation>Nombre del Paquete</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="28"/>
        <source>Software Packages</source>
        <translation>Paquetes de Software</translation>
    </message>
    <message>
        <location filename="../qml/PageSoftwareLicenses.qml" line="242"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
</context>
<context>
    <name>PageSysinfo</name>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="75"/>
        <source>A64 Controller SN</source>
        <translation>Num.Serie Controlador A64</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="191"/>
        <source>Ambient Temperature</source>
        <translation>Temperatura Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="206"/>
        <source>Blower Fan</source>
        <translation>Ventilador Radial</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="112"/>
        <source>Booster Board SN</source>
        <translation>Num.Serie Placa Booster</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="182"/>
        <location filename="../qml/PageSysinfo.qml" line="187"/>
        <location filename="../qml/PageSysinfo.qml" line="192"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Closed</source>
        <translation>Cerrada</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="176"/>
        <source>Cover State</source>
        <translation>Estado Cubierta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="181"/>
        <source>CPU Temperature</source>
        <translation>Temperatura de la CPU</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="146"/>
        <source>Ethernet IP Address</source>
        <translation>Dirección IP Ethernet</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="118"/>
        <source>Exposure display SN</source>
        <translation>Num.Serie Pantalla de exposición</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="199"/>
        <location filename="../qml/PageSysinfo.qml" line="209"/>
        <location filename="../qml/PageSysinfo.qml" line="219"/>
        <source>Fan Error!</source>
        <translation>¡Error del ventilador!</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="272"/>
        <source>Finished Projects</source>
        <translation>Proyectos Acabados</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="124"/>
        <source>GUI Version</source>
        <translation>Versión GUI</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="136"/>
        <source>Hardware State</source>
        <translation>Estado de Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="258"/>
        <location filename="../qml/PageSysinfo.qml" line="263"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="107"/>
        <source>Motion Controller HW Revision</source>
        <translation>Revisión Controlador Movimiento HW</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="97"/>
        <source>Motion Controller SN</source>
        <translation>Num.Serie Controlador Movimiento</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="102"/>
        <source>Motion Controller SW Version</source>
        <translation>Versión SW Controlador Movimiento</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="147"/>
        <location filename="../qml/PageSysinfo.qml" line="152"/>
        <location filename="../qml/PageSysinfo.qml" line="235"/>
        <location filename="../qml/PageSysinfo.qml" line="268"/>
        <location filename="../qml/PageSysinfo.qml" line="273"/>
        <location filename="../qml/PageSysinfo.qml" line="278"/>
        <location filename="../qml/PageSysinfo.qml" line="283"/>
        <location filename="../qml/PageSysinfo.qml" line="288"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="141"/>
        <source>Network State</source>
        <translation>Estado de Red</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="69"/>
        <source>Newer than SL1S</source>
        <comment>Printer model is unknown, but better than SL1S</comment>
        <translation>Mas nueva que la SL1S</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="65"/>
        <source>None</source>
        <comment>Printer model is not known/can&apos;t be determined</comment>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Not triggered</source>
        <translation>Sin activar</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Offline</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="142"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="177"/>
        <source>Open</source>
        <translation>Abierta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="57"/>
        <source>OS Image Version</source>
        <translation>Versión de la Imagen del SO</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="92"/>
        <source>Other Components</source>
        <translation>Otros Componentes</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="240"/>
        <source>Power Supply Voltage</source>
        <translation>Voltaje Fuente Alimentación</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="262"/>
        <source>Print Display Time Counter</source>
        <translation>Contador de Tiempo de la Pantalla de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="62"/>
        <source>Printer Model</source>
        <translation>Modelo de Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="80"/>
        <source>Printer Password</source>
        <translation>Contraseña de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="216"/>
        <source>Rear Fan</source>
        <translation>Ventilador Trasero</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="171"/>
        <source>Resin Sensor State</source>
        <translation>Estado Sensor Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="201"/>
        <location filename="../qml/PageSysinfo.qml" line="211"/>
        <location filename="../qml/PageSysinfo.qml" line="221"/>
        <source>RPM</source>
        <translation>RPM</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="157"/>
        <location filename="../qml/PageSysinfo.qml" line="162"/>
        <location filename="../qml/PageSysinfo.qml" line="167"/>
        <source>seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="298"/>
        <source>Show software packages &amp; licenses</source>
        <translation>Mostrar paquetes de software &amp; licencias</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="267"/>
        <source>Started Projects</source>
        <translation>Proyectos Comenzados</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="252"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="52"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="30"/>
        <source>System Information</source>
        <translation>Info del Sistema</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="156"/>
        <source>Time of Fast Tilt</source>
        <translation>Tiempo de Inclinación Rápida</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="166"/>
        <source>Time of High Viscosity Tilt:</source>
        <translation>Tiempo de inclinación  Alta Viscosidad:</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="161"/>
        <source>Time of Slow Tilt</source>
        <translation>Tiempo para Inclinación Lenta</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="277"/>
        <source>Total Layers Printed</source>
        <translation>Capas Totales Impresas</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="282"/>
        <source>Total Print Time</source>
        <translation>Tiempo Total de Impresión</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="287"/>
        <source>Total Resin Consumed</source>
        <translation>Resina Total Consumida</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="172"/>
        <source>Triggered</source>
        <translation>Activado</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="226"/>
        <source>UV LED</source>
        <translation>LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="196"/>
        <source>UV LED Fan</source>
        <translation>Ventilador LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="186"/>
        <source>UV LED Temperature</source>
        <translation>Temeperatura LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="257"/>
        <source>UV LED Time Counter</source>
        <translation>Contador Tiempo LED UV</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="241"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../qml/PageSysinfo.qml" line="151"/>
        <source>Wifi IP Address</source>
        <translation>Dirección IP Wifi</translation>
    </message>
</context>
<context>
    <name>PageTankSurfaceCleanerWizard</name>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="160"/>
        <source>Cleaning Adaptor</source>
        <translation>Adaptador de Limpieza</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="149"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="134"/>
        <source>Exposure[s]:</source>
        <translation>Exposición[s]:</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="81"/>
        <source>Here you can optionally adjust the exposure settings before cleaning starts.</source>
        <translation>Aquí puedes ajustar opcionalmente la configuración de la exposición antes de que comience la limpieza.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="89"/>
        <source>Less</source>
        <translation>Menos</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="108"/>
        <source>More</source>
        <translation>Más</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="172"/>
        <source>Now remove the cleaning adaptor along with the exposed film, careful not to tear it.</source>
        <translation>Ahora, retira el adaptador de limpieza junto con la peñicula expuesta, con cuidado de no romperla.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="161"/>
        <source>Please insert the cleaning adaptor as is ilustrated in the picture on the left.</source>
        <translation>Introduzce el adaptador de limpieza como se muestra en la imagen de la izquierda.</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="171"/>
        <source>Remove Adaptor</source>
        <translation>Retirar Adaptador</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="33"/>
        <source>Resin Tank Cleaning</source>
        <translation>Limpieza Tanque Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="61"/>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="69"/>
        <source>Tank Cleaning</source>
        <translation>Limpieza del Tanque</translation>
    </message>
    <message>
        <location filename="../qml/PageTankSurfaceCleanerWizard.qml" line="62"/>
        <source>This wizard will help you clean your resin tank and remove resin debris.

You will need a special tool - a cleaning adaptor. You can print it yourself - it is included as an example print in this machine&apos;s internal storage. Or you can download it at printables.com</source>
        <translation>Este asistente te ayudará a limpiar tu tanque de resina y a eliminar los restos de resina.

Necesitarás una herramienta especial - un adaptador de limpieza. Puede imprimirlo por ti mismo - se incluye como ejemplo de impresión en el almacenamiento interno de esta máquina. O puedes descargarlo en printables.com</translation>
    </message>
</context>
<context>
    <name>PageTiltmove</name>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="100"/>
        <source>Down</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="84"/>
        <source>Fast Down</source>
        <translation>Bajar Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="76"/>
        <source>Fast Up</source>
        <translation>Subir Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="32"/>
        <source>Move Resin Tank</source>
        <translation>Mover Tanque Resina</translation>
    </message>
    <message>
        <location filename="../qml/PageTiltmove.qml" line="92"/>
        <source>Up</source>
        <translation>Arriba</translation>
    </message>
</context>
<context>
    <name>PageTimeSettings</name>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="139"/>
        <source>12-hour</source>
        <comment>12h time format</comment>
        <translation>12 horas</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="140"/>
        <source>24-hour</source>
        <comment>24h time format</comment>
        <translation>24 horas</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="163"/>
        <source>Automatic time settings using NTP ...</source>
        <translation>Ajuste de hora automático usando NTP ...</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="94"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="138"/>
        <source>Native</source>
        <comment>Default time format determined by the locale</comment>
        <translation>Nativo</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="73"/>
        <source>Time</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="133"/>
        <source>Time Format</source>
        <translation>FormatoHorario</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="35"/>
        <source>Time Settings</source>
        <translation>Ajustes de Hora</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="115"/>
        <source>Timezone</source>
        <translation>Huso horario</translation>
    </message>
    <message>
        <location filename="../qml/PageTimeSettings.qml" line="47"/>
        <source>Use NTP</source>
        <translation>Ajuste automático de hora</translation>
    </message>
</context>
<context>
    <name>PageTowermove</name>
    <message>
        <location filename="../qml/PageTowermove.qml" line="101"/>
        <source>Down</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="85"/>
        <source>Fast Down</source>
        <translation>Bajar Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="77"/>
        <source>Fast Up</source>
        <translation>Subir Rápido</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="32"/>
        <source>Move Platform</source>
        <translation>Mover Plataforma</translation>
    </message>
    <message>
        <location filename="../qml/PageTowermove.qml" line="93"/>
        <source>Up</source>
        <translation>Arriba</translation>
    </message>
</context>
<context>
    <name>PageUnpackingCompleteWizard</name>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="79"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Despega con cuidado el adhesivo de protección de la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="69"/>
        <source>Please remove the safety sticker and open the cover.</source>
        <translation>Retira el adhesivo de seguridad y abre la tapa.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="59"/>
        <source>Remove the black foam from both sides of the platform.</source>
        <translation>Retira la espuma negra de ambos lados de la plataforma.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Desembalaje</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="89"/>
        <source>Unpacking done.</source>
        <translation>Desembalaje terminado.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingCompleteWizard.qml" line="49"/>
        <source>Unscrew and remove the resin tank and remove the black foam underneath it.</source>
        <translation>Desatornilla y retira el tanque de resina y retira la espuma negra que se encuentra debajo.</translation>
    </message>
</context>
<context>
    <name>PageUnpackingKitWizard</name>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="46"/>
        <source>Carefully peel off the protective sticker from the exposition display.</source>
        <translation>Despega con cuidado el adhesivo de protección de la pantalla de exposición.</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="32"/>
        <source>Unpacking</source>
        <translation>Desembalaje</translation>
    </message>
    <message>
        <location filename="../qml/PageUnpackingKitWizard.qml" line="56"/>
        <source>Unpacking done.</source>
        <translation>Desembalaje terminado.</translation>
    </message>
</context>
<context>
    <name>PageUpdatingFirmware</name>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="419"/>
        <source>A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing &quot;back&quot;</source>
        <translation>Ha ocurrido un problema durante la actualización, háznoslo saber (envíanos los registros). Que no cunda el pánico, tu impresora sigue funcionando igual que antes. Continuar presionando &quot;atrás&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="296"/>
        <source>Do not power off the printer while updating!&lt;br/&gt;Printer will be rebooted after a successful update.</source>
        <translation>¡No apagues la impresora durante la actualización!&lt;br/&gt;La impresora se reiniciará después de actualizarse con éxito.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="229"/>
        <source>Download failed.</source>
        <translation>Descarga fallida.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="170"/>
        <source>Downloading</source>
        <translation>Descargando</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="211"/>
        <source>Downloading firmware, installation will begin immediately after.</source>
        <translation>Descargando firmware, la instalación comenzará inmediatamente después.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="255"/>
        <source>Installing Firmware</source>
        <translation>Instalando Firmware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="360"/>
        <source>Printer is being restarted into the new firmware(%1), please wait</source>
        <translation>La impresora se reinicia con el nuevo firmware(%1), por favor espere</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="36"/>
        <source>Printer Update</source>
        <translation>Actualización de la Impresora</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="42"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="343"/>
        <source>Updated to %1 failed.</source>
        <translation>Actualizado a %1 fallido.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpdatingFirmware.qml" line="386"/>
        <source>Update to %1 failed.</source>
        <translation>Actualización a %1 fallida.</translation>
    </message>
</context>
<context>
    <name>PageUpgradeWizard</name>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="32"/>
        <source>Hardware Upgrade</source>
        <translation>Actualización de Hardware</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="90"/>
        <source>Only use the platform supplied. Using a different platform may cause resin to spill and damage your printer!</source>
        <translation>Emplea solamente la plataforma suministrada. Si usas otra, ¡podrías causar vertidos de resina y daños a tu impresora!</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="100"/>
        <source>Please note that downgrading is not supported. 

Downgrading your printer will erase your UV calibration and your printer will not work properly. 

You will need to recalibrate it using an external UV calibrator.</source>
        <translation>Tenga en cuenta que no se admite la degradación.

La degradación de su impresora borrará su calibración UV y su impresora no funcionará correctamente.

Deberá volver a calibrarla con un calibrador UV externo.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="49"/>
        <source>Proceed?</source>
        <translation>¿Continuar?</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="60"/>
        <source>Reassemble SL1 components and power on the printer. This will restore the original state.</source>
        <translation>Vuelve a montar las piezas de la SL1 y enciende la impresora. Esto la devolverá al estado original.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="47"/>
        <source>SL1S components detected (upgrade from SL1).</source>
        <translation>Componentes SL1S detectados (actualización para SL1).</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="69"/>
        <source>The configuration is going to be cleared now.</source>
        <translation>La configuración se borrará ahora.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="70"/>
        <source>The printer will ask for the inital setup after reboot.</source>
        <translation>La impresora solicitará la configuración inicial después de reiniciar.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="59"/>
        <source>The printer will power off now.</source>
        <translation>Ahora la impresora se apagará.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="48"/>
        <source>To complete the upgrade procedure, printer needs to clear the configuration and reboot.</source>
        <translation>Para completar el procedimiento de actualización, la impresora debe borrar la configuración y reiniciarse.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="109"/>
        <source>Upgrade done. In the next step, the printer will be restarted.</source>
        <translation>Actualización realizada. En el siguiente paso, se reiniciará la impresora.</translation>
    </message>
    <message>
        <location filename="../qml/PageUpgradeWizard.qml" line="80"/>
        <source>Use only the plastic resin tank supplied. Using the old metal resin tank may cause resin to spill and damage your printer!</source>
        <translation>Utiliza únicamente el tanque de resina plástica suministrado. El uso del anterior tanque de resina de metal puede hacer que la resina se derrame y dañe la impresora!</translation>
    </message>
</context>
<context>
    <name>PageUvCalibrationWizard</name>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="50"/>
        <source>1. If the resin tank is in the printer, remove it along with the screws.</source>
        <translation>1. Si el tanque de resina está en la impresora, retíralo junto con los tornillos.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="71"/>
        <source>1. Place the UV calibrator on the print display and connect it to the front USB.</source>
        <translation>1. Coloca el calibrador UV sobre la pantalla de impresión y conéctalo al USB frontal.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="52"/>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="73"/>
        <source>2. Close the cover, don&apos;t open it! UV radiation is harmful!</source>
        <translation>2. Cierra la tapa, no la abras. ¡La radiación UV puede ser perjudicial!</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="55"/>
        <source>Intensity: center %1, edge %2</source>
        <translation>Intensidad: centro %1, borde %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="84"/>
        <source>Open the cover, &lt;b&gt;remove and disconnect&lt;/b&gt; the UV calibrator.</source>
        <translation>Abre la tapa, &lt;br&gt;retira y desconecta&lt;/b&gt; el calibrador UV.</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="107"/>
        <source>The printer has been successfully calibrated!
Would you like to apply the calibration results?</source>
        <translation>¡La impresora se ha calibrado correctamente!
¿Te gustaría aplicar el resultado de la calibración?</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="100"/>
        <source>The result of calibration:</source>
        <translation>El resultado de la calibración:</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="32"/>
        <source>UV Calibration</source>
        <translation>Calibración UV</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="102"/>
        <source>UV Intensity: %1, σ = %2</source>
        <translation>Intensidad UV:%1, σ =%2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="103"/>
        <source>UV Intensity min: %1, max: %2</source>
        <translation>Intensidad UV min: %1, max: %2</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="101"/>
        <source>UV PWM: %1</source>
        <translation>PWM UV: %1</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="57"/>
        <source>Warm-up: %1 s</source>
        <translation>Calentamiento:%1 s</translation>
    </message>
    <message>
        <location filename="../qml/PageUvCalibrationWizard.qml" line="48"/>
        <source>Welcome to the UV calibration.</source>
        <translation>Bienvenido a la calibración UV.</translation>
    </message>
</context>
<context>
    <name>PageVerticalList</name>
    <message>
        <location filename="../qml/PageVerticalList.qml" line="57"/>
        <source>Loading, please wait...</source>
        <translation>Cargando, por favor espera...</translation>
    </message>
</context>
<context>
    <name>PageVideos</name>
    <message>
        <location filename="../qml/PageVideos.qml" line="26"/>
        <source>Scanning the QR code will take you to our YouTube playlist with videos about this device.

Alternatively, use this link:</source>
        <translation>Al escanear el código QR, te llevará a nuestra lista de reproducción de YouTube con videos sobre este dispositivo.

Alternativamente, usa este enlace:</translation>
    </message>
    <message>
        <location filename="../qml/PageVideos.qml" line="23"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
</context>
<context>
    <name>PageWait</name>
    <message>
        <location filename="../qml/PageWait.qml" line="27"/>
        <source>Please Wait</source>
        <translation>Por favor Espere</translation>
    </message>
</context>
<context>
    <name>PageWifiNetworkSettings</name>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="193"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="203"/>
        <source>Configuring the connection,
please wait...</source>
        <comment>This is horizontal-center aligned, ideally 2 lines</comment>
        <translation>Configurando la conexión, espera...</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Connected</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="109"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="90"/>
        <source>Disconnected</source>
        <translation>Desconectado</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="233"/>
        <source>Do you really want to forget this network&apos;s settings?</source>
        <translation>¿Realmente quieres olvidar la configuración de esta red?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="232"/>
        <source>Forget network?</source>
        <translation>¿Olvidar red?</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="223"/>
        <source>Forget network</source>
        <comment>Removes all information about the network(settings, passwords,...)</comment>
        <translation>Olvidar red</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="102"/>
        <source>Network Info</source>
        <translation>Información de Red</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="212"/>
        <source>Revert</source>
        <comment>Turn back the changes and go back to the previous configuration.</comment>
        <translation>Revertir</translation>
    </message>
    <message>
        <location filename="../qml/PageWifiNetworkSettings.qml" line="34"/>
        <source>Wireless Settings</source>
        <translation>Ajustes Inalámbricos</translation>
    </message>
</context>
<context>
    <name>PageYesNoSimple</name>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>¿Estás Seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="71"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSimple.qml" line="59"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
</context>
<context>
    <name>PageYesNoSwipe</name>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="29"/>
        <source>Are You Sure?</source>
        <translation>¿Estás Seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="115"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="76"/>
        <source>Swipe to proceed</source>
        <translation>Desliza para continuar</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoSwipe.qml" line="102"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
</context>
<context>
    <name>PageYesNoWithPicture</name>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="28"/>
        <source>Are You Sure?</source>
        <translation>¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="265"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="174"/>
        <source>Swipe for a picture</source>
        <translation>Desliza para ver imagen</translation>
    </message>
    <message>
        <location filename="../qml/PageYesNoWithPicture.qml" line="253"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
</context>
<context>
    <name>PrusaPicturePictureButtonItem</name>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Plugged in</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../qml/PrusaPicturePictureButtonItem.qml" line="58"/>
        <source>Unplugged</source>
        <translation>Desconectado</translation>
    </message>
</context>
<context>
    <name>PrusaSwitch</name>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="113"/>
        <source>Off</source>
        <translation>Desact</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/PrusaSwitch.qml" line="172"/>
        <source>On</source>
        <translation>Act</translation>
    </message>
</context>
<context>
    <name>PrusaWaitOverlay</name>
    <message>
        <location filename="../qml/PrusaWaitOverlay.qml" line="74"/>
        <source>Please wait...</source>
        <comment>can be on multiple lines</comment>
        <translation>Por favor espere...</translation>
    </message>
</context>
<context>
    <name>SwipeSign</name>
    <message>
        <location filename="../qml/SwipeSign.qml" line="98"/>
        <source>Swipe to confirm</source>
        <translation>Desliza para confirmar</translation>
    </message>
</context>
<context>
    <name>WarningText</name>
    <message>
        <location filename="../qml/WarningText.qml" line="45"/>
        <source>Must not be empty, only 0-9, a-z, A-Z, _ and - are allowed here!</source>
        <translation>No debe estar vacío, ¡solo 0-9, a-z, A-Z, _ y - se permite aquí!</translation>
    </message>
</context>
<context>
    <name>WindowHeader</name>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="115"/>
        <source>back</source>
        <translation>atrás</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="130"/>
        <source>cancel</source>
        <translation>cancelar</translation>
    </message>
    <message>
        <location filename="../PrusaComponents/Delegates/WindowHeader.qml" line="144"/>
        <source>close</source>
        <translation>cerrar</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="65"/>
        <source>Activating</source>
        <translation>Activando</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="104"/>
        <source>ambient temperature</source>
        <translation>temperatura ambiente</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="113"/>
        <source>blower fan</source>
        <translation>ventilador radial</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="650"/>
        <source>Cancel?</source>
        <translation>Cancelar?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="665"/>
        <source>Cancel the current print job?</source>
        <translation>¿Cancelar el trabajo de impresión actual?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="66"/>
        <source>Connected</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="107"/>
        <source>CPU temperature</source>
        <translation>Temperatura de la CPU</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="68"/>
        <source>Deactivated</source>
        <translation>Desactivado</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="67"/>
        <source>Deactivating</source>
        <translation>Desactivando</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="699"/>
        <location filename="../qml/main.qml" line="723"/>
        <source>DEPRECATED PROJECTS</source>
        <translation>PROYECTOS OBSOLETOS</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="407"/>
        <source>Initializing...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="417"/>
        <source>MC Update</source>
        <translation>Actualización MC</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="310"/>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="356"/>
        <source>Now it&apos;s safe to remove USB drive</source>
        <translation>Ahora es seguro retirar la unidad USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>Press &quot;cancel&quot; to abort.</source>
        <translation>Pulsa &quot;Cancelar&quot; para abortar.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="688"/>
        <source>Printer should not be turned off in this state.
Finish or cancel the current action and try again.</source>
        <translation>La impresora no se puede apagar en este estado. Finaliza o cancela la acción actual e inténtalo de nuevo.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="50"/>
        <source>Prusa SL1 Touchscreen User Interface</source>
        <translation>Interfaz de Usuario de la Pantalla Táctil Prusa SL1</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="116"/>
        <source>rear fan</source>
        <translation>ventilador trasero</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="355"/>
        <source>Remove USB</source>
        <translation>Retirar USB</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="339"/>
        <source>Remove USB flash drive?</source>
        <translation>¿Retirar la memoria USB?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="714"/>
        <location filename="../qml/main.qml" line="737"/>
        <source>&lt;printer IP&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="724"/>
        <source>Some incompatible file were found, you can view them at http://%1/old-projects.

Would you like to remove them?</source>
        <translation>Se encontraron algunos proyectos incompatibles, puedes descargarlos en

http://%1/old-projects.

¿Quieres eliminarlos?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="700"/>
        <source>Some incompatible projects were found, you can download them at 

http://%1/old-projects

Do you want to remove them?</source>
        <translation>Se encontraron algunos proyectos incompatibles, puede descargarlos en

http://%1/old-projects

¿Quieres eliminarlos?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="418"/>
        <source>The Motion Controller firmware is being updated.

Please wait...</source>
        <translation>Se está actualizando el firmware del controlador de movimiento.

Espera por favor...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="408"/>
        <source>The printer is initializing, please wait ...</source>
        <translation>La impresora se está inicializando, espere por favor ...</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="364"/>
        <source>The USB drive cannot be safely removed because it is now in use, please wait.</source>
        <translation>La unidad USB no se puede extraer de forma segura porque está en uso, por favor espere.</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="637"/>
        <source>Turn Off?</source>
        <translation>¿Apagar?</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="119"/>
        <source>unknown device</source>
        <translation>dispositivo desconocido</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="64"/>
        <location filename="../qml/main.qml" line="69"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="110"/>
        <source>UV LED fan</source>
        <translation>Ventilador LED UV</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="101"/>
        <source>UV LED temperature</source>
        <translation>Temperatura de los LED UV</translation>
    </message>
</context>
<context>
    <name>utils</name>
    <message>
        <location filename="../qml/utils.js" line="47"/>
        <source>Less than a minute</source>
        <translation>Menos de un minuto</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="50"/>
        <source>%n h</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n h</numerusform>
            <numerusform>%n h</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="55"/>
        <source>%n hour(s)</source>
        <comment>how many hours</comment>
        <translation>
            <numerusform>%n hora</numerusform>
            <numerusform>%n horas</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="51"/>
        <source>%n min</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n min</numerusform>
            <numerusform>%n min</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/utils.js" line="56"/>
        <source>%n minute(s)</source>
        <comment>how many minutes</comment>
        <translation>
            <numerusform>%n minuto</numerusform>
            <numerusform>%n minutos</numerusform>
        </translation>
    </message>
</context>
</TS>
