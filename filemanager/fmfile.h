/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FMFILE_H
#define FMFILE_H

#include <algorithm>
#include <QObject>
#include <QDebug>
#include <QString>
#include <QVector>
#include <QVariant>

class FMFile
{
    Q_GADGET
public:
    enum Type {
        InvalidType = 0,
        File = 1,
        Directory,
    };
    Q_ENUM(Type)
    static Type parseType(const QString & raw_type);

    enum Origin {
        InvalidOrigin = 0,
        Local,
        Remote,
        Usb,
        PreviousPrints,
        Raucb
    };
    Q_ENUM(Origin)
    static Origin parseOrigin(const QString & raw_origin);

    FMFile(QString _path = QStringLiteral(""),
           Origin _origin = Origin::InvalidOrigin,
           size_t _size = 0,
           qreal _mtime = 0,
           FMFile * _parentFile = nullptr);

    virtual ~FMFile();

    Q_PROPERTY(QString path MEMBER path)
    Q_PROPERTY(Type type READ type)
    Q_PROPERTY(Origin origin MEMBER origin)
    Q_PROPERTY(qreal size READ getSize)
    Q_PROPERTY(qreal mtime MEMBER mtime)

    // Transform to qreal
    qreal getSize() const {return static_cast<qreal>(size); }

    virtual Type type() const {return Type::File;}

    // Path as given by the filemanager
    QString path;

    Origin origin;

    // size in bytes
    size_t size;

    // Time of the last modification
    qreal mtime;

    // Children, if applicable
    QVector<FMFile*> children;

    inline friend QDebug operator<<(QDebug debug, const FMFile &file) {
        debug << "FMFile[" << file.path << file.origin << file.size << file.mtime << "]";
        return debug;
    }

    template<class RandomIt>
    static void sortByTime(RandomIt begin,RandomIt end) {
        std::sort(begin, end, [](FMFile* a, FMFile* b) {
            Q_ASSERT(a != nullptr);
            Q_ASSERT(b != nullptr);
            Q_ASSERT(a->path != "");
            Q_ASSERT(b->path != "");
            if(a->type() == FMFile::Directory && b->type() == FMFile::File) {
                return true;
            }
            else if(a->type() ==  b->type()) {
                return a->mtime > b->mtime;
            }
            return false;
        });
    }
};

#endif // FMFILE_H
