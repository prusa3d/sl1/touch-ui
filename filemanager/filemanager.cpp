/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMetaObject>
#include <QMetaProperty>
#include <QHash>
#include "filemanager_proxy.h"
#include <filemanager.h>
#include <dbusutils.h>
#include <basedbusobject.h>

FileManager::FileManager(QJSEngine *_engine, QObject *parent) :
    BaseDBusObject(*_engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    m_filemanagerProxy(service, path, QDBusConnection::systemBus(), this),
    m_engine(*_engine)
{
    m_filemanagerProxy.setTimeout(210000);
    registerPrinterTypes();

    connect(&m_filemanagerProxy, &FileManagerProxy::MediaInserted, this, &FileManager::mediaInserted);
    connect(&m_filemanagerProxy, &FileManagerProxy::MediaEjected, this, &FileManager::mediaEjected);
    connect(&m_filemanagerProxy, &FileManagerProxy::OneClickPrintFile, this, &FileManager::oneClickPrintFile);
}

const QString &FileManager::current_media_path() const
{
    return m_current_media_path;
}

void FileManager::media_umount(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.media_umount();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::media_mount(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.media_mount();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::setCurrent_media_path(const QString &newCurrent_media_path)
{
    if (m_current_media_path == newCurrent_media_path)
        return;
    m_current_media_path = newCurrent_media_path;
    emit current_media_pathChanged();
}

void FileManager::start_transfer(const QString &path, int size, const QString &transfer_type, bool to_print, int start_cmd_id, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.start_transfer(path, size, transfer_type, to_print, start_cmd_id);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::transfer_progress(float progress, int transferred, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.transfer_progress(progress, transferred);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::stop_transfer(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.stop_transfer();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::download_from_url(const QString &url, const QString &storage, const QString &path, NMStringMap headers, const QString &transfer_type, int start_cmd_id, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.download_from_url(url, storage, path, headers, transfer_type, start_cmd_id);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::convert_path(const QString &target, const QString &path, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.convert_path(target, path);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::validate_project(const QString &filename, const QString &target, const QString &path, int content_length, bool overwrite, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.validate_project(filename, target, path, content_length, overwrite);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::create_folder(const QString &path, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.create_folder(path);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::setDisk_usage(QVariantMap disk_usage)
{
    if (m_disk_usage == disk_usage)
        return;

    m_disk_usage = disk_usage;
    emit disk_usageChanged(m_disk_usage);
}

void FileManager::setExtensions(QStringList extensions)
{
    if (m_extensions == extensions)
        return;

    m_extensions = extensions;
    emit extensionsChanged(m_extensions);
}

void FileManager::setExtensionsDeprecated(QStringList obsoleteExtensions)
{
    if (m_extensionsDeprecated == obsoleteExtensions)
        return;

    m_extensionsDeprecated = obsoleteExtensions;
    emit extensionsDeprecatedChanged(m_extensionsDeprecated);
}

void FileManager::setEtag(QVariantMap etag)
{
    if (m_etag == etag)
        return;

    m_etag = etag;
    emit etagChanged(m_etag);
}

QVariantMap FileManager::last_exception() const
{
    return m_last_exception;
}

QVariantMap FileManager::last_update() const
{
    return m_last_update;
}

QVariantMap FileManager::has_deprecated() const
{
    return m_has_deprecated;
}

void FileManager::setLast_exception(QVariantMap new_val)
{
    if (new_val != m_last_exception)
    {
        m_last_exception = new_val;
        emit last_exceptionChanged(new_val);
    }
}

void FileManager::setLast_update(QVariantMap new_val)
{
    if (new_val != m_last_update)
    {
        m_last_update = new_val;
        emit last_updateChanged(new_val);
    }
}

void FileManager::setHas_deprecated(QVariantMap new_val)
{

    if (new_val != m_has_deprecated)
    {
        m_has_deprecated = new_val;
        emit has_deprecatedChanged(new_val);
    }
}

QVariantMap FileManager::transform(QVariant value){

    QVariantMap orig = value.value<QVariantMap>();
    QVariantMap tmp;
    if (value.canConvert<QDBusArgument>())
    {
        QDBusArgument arg = value.value<QDBusArgument>();
        arg >> orig;
    }
    foreach (const QString &key, orig.keys())
    {

        if (key == "children"){

            QDBusArgument argument = orig[key].value<QDBusArgument>();
            QVariantList parseList = qdbus_cast<QVariantList>(argument);
            QVariantList result;

            for (int i = 0; i < parseList.size(); ++i) {
                QDBusArgument arg = parseList[i].value<QDBusArgument>();
                QVariantMap item = qdbus_cast<QVariantMap>(arg);
                result << item;
            }
            tmp[key] = result;

        } else if (orig[key].canConvert<QDBusArgument>()){

            tmp[key] = transform(orig[key]);
        }
        else{
            tmp[key] = QVariant::fromValue(orig[key]);
        }
    }
    return tmp;
}

bool FileManager::set(const QString &name, const QVariant & value)
{
    if (name == QStringLiteral("has_deprecated")) {
        MapStringBool orig = value.value<MapStringBool>();
        QVariantMap tmp1;
        if(value.canConvert<QDBusArgument>()) {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> orig;
        }
        foreach(const QString & key, orig.keys()) {
            tmp1[key] = QVariant::fromValue(orig[key]);
        }

        setHas_deprecated(tmp1);
        return true;
    }
    else  if (name == QStringLiteral("len_download_deprecated_files")){
        setLen_download_deprecated_files(value.toInt());
    }
    else if (name == QStringLiteral("last_update"))
    {
        QVariantMap tmp = transform(value);
        setLast_update(tmp);
    }
    else if (name == QStringLiteral("last_exception"))
    {
        QVariantMap tmp = transform(value);
        setLast_exception(tmp);
    }
    else if (name == QStringLiteral("disk_usage")) {
        QVariantMap tmp = transform(value);
        setDisk_usage(tmp);
    }
    else if(name == QStringLiteral("etag")) {
        QVariantMap tmp = transform(value);
        setEtag(tmp);
    }
    else if(name == QStringLiteral("extensions")) {
        setExtensions(DBusUtils::convert<QStringList>(value));
    }
    else if(name == QStringLiteral("extensions_deprecated")) {
        setExtensionsDeprecated(DBusUtils::convert<QStringList>(value));
    }
    else if(name == QStringLiteral("current_media_path")) {
        setCurrent_media_path(DBusUtils::convert<QString>(value));
    }
    else if(name == QStringLiteral("transfer")) {
        auto transfer = DBusUtils::convert<QVariantMap>(value);
        setTransfer(transfer);
    }
    else if(name == QStringLiteral("usb_name")) {
        setUsb_name(DBusUtils::convert<QString>(value));
    }
    else if(name == QStringLiteral("usb_mount_point")) {
        setUsb_mount_point(DBusUtils::convert<QString>(value));
    }
    else if(name == QStringLiteral("media_present")){
        setMedia_present(DBusUtils::convert<bool>(value));
    }
    else if(name == QStringLiteral("media_mounted")) {
        setMedia_mounted(DBusUtils::convert<bool>(value));
    }
    else
    {
        bool r = BaseDBusObject::set(name, value);
        if(!r) {
            qDebug() << "An attempt to set unknown property " << name;
            return false; // Unknown property
        }
    }
    return true;
}

void FileManager::get_all(const int maxdepth, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.get_all(maxdepth);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::get_all(const int maxdepth, std::function<void(QVariantList)> callback_ok, std::function<void(QVariantList)> callback_fail)
{
    auto pending = m_filemanagerProxy.get_all(maxdepth);
    DBusUtils::handle_callbacks(pending, callback_ok, callback_fail);
}

void FileManager::get_from_path(const QString &path, const int maxdepth, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.get_from_path(path, maxdepth);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::remove(QString path, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.remove(path);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::remove_dir(QString path, bool recursive, std::function<void (QVariantList)> callback_ok, std::function<void (QVariantList)> callback_fail)
{
    auto pending = m_filemanagerProxy.remove_dir(path, recursive);
    DBusUtils::handle_callbacks(pending, callback_ok, callback_fail);
}

void FileManager::remove(QString path, std::function<void (QVariantList)> callback_ok, std::function<void (QVariantList)> callback_fail)
{
    auto pending = m_filemanagerProxy.remove(path);
    DBusUtils::handle_callbacks(pending, callback_ok, callback_fail);
}

void FileManager::move(QString srcPath, QString dstPath, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.move(srcPath, dstPath);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::move(QString srcPath, QString dstPath, std::function<void (QVariantList)> callback_ok, std::function<void (QVariantList)> callback_fail)
{
    auto pending = m_filemanagerProxy.move(srcPath, dstPath);
    DBusUtils::handle_callbacks(pending, callback_ok, callback_fail);
}

void FileManager::current_project_set(QString path, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.current_project_set(path);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::current_project_get(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.current_project_get();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::current_project_clean(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.current_project_clean();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}


void FileManager::get_metadata(const QString &path, const bool thumbnail, std::function<void(QVariantList)> callback_ok,  std::function<void(QVariantList)> callback_fail)
{
    auto pending = m_filemanagerProxy.get_metadata(path, thumbnail);
    DBusUtils::handle_callbacks(pending, callback_ok, callback_fail);
}

QList<QString> FileManager::sync_has_deprecated() const
{
    QList<QString> rval;
    MapStringBool objMap = m_filemanagerProxy.has_deprecated();
    // include only if is true
    foreach(const QString & key, objMap.keys()) {
        bool value = objMap[key];
        if(value){
            rval <<  key;
        }
    }
    return rval;
}

QStringList FileManager::sync_extensions_deprecated() const
{
    return m_filemanagerProxy.extensions_deprecated();
}

QVariantMap FileManager::sync_last_exception() const
{
    return m_filemanagerProxy.last_exception();
}

void FileManager::remove_all_deprecated_files(QJSValue callback_ok, QJSValue callback_fail){
    auto pending = m_filemanagerProxy.remove_all_deprecated_files();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::move_deprecated_files(QJSValue callback_ok, QJSValue callback_fail){
    auto pending = m_filemanagerProxy.move_deprecated_files();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::remove_downloaded_deprecated_files(QJSValue callback_ok, QJSValue callback_fail){
    auto pending = m_filemanagerProxy.remove_downloaded_deprecated_files();
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

void FileManager::average_extraction_time(QString path, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_filemanagerProxy.average_extraction_time(path);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
}

int FileManager::len_download_deprecated_files() const
{
    return m_len_download_deprecated_files;
}

void FileManager::setLen_download_deprecated_files(int new_val)
{
     if (new_val != m_len_download_deprecated_files)
    {
        m_len_download_deprecated_files = new_val;
        emit len_download_deprecated_filesChanged(new_val);
    }
}

int FileManager::sync_len_download_deprecated_files() const
{
    return m_filemanagerProxy.len_download_deprecated_files();
}

QVariantMap FileManager::transfer() const
{
    return m_transfer;
}

void FileManager::setTransfer(const QVariantMap &newTransfer)
{
    if (m_transfer == newTransfer)
        return;
    m_transfer = newTransfer;
    emit transferChanged();
}

QString FileManager::usb_name() const
{
    return m_usb_name;
}

void FileManager::setUsb_name(const QString &newUsb_name)
{
    if (m_usb_name == newUsb_name)
        return;
    m_usb_name = newUsb_name;
    emit usb_nameChanged();
}

QString FileManager::usb_mount_point() const
{
    return m_usb_mount_point;
}

void FileManager::setUsb_mount_point(const QString &newUsb_mount_point)
{
    if (m_usb_mount_point == newUsb_mount_point)
        return;
    m_usb_mount_point = newUsb_mount_point;
    emit usb_mount_pointChanged();
}

bool FileManager::busy() const
{
    return m_busy;
}

void FileManager::setBusy(bool newBusy)
{
    if (m_busy == newBusy)
        return;
    m_busy = newBusy;
    emit busyChanged();
}

bool FileManager::media_present() const
{
    return m_media_present;
}

void FileManager::setMedia_present(bool newMedia_present)
{
    if (m_media_present == newMedia_present)
        return;
    m_media_present = newMedia_present;
    emit media_presentChanged();
}

bool FileManager::media_mounted() const
{
    return m_media_mounted;
}

void FileManager::setMedia_mounted(bool newMedia_mounted)
{
    if (m_media_mounted == newMedia_mounted)
        return;
    m_media_mounted = newMedia_mounted;
    emit media_mountedChanged();
}
