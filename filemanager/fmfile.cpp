/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include "fmfile.h"

FMFile::Type FMFile::parseType(const QString &raw_type) {
    if(raw_type == QStringLiteral("file")) return Type::File;
    else if(raw_type == QStringLiteral("folder") || raw_type == QStringLiteral("directory")) return Type::Directory;
    else return Type::InvalidType;
}

FMFile::Origin FMFile::parseOrigin(const QString &raw_origin) {
    if(raw_origin == QStringLiteral("usb")) return Origin::Usb;
    else if(raw_origin == QStringLiteral("local")) return Origin::Local;
    else if(raw_origin == QStringLiteral("remote")) return Origin::Remote;
    else if(raw_origin == QStringLiteral("previous-prints")) return Origin::PreviousPrints;
    else if(raw_origin == QStringLiteral("raucb")) return Origin::Raucb;
    else return InvalidOrigin;
}

FMFile::FMFile(QString _path,
               Origin _origin,
               size_t _size,
               qreal _mtime,
               FMFile *_parentFile) :
    path(_path),
    origin(_origin),
    size(_size),
    mtime(_mtime)
{
    Q_UNUSED(_parentFile)
}

FMFile::~FMFile() {
}
