/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "fmdata.h"
#include "fmcontext.h"
#include "fmfile.h"
#include "fmdirectory.h"
#include "filemanager.h"

#include <QVariantMap>
#include <QDBusArgument>
#include <QList>
#include <QVector>
#include <QVariantList>
#include <QTimer>
#include <QQmlEngine>
/** Convert from possible QVariant(QDBusArgument) to the correct type */
template<class T = QVariant>
T convert(const QVariant & value) {
    T orig = value.value<T>();
    if (value.canConvert<QDBusArgument>())
    {
        QDBusArgument arg = value.value<QDBusArgument>();
        arg >> orig;
    }
    return orig;
}

FMDirectory* processDirectory(QVariantMap raw_directory) {
    FMDirectory * directory = new FMDirectory(
            convert<QString>(raw_directory["path"]),
            FMFile::parseOrigin(convert<QString>(raw_directory["origin"])),
            convert<qreal>(raw_directory["mtime"])
            );
    QVariantList raw_children = convert<QVariantList>(raw_directory["children"]);
    for(const auto & raw_child : raw_children) {
        QVariantMap child = convert<QVariantMap>(raw_child);
        switch(FMFile::parseType(convert<QString>(child["type"]))) {
        case FMFile::Type::Directory:
            directory->addDirectory(processDirectory(child));
            break;
        case FMFile::Type::File:
            directory->addFile(
                        convert<QString>(child["path"]),
                        FMFile::parseOrigin(convert<QString>(child["origin"])),
                        convert<qlonglong>(child["size"]),
                        convert<qreal>(child["mtime"])
                        );
            break;
        default:
            qWarning() << "Record of unknown type:" << convert<QString>(child["type"]);
        }
    }

    FMFile::sortByTime(std::next(directory->children.begin()), directory->children.end());
    return directory;
}

void FMData::handleUpdate(QVariantMap last_update) {
    Q_UNUSED(last_update)
    // TODO: Property last_update is changed before the new FS data is available via get_all(..),
    // the reading is delayed by QTimer here because of that. This should be fix by
    // a) partial update
    // b) delay modifying the property to a time after the new items have been loaded?
    // Maybe the delay is introduced by the mounting mechanism: folder is created first -> event generated, and content is mounted afterwards?
    //
    QTimer::singleShot(1000, this, [&]{
        // Initiate a new reading
        FileManager::getInstance()->get_all(
                    -1,
        [this](QVariantList data) {
            this->refreshData(data);
        },
        [](QVariantList errors){
            for(const QVariant & item : errors) {
                qWarning() << "Error: " << item;
            }
        }
        );
    });


}

void FMData::handleFilemanagerDeath() {
    emit dataChangeBegin();
    root->clear(); // Remove the original content
    emit dataChangeEnd();
}


FMData::FMData(QQmlEngine *parent) : QObject(parent), root(new FMDirectory("/", FMFile::Origin::InvalidOrigin, 0, {}, nullptr))
{
    setObjectName("FMDataInstance");

    root->children[0] = root; // Root is it's own parent.

    // Refresh the whole tree on every change(TODO: apply selective updates)
    connect(FileManager::getInstance(), &FileManager::last_updateChanged, this, &FMData::handleUpdate);
    connect(FileManager::getInstance(), &FileManager::isServiceRegisteredChanged, this, [this]() {
        FileManager * filemanager(FileManager::getInstance());
        if(filemanager->isServiceRegistered()) this->handleUpdate(filemanager->last_update());
        else handleFilemanagerDeath();
    });
}

FMData::~FMData() {
    if(root) delete root;
}

FMFile *FMData::getFile(QString _path, FMFile::Origin _origin, FMFile * top) {
    if(top == nullptr) top = getRoot(); // Effective default
    if(top->path == _path && (top->origin == _origin || _origin == FMFile::Origin::InvalidOrigin)) {
        return top;
    }
    else if(top->type() == FMFile::Directory) {
        FMDirectory * dir = static_cast<FMDirectory*>(top);
        for(FMFile* child : qAsConst(dir->children)) {
            if(child != dir->children[0]) { // First child is reference to parent
                FMFile * subresult = getFile(_path, _origin,  child);
                if(subresult) return subresult;
            }
        }
    }
    return nullptr;
}

void FMData::get_metadata(QString path, bool get_thumbnails, std::function<void (metadata_t)> callback_ok, std::function<void (QVariantList)> callback_fail) {
    FileManager::getInstance()->get_metadata(path, get_thumbnails, [=](QVariantList params){
        QVariantMap resp = convert<QVariantMap>(params.first());
        callback_ok(metadata_t(resp));

    }, [=](QVariantList errors){
        callback_fail(errors);
    });
}

void FMData::refreshData(QVariantList data)
{
    QVariant value;
    if(data.length() > 0) {
        value = data.first();
    }
    QVariantMap orig = convert<QVariantMap>(value);

    emit dataChangeBegin();
    root->clear(); // Remove the original content

    // Replace it with new snapshot
    for(const auto & key : orig.keys()) {
        if(key == "last_update") {
            /* TODO: conspicuous nothing */
        }
        else if(key == "local") {
            auto source = convert<QVariantMap>(orig[key]);
            root->addDirectory(processDirectory(source));
        }
        else if(key == "usb") {
            auto source = convert<QVariantMap>(orig[key]);
            root->addDirectory(processDirectory(source));
        }
        else if(key == "remote") {
            auto source = convert<QVariantMap>(orig[key]);
            root->addDirectory(processDirectory(source));
        }
        else if(key == "previous-prints") {
            auto source = convert<QVariantMap>(orig[key]);
            root->addDirectory(processDirectory(source));
        }
        else if(key == "raucb") {
            auto source = convert<QVariantMap>(orig[key]);
            root->addDirectory(processDirectory(source));
        }
        else {
            qDebug() << "Unknown FMData source: " << key;
        }
    }
    emit dataChangeEnd();
}

void FMData::qmlRegister(QQmlEngine &engine) {
    // Create the instance with QQmlEngine as a parent, so it's released on its deletion
    FileManager::getInstance(&engine);
    FMData::getInstance(&engine);

    qmlRegisterUncreatableType<FMFile>("cz.prusa3d.sl1.filemanager", 1, 0, "FMFile", "No need.");
    qmlRegisterType<FMContext>("cz.prusa3d.sl1.filemanager", 1,0, "FMContext");
    qmlRegisterSingletonInstance("cz.prusa3d.sl1.filemanager", 1, 0, "Filemanager", FileManager::getInstance());
}

FMData::metadata_t::metadata_t(QVariantMap data) : valid(false)
{
    QVariantMap resp = convert<QVariantMap>(data);
    QVariantMap files = convert<QVariantMap>(resp["files"]);
    QVariantMap metadata = convert<QVariantMap>(files["metadata"]);
    QVariantMap config = convert<QVariantMap>(metadata["config"]);

    bool ok;
    bool nok = false;

    // Note: In case of any problem, default-initialized object is returned both by [] and conversion method

    path = files["path"].toString();
    size = files["size"].toLongLong(&ok); nok |= !ok;
    qreal mtime_flat = files["mtime"].toReal(&ok); nok |= !ok;
    mtime = QDateTime::fromSecsSinceEpoch(mtime_flat);

    expTime = config["expTime"].toReal(&ok); nok |= !ok;
    expTimeFirst = config["expTimeFirst"].toReal(&ok); nok |= !ok;
    fileCreationTimestamp = config["fileCreationTimestamp"].toString();
    layerHeight = config["layerHeight"].toReal(&ok); nok |= !ok;
    materialName = config["materialName"].toString();
    numFade = config["numFade"].toInt(&ok); nok |= !ok;
    numFast = config["numFast"].toInt(&ok); nok |= !ok;
    numSlow = config["numSlow"].toInt(&ok); nok |= !ok;
    printProfile = config["printProfile"].toString();
    printTime = config["printTime"].toReal();
    printerModel = config["printerModel"].toString();
    printerProfile = config["printerProfile"].toString();
    printerVariant = config["printerVariant"].toString();
    prusaSlicerVersion = config["prusaSlicerVersion"].toString();
    usedMaterial = config["usedMaterial"].toReal(&ok); nok |= !ok;

    if(metadata.contains("thumbnail")) {
        QVariantMap thumbnails = convert<QVariantMap>(metadata["thumbnail"]);
        QStringList files = thumbnails["files"].toStringList();
        for(const QString &  v : files) {
            this->thumbnails.push_back(v);
        }
    }
    valid = ! nok;
}

QDebug &operator<<(QDebug & d, const FMData::metadata_t &metadata) {
    d << "metadata_t["
      << "expTime:" << metadata.expTime
      << "expTimeFirst:"<<metadata.expTimeFirst
      << "fileCreationTimestamp:" << metadata.fileCreationTimestamp
      << "layerHeight:" << metadata.layerHeight
      << "printerModel:" << metadata.printerModel
      << "printTime:" << metadata.printTime
      << "usedMaterial:" << metadata.usedMaterial
      << "thumbnails:" << metadata.thumbnails.length()
      << "valid:" << metadata.valid
      << "]";
    return d;
}
