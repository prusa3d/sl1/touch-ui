/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include <QJSValue>
#include <QJSEngine>
#include <QQmlEngine>
#include "printer_types.h"
#include "filemanager_proxy.h"
#include "properties_proxy.h"
#include <basedbusobject.h>
/*
 * Proxy class for interface cz.prusa3d.sl1.filemanager0
 */
class FileManager : public BaseDBusObject
{
    Q_OBJECT

    inline static  const QString service{"cz.prusa3d.sl1.filemanager0"};
    inline static  const QString interface{"cz.prusa3d.sl1.filemanager0"};
    inline static const QString path{"/cz/prusa3d/sl1/filemanager0"};

    FileManagerProxy m_filemanagerProxy;
    QJSEngine &m_engine;
    QVariantMap m_last_exception;
    QVariantMap m_last_update;
    QVariantMap m_has_deprecated;
    int m_len_download_deprecated_files;
    QVariantMap m_disk_usage;
    QStringList m_extensions;
    QStringList m_extensionsDeprecated;
    QVariantMap m_etag;
    QString m_current_media_path;

    explicit FileManager(QJSEngine *_engine, QObject *parent = nullptr);

    void setLast_exception(QVariantMap new_val);

    void setLast_update(QVariantMap new_val);

    void setHas_deprecated(QVariantMap new_val);

    void setLen_download_deprecated_files(int new_val);

    QVariantMap transform(QVariant value);

    void setCurrent_media_path(const QString &newCurrent_media_path);


public:
    static FileManager * getInstance(QQmlEngine * parent = nullptr) {
        static FileManager * instance = nullptr;
        if(instance) return instance;
        else {
            Q_ASSERT_X(parent, "FileManager::getInstance", "No parent. Parent is expected to cleanup the instance, if you actually want to handle it yourself, remove this assert!");
            return instance = new FileManager(parent, parent);
        }
    }

    /**
     * @brief start_transfer Start file transfer from remote source
     * @param path Path to the source file
     * @param size Size of the source file
     * @param transfer_type AFAIK. only valid value is "FROM_WEB"
     * @param to_print AFAIK No effect
     * @param start_cmd_id ID of the transfer command
     * @param callback_ok Javascript function(int transfer_id)
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void start_transfer(
        const QString & path,
        int size,
        const QString & transfer_type,
        bool to_print,
        int start_cmd_id,
        QJSValue callback_ok,
        QJSValue callback_fail
    );


    /**
     * @brief transfer_progress Set transfer progress
     * @param progress Progress value (from 0.0 to 1.0)
     * @param transferred Bytes transfered
     * @param callback_ok Javascript function(None)
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void transfer_progress(
        float progress,
        int transferred,
        QJSValue callback_ok,
        QJSValue callback_fail
    );

    /**
     * @brief stop_transfer Cleanup the current transfer(remove its record)
     * @param callback_ok Javascript function()
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void stop_transfer(QJSValue callback_ok, QJSValue callback_fail);

    /**
     * @brief download_from_url Download a file via HTTP? using requests.get
     * @param url Remote URL
     * @param storage Part of destination path( / + <storage> + <path> )
     * @param path Part of destination path( / + <storage> + <path> )
     * @param headers Header of the outgoing HTTP request
     * @param transfer_type "FROM_WEB"
     * @param start_cmd_id ID of the transfer this download is a part of
     * @param callback_ok Javascript function(int handle)
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void download_from_url(
        const QString & url,
        const QString & storage,
        const QString & path,
        NMStringMap headers,
        const QString & transfer_type,
        int start_cmd_id,
        QJSValue callback_ok,
        QJSValue callback_fail
    );

    /**
     * @brief convert_path  ???
     * @param target
     * @param path
     * @param callback_ok Javascript function(str resulting path)
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void convert_path(
        const QString & target,
        const QString & path,
        QJSValue callback_ok,
        QJSValue callback_fail
    );

    /**
     * @brief validate_project Check for common project errors
     * Errors are:
     *  - Invalid filename (FILE_NOT_FOUND)
     *  - Already exists(FILE_ALREADY_EXISTS)
     *  - Invalid extension(INVALID_EXTENSION)
     *  - Not enough space left on the device(NOT_ENOUGH_INTERNAL_SPACE)
     *
     * Projects are expected to be copied into the internal storage.
     *
     * @returns Filepath of the target file in the internal storage
     * @param filename
     * @param target "root" of the internal storage for projects
     * @param path
     * @param content_length size of the file in bytes
     * @param overwrite True/False
     * @param callback_ok Javascript function(string path)
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void validate_project(
        const QString & filename,
        const QString & target,
        const QString & path,
        int content_length,
        bool overwrite,
        QJSValue callback_ok,
        QJSValue callback_fail
    );

    Q_PROPERTY(QString usb_name READ usb_name /* WRITE setUsb_name */ NOTIFY usb_nameChanged FINAL)

    Q_PROPERTY(QString usb_mount_point READ usb_mount_point WRITE setUsb_mount_point NOTIFY usb_mount_pointChanged FINAL)

    /**
     * @brief transfer Return details of the current transfer
     *
       transfer = {
            "id": transfer_id,
            "path": path,
            "size": size,
            "type": transfer_type,
            "to_print": to_print,
            "start_cmd_id": start_cmd_id,
            "progress": 0,
            "time_remaining": 0,
            "transferred": 0,
            "start_time": time(),
        }
     */
    Q_PROPERTY(QVariantMap transfer READ transfer /* WRITE setTransfer */ NOTIFY transferChanged FINAL)

    Q_INVOKABLE void create_folder(const QString & path, QJSValue callback_ok, QJSValue callback_fail);

    Q_INVOKABLE QList<QString> sync_has_deprecated() const;

    Q_PROPERTY(int len_download_deprecated_files READ len_download_deprecated_files NOTIFY len_download_deprecated_filesChanged)
    int len_download_deprecated_files() const;

    Q_INVOKABLE int sync_len_download_deprecated_files() const;

    Q_INVOKABLE QStringList sync_extensions_deprecated() const;

    virtual bool set(const QString &name, const QVariant & value) override;

    Q_PROPERTY(QVariantMap last_update READ last_update NOTIFY last_updateChanged)
    QVariantMap last_update() const;

    Q_PROPERTY(QVariantMap last_exception READ last_exception NOTIFY last_exceptionChanged)
    QVariantMap last_exception() const;

    Q_INVOKABLE QVariantMap sync_last_exception() const;

    Q_PROPERTY(QVariantMap disk_usage READ disk_usage NOTIFY disk_usageChanged)

    Q_PROPERTY(QStringList extensions READ extensions /*WRITE setExtensions*/ NOTIFY extensionsChanged)
    Q_PROPERTY(QStringList extensionsDeprecated READ extensionsDeprecated /*WRITE setObsoleteExtensions*/ NOTIFY extensionsDeprecatedChanged)
    Q_PROPERTY(QVariantMap etag READ etag /*WRITE setEtag*/ NOTIFY etagChanged)
    Q_PROPERTY(QString current_media_path READ current_media_path /*WRITE setCurrent_media_path */ NOTIFY current_media_pathChanged)
    Q_PROPERTY(bool busy READ busy WRITE setBusy NOTIFY busyChanged FINAL)
    Q_PROPERTY(bool media_present READ media_present WRITE setMedia_present NOTIFY media_presentChanged FINAL)
    Q_PROPERTY(bool media_mounted READ media_mounted WRITE setMedia_mounted NOTIFY media_mountedChanged FINAL)

    /** List all files in the form of tree composed from variants */
    Q_INVOKABLE void get_all(const int maxdepth, QJSValue callback_ok, QJSValue callback_fail );

    /** List all files in the form of tree composed from variants - native callbacks */
    void get_all(const int maxdepth,  std::function<void(QVariantList)> callback_ok = [](QVariantList){}, std::function<void(QVariantList)> callback_fail = [](QVariantList){});

    Q_INVOKABLE void get_from_path(const QString &path, const int maxdepth, QJSValue callback_ok, QJSValue callback_fail);

    /** Remove file at path */
    Q_INVOKABLE void remove(QString path, QJSValue callback_ok, QJSValue callback_fail);

    /** Remove file at path - native callbacks*/
    void remove(QString path,  std::function<void(QVariantList)> callback_ok = [](QVariantList){}, std::function<void(QVariantList)> callback_fail = [](QVariantList){});

    /** Remove a directory  at path - native callbacks*/
    void remove_dir(QString path,  bool recursive = true,  std::function<void(QVariantList)> callback_ok = [](QVariantList){}, std::function<void(QVariantList)> callback_fail = [](QVariantList){});

    /** Move file from srcPath to dstPath */
    Q_INVOKABLE void move(QString srcPath, QString dstPath, QJSValue callback_ok, QJSValue callback_fail);

    Q_PROPERTY(QVariantMap has_deprecated READ has_deprecated NOTIFY has_deprecatedChanged)
    QVariantMap has_deprecated() const;

    /** Move file from srcPath to dstPath with native callbacks */
    void move(QString srcPath, QString dstPath,  std::function<void(QVariantList)> callback_ok = [](QVariantList){}, std::function<void(QVariantList)> callback_fail = [](QVariantList){});

    Q_INVOKABLE void current_project_set(QString path, QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void current_project_get(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void current_project_clean(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void remove_all_deprecated_files(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void move_deprecated_files(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void remove_downloaded_deprecated_files(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE const QString &current_media_path() const;

    /**
     * @brief media_umount
     * @param callback_ok Javascript function(bool success)
     * @param callback_fail
     */
    Q_INVOKABLE void media_umount(QJSValue callback_ok, QJSValue callback_fail);

    /**
     * @brief media_mount
     * @param callback_ok Javascript function(bool success)
     * @param callback_fail
     */
    Q_INVOKABLE void media_mount(QJSValue callback_ok, QJSValue callback_fail);


    /**
     * @brief average_extraction_time
     * @param path
     * @param callback_ok Javascript function(string_list)
     * @param callback_fail Javascript function(error_string)
     */
    Q_INVOKABLE void average_extraction_time(QString path, QJSValue callback_ok, QJSValue callback_fail);

    /**
     * @brief get_metadata Get metadata from a file
     * result:
    {
   "files":{
      "path":"/var/sl1fw/projects/pizerohingedcase_camera_gpio_v2.sl1",
      "origin":"local",
      "type":"file",
      "size":237423,
      "mtime":1612800996.9828205,
      "metadata":{
         "config":{
            "action":"print",
            "jobDir":"pizerohingedcase_camera_gpio_v2",
            "expTime":13,
            "expTimeFirst":25,
            "fileCreationTimestamp":"2020-10-14 at 19:34:27 UTC",
            "layerHeight":0.05,
            "materialName":"3DM-ABS @0.05",
            "numFade":10,
            "numFast":9,
            "numSlow":0,
            "printProfile":"0.05 Normal",
            "printTime":247.090909,
            "printerModel":"SL1",
            "printerProfile":"Original Prusa SL1 - Kopie",
            "printerVariant":"default",
            "prusaSlicerVersion":"PrusaSlicer-2.2.0+linux-x64-202003211856",
            "usedMaterial":0.104315
         },
         "thumbnail":{
            "files":[
               "/tmp/projects_metadata/local/tmpld73f4_k/thumbnail/thumbnail400x400.png",
               "/tmp/projects_metadata/local/tmpld73f4_k/thumbnail/thumbnail800x480.png"
            ],
            "dir":"/tmp/projects_metadata/local/tmpld73f4_k"
         }
      }
   },
   "last_update":{
      "local":1612803064.881588,
      "usb":1612785679.342449
   }
}
     */
    void get_metadata(const QString &path, const bool thumbnail, std::function<void(QVariantList)> callback_ok,  std::function<void(QVariantList)> callback_fail);

    enum EventNotify {
        MEDIUM_EJECTED = 1,
        MEDIUM_INSERTED = 2,
        FILE_CREATED = 3,
        FILE_CHANGED = 4,
        FILE_DELETED = 5,
    };
    Q_ENUM(EventNotify)

    QVariantMap disk_usage() const
    {
        return m_disk_usage;
    }

    QStringList extensions() const
    {
        return m_extensions;
    }

    QStringList extensionsDeprecated() const
    {
        return m_extensionsDeprecated;
    }

    QVariantMap etag() const
    {
        return m_etag;
    }

    QVariantMap transfer() const;
    void setTransfer(const QVariantMap &newTransfer);

    QString usb_name() const;
    void setUsb_name(const QString &newUsb_name);

    QString usb_mount_point() const;
    void setUsb_mount_point(const QString &newUsb_mount_point);

    bool busy() const;
    void setBusy(bool newBusy);

    bool media_present() const;
    void setMedia_present(bool newMedia_present);

    bool media_mounted() const;
    void setMedia_mounted(bool newMedia_mounted);

signals:


    void last_exceptionChanged(QVariantMap last_exception);
    void last_updateChanged(QVariantMap last_update);
    void disk_usageChanged(QVariantMap disk_usage);
    void has_deprecatedChanged(QVariantMap value);
    void len_download_deprecated_filesChanged(int value);

    void extensionsChanged(QStringList extensions);

    void extensionsDeprecatedChanged(QStringList extensionsDeprecated);

    void etagChanged(QVariantMap etag);

    void mediaInserted(QString path);
    void mediaEjected(QString rootPath);
    void oneClickPrintFile(QString path);


public slots:

//    void slotPropertiesChanged(const QString &interface, const QMap<QString, QVariant> &changed_properties, const QStringList &invalidates_properties);

    void setDisk_usage(QVariantMap disk_usage);
    void setExtensions(QStringList extensions);
    void setExtensionsDeprecated(QStringList obsoleteExtensions);
    void setEtag(QVariantMap etag);
signals:
    void current_media_pathChanged();
    void transferChanged();
    void usb_nameChanged();

    void usb_mount_pointChanged();

    void busyChanged();

    void media_presentChanged();

    void media_mountedChanged();

private:
    QVariantMap m_transfer;
    QString m_usb_name;
    QString m_usb_mount_point;
    bool m_busy;
    bool m_media_present;
    bool m_media_mounted;
};

#endif // FILEMANAGER_H
