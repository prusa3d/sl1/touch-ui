/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FMDATA_H
#define FMDATA_H

#include <QObject>
#include <QDebug>
#include <QQmlEngine>
#include <QDateTime>
#include "fmfile.h"

class FMFile;
class FMDirectory;


/** Stores the file tree structure, singleton.
 * Position of a file in the directory tree is determined solely by the filemanager,
 * path attribute of a FSFile does not specify the tree structure.
 *
 */
class FMData : public QObject
{
    Q_OBJECT
    explicit FMData(QQmlEngine *parent = nullptr);
    FMDirectory *root;
public:

    struct metadata_t {
      /*
       {
           "files":{
              "path":"/run/media/system/sda1/test_project_pi.sl1",
              "origin":"usb",
              "type":"file",
              "size":237423,
              "mtime":1602705188.0,
              "metadata":{
                 "config":{
                    "action":"print",
                    "jobDir":"pizerohingedcase_camera_gpio_v2",
                    "expTime":13,
                    "expTimeFirst":25,
                    "fileCreationTimestamp":"2020-10-14 at 19:34:27 UTC",
                    "layerHeight":0.05,
                    "materialName":"3DM-ABS @0.05",
                    "numFade":10,
                    "numFast":9,
                    "numSlow":0,
                    "printProfile":"0.05 Normal",
                    "printTime":247.090909,
                    "printerModel":"SL1",
                    "printerProfile":"Original Prusa SL1 - Kopie",
                    "printerVariant":"default",
                    "prusaSlicerVersion":"PrusaSlicer-2.2.0+linux-x64-202003211856",
                    "usedMaterial":0.104315
                 },
                 "thumbnail":{
                    "files":[
                       "/tmp/projects_metadata/usb/tmp9za9ykh3/thumbnail/thumbnail400x400.png",
                       "/tmp/projects_metadata/usb/tmp9za9ykh3/thumbnail/thumbnail800x480.png"
                    ],
                    "dir":"/tmp/projects_metadata/usb/tmp9za9ykh3"
                 }
              }
           },
           "last_update":{
              "local":1612446425.2272303,
              "usb":1612446425.2287843
           }
        }
        */
        // For internal purposes, signals that the data was parsed correctly
        bool valid;

        // Some of the standart file properties
        QString path;
        size_t size;
        QDateTime mtime;

        // Selected items from metadata
        qreal expTime; ///< Normal layer exposure time
        qreal expTimeFirst; ///< First layer exposure time
        QString fileCreationTimestamp; ///< i.e. '2020-10-14 at 19:34:27 UTC' (not standardized format)
        qreal layerHeight;
        QString materialName;
        qreal numFade;
        qreal numFast;
        qreal numSlow;
        QString printProfile;
        qreal printTime;
        QString printerModel;
        QString printerProfile;
        QString printerVariant;
        QString prusaSlicerVersion;
        qreal usedMaterial;

        // List of thumbnail files
        QVector<QString> thumbnails;

        metadata_t() : valid(false) {}

        /** Constructs the metadata_t object from QVariantMap given as a result of get_metadata() method */
        metadata_t(QVariantMap m);

        friend QDebug &operator <<(QDebug &d, const metadata_t &metadata);

    };

    static FMData * getInstance(QQmlEngine * parent = nullptr) {
        static FMData * instance = nullptr;
        if(instance) return instance;
        else {
            Q_ASSERT_X(parent, "FMData::getInstance", "No parent. Parent is expected to cleanup the instance, if you actually want to handle it yourself, remove this assert!");
            return instance = new FMData(parent);
        }
    }

    ~FMData();

    /** Search the tree and look for file(or directory) with "path" */
    Q_INVOKABLE FMFile * getFile(QString _path, FMFile::Origin _origin = FMFile::Origin::InvalidOrigin, FMFile *top = nullptr);

    /** Get the root directory */
    Q_INVOKABLE FMDirectory* getRoot() const {
        return root;
    }

    /**
     * @brief get_metadata Async get metadata(as a structure)
     * @param path Path to the file
     * @param get_thumbnails Extract thumbnails?
     * @param callback_ok
     * @param callback_fail
     */
    void get_metadata(QString path, bool get_thumbnails, std::function<void(metadata_t)> callback_ok, std::function<void(QVariantList)> callback_fail);

    /**
     * @brief refreshData Refresh the file data from filemanager
     * Will delete the old data and recreate it according to _data_ parameter.
     * @param data Raw data given by get_all(-1) method of the filemanager
     */
    void refreshData(QVariantList data);

    /** Register the required types with QQmlEngine */
    static void qmlRegister(QQmlEngine & engine);

signals:
    /** Notify all listeners that the data is about to change */
    void dataChangeBegin();

    /** Notify all listeners that the data has been changed/replaced and
     * it's safe to access it again. Old pointers are invalid.
    **/
    void dataChangeEnd();

public slots:
    /** Refresh the data after filemanager reports changes */
    void handleUpdate(QVariantMap last_update);

    /**
     * @brief handleFilemanagerDeath This should be called if the filemanager becomes unavailable
     */
    void handleFilemanagerDeath();
};


#endif // FMDATA_H
