/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import "."

/* ExpandingSection from top down */

Item {
    id: root
    property int maximumHeight: 420
    property int peekHeight: 80
    property alias backgroundColor: background.color
    readonly property real position: height / maximumHeight
    readonly property bool isClosed: height == 0
    readonly property bool isOpen: height == maximumHeight
    function open() {height = maximumHeight; peekResetTimer.stop() }
    function close() {height = 0; peekResetTimer.stop()}
    function peek() {if( isClosed ) {height = peekHeight; peekResetTimer.start()}}
    function toggle() {
        if(isClosed) open()
        else if(isOpen) close()
    }

    implicitWidth: 800

    visible: height > 0
    width: parent.width
    height: 0
    anchors {
        top: parent.bottom
    }
    clip: true

    Rectangle {
        id: background
        anchors.fill: parent
        color: Theme.color.background
        opacity: 0.6
    }

    Rectangle {
        width: parent.width
        height: 3
        anchors.bottom: parent.bottom
        color: Theme.color.separator
    }

    Behavior on height {
        PropertyAnimation {
            duration: 300
            easing.type:Easing.InOutCubic
        }
    }

    Timer {
        id: peekResetTimer
        interval: 1000
        onTriggered: () => height = 0
        repeat: false
    }

    SwipeArea {
        anchors.fill: parent
        onSwipeUp: () => root.close()
    }
}
