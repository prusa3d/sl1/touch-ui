/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Templates 2.12 as T
import "."

T.TextField {
    id: control
        implicitWidth: implicitBackgroundWidth + leftInset + rightInset
                       || Math.max(contentWidth, placeholder.implicitWidth) + leftPadding + rightPadding
        implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                                 contentHeight + topPadding + bottomPadding,
                                 placeholder.implicitHeight + topPadding + bottomPadding)

        padding: 6
        leftPadding: padding + 4
        rightPadding: /* pencil.implicitWidth*/  (enabled ? control.height : 0)+ padding
        color: Theme.color.text
        selectionColor: Theme.color.selectionBackground
        selectedTextColor: Theme.color.selection
        placeholderTextColor: Theme.color.placeholderText
        verticalAlignment: TextInput.AlignVCenter
        font {
            family: Theme.font.fontName
            pixelSize: Theme.font.normal
        }

        Text {
            id: placeholder
            x: control.leftPadding
            y: control.topPadding
            width: control.width - (control.leftPadding + control.rightPadding -  pencil.width )
            height: control.height - (control.topPadding + control.bottomPadding)

            text: control.placeholderText
            font: control.font
            color: control.placeholderTextColor
            verticalAlignment: control.verticalAlignment
            visible: !control.length && !control.preeditText && (!control.activeFocus || control.horizontalAlignment !== Qt.AlignHCenter)
            elide: Text.ElideRight
            renderType: control.renderType
        }

        Image {
                id: pencil
                x: control.width - pencil.width
                y: control.topPadding
                width: control.enabled ? height : 0
                height: control.height - (control.topPadding + control.bottomPadding)
                sourceSize.width: width
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                source: Theme.pictogram.path + "/" + Theme.pictogram.edit


                ColorOverlay {
                    visible: ! control.acceptableInput
                    anchors.fill: parent
                    source: parent
                    color: "red"  // make image like it lays under red glass
                }
            }

        background: Rectangle {
            implicitWidth: 200
            implicitHeight: 40
            border.width: 1
            color: Theme.color.background
            border.color: control.activeFocus ? Theme.color.selection : color
            radius: height / 4
        }
}
