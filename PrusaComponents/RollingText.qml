/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick.Controls 2.12
import QtQuick 2.12
import "."

Item {
    id: root
    property alias text: rollingText.text
    property alias color: rollingText.color
    property int overshoot: 30
    property int _difference: rollingText.width - root.width
    property alias font: rollingText.font

    property bool animationShouldRun: root._difference > 0 && root.visible

    implicitHeight: rollingText.height
    implicitWidth: 200

    clip: true

    Component.onCompleted:  () => { rollingText.x = 0; animator.running = animationShouldRun }
    onAnimationShouldRunChanged: () => { animator.running = animationShouldRun }
    onTextChanged: () => { animator.stop(); animator.running = animationShouldRun}

    Text {
        id: rollingText
        verticalAlignment: Text.AlignVCenter
    }

    SequentialAnimation {
        id: animator
        loops: Animation.Infinite

        PropertyAnimation {
            property: "x"
            target: rollingText
            from: root.overshoot
            to: - root._difference - overshoot
            duration: Math.max(0, 1000 + 50 * root._difference)
            easing.type: Easing.InOutCubic
        }

        PropertyAnimation {
            property: "x"
            target: rollingText
            from: - root._difference - overshoot
            to: root.overshoot
            duration: Math.max(0, 1000 + 50 * root._difference)
            easing.type: Easing.InOutCubic
        }
    }

    Rectangle {
        visible: _debug_rectangles
        anchors.fill: parent
        color: "transparent"
        border {
            width: 1
            color: Theme.color.debug
        }
    }


}

