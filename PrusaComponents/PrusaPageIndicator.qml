import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import "."

PageIndicator {
    id: indicator
    delegate: Item {
        height: 15
        width: 30
        Rectangle {
            width: 15
            height: 15
            color: index === currentIndex ? Theme.color.highlight : Theme.color.grey
            radius: 7
            anchors.margins: 10
        }
    }
    anchors.bottom: parent.bottom
    anchors.margins: 5
    anchors.horizontalCenter: parent.horizontalCenter
}
