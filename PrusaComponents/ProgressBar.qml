
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12

import "."

/*
    Simple progressbar indicator without any labels.
*/
Item {
    id: id_root
    property real value: 0.0
    property real maximum: 100
    property real sizeFraction: 1.0
    property int radius: 0
    property alias indicator: indicator
    property alias background: background
    signal clicked(var clicked_value)

    width: 600
    height: 40

    Rectangle {
        id: background
        anchors.fill: parent
        color: Theme.color.grey
        radius: id_root.radius
    }

    Rectangle {
        id: indicator
        height: background.height * sizeFraction
        width: background.width * sizeFraction * (value/maximum)
        color: Theme.color.highlight
        radius: background.radius
        anchors {
            verticalCenter: parent.verticalCenter
            left: background.left
            leftMargin: background.width * ( 1 - sizeFraction) / 2
        }
    }

    MouseArea {
        anchors.fill: parent
        onReleased:  (mouse) => {
            var pos = (mouse.x / width) * maximum
            if(pos > maximum) pos = 1.0
            id_root.clicked(pos)
        }
        onPositionChanged: (mouse) => {
            var pos = (mouse.x / width) * maximum
            if(pos > maximum) pos = 1.0
            id_root.clicked(pos)
        }
        onPressed:  (mouse) => {
            var pos = (mouse.x / width) * maximum
            if(pos > maximum) pos = 1.0
            id_root.clicked(pos)
        }
    }
}
