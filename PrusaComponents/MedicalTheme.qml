/*
    Copyright 2021, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import "."

PrusaTheme {
    Component.onCompleted: () => {
                               color.buttonHighlight = "#5EA796"
                               color.selection = "#5EA796"
                               color.highlight = "#5EA796"
                               pictogram.path = "/pictograms/m1"
                               console.log("MedicalTheme created.")
                           }
}
