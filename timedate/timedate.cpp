/*
    Copyright 2021-2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "timedate.h"
#include <QDir>
#include <QDirIterator>

void Timedate::_setNtp(bool ntp)
{
    if (m_ntp == ntp)
        return;

    m_ntp = ntp;
    emit ntpChanged(m_ntp);
}

void Timedate::_setRtcTime(QDateTime rtcTime)
{
    if (m_rtcTime == rtcTime)
        return;

    m_rtcTime = rtcTime;
    emit rtcTimeChanged(m_rtcTime);
}

void Timedate::_setRtcTime(qlonglong rtcTime, QTimeZone timezone)
{
    auto newTime = QDateTime::fromMSecsSinceEpoch(rtcTime/1000, timezone);
    _setRtcTime(newTime);
}

void Timedate::_setTime(QDateTime time)
{
    if (m_time == time)
        return;

    m_time = time;
    emit timeChanged(m_time);
}

void Timedate::_setTime(qlonglong time, QTimeZone timezone)
{
    auto newTime = QDateTime::fromMSecsSinceEpoch(time/1000, timezone);
    _setTime(newTime);
}

void Timedate::_setTimezone(QString timezone)
{
    if (m_timezone == timezone)
        return;

    m_timezone = timezone;
    m_time.setTimeZone(QTimeZone::systemTimeZone()); // Refresh
    emit timezoneChanged(m_timezone);
}

void Timedate::_setLocalRTC(bool localRTC)
{
    if (m_localRTC == localRTC)
        return;

    m_localRTC = localRTC;
    emit localRTCChanged(m_localRTC);
}

void Timedate::_setNtpSynchronized(bool ntpSynchronized)
{
    if (m_ntpSynchronized == ntpSynchronized)
        return;

    m_ntpSynchronized = ntpSynchronized;
    emit ntpSynchronizedChanged(m_ntpSynchronized);
}

void Timedate::_setCanNTP(bool canNTP)
{
    if(m_canNTP == canNTP) return;
    m_canNTP = canNTP;
    emit canNTPChanged(canNTP);
}

void Timedate::reloadTimezones(QString timezoneDirPath)
{
    QDir timezoneDir(timezoneDirPath);
    if(! timezoneDir.exists()) {
        return;
    }
    timezoneDir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
    timezoneDir.setSorting(QDir::Name);
    QFileInfoList regionDirList = timezoneDir.entryInfoList();
    foreach(auto regionFInfo, regionDirList) {
        Q_ASSERT(regionFInfo.isDir());
        QDir regionDir(regionFInfo.absoluteFilePath());
        auto cities = regionDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot, QDir::Name);
        foreach(auto city, cities) {
            Q_ASSERT(city.isFile());
            QString regionName = city.dir().dirName();
            QString cityName = city.fileName();
            // Append or create record
            if(m_timezoneMapping.contains(regionName)) m_timezoneMapping[regionName].append(cityName);
            else m_timezoneMapping.insert(regionName, {cityName});
        }
    }
}

Timedate::Timedate(QJSEngine &_engine, QObject *parent) :
    BaseDBusObject(_engine, service, interface, path, QDBusConnection::systemBus(), parent),
    m_timedateProxy(service, path, QDBusConnection::systemBus(), this)
{
    setServiceIsOnDemand(true);
    m_timedateProxy.setTimeout(8000);
    m_timeSyncTimer.setInterval(5*60*1000);
    m_secTickTimer.setInterval(1000);
    m_secTickTimer.start();
    m_timeSyncTimer.start();

    connect(&m_timeSyncTimer, &QTimer::timeout, this, [this](){
        this->reload_properties();
    });

    connect(&m_secTickTimer, &QTimer::timeout, this, [this](){
        m_time =  m_time.addSecs(1);
        emit timeChanged(m_time);
    });

    reloadTimezones(TIMEZONE_DIR);
    Timedate::reload_properties();
}

void Timedate::setLocalRTC(bool localRTC, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_timedateProxy.SetLocalRTC(localRTC, false, false);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
    QTimer::singleShot(500, this, [this](){this->reload_properties();});
}

void Timedate::setNtp(bool ntp, QJSValue callback_ok, QJSValue callback_fail) {
    auto pending = m_timedateProxy.SetNTP(ntp, false);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
    QTimer::singleShot(1500, this, [this](){this->reload_properties();});
}

void Timedate::setTime(QDateTime time, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_timedateProxy.SetTime(time.toMSecsSinceEpoch() * 1000, false, false);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
    QTimer::singleShot(500, this, [this](){this->reload_properties();});
}

void Timedate::setTimezone(QString timezone, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_timedateProxy.SetTimezone(timezone, false);
    DBusUtils::handle_callbacks(m_engine, pending, callback_ok, callback_fail);
    QTimer::singleShot(500, this, [this](){this->reload_properties();});
}

bool Timedate::ntp() const
{
    return m_ntp;
}

bool Timedate::ntpSynchronized() const
{
    return m_ntpSynchronized;
}

QDateTime Timedate::rtcTime() const
{
    return m_rtcTime;
}

QDateTime Timedate::time() const
{
    return m_time;
}

QString Timedate::timezone() const
{
    return m_timezone;
}


bool Timedate::canNTP() const
{
    return m_canNTP;
}


/** To be called when value changes in remove object */
bool Timedate::set(const QString & name, const QVariant & value)
{
    if(name == QStringLiteral("CanNTP")) {
       _setCanNTP(value.toBool());
    }
    else if(name == QStringLiteral("LocalRTC")) {
        _setLocalRTC(value.toBool());
    }
    else if(name == QStringLiteral("NTP")) {
        _setNtp(value.toBool());
    }
    else if(name == QStringLiteral("NTPSynchronized")) {
        _setNtpSynchronized(value.toBool());
    }
    else if(name == QStringLiteral("RTCTimeUSec")) {
        _setRtcTime(value.value<qlonglong>(), localRTC() ? QTimeZone::systemTimeZone() : QTimeZone::utc());
    }
    else if( name == QStringLiteral("TimeUSec")) {
        _setTime(value.value<qlonglong>(), QTimeZone::systemTimeZone());
    }
    else if(name == QStringLiteral("Timezone")) {
        _setTimezone(value.toString());
    }
    else {
        return BaseDBusObject::set(name, value);
    }
    return true;
}

