/*
    Copyright 2021-2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TIMEDATE_H
#define TIMEDATE_H

#include <QObject>
#include <QString>
#include <QTimer>
#include <QDateTime>
#include <QJSEngine>
#include <QJSValue>
#include <timedate_proxy.h>
#include <properties_proxy.h>
#include <dbusutils.h>
#include <basedbusobject.h>

/**
 * @brief Class handling communication with org.freedesktop.timedate1 service.
 *
 * timedated does not handle concurrent calls, does not send
 * changed signals and also is not aware of the timezones, since
 * it's only aware of those in the file /usr/share/zoneinfo/zone.tab(missing).
 * Thats why properties are read-only and writes are faciliated via methods - can fail.
 *
 * Time in property 'QDateTime time' is counted by timer, but is periodically synced with system time.
 */
class Timedate : public BaseDBusObject
{
    Q_OBJECT

    inline static const QString service{"org.freedesktop.timedate1"};
    inline static const QString interface{"org.freedesktop.timedate1"};
    inline static const QString path{"/org/freedesktop/timedate1"};

    TimedateProxy m_timedateProxy;

    /** Time, and other properties will be synchronized periodically with the system time */
    QTimer m_timeSyncTimer;

    /** Timer to increment time every second */
    QTimer m_secTickTimer;

    bool m_localRTC{false};

    bool m_ntp{false};

    bool m_ntpSynchronized{false};

    QDateTime m_rtcTime;

    QDateTime m_time;

    QString m_timezone;

    bool m_canNTP{false};

    /** Timezones in format: region -> { City, .. } */
    QMap<QString, QStringList> m_timezoneMapping;

    void _setNtp(bool ntp);
    void _setRtcTime(QDateTime rtcTime); // UTC
    void _setRtcTime(qlonglong rtcTime,  QTimeZone timezone = QTimeZone::utc()); // UTC, millis since epoch
    void _setTime(QDateTime time); // local timezone
    void _setTime(qlonglong time, QTimeZone timezone = QTimeZone::utc()); // local timezone, millis since epoch
    void _setTimezone(QString timezone);

    void _setLocalRTC(bool localRTC);
    void _setNtpSynchronized(bool ntpSynchronized);
    void _setCanNTP(bool canNTP);

    void reloadTimezones(QString timezoneDirPath);

private:
    const QString TIMEZONE_DIR = QStringLiteral("/usr/share/zoneinfo");
public:
    explicit Timedate(QJSEngine & _engine, QObject *parent = nullptr);

    // Properties are read-only, setters are used internally
    Q_PROPERTY(bool canNTP READ canNTP NOTIFY canNTPChanged)
    Q_PROPERTY(bool localRTC READ localRTC  NOTIFY localRTCChanged)
    Q_PROPERTY(bool ntp READ ntp  NOTIFY ntpChanged)
    Q_PROPERTY(bool ntpSynchronized READ ntpSynchronized NOTIFY ntpSynchronizedChanged)
    Q_PROPERTY(QDateTime rtcTime READ rtcTime NOTIFY rtcTimeChanged)
    Q_PROPERTY(QDateTime time READ time NOTIFY timeChanged)
    Q_PROPERTY(QString timezone READ timezone  NOTIFY timezoneChanged)

    /** Control whether the RTC is in local time or UTC.
     *  It is strongly recommended to maintain the RTC in UTC.
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param localRTC true=RTC in local timezone, false=UTC
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setLocalRTC(bool localRTC, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** Whether the system clock is synchronized with
     *  the network using systemd-timesyncd. This will enable and start or
     *  disable and stop the chosen time synchronization service.
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param ntp true=use NTP, false=use local time
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setNtp(bool ntp, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** @brief Change the system clock.
     *  Pass a value of microseconds since the UNIX epoch (1 Jan 1970 UTC).
     *  NOTE: QDateTime in the local timezone is used instead, QML JS does not handle 64-bit numbers anyway.
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param time Time with the timezone set.
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setTime(QDateTime time, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    /** Set the system timezone. Pass a value like
     * "Europe/Berlin" to set the timezone. Valid timezones are listed in
     * /usr/share/zoneinfo/zone.tab. If the RTC is configured to be
     * maintained in local time, it will be updated accordingly.
     *
     * Call over DBus will run asynchronously and call the appropriate callback when done.
     * @param timezone Timezone string
     * @param callback_ok JS function()
     * @param callback_fail JS function(string name_of_error)
    **/
    Q_INVOKABLE void setTimezone(QString timezone, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());


    /** Get the list if available timezone regions */
    Q_INVOKABLE QStringList timezoneRegions() const { return m_timezoneMapping.keys(); }

    /** Get the list of cities available for a given region */
    Q_INVOKABLE QStringList timezoneCities(const QString & region) {
        if(m_timezoneMapping.contains(region)) {
            return m_timezoneMapping[region];
        }
        else {
            return QStringList();
        }
    }

    /** Get the current region */
    Q_INVOKABLE QString timezoneRegion() {
        QStringList sp = timezone().split("/");
        if(sp.length() == 2) return sp[0];
        else return timezone();
    }

    /** Get the current city */
    Q_INVOKABLE QString timezoneCity() {
        QStringList sp = timezone().split("/");
        if(sp.length() == 2) return sp[1];
        else return timezone();
    }

    /** Control whether the RTC is in local time or UTC. It is strongly recommended to maintain the RTC in UTC. */
    bool localRTC() const
    {
        return m_localRTC;
    }

    /** Control whether the system clock is synchronized with the network using systemd-timesyncd.
     * This will enable/start resp. disable/stop the systemd-timesyncd service.
    **/
    bool ntp() const;

    /** shows whether the kernel reports the time as
     *  synchronized (c.f.  adjtimex(3))
    **/
    bool ntpSynchronized() const;

    /** Time on the RTC clock */
    QDateTime rtcTime() const;

    /** System time */
    QDateTime time() const;

    /** TimeZone of system time */
    QString timezone() const;

    /** shows whether a service to perform time
     *  synchronization over the network is available
    **/
    bool canNTP() const;


    /** @brief Universal setter with value preprocessing
     *  Some properties do not have declared setter(read-only), but it's convenient to use it internally.
     *  Some properties do not map to the ones in the proxy object 1:1 and need to be preprocessed
     */
    bool set(const QString & name, const QVariant & value) override;

signals:

    void localRTCChanged(bool localRTC);

    void ntpChanged(bool ntp);

    void ntpSynchronizedChanged(bool ntpSynchronized);

    void rtcTimeChanged(QDateTime rtcTime);

    void timeChanged(QDateTime time);

    void timezoneChanged(QString timezone);

    void canNTPChanged(bool canNTP);
};

#endif // TIMEDATE_H
