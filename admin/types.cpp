/*
	Copyright 2020, Prusa Development a.s.

	This file is part of SLAGUI

	SLAGUI is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "types.h"

QDBusArgument &operator<<(QDBusArgument &argument, const AdminItemPointer &adminItemPointer) {
	argument.beginStructure();
	argument << adminItemPointer.interface;
	argument << adminItemPointer.path;
	argument.endStructure();
	return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, AdminItemPointer &adminItemPointer) {
	argument.beginStructure();
	argument >> adminItemPointer.interface;
	argument >> adminItemPointer.path;
	argument.endStructure();
	return argument;
}
