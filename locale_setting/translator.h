/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QObject>
#include <QHash>
#include <QTranslator>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>

/** @brief Handles reloading of language files according to the set language */
class Translator : public QObject
{
    Q_OBJECT

    QVariantList * m_languageKeys;

public:
    enum Language {
        Default = 0,
        Czech,
        German,
        English,
        Spanish,
        French,
        Italian,
        Polish,
        LanguageCount
    };
    Q_ENUM(Language)


    Q_PROPERTY(Language defaultLanguage READ defaultLanguage CONSTANT)

    /** Current language */
    Q_PROPERTY(Language language READ language WRITE setLanguage NOTIFY languageChanged)

    /** Possible languages (list) */
    Q_PROPERTY(QStringList languages READ languages CONSTANT)

    /** Possible language keys (list of ints)*/
    Q_PROPERTY(QVariantList languageKeys READ languageKeys CONSTANT)

    /** Use locale in the form "cs_CZ" to set a language appropriate for it */
    Q_INVOKABLE Translator::Language localeToLanguage(QString loc);
    Q_INVOKABLE QString languageToLocale(Translator::Language lang) { return languageLocales()[lang];}


protected:
    QTranslator * m_translator;

    QGuiApplication & m_Application;
    QQmlApplicationEngine & m_Engine;
    Language m_language;

    /** Names to be presented to the user */
    const QHash<int, QString> languageNames() const {
        static const QHash<int, QString> names {
            {Default, "Default"},
            {Czech, "Česky"},
            {German, "Deutsch"},
            {English, "English"},
            {Spanish, "Español"},
            {French, "Français"},
            {Italian, "Italiano"},
            {Polish, "Polski"}
        };
        return names;
    };

    /** Language files processed by lrelease */
    const QHash<int, QString> languageFiles() const {
        static const QHash<int, QString> files{
            {Default,  ":/translations/en_US.qm"},
            {Czech, ":/translations/cs_CZ.qm"},
            {German, ":/translations/de_DE.qm"},
            {English, ":/translations/en_US.qm"},
            {Spanish, ":/translations/es_ES.qm"},
            {French, ":/translations/fr_FR.qm"},
            {Italian, ":/translations/it_IT.qm"},
            {Polish, ":/translations/pl_PL.qm"}
        };
        return files;
    };


        /** Locale to language */
        const QHash<int, QString> languageLocales() const {
            static const QHash<int, QString> locale{
                {Default,  "C"},
                {Czech, "cs_CZ"},
                {German, "de_DE"},
                {English, "en_US"},
                {Spanish, "es_ES"},
                {French, "fr_FR"},
                {Italian, "it_IT"},
                {Polish, "pl_PL"}
            };
            return locale;
        }

        /** Create a list of available languages(in correct order)*/
        QStringList  languages() const
        {
            static QStringList _languages;
            if(_languages.empty())  {
                for(const auto &key : languageKeys()) {
                    bool ok;
                    int i = key.toInt(&ok);
                    Q_ASSERT_X(ok, __PRETTY_FUNCTION__, "Failed to process key");
                    _languages.append(languageNames()[i]);
                }
            }
            return _languages;
        }

        /** Create a list of keys for available languages(in correct order) */
        QVariantList  languageKeys() const
        {
            static QVariantList _languageKeys;
            if(_languageKeys.empty()) {
                Q_ASSERT_X(languageNames().count() >= 2, "Translator::Translator", "languageKeys must contain at least \"Default\" and \"LanguageCount\"");
                for(int i = 1; i < LanguageCount; ++i) {
                    _languageKeys.append(i);
                }
            }
            return _languageKeys;
        }

        Language language() const
        {
            return m_language;
        }

        Language defaultLanguage() const {
            return English;
        }

public:
    explicit Translator(QGuiApplication & app, QQmlApplicationEngine & engine, QObject *parent = nullptr);

public slots:
    void setLanguage(Translator::Language language);

    /** For convenience, can be connected to localeChanged of LocaleSetting */
    void setLanguageByLocale(QString loc) {
        qDebug() << "Trying to set language by locale: " << loc;
        setLanguage(localeToLanguage(loc));
    }

signals:
    void languageChanged(Translator::Language language);
};

#endif // TRANSLATOR_H
