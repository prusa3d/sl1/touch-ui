/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXAMPLES_H
#define EXAMPLES_H

#include <QObject>
#include <QJSEngine>
#include "examples_proxy.h"
#include "dbusutils.h"
#include "basedbusobject.h"

class Examples : public BaseDBusObject
{
    Q_OBJECT

public:
    enum State {
        INITIALIZING = 0,
        DOWNLOADING = 1,
        UNPACKING = 2,
        COPYING = 3,
        CLEANUP = 4,
        COMPLETED = 5,
        FAILURE = 6
    };
    Q_ENUM(State);
protected:
    ExamplesProxy m_proxy;

    virtual bool set(const QString & name, const QVariant & value) override;
    State m_state;

    double m_download_progress;

    double m_unpack_progress;

    double m_copy_progress;

public:

    inline static const QString service{"cz.prusa3d.sl1.examples0"};
    inline static const QString interface{"cz.prusa3d.sl1.examples0"};
    inline static const QString path{"/cz/prusa3d/sl1/examples0"};

    Q_PROPERTY(State state READ state NOTIFY stateChanged)
    Q_PROPERTY(double download_progress READ download_progress NOTIFY download_progressChanged)
    Q_PROPERTY(double unpack_progress READ unpack_progress NOTIFY unpack_progressChanged)
    Q_PROPERTY(double copy_progress READ copy_progress NOTIFY copy_progressChanged)

    explicit Examples(QJSEngine &_engine, QObject *parent = nullptr);


    State state() const
    {
        return m_state;
    }

    double download_progress() const
    {
        return m_download_progress;
    }

    double unpack_progress() const
    {
        return m_unpack_progress;
    }

    double copy_progress() const
    {
        return m_copy_progress;
    }

signals:
    void stateChanged(Examples::State state);

    void download_progressChanged(double download_progress);

    void unpack_progressChanged(double unpack_progress);

    void copy_progressChanged(double copy_progress);

public slots:

    void setState(Examples::State state)
    {
        if (m_state == state)
            return;

        m_state = state;
        emit stateChanged(m_state);
    }
    void setDownload_progress(double download_progress)
    {
        if (qFuzzyCompare(m_download_progress, download_progress))
            return;

        m_download_progress = download_progress;
        emit download_progressChanged(m_download_progress);
    }
    void setUnpack_progress(double unpack_progress)
    {
        if (qFuzzyCompare(m_unpack_progress, unpack_progress))
            return;

        m_unpack_progress = unpack_progress;
        emit unpack_progressChanged(m_unpack_progress);
    }
    void setCopy_progress(double copy_progress)
    {
        if (qFuzzyCompare(m_copy_progress, copy_progress))
            return;

        m_copy_progress = copy_progress;
        emit copy_progressChanged(m_copy_progress);
    }
};

#endif // EXAMPLES_H
