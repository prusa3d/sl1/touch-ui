/*
    Copyright 2021-2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONFIG_H
#define CONFIG_H
#include <QTimer>
#include <QObject>
#include <dbusutils.h>
#include <basedbusobject.h>
#include "properties_proxy.h"
#include "config_proxy.h"

class Config : public BaseDBusObject
{
    Q_OBJECT

    inline static const QString service{"cz.prusa3d.sl1.config0"};
    inline static const QString interface{"cz.prusa3d.sl1.config0"};
    inline static const QString path{"/cz/prusa3d/sl1/config0"};

    ConfigProxy m_configProxy;

    int m_MCBoardVersion{0};
    bool m_mCversionCheck{false};
    bool m_autoOff{false};
    int m_calibTowerOffset{0};
    bool m_calibrated{false};
    bool m_coverCheck{false};
    int m_delayAfterExposure{0};
    int m_delayBeforeExposure{0};
    bool m_fanUvLedEnabled{false};
    int m_fanUvLedRpm{0};
    bool m_fan2Enabled{false};
    int m_fan2Rpm{0};
    bool m_fan3Enabled{false};
    int m_fan3Rpm{0};
    bool m_fanCheck{false};
    int m_layerTowerHop{0};
    int m_limit4fast{0};
    int m_measuringMoves{0};
    double m_microStepsMM{0.0};
    bool m_mute{false};
    bool m_perPartes{false};
    int m_pwrLedPwm{0};
    bool m_resinSensor{false};
    int m_screwMm{0};
    bool m_showI18nSelect{false};
    bool m_showUnboxing{false};
    bool m_showWizard{false};
    bool m_tilt{true};
    double m_tiltFastTime{0.0};
    int m_tiltHeight{0};
    int m_tiltSensitivity{0};
    double m_tiltSlowTime{0.0};
    double m_tiltHighViscosityTime{0.0};
    int m_towerHeight{0};
    int m_towerSensitivity{0};
    int m_tower_microstep_size_nm{0};
    int m_trigger{0};

    Int2D m_tuneTilt;
    int m_uvCalibIntensity{0};
    int m_uvCalibMinIntEdge{0};
    double m_uvCurrent{0.0};
    int m_uvPwm{0};
    int m_uvWarmUpTime{0};
    int m_forceSlowTiltHeight{0};

    int m_uvCalibBoostTolerance{0};
    int m_rpmControlUvFanMinRpm{0};
    int m_vatRevision{0};
    bool m_rpmControlOverride{false};
    int m_uvPwmPrint{0};
    int m_rpmControlUvFanMaxRpm{0};
    int m_uvPwmTune{0};
    int m_tiltMin{0};
    int m_tiltMax{0};
    QString m_currentProfilesSet;

    bool m_lockProfiles{false};
    int m_rpmControlUvLedMaxTemp{0};
    int m_rpmControlUvLedMinTemp{0};
    int m_max_tower_height_mm{0};
    int m_tankCleaningExposureTime{0};

    int m_calib_tower_offset_nm{0};
    QVariantMap m_constraints;
    int m_layer_tower_hop_nm{0};
    int m_tower_height_nm{0};
    int m_up_and_down_z_offset_nm{0};
    int m_tankCleaningMinDistance_nm{0};
    int m_tankCleaningGentlyUpProfile{0};

    int m_tankCleaningAdaptorHeight_nm;

public:
    explicit Config(QJSEngine &engine, QObject *parent = nullptr);
    void debugPrint() const;

    Q_PROPERTY ( int mCBoardVersion READ mCBoardVersion WRITE setMCBoardVersion NOTIFY mCBoardVersionChanged )
    Q_PROPERTY ( bool mCversionCheck READ mCversionCheck WRITE setMCversionCheck NOTIFY mCversionCheckChanged )
    Q_PROPERTY ( bool autoOff READ autoOff WRITE setAutoOff NOTIFY autoOffChanged )
    Q_PROPERTY ( int calibTowerOffset READ calibTowerOffset WRITE setCalibTowerOffset NOTIFY calibTowerOffsetChanged )
    Q_PROPERTY ( bool calibrated READ calibrated WRITE setCalibrated NOTIFY calibratedChanged )
    Q_PROPERTY ( bool coverCheck READ coverCheck WRITE setCoverCheck NOTIFY coverCheckChanged )
    Q_PROPERTY ( int delayAfterExposure READ delayAfterExposure WRITE setDelayAfterExposure NOTIFY delayAfterExposureChanged )
    Q_PROPERTY ( int delayBeforeExposure READ delayBeforeExposure WRITE setDelayBeforeExposure NOTIFY delayBeforeExposureChanged )
    Q_PROPERTY ( bool fanUvLedEnabled READ fanUvLedEnabled WRITE setFanUvLedEnabled NOTIFY fanUvLedEnabledChanged )
    Q_PROPERTY ( int fanUvLedRpm READ fanUvLedRpm WRITE setFanUvLedRpm NOTIFY fanUvLedRpmChanged )
    Q_PROPERTY ( bool fanBlowerEnabled READ fanBlowerEnabled WRITE setFanBlowerEnabled NOTIFY fanBlowerEnabledChanged )
    Q_PROPERTY ( int fanBlowerRpm READ fanBlowerRpm WRITE setFanBlowerRpm NOTIFY fanBlowerRpmChanged )
    Q_PROPERTY ( bool fanRearEnabled READ fanRearEnabled WRITE setFanRearEnabled NOTIFY fanRearEnabledChanged )
    Q_PROPERTY ( int fanRearRpm READ fanRearRpm WRITE setFanRearRpm NOTIFY fanRearRpmChanged )
    Q_PROPERTY ( bool fanCheck READ fanCheck WRITE setFanCheck NOTIFY fanCheckChanged )
    Q_PROPERTY ( int layerTowerHop READ layerTowerHop WRITE setLayerTowerHop NOTIFY layerTowerHopChanged )
    Q_PROPERTY ( int limit4fast READ limit4fast WRITE setLimit4fast NOTIFY limit4fastChanged )
    Q_PROPERTY ( int measuringMoves READ measuringMoves WRITE setMeasuringMoves NOTIFY measuringMovesChanged )
    Q_PROPERTY ( double microStepsMM READ microStepsMM WRITE setMicroStepsMM NOTIFY microStepsMMChanged )
    Q_PROPERTY ( bool mute READ mute WRITE setMute NOTIFY muteChanged )
    Q_PROPERTY ( bool perPartes READ perPartes WRITE setPerPartes NOTIFY perPartesChanged )
    Q_PROPERTY ( int pwrLedPwm READ pwrLedPwm WRITE setPwrLedPwm NOTIFY pwrLedPwmChanged )
    Q_PROPERTY ( bool resinSensor READ resinSensor WRITE setResinSensor NOTIFY resinSensorChanged )
    Q_PROPERTY ( int screwMm READ screwMm WRITE setScrewMm NOTIFY screwMmChanged )
    Q_PROPERTY ( bool showI18nSelect READ showI18nSelect WRITE setShowI18nSelect NOTIFY showI18nSelectChanged )
    Q_PROPERTY ( bool showUnboxing READ showUnboxing WRITE setShowUnboxing NOTIFY showUnboxingChanged )
    Q_PROPERTY ( bool showWizard READ showWizard WRITE setShowWizard NOTIFY showWizardChanged )
    Q_PROPERTY ( bool tilt READ tilt WRITE setTilt NOTIFY tiltChanged )
    Q_PROPERTY ( int tiltHeight READ tiltHeight WRITE setTiltHeight NOTIFY tiltHeightChanged )
    Q_PROPERTY ( int tiltSensitivity READ tiltSensitivity WRITE setTiltSensitivity NOTIFY tiltSensitivityChanged )
    Q_PROPERTY ( double tiltFastTime READ tiltFastTime WRITE setTiltFastTime NOTIFY tiltFastTimeChanged )
    Q_PROPERTY ( double tiltSlowTime READ tiltSlowTime WRITE setTiltSlowTime NOTIFY tiltSlowTimeChanged )
    Q_PROPERTY ( double tiltHighViscosityTime READ tiltHighViscosityTime WRITE setTiltHighViscosityTime NOTIFY tiltHighViscosityTimeChanged)
    Q_PROPERTY ( int towerHeight READ towerHeight WRITE setTowerHeight NOTIFY towerHeightChanged )
    Q_PROPERTY ( int towerSensitivity READ towerSensitivity WRITE setTowerSensitivity NOTIFY towerSensitivityChanged )
    Q_PROPERTY ( int tower_microstep_size_nm READ tower_microstep_size_nm WRITE setTower_microstep_size_nm NOTIFY tower_microstep_size_nmChanged )
    Q_PROPERTY ( int trigger READ trigger WRITE setTrigger NOTIFY triggerChanged )
    Q_PROPERTY ( Int2D tuneTilt READ tuneTilt WRITE setTuneTilt NOTIFY tuneTiltChanged )
    Q_PROPERTY ( int uvCalibIntensity READ uvCalibIntensity WRITE setUvCalibIntensity NOTIFY uvCalibIntensityChanged )
    Q_PROPERTY ( int uvCalibMinIntEdge READ uvCalibMinIntEdge WRITE setUvCalibMinIntEdge NOTIFY uvCalibMinIntEdgeChanged )
    Q_PROPERTY ( double uvCurrent READ uvCurrent WRITE setUvCurrent NOTIFY uvCurrentChanged )
    Q_PROPERTY ( int uvPwm READ uvPwm WRITE setUvPwm NOTIFY uvPwmChanged )
    Q_PROPERTY ( int uvWarmUpTime READ uvWarmUpTime WRITE setUvWarmUpTime NOTIFY uvWarmUpTimeChanged )
    Q_PROPERTY ( int max_tower_height_mm READ max_tower_height_mm WRITE setMax_tower_height_mm NOTIFY max_tower_height_mmChanged)
    Q_PROPERTY(int calib_tower_offset_nm READ calib_tower_offset_nm WRITE setCalib_tower_offset_nm NOTIFY calib_tower_offset_nmChanged)
    Q_PROPERTY(QVariantMap constraints READ constraints NOTIFY constraintsChanged)
    Q_PROPERTY(int layer_tower_hop_nm READ layer_tower_hop_nm WRITE setLayer_tower_hop_nm NOTIFY layer_tower_hop_nmChanged)
    Q_PROPERTY(int tower_height_nm READ tower_height_nm WRITE setTower_height_nm NOTIFY tower_height_nmChanged)
    Q_PROPERTY(int up_and_down_z_offset_nm READ up_and_down_z_offset_nm WRITE setUp_and_down_z_offset_nm NOTIFY up_and_down_z_offset_nmChanged)


    Q_PROPERTY(int forceSlowTiltHeight READ forceSlowTiltHeight WRITE setForceSlowTiltHeight NOTIFY forceSlowTiltHeightChanged)
    Q_PROPERTY(int uvCalibBoostTolerance READ uvCalibBoostTolerance WRITE setUvCalibBoostTolerance NOTIFY uvCalibBoostToleranceChanged)
    Q_PROPERTY(int rpmControlUvFanMinRpm READ rpmControlUvFanMinRpm WRITE setRpmControlUvFanMinRpm NOTIFY rpmControlUvFanMinRpmChanged)
    Q_PROPERTY(int vatRevision READ vatRevision WRITE setVatRevision NOTIFY vatRevisionChanged)
    Q_PROPERTY(bool rpmControlOverride READ rpmControlOverride WRITE setRpmControlOverride NOTIFY rpmControlOverrideChanged)
    Q_PROPERTY(int tankCleaningExposureTime READ tankCleaningExposureTime WRITE setTankCleaningExposureTime NOTIFY tankCleaningExposureTimeChanged)
    Q_PROPERTY(int tankCleaningMinDistance_nm READ tankCleaningMinDistance_nm WRITE setTankCleaningMinDistance_nm NOTIFY tankCleaningMinDistance_nmChanged)
    Q_PROPERTY(int tankCleaningGentlyUpProfile READ tankCleaningGentlyUpProfile WRITE setTankCleaningGentlyUpProfile NOTIFY tankCleaningGentlyUpProfileChanged)
    Q_PROPERTY(int uvPwmPrint READ uvPwmPrint WRITE setUvPwmPrint NOTIFY uvPwmPrintChanged)
    Q_PROPERTY(int rpmControlUvFanMaxRpm READ rpmControlUvFanMaxRpm WRITE setRpmControlUvFanMaxRpm NOTIFY rpmControlUvFanMaxRpmChanged)
    Q_PROPERTY(int uvPwmTune READ uvPwmTune WRITE setUvPwmTune NOTIFY uvPwmTuneChanged)
    Q_PROPERTY(int tiltMin READ tiltMin WRITE setTiltMin NOTIFY tiltMinChanged)
    Q_PROPERTY(int tiltMax READ tiltMax WRITE setTiltMax NOTIFY tiltMaxChanged)
    Q_PROPERTY(QString currentProfilesSet READ currentProfilesSet WRITE setCurrentProfilesSet NOTIFY currentProfilesSetChanged)
    Q_PROPERTY(bool lockProfiles READ lockProfiles WRITE setLockProfiles NOTIFY lockProfilesChanged)
    Q_PROPERTY(int rpmControlUvLedMaxTemp READ rpmControlUvLedMaxTemp WRITE setRpmControlUvLedMaxTemp NOTIFY rpmControlUvLedMaxTempChanged)
    Q_PROPERTY(int rpmControlUvLedMinTemp READ rpmControlUvLedMinTemp WRITE setRpmControlUvLedMinTemp NOTIFY rpmControlUvLedMinTempChanged)
    Q_PROPERTY(int tankCleaningAdaptorHeight_nm READ tankCleaningAdaptorHeight_nm WRITE setTankCleaningAdaptorHeight_nm NOTIFY tankCleaningAdaptorHeight_nmChanged)

    Q_PROPERTY(int default_tower_height_mm READ default_tower_height_mm WRITE setDefault_tower_height_mm NOTIFY default_tower_height_mmChanged FINAL)
    Q_PROPERTY(bool delayedEnd READ delayedEnd WRITE setDelayedEnd NOTIFY delayedEndChanged FINAL)
    Q_PROPERTY(bool rpmControlUvEnabled READ rpmControlUvEnabled WRITE setRpmControlUvEnabled NOTIFY rpmControlUvEnabledChanged FINAL)
    Q_PROPERTY(int stirring_delay_ms READ stirring_delay_ms WRITE setStirring_delay_ms NOTIFY stirring_delay_msChanged FINAL)
    Q_PROPERTY(int stirring_moves READ stirring_moves WRITE setStirring_moves NOTIFY stirring_movesChanged FINAL)
    Q_PROPERTY(int tower_calib_position_nm READ tower_calib_position_nm WRITE setTower_calib_position_nm NOTIFY tower_calib_position_nmChanged FINAL)

    Q_PROPERTY(int tower_resin_measure_end_nm READ tower_resin_measure_end_nm WRITE setTower_resin_measure_end_nm NOTIFY tower_resin_measure_end_nmChanged FINAL)

    Q_PROPERTY(int tower_resin_measure_start_nm READ tower_resin_measure_start_nm WRITE setTower_resin_measure_start_nm NOTIFY tower_resin_measure_start_nmChanged FINAL)
    Q_PROPERTY(int up_and_down_every_layer READ up_and_down_every_layer WRITE setUp_and_down_every_layer NOTIFY up_and_down_every_layerChanged FINAL)
    Q_PROPERTY(int up_and_down_expo_comp_ms READ up_and_down_expo_comp_ms WRITE setUp_and_down_expo_comp_ms NOTIFY up_and_down_expo_comp_msChanged FINAL)
    Q_PROPERTY(bool up_and_down_uv_on READ up_and_down_uv_on WRITE setUp_and_down_uv_on NOTIFY up_and_down_uv_onChanged FINAL)
    Q_PROPERTY(int up_and_down_wait READ up_and_down_wait WRITE setUp_and_down_wait NOTIFY up_and_down_waitChanged FINAL)

    /// Save the current configuration, also clears the saved snapshot of the configuration
    Q_INVOKABLE void save();
    Q_INVOKABLE void update_motor_sensitivity();

    int tankCleaningAdaptorHeight_nm() const;
    void setTankCleaningAdaptorHeight_nm(int newTankCleaningAdaptorHeight_nm);

    int default_tower_height_mm() const;
    void setDefault_tower_height_mm(int newDefault_tower_height_mm);

    int up_and_down_wait() const;
    void setUp_and_down_wait(int newUp_and_down_wait);

    bool up_and_down_uv_on() const;
    void setUp_and_down_uv_on(bool newUp_and_down_uv_on);

    int up_and_down_expo_comp_ms() const;
    void setUp_and_down_expo_comp_ms(int newUp_and_down_expo_comp_ms);

    int tower_resin_measure_end_nm() const;
    void setTower_resin_measure_end_nm(int val);

    int up_and_down_every_layer() const;
    void setUp_and_down_every_layer(int newUp_and_down_every_layer);

    bool delayedEnd() const;
    void setDelayedEnd(bool newDelayedEnd);

    bool rpmControlUvEnabled() const;
    void setRpmControlUvEnabled(bool newRpmControlUvEnabled);

    int stirring_delay_ms() const;
    void setStirring_delay_ms(int newStirring_delay_ms);

    int stirring_moves() const;
    void setStirring_moves(int newStirring_moves);

    int tower_calib_position_nm() const;
    void setTower_calib_position_nm(int newTower_calib_position_nm);

    int tower_resin_measure_start_nm() const;
    void setTower_resin_measure_start_nm(int newTower_resin_measure_start_nm);

signals:

    void mCBoardVersionChanged(int value);
    void mCversionCheckChanged(bool value);
    void autoOffChanged(bool value);
    void calibTowerOffsetChanged(int value);
    void calibratedChanged(bool value);
    void coverCheckChanged(bool value);
    void delayAfterExposureChanged(int value);
    void delayBeforeExposureChanged(int value);
    void fanUvLedEnabledChanged(bool value);
    void fanUvLedRpmChanged(int value);
    void fanBlowerEnabledChanged(bool value);
    void fanBlowerRpmChanged(int value);
    void fanRearEnabledChanged(bool value);
    void fanRearRpmChanged(int value);
    void fanCheckChanged(bool value);
    void layerTowerHopChanged(int value);
    void limit4fastChanged(int value);
    void measuringMovesChanged(int value);
    void microStepsMMChanged(double value);
    void muteChanged(bool value);
    void perPartesChanged(bool value);
    void pwrLedPwmChanged(int value);
    void resinSensorChanged(bool value);
    void screwMmChanged(int value);
    void showI18nSelectChanged(bool value);
    void showUnboxingChanged(bool value);
    void showWizardChanged(bool value);
    void tiltChanged(bool value);
    void tiltHeightChanged(int value);
    void tiltSensitivityChanged(int value);
    void tiltFastTimeChanged(double value);
    void tiltSlowTimeChanged(double value);
    void tiltHighViscosityTimeChanged();
    void towerHeightChanged(int value);
    void towerSensitivityChanged(int value);
    void tower_microstep_size_nmChanged(int value);
    void triggerChanged(int value);
    void tuneTiltChanged(Int2D value);
    void uvCalibIntensityChanged(int value);
    void uvCalibMinIntEdgeChanged(int value);
    void uvCurrentChanged(double value);
    void uvPwmChanged(int value);
    void uvWarmUpTimeChanged(int value);
    void forceSlowTiltHeightChanged();
    void vatRevisionChanged();
    void rpmControlUvFanMinRpmChanged();
    void uvCalibBoostToleranceChanged();
    void rpmControlOverrideChanged();
    void uvPwmPrintChanged();
    void rpmControlUvFanMaxRpmChanged();
    void uvPwmTuneChanged();
    void tiltMinChanged();
    void tiltMaxChanged();
    void currentProfilesSetChanged();
    void lockProfilesChanged();
    void rpmControlUvLedMaxTempChanged();
    void rpmControlUvLedMinTempChanged();

    void tankCleaningExposureTimeChanged();
    void max_tower_height_mmChanged();

    void calib_tower_offset_nmChanged();

    void constraintsChanged();

    void layer_tower_hop_nmChanged();

    void tower_height_nmChanged();

    void up_and_down_z_offset_nmChanged();

    void tankCleaningMinDistance_nmChanged();

    void tankCleaningGentlyUpProfileChanged();

    void tankCleaningAdaptorHeight_nmChanged();

    void default_tower_height_mmChanged();

    void up_and_down_waitChanged();

    void up_and_down_uv_onChanged();

    void up_and_down_expo_comp_msChanged();

    void tower_resin_measure_end_nmChanged();

    void up_and_down_every_layerChanged();

    void delayedEndChanged();

    void rpmControlUvEnabledChanged();

    void stirring_delay_msChanged();

    void stirring_movesChanged();

    void tower_calib_position_nmChanged();

    void tower_resin_measure_start_nmChanged();

public slots:
    int mCBoardVersion();
    bool mCversionCheck();
    bool autoOff();
    int calibTowerOffset();
    bool calibrated();
    bool coverCheck();
    int delayAfterExposure();
    int delayBeforeExposure();
    bool fanUvLedEnabled();
    int fanUvLedRpm();
    bool fanBlowerEnabled();
    int fanBlowerRpm();
    bool fanRearEnabled();
    int fanRearRpm();
    bool fanCheck();
    int layerTowerHop();
    int limit4fast();
    int measuringMoves();
    double microStepsMM();
    bool mute();
    bool perPartes();
    int pwrLedPwm();
    bool resinSensor();
    int screwMm();
    bool showI18nSelect();
    bool showUnboxing();
    bool showWizard();
    bool tilt();
    int tiltHeight();
    int tiltSensitivity();
    double tiltFastTime();
    double tiltSlowTime();
    double tiltHighViscosityTime();
    int towerHeight();
    int towerSensitivity();
    int tower_microstep_size_nm();
    int trigger();
    Int2D tuneTilt();
    int uvCalibIntensity();
    int uvCalibMinIntEdge();
    double uvCurrent();
    int uvPwm();
    int uvWarmUpTime();
    int max_tower_height_mm() const;

    void setMCBoardVersion(int value);
    void setMCversionCheck(bool value);
    void setAutoOff(bool value);
    void setCalibTowerOffset(int value);
    void setCalibrated(bool value);
    void setCoverCheck(bool value);
    void setDelayAfterExposure(int value);
    void setDelayBeforeExposure(int value);
    void setFanUvLedEnabled(bool value);
    void setFanUvLedRpm(int value);
    void setFanBlowerEnabled(bool value);
    void setFanBlowerRpm(int value);
    void setFanRearEnabled(bool value);
    void setFanRearRpm(int value);
    void setFanCheck(bool value);
    void setLayerTowerHop(int value);
    void setLimit4fast(int value);
    void setMeasuringMoves(int value);
    // Read-only, changes will be ignored and not propagated to the config0 object
    void setMicroStepsMM(double value);
    void setMute(bool value);
    void setPerPartes(bool value);
    void setPwrLedPwm(int value);
    void setResinSensor(bool value);
    void setScrewMm(int value);
    void setShowI18nSelect(bool value);
    void setShowUnboxing(bool value);
    void setShowWizard(bool value);
    void setTilt(bool value);
    void setTiltHeight(int value);
    void setTiltSensitivity(int value);
    void setTiltFastTime(double value);
    void setTiltSlowTime(double value);
    void setTiltHighViscosityTime(double value);
    void setTowerHeight(int value);
    void setTowerSensitivity(int value);
    // Read-only, changes will be ignored and not propagated to the config0 object
    void setTower_microstep_size_nm(int value);
    void setTrigger(int value);
    // Read-only, changes will be ignored and not propagated to the config0 object
    void setTuneTilt(Int2D value);
    void setUvCalibIntensity(int value);
    void setUvCalibMinIntEdge(int value);
    void setUvCurrent(double value);
    void setUvPwm(int value);
    void setUvWarmUpTime(int value);
    void setMax_tower_height_mm(int value);

    int forceSlowTiltHeight() const;
    void setForceSlowTiltHeight(int newForceSlowTiltHeight);
    int uvCalibBoostTolerance() const;
    void setUvCalibBoostTolerance(int newUvCalibBoostTolerance);

    int rpmControlUvFanMinRpm() const;
    void setRpmControlUvFanMinRpm(int newRpmControlUvFanMinRpm);

    int vatRevision() const;
    void setVatRevision(int newVatRevision);

    bool rpmControlOverride() const;
    void setRpmControlOverride(bool newRpmControlOverride);

    int uvPwmPrint() const;
    void setUvPwmPrint(int newUvPwmPrint);

    int rpmControlUvFanMaxRpm() const;
    void setRpmControlUvFanMaxRpm(int newRpmControlUvFanMaxRpm);

    int uvPwmTune() const;
    void setUvPwmTune(int newUvPwmTune);

    int tiltMin() const;
    void setTiltMin(int newTiltMin);

    int tiltMax() const;
    void setTiltMax(int newTiltMax);

    const QString &currentProfilesSet() const;
    void setCurrentProfilesSet(const QString &newCurrentProfilesSet);

    bool lockProfiles() const;
    void setLockProfiles(bool newLockProfiles);

    int rpmControlUvLedMaxTemp() const;
    void setRpmControlUvLedMaxTemp(int newRpmControlUvLedMaxTemp);

    int rpmControlUvLedMinTemp() const;
    void setRpmControlUvLedMinTemp(int newRpmControlUvLedMinTemp);

    int tankCleaningExposureTime() const;
    void setTankCleaningExposureTime(int newTankCleaningExposureTime);

    int calib_tower_offset_nm() const;
    void setCalib_tower_offset_nm(int newCalib_tower_offset_nm);

    const QVariantMap &constraints() const;

    int layer_tower_hop_nm() const;
    void setLayer_tower_hop_nm(int newLayer_tower_hop_nm);

    int tower_height_nm() const;
    void setTower_height_nm(int newTower_height_nm);

    int up_and_down_z_offset_nm() const;
    void setUp_and_down_z_offset_nm(int newUp_and_down_z_offset_nm);

    int tankCleaningMinDistance_nm() const;
    void setTankCleaningMinDistance_nm(int newTankCleaningMinDistance_nm);

    int tankCleaningGentlyUpProfile() const;
    void setTankCleaningGentlyUpProfile(int newTankCleaningGentlyUpProfile);

    // BaseDBusObject interface
protected:
    virtual bool set(const QString &name, const QVariant &value) override;
private:
    int m_default_tower_height_mm;
    int m_up_and_down_wait;
    bool m_up_and_down_uv_on;
    int m_up_and_down_expo_comp_ms;
    int m_tower_resin_measure_end_nm;
    int m_up_and_down_every_layer;
    bool m_delayedEnd;
    bool m_rpmControlUvEnabled;
    int m_stirring_delay_ms;
    int m_stirring_moves;
    int m_tower_calib_position_nm;
    int m_tower_resin_measure_start_nm;
};

#endif // CONFIG_H
