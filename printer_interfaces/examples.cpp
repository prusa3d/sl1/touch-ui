/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "examples.h"

Examples::Examples(QJSEngine &_engine, QObject *parent) :
    BaseDBusObject(_engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    m_proxy(service, path, QDBusConnection::systemBus(), this)

{
    m_proxy.setTimeout(210000);
    registerPrinterTypes();
}

bool Examples::set(const QString &name, const QVariant &value)
{
    if(name == QStringLiteral("state")) {
        setState(State(DBusUtils::convert<int>(value)));
    } else if(name == QStringLiteral("download_progress")) {
        setDownload_progress(DBusUtils::convert<double>(value));
    } else if(name == QStringLiteral("unpack_progress")) {
        setUnpack_progress(DBusUtils::convert<double>(value));
    } else if(name == QStringLiteral("copy_progress")) {
        setCopy_progress(DBusUtils::convert<double>(value));
    } else {
        qDebug() << "An attempt to set unknown property "<< name;
        return false; // Unknown property
    }
    return true;

}
