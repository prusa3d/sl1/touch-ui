/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <QMetaEnum>
#include "checksmodel.h"

ChecksModel::ChecksModel(QObject *parent)
    : QAbstractListModel(parent)
{
    resetModel();
}

void ChecksModel::resetModel() {
    m_data.clear();
    QMetaEnum   en = QMetaEnum::fromType<Check>();
    for(int i = 0; i < en.keyCount(); ++i) {
        m_data.append(CheckRecord(Check(en.value(i)), Scheduled, false));
    }
}

QHash<int, QByteArray> ChecksModel::roleNames() const
{
    QHash<int, QByteArray> ret;
    ret[RoleCheckId] = "checkId";
    ret[RoleCheckState] = "checkState";
    ret[RoleHidden] = "hidden";
    return ret;
}

int ChecksModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_data.length();
}

QVariant ChecksModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int idx = index.row();
    if(idx < 0 || idx >= rowCount()) return QVariant();
    switch(role) {
    case RoleCheckId:
        return m_data[idx].checkId;
    case RoleCheckState:
        return m_data[idx].checkState;
    case RoleHidden:
        return m_data[idx].hidden;
    default:
        ;
    }
    return QVariant();
}

bool ChecksModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        int idx = index.row();
        if(idx < 0 || idx >= rowCount()) return false;
        switch(role) {
        case RoleCheckId:
            m_data[idx].checkId = Check(value.toInt());
            break;
        case RoleCheckState:
            m_data[idx].checkState = CheckState(value.toInt());
            qDebug() << "CheckStateChanged: "
                     << QMetaEnum::fromType<Check>().valueToKey( m_data[idx].checkId)
                     << ":" << QMetaEnum::fromType<CheckState>().valueToKey( m_data[idx].checkState) ;
            break;
        case RoleHidden:
            m_data[idx].hidden = value.toBool();
            break;
        default:
            return false;
        }
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ChecksModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

bool ChecksModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if(row < 0 || row > rowCount() || count < 1) return false;
    beginInsertRows(parent, row, row + count - 1);
    for(int i = row; i < row + count - 1; ++i) {
        m_data.insert(row, CheckRecord());
    }
    endInsertRows();
    return true;
}

bool ChecksModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if(row < 0 || row + count - 1 > rowCount() || count < 1) return false;
    beginRemoveRows(parent, row, row + count - 1);
    for(int i = 0; i < count; ++i) {
        m_data.removeAt(row);
    }
    endRemoveRows();
    return true;
}

void ChecksModel::removeRow(int row) {removeRows(row, 1); }

void ChecksModel::change(const QVariantMap &newValues)
{
    foreach(const QString & key, newValues.keys()) {
        bool ok = false;
        int intKey = key.toInt(&ok);
        if(!ok) {
            qWarning() << "Key is not int" << newValues;
            break;
        }
        Check checkId = Check(intKey);
        if(! newValues[key].canConvert<int>()) {
            qWarning() << "CheckState is not int!" << newValues;
            break;
        }
        CheckState checkState = CheckState(newValues[key].toInt());

        // Apply the change
        for(int i = 0; i < rowCount(); ++i) {
            if(m_data[i].checkId == checkId) {
                if( m_data[i].checkState != checkState) {
                    m_data[i].checkState = checkState;
                    QModelIndex idx = this->createIndex(i, 0);
                    emit dataChanged(idx, idx, QVector<int>() << RoleCheckState);
                }
                break;
            }
        }
    }
}
