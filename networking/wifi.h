/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WIFI_H
#define WIFI_H


#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QObject>
#include <QJSValue>

#include <arpa/inet.h>

#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Device>
#include <NetworkManagerQt/WirelessDevice>
#include <NetworkManagerQt/AccessPoint>
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/WirelessSetting>
#include <NetworkManagerQt/WirelessSecuritySetting>
#include <NetworkManagerQt/Ipv4Setting>
#include <NetworkManagerQt/WiredDevice>
#include <QDBusMetaType>
#include <QTextStream>
#include <QUuid>


class Wifi : public QObject
{
    Q_OBJECT
public:
    enum ConfigMethod {Automatic, LinkLocal, Manual, Shared, Disabled};
    Q_ENUM(ConfigMethod)
    explicit Wifi(QQmlApplicationEngine * _enginePtr, QObject *parent = nullptr);

    Q_PROPERTY(QString hardwareAddress READ hardwareAddress NOTIFY hardwareAddressChanged)

    Q_INVOKABLE bool addConnectionAuto(QString ssid, QString psk, bool hidden, bool connect, QJSValue callbackSuccess, QJSValue callbackFailure);
    Q_INVOKABLE bool addConnectionManual(QString ssid, QString psk, QString ip, QString mask, QString gateway, QString dns, bool hidden = false, bool connect = true, QJSValue callbackSuccess = QJSValue(), QJSValue callbackFailure = QJSValue());
    Q_INVOKABLE bool addConnectionAP(QString ssid, QString psk, bool hidden = false, bool connect = true);

    //Q_INVOKABLE bool createConnectionAuto(QString ssid, QString psk, bool hidden);
    //Q_INVOKABLE bool createConnectionManual(QString ssid, QString psk, bool hidden);

    Q_INVOKABLE bool isHotspotActive();
    Q_INVOKABLE bool isClientActive();
    Q_INVOKABLE void deactivateCurrentConnection();
    Q_INVOKABLE bool modifyCurrent(QString ip, QString netmask, QString gateway, QString dns);
    Q_INVOKABLE QJSValue getPSK(QString connectionPath);
    Q_INVOKABLE QJSValue getPSKForId(QString connectionId);
    Q_INVOKABLE QList<QString> listConnections();
    Q_INVOKABLE QList<QString> findConnectionsForSSID(QString ssid);
    NetworkManager::Connection::List  findConnectionsForSssidPtr(QString ssid);
    Q_INVOKABLE QList<QString> findConnectionsForId(QString connectionId);
    /** @return pair ssid, psk */
    Q_INVOKABLE QList<QString> getSsidAndPskForConnection(QString connectionPath);
    /** @return pair ssid, psk */
    Q_INVOKABLE QList<QString> getSsidAndPskForId(QString connectionId);
    Q_INVOKABLE void remove_connections(NetworkManager::WirelessSetting::NetworkMode mode, QString ssid = "");


    /* - An instance will be registered as a context property instead.
    static void registerQml() {
        qmlRegisterType<Wifi>("Native", 1, 0, "Wifi");
    }
    */
    QString hardwareAddress() const;
private:
    /** The QQmlApplicationEngine must be destroyed only after all the Wifi instances using it */
    QQmlApplicationEngine * enginePtr;

    // Lets assume that devices will not dispappear at runtime
    NetworkManager::WirelessDevice::Ptr m_WifiDevice;
signals:
    void connectionsChanged();
    //void connectionAdded(QString path);
    void hardwareAddressChanged(QString hwAddress);
public slots:
};

#endif // WIFI_H
