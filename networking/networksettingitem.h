/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NETWORKSETTINGITEM_H
#define NETWORKSETTINGITEM_H

#include <QAbstractListModel>
#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Connection>

class SettingItem : public QObject {
protected:
    Q_OBJECT
public:
    typedef QSharedPointer<SettingItem> Ptr;
    enum Sections {
        SectionIpv4 = 1,
        SectionIpv6,
        SectionWired,
        SectionWireless,
        SectionWirelessSecurity,
    };
    Q_ENUM(Sections)

    enum DelegateType {
        DelegateDefault = 0,
        DelegateIp4  = 1,
        DelegateIp6 = 2,
        DelegateSwitch = 3,
        DelegateCheck = 4,
    };
    Q_ENUM(DelegateType)
    int listPosition;
    Sections section;
    DelegateType delegateType;

    QString identifier;
    QString label;
    QVariant data;
    bool readOnly;
    bool editable;

    explicit SettingItem(QObject * parent = nullptr) : QObject(parent) {}
    SettingItem(int _listPosition,
                const SettingItem::Sections &_section,
                const SettingItem::DelegateType &_delegateType,
                const QString & _identifier,
                const QString & _label = QStringLiteral(""),
                const QVariant & _data = QVariant(),
                bool _readOnly = true,
                bool _editable = false,
                QObject * parent = nullptr);

    static Ptr create(int _listPosition,
                const SettingItem::Sections &_section,
                const SettingItem::DelegateType &_delegateType,
                const QString & _identifier,
                const QString & _label = QStringLiteral(""),
                const QVariant & _data = QVariant(),
                bool _readOnly = true,
                bool _editable = false,
                QObject * parent = nullptr);

public slots:
    void slotDataChanged(QVariant x) {data = x; }
};

#endif // NETWORKSETTINGITEM_H
