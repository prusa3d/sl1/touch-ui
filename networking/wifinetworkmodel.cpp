/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "wifinetworkmodel.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QObject>
#include <QJSValue>
#include <QUuid>

#include <arpa/inet.h>

#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Device>
#include <NetworkManagerQt/WirelessDevice>
#include <NetworkManagerQt/AccessPoint>
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/WirelessSetting>
#include <NetworkManagerQt/WirelessSecuritySetting>
#include <NetworkManagerQt/Settings>
#include <NetworkManagerQt/Utils>
#include <NetworkManagerQt/Ipv4Setting>
#include <NetworkManagerQt/WiredDevice>
//#include <QDBusMetaType>
WifiNetworkModel::WifiNetworkModel(QObject *parent)
    : QAbstractListModel(parent), m_SignalRefresher(), m_PermanentTail(0), m_PermanentHead(0)
{


    // Find the first Wireless device(there should only be one)
    NetworkManager::Device::List devices = NetworkManager::networkInterfaces();
    NetworkManager::Device::Ptr dev;
    Q_FOREACH(dev, devices) {
        if(dev->type() == NetworkManager::Device::Wifi) {
            m_Device = qobject_cast<QSharedPointer<NetworkManager::WirelessDevice> >(dev);
            break;
        }
    }
    if(! m_Device) {
        m_WifiEnabled = false;
        return;
    }

    // Setup the network setting model
    // FIXME: networkSettingModel is disabled
    m_NetworkSettingModel = nullptr; //new NetworkSettingModel(m_Device.dynamicCast<NetworkManager::Device>(), this);

    // Fill-in the m_Network with items
    refreshItems();

    connect(m_Device.data(), &NetworkManager::WirelessDevice::networkAppeared, this, &WifiNetworkModel::networkAppeared);
    connect(m_Device.data(), &NetworkManager::WirelessDevice::networkDisappeared, this,  &WifiNetworkModel::networkDisappeared);

    //connect(m_Device.data(), &NetworkManager::WirelessDevice::lastScanChanged, this, &WifiNetworkModel::lastScanChangedSlot);

    connect(m_Device.data(), &NetworkManager::WirelessDevice::modeChanged, this, &WifiNetworkModel::modeChanged);
    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::activeConnectionChanged, this, &WifiNetworkModel::slotActiveConnectionChanged);

    m_ActiveConnection = m_Device->activeConnection();
    if(m_ActiveConnection) {
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChangedReason, this, &WifiNetworkModel::slotActiveConnectionStateChangedReason);
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChanged, this, &WifiNetworkModel::slotActiveConnectionStateChanged);
    }
    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::ipV4AddressChanged, this, &WifiNetworkModel::refreshScratchpad);
    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::dhcp4ConfigChanged, this, &WifiNetworkModel::refreshScratchpad);

    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::ipV4ConfigChanged, this, [=](){emit ipAddressChanged(ipAddress()); });


    connect(m_Device.data(), &NetworkManager::WirelessDevice::accessPointAppeared, this, &WifiNetworkModel::accessPointAppeared);
    connect(m_Device.data(), &NetworkManager::WirelessDevice::accessPointDisappeared, this, &WifiNetworkModel::accessPointDisappeared);
    connect(m_Device.data(), &NetworkManager::WirelessDevice::activeAccessPointChanged, this, &WifiNetworkModel::activeAccessPointChanged);

    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::availableConnectionAppeared, this, &WifiNetworkModel::slotAvailableConnectionAppeared);
    connect((NetworkManager::Device*)m_Device.data(), &NetworkManager::Device::availableConnectionDisappeared, this, &WifiNetworkModel::slotAvailableConnectionDisappeared);

    connect(m_Device.data(), &NetworkManager::WirelessDevice::lastScanChanged, this, [=](const QDateTime & when){emit lastScanChanged(when); });

    slotActiveConnectionChanged(); // Just to set the active flag on start
    m_OperationMode = m_Device->mode();
    /* FIXME: networkSettingModel is disabled
    m_NetworkSettingModel->setConnection(m_ActiveConnection);
    emit networkSettingModelChanged(m_NetworkSettingModel);
    */
    refreshScratchpad();
    m_WifiEnabled = wifiEnabled();
    m_SignalRefresher.setInterval(1000);
    m_SignalRefresher.setSingleShot(false);
    connect(&m_SignalRefresher, &QTimer::timeout, this, [=](){
        emit activeConnectionSignalStrengthChanged();
        if(m_WifiEnabled != wifiEnabled()) {
            m_WifiEnabled = wifiEnabled();
            emit wifiEnabledChanged(m_WifiEnabled);
            qDebug() << "WIFI -> " << m_WifiEnabled;
        }

    });
    m_SignalRefresher.start();




}


int WifiNetworkModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_Networks.length();
}


QVariant WifiNetworkModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        qDebug() << "WifiNetworkModel::data - Invalid index: " << index;
        return QVariant();
    }
    NetworkItem::Ptr item = m_Networks.at(index.row());
    switch(role) {
    case RoleName:
        return item->networkName;
    case RoleStrength:
        return item->signalStrength;
    case RoleConnection:
        //Q_ASSERT((item->connectionPath == "") == (item->connection.isNull()));
        return item->connectionPath;
    case RoleReferenceAp:
        {
            if(item->network.isNull()) {
                qDebug() << "Warn: WifiNetworkModel::data, " << item->networkName << " network is NULL";
                return QVariant();
            }
            NetworkManager::AccessPoint::Ptr ap =  item->network->referenceAccessPoint();
            if(ap.isNull()) {
                qDebug() << "Warn: WifiNetworkModel::data, " << item->networkName << " ref. AP is NULL";
                return QVariant();
            }
            return ap->uni();
        }
    case RoleActive:
        return  QVariant::fromValue(item->active);
    case RoleSecurity:
        {
            if(item->network.isNull()) {
                qDebug() << "Warn: WifiNetworkModel::data, " << item->networkName << " network is NULL";
                return QVariant();
            }
            NetworkManager::AccessPoint::Ptr ap =  item->network->referenceAccessPoint();
            if(ap.isNull()) {
                qDebug() << "Warn: WifiNetworkModel::data, " << item->networkName << " ref. AP is NULL";
                return QVariant();
            }
            return static_cast<int>(ap->wpaFlags());
        }
    case RoleSection:
        return item->section;
    case RoleNetwork:
        return item->network ? item->networkName : QVariant();
    case RoleHidden:
        if(item->connection.isNull()) return false;
        else {
            auto settings = item->connection->settings();
            NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting.isNull()) {
                qDebug() << "Wireless network " << item->networkName << " does not have wirelessSetting!";
                return QVariant();
            }
            //qDebug() << "Reading hidden? for network " << item->networkName << ": " << wirelessSetting->hidden();
            return wirelessSetting->hidden();
        }
    case RolePsk:
        return item->psk;
    }



    return QVariant();
}

QHash<int, QByteArray> WifiNetworkModel::roleNames() const
{
    QHash<int, QByteArray> ret;
    ret[RoleName] = "name";
    ret[RoleStrength] = "strength";
    ret[RoleConnection] = "connection";
    ret[RoleReferenceAp] = "referenceAP";
    ret[RoleActive] = "active";
    ret[RoleSecurity] = "security";
    ret[RoleSection] = "section";
    ret[RoleNetwork] = "network";
    ret[RoleHidden] = "hidden";
    ret[RolePsk] = "psk";
    return ret;
}

QString prefix2mask(int prefix) {
    int n = 0xffffffff << (32 - prefix);
    QList<QString> ret;
    for(int i = 0; i < 4; i++) {
        ret.append( QString::number((n >> i*8) & 0xff) );
    }
    QString s;
    for(int i = 3; i >= 0; i--) {
        s += ret[i];
        if(i != 0) {
            s+=".";
        }
    }
    return s;

}
void WifiNetworkModel::refreshScratchpad()
{
    if(! m_Device) return;
    if(m_ActiveConnection) {
        NetworkManager::IpConfig ipconfig =  m_ActiveConnection->ipV4Config();
        NetworkManager::Dhcp4Config::Ptr dhcpconfig = m_ActiveConnection->dhcp4Config();
        NetworkManager::Ipv4Setting::Ptr ipsetting = m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
        if(ipsetting->method() == NetworkManager::Ipv4Setting::Automatic) {
            m_ScratchpadDhcp = true;
        }
        else {
            m_ScratchpadDhcp = false;
        }

        QVariantMap info = getActiveConnectionInfo();
        m_ScratchpadDns = (info.find("dns") != info.end() ? info["dns"].toString() : "");
        m_ScratchpadGateway = (info.find("gateway") != info.end() ? info["gateway"].toString() : "");
        m_ScratchpadIp = (info.find("address") != info.end() ? info["address"].toString() : "");
        m_ScratchpadMask = (info.find("mask") != info.end() ? info["mask"].toString() : "");


    }
    emit scratchPadChanged();
}

void WifiNetworkModel::applyScratchpad(QJSValue callbackSuccess, QJSValue callbackFail)
{
    if(! m_Device) return;
    auto connection = m_ActiveConnection->connection();
    auto settings = connection->settings();
    NetworkManager::Ipv4Setting::Ptr ipsetting = m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
    NetworkManager::Ipv4Setting::Ptr newIpSetting = NetworkManager::Ipv4Setting::Ptr(new NetworkManager::Ipv4Setting());
    if(m_ScratchpadDhcp) {
        //newIpSetting->setMethod(NetworkManager::Ipv4Setting::Automatic);
    }
    else {
        QHostAddress adr;
        if(! adr.setAddress(m_ScratchpadDns)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad DNS"});
            }
        }
        if(! adr.setAddress(m_ScratchpadIp)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad IP"});
            }
        }
        if(! adr.setAddress(m_ScratchpadMask)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad Mask"});
            }
        }
        if(! adr.setAddress(m_ScratchpadGateway)) {
            if(callbackFail.isCallable()) {
                callbackFail.call({"Bad Gateway"});
            }
        }
    }

    NMVariantMapMap toUpdateMap = connection->settings()->toMap();



    if (newIpSetting->method() == NetworkManager::Ipv4Setting::Automatic || newIpSetting->method() == NetworkManager::Ipv4Setting::Manual) {
        if (m_ScratchpadDhcp == true) {
            newIpSetting->setMethod(NetworkManager::Ipv4Setting::Automatic);
        }

        else if (m_ScratchpadDhcp == false) {
            newIpSetting->setMethod(NetworkManager::Ipv4Setting::Manual);
            NetworkManager::IpAddress ipaddr;
            ipaddr.setIp(QHostAddress(m_ScratchpadIp));
            //ipaddr.setPrefixLength(map["prefix"].toInt());
            ipaddr.setNetmask(QHostAddress(m_ScratchpadMask));
            ipaddr.setGateway(QHostAddress(m_ScratchpadGateway));
            newIpSetting->setAddresses(QList<NetworkManager::IpAddress>({ipaddr}));
            newIpSetting->setDns(QList<QHostAddress>({QHostAddress(m_ScratchpadDns)}));
        }

        toUpdateMap.insert("ipv4", newIpSetting->toMap());
    }

    qWarning() << toUpdateMap;
    QDBusPendingReply<> reply = connection->update(toUpdateMap);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
        QDBusPendingReply<> reply = *watcher;
        if(reply.isValid()) {
                if(callbackSuccess.isCallable()) callbackSuccess.call();
                auto replyX = NetworkManager::deactivateConnection(m_ActiveConnection->path());

                QDBusPendingCallWatcher *watcherX = new QDBusPendingCallWatcher(replyX, this);
                connect(watcherX, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
                    QDBusPendingReply<> reply = *watcher;
                    auto replyY = NetworkManager::activateConnection(connection->path(), m_Device->uni(), "/");
                    QDBusPendingCallWatcher *watcherY = new QDBusPendingCallWatcher(replyY, this);

                    connect(watcherY, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
                        QDBusPendingReply<> reply = *watcher;
                        if(reply.isValid() && callbackSuccess.isCallable()) callbackSuccess.call();
                        if(reply.isError() && callbackFail.isCallable()) callbackFail.call({"Could not reactivate connection after changing parameters: "  + reply.error().message()});
                        watcher->deleteLater();
                    });


                    watcher->deleteLater();
                });
        }
        if(reply.isError() && callbackFail.isCallable()) callbackFail.call({"Could not reactivate connection after changing parameters: "  + reply.error().message()});
        watcher->deleteLater();
    });

}

bool WifiNetworkModel::connectToNetwork(QString ssid, QJSValue psk, QJSValue doneCallback, QJSValue errorCallback)
{
    if(! m_Device) return false;
    //Q_ASSERT_X(passwordRequestCallback.isUndefined() || passwordRequestCallback.isCallable(), "connectToNetwork", "Invalid value for passwordRequestCallback");
    Q_ASSERT_X(doneCallback.isNull() ||doneCallback.isUndefined() || doneCallback.isCallable(), "connectToNetwork", "Invalid value for doneCallback");
    Q_ASSERT_X(errorCallback.isNull() || errorCallback.isUndefined() || errorCallback.isCallable(), "connectToNetwork", "Invalid value for errorCallback");

    NetworkManager::WirelessNetwork::Ptr network = m_Device->findNetwork(ssid);
    if(network.isNull()) {
        qDebug() << "WifiNetworkModel::connectToNetwork - nonexistent network(ssid=" << ssid << ")";
        return false;
    }
    NetworkManager::AccessPoint::Ptr ap = network->referenceAccessPoint();

    // Try looking for the connection of the same name
    NetworkManager::Connection::List connections = m_Device->availableConnections();
    NetworkManager::Connection::Ptr connectionX, connection = nullptr;
    Q_FOREACH(connectionX, connections) {
        if(connectionX->name() == ssid) {
            connection = connectionX;
            break;
        }
    }
    // If it doesn't exist, create a new one
    if(connection.isNull()) {
        NetworkManager::ConnectionSettings::Ptr settings(new NetworkManager::ConnectionSettings(NetworkManager::ConnectionSettings::Wireless));
        settings->setId(ssid);
        settings->setUuid(QUuid::createUuid().toString().mid(1, QUuid::createUuid().toString().length() - 2));
        NetworkManager::WirelessSetting::Ptr wirelessSetting = settings->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
        wirelessSetting->setSsid(ssid.toUtf8());
        NetworkManager::WirelessSecuritySetting::Ptr wifiSecurity = settings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
        wifiSecurity->setKeyMgmt(NetworkManager::WirelessSecuritySetting::WpaPsk);
        wifiSecurity->setPsk(psk.toString());
        wifiSecurity->setInitialized(true);
        wirelessSetting->setSecurity("802-11-wireless-security");
        // We try to add and activate our new wireless connection
        QDBusPendingReply <QDBusObjectPath, QDBusObjectPath > reply = NetworkManager::addAndActivateConnection(settings->toMap(), m_Device->uni(), ap->uni());

        // Don't wait for the result, make watcher call callbacks when done
        QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
        QObject::connect(watcher, &QDBusPendingCallWatcher::finished,
                         this, [=](QDBusPendingCallWatcher* watcher) mutable {

            QDBusPendingReply<QDBusObjectPath, QDBusObjectPath > reply = *watcher;
            if (reply.isError()) {
                qDebug() << "ConnectoToNetwork: connection creation error: " << QDBusError::errorString( reply.error().type());
                if(errorCallback.isCallable()) errorCallback.call({reply.error().message()});
            } else {
                qDebug() << "ConnectToNetwork: connection created: " << reply.argumentAt<0>().path() << ", " << reply.argumentAt<1>().path();
                if(doneCallback.isCallable()) doneCallback.call({reply.argumentAt<0>().path(), reply.argumentAt<1>().path()});
            }
            watcher->deleteLater();
        } );

    }// If it does exist, try to activate it
    else {
        NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
        NetworkManager::WirelessSecuritySetting::Ptr wifiSecurity = settings->setting(NetworkManager::Setting::WirelessSecurity).dynamicCast<NetworkManager::WirelessSecuritySetting>();
        // Will be ignored if it's undefined or null
        if(psk.isString()) {
                wifiSecurity->setPsk(psk.toString());
                connection->update(settings->toMap());
        }
        NetworkManager::activateConnection(connection->path(), m_Device->uni(), ap->uni());
        return true;
    }
    return false;
}

/* This one is blocking, but should be still to fast to notice */
QJSValue WifiNetworkModel::getPSK(QString connectionPath, QJSValue doneCallback, QJSValue errorCallback)
{
    if(! m_Device) return QJSValue::NullValue;
    NetworkManager::Connection::Ptr connection(new NetworkManager::Connection(connectionPath));
    if(!connection->isValid()) {
        return false;
    }
    NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
    if(connection->settings()->connectionType() != NetworkManager::ConnectionSettings::Wireless) {
        return false;
    }

    NetworkManager::WirelessSecurityType securityType = NetworkManager::securityTypeFromConnectionSetting(settings);
    const auto key = QStringLiteral("802-11-wireless-security");
    QDBusPendingReply<NMVariantMapMap> reply = connection->secrets(key);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);

    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
        QDBusPendingReply<NMVariantMapMap> reply = *watcher;
        if (reply.isError()) {
            qDebug() << "getPSK: error: " << QDBusError::errorString( reply.error().type());
            if(errorCallback.isCallable()) errorCallback.call({reply.error().message()});
        } else {
            //qDebug() << "getPSK: done: " << reply.argumentAt<0>() ;
            const auto response = reply.argumentAt<0>();
            if(!response.contains(key)) {
                qDebug() << "Wifi::getPSK path " << connectionPath << " does not contain 802-11-wireless-security";
                if(doneCallback.isCallable()) doneCallback.call({""});
            }
            const auto secret = response[key];
            QString pass;
            switch (securityType) {
                case NetworkManager::NoneSecurity:
                    break;
                case NetworkManager::WpaPsk:
                case NetworkManager::Wpa2Psk:
                    pass = secret["psk"].toString();
                    break;
                default:
                    qDebug() << "Wifi::getPSK path " << connectionPath << " does not have a PSK set";
                    if(doneCallback.isCallable()) doneCallback.call({""});
            }
            if(doneCallback.isCallable()) doneCallback.call({pass});
        }
        watcher->deleteLater();
    });
    return true;
}

QJSValue WifiNetworkModel::getPSKbyName(QString name, QJSValue doneCallback, QJSValue errorCallback)
{
    if(! m_Device) return QJSValue::NullValue;
    NetworkManager::Connection::List connections = m_Device->availableConnections();
    if(m_ActiveConnection) {
        connections.append(m_ActiveConnection->connection());
    }
    NetworkManager::Connection::Ptr connection;
    Q_FOREACH(connection, connections) {
        if(connection->name() == name) {
            getPSK(connection->path(), doneCallback, errorCallback);
            return true;
        }
    }
    return false;
}

void WifiNetworkModel::requestScan() const
{
    if(m_Device) {
        m_Device->requestScan();
    }
}

//QDateTime WifiNetworkModel::lastScan() const
//{
//    return m_Device->lastScan();

//}

WifiNetworkModel::ActiveConnectionState WifiNetworkModel::activeConnectionState() const
{
    if(m_ActiveConnection.isNull()) {
        return WifiNetworkModel::ActiveConnectionState::Unknown;
    }
    else return WifiNetworkModel::ActiveConnectionState(static_cast<int>(m_ActiveConnection->state()));
}

QString WifiNetworkModel::activeConnectionName() const
{
    if(m_ActiveConnection.isNull()) return "";
    return m_ActiveConnection->connection()->name();
}

QString WifiNetworkModel::activeConnectionSsid() const
{
    if(m_ActiveConnection.isNull()) return "";
    return m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>()->ssid();
}

bool WifiNetworkModel::isHotspotRunning() const
{
    return (m_OperationMode == NetworkManager::WirelessDevice::ApMode && !m_ActiveConnection.isNull() && m_OperationMode != NetworkManager::WirelessDevice::Infra);
}
void WifiNetworkModel::setHotspotRunning(bool value)
{
    if(! m_Device) return;

    if(value) {
        // Find & activate hotspot connection, if any present. If none exist, do nothing
        NetworkManager::Connection::List connections = m_Device->availableConnections();
        NetworkManager::Connection::Ptr connection;

        Q_FOREACH(NetworkManager::Connection::Ptr con, connections) {
            NetworkManager::WirelessSetting::Ptr ws;
            ws = con->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(ws->mode() == NetworkManager::WirelessSetting::Ap ) {
                connection = con;
                break;
            }
        }
        if(connection) {
            NetworkManager::WirelessSetting::Ptr wirelessSetting = connection->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            qDebug() << "Activating hotspot: " << wirelessSetting->ssid();
            NetworkManager::activateConnection(connection->path(), m_Device->uni(), "/");
        }


    }
    else {
        if(m_Device->activeConnection()) {
            auto wirelessSetting = m_Device->activeConnection()->connection()->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            qDebug() << "Deactivating hotspot: " << wirelessSetting->ssid();
            if(wirelessSetting->mode() == NetworkManager::WirelessSetting::Ap) {

                QDBusPendingReply<> reply = NetworkManager::deactivateConnection(m_Device->activeConnection()->path());
                QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
                connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * watcher){
                    QDBusPendingReply<> reply = *watcher;
                    if (reply.isError() || !reply.isValid()) {
                        qDebug() << "Deactivating hotspot failed" << reply.error() << ", " << reply.reply();
                    }
                    else {
                        qDebug() << "Deactivating hotspot succeeded";
                    }
                    watcher->deleteLater();
                });
            }
        }
    }
}

NetworkSettingModel *WifiNetworkModel::getNetworkSettingModel() const
{
    //Q_ASSERT(m_NetworkSettingModel);
    return m_NetworkSettingModel;
}

QVector<NetworkItem::Ptr> * WifiNetworkModel::items()
{
    return &m_Networks;
}


QVariantMap WifiNetworkModel::getActiveConnectionInfo() const
{
    if(! m_Device) return {};
    if (m_ActiveConnection.isNull())
            return QVariantMap();
    QVariantMap map;
    //auto m_Device->
    auto ipconfig = m_Device->ipV4Config();
    if (ipconfig.addresses().count() > 0) {
        map.insert("address",QVariant(ipconfig.addresses().constFirst().ip().toString()));
        map.insert("mask",QVariant(ipconfig.addresses().constFirst().netmask().toString()));
    }

    map.insert("gateway",QVariant(ipconfig.gateway()));
    if (ipconfig.nameservers().count() > 0)
        map.insert("dns",QVariant(ipconfig.nameservers().first().toString()));
    return map;
}

QString WifiNetworkModel::ipAddress() const
{
    if(! m_Device) return "0.0.0.0";
    QVariantMap map = getActiveConnectionInfo();
    if(map.constFind("address") != map.constEnd()) {
        return map.value("address").toString();
    }
    else return "";
}

int WifiNetworkModel::activeConnectionSignalStrength() const
{
    if(! m_Device) return 0;
    if(m_ActiveConnection) {
        QString name = m_ActiveConnection->connection()->name();
        auto network = m_Device->findNetwork(name);
        if(network) {
            return network->referenceAccessPoint()->signalStrength();
        }
        else return 0;
    }
    else return 0;
}

bool WifiNetworkModel::wifiEnabled() const
{
    return NetworkManager::isWirelessEnabled();
}

void WifiNetworkModel::wifiEnabledSet(bool value)
{
    NetworkManager::setWirelessEnabled(value);
    if(value) {
        refreshItems();
    }else { // Remove all the things that would otherwise be left there

        beginRemoveRows(QModelIndex(), 1, m_Networks.length()-1);
        m_Networks.remove(1, m_Networks.length() -1);
        endRemoveRows();
        for(int i = 0; i < m_Networks.length();) {
            auto sec = m_Networks[i]->section;
            if(     sec == SectionQrcode ||
                    sec == SectionAddNetwork ||
                    sec == SectionWifiClientOnOff ||
                    (sec == SectionNetwork && m_Networks[i]->network.isNull()))
            {
                beginRemoveRows(QModelIndex(), i, i);
                m_Networks.remove(i);
                endRemoveRows();
            }
            else {++i; }
        }
        for(int i = 0; i < m_Networks.length(); ++i) m_Networks[i]->listPosition = i;
    }


}

QDateTime WifiNetworkModel::lastScan() const
{
    if(! m_Device) return QDateTime();
    return m_Device->lastScan();
}



NetworkManager::Connection::Ptr WifiNetworkModel::connectionBySsid(const QString &ssid)
{
    if(! m_Device) return nullptr;
    auto connections = m_Device->availableConnections();

    // Why would I want to ignore the active connection??
    if(m_ActiveConnection) {
        auto wirelessSetting = m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
        if(wirelessSetting->mode() == NetworkManager::WirelessSetting::Infrastructure) {
            connections.append(m_ActiveConnection->connection());
        }
    }

    Q_FOREACH(auto c, connections) {
        if(c->settings()->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            auto wirelessSetting = c->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting->ssid() == ssid) return c;
        }
    }
    return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::itemByConnection(const QString &connectionPath)
{
    Q_FOREACH(auto item, m_Networks) {
        if(item->connectionPath == connectionPath) return item;
    }
    return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::getItem(const QString &name)
{
    for(auto it = m_Networks.begin(); it != m_Networks.end(); ++it) {
        if((*it)->networkName == name) return *it;
    }
    return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::insertOrGetItem(const QString &name)
{
    if(! m_Device) return nullptr;
    if(getItem(name).isNull()) {
        int idx = m_Networks.length() - m_PermanentTail;
        Q_ASSERT(idx >= m_PermanentHead);
        //qDebug() << "Inserting item " << name << " to index: " << idx;

        NetworkItem::Ptr item(new NetworkItem(this));
        item->networkName = name;
        item->network = m_Device->findNetwork(name);
        item->connection = connectionBySsid(name);
        item->connectionPath = item->connection.isNull() ? "" : item->connection->path();
        item->signalStrength = item->network ? item->network->signalStrength() : 0;
        item->listPosition = idx;
        item->section = SectionNetwork;
        item->setObjectName(item->networkName);

        if(item->network) { // Signal strength should be updated on change
            connect(item->network.data(), &NetworkManager::WirelessNetwork::signalStrengthChanged, item.data(), &NetworkItem::slot_SignalStrengthChanged);
        }
        if(item->connection) {
            connect(item->connection.data(), &NetworkManager::Connection::removed, this, &WifiNetworkModel::slotConnectionRemoved);
        }
        beginInsertRows(QModelIndex(), idx, idx);
        m_Networks.insert(idx, item);
        endInsertRows();

        // After inserting the item, renumbering all the items that follow
        // the inserted one is necessary. No need to emit dataChanged, listPosition is not a role.
        for(int i = idx; i < m_Networks.length(); ++i) {
            m_Networks[i]->listPosition = i;
        }
        sortItems();

        return item;
    }
    else return getItem(name);
}

bool WifiNetworkModel::tryRemoveItem(const QString &name)
{
    auto item = getItem(name);
    return tryRemoveItem(item);
}

bool WifiNetworkModel::tryRemoveItem(NetworkItem::Ptr item)
{
#ifdef QT_DEBUG
    // This should actually be true for entire time the application is running
    for(int i = 0; i < m_Networks.length(); ++i) {
        Q_ASSERT(m_Networks[i]->listPosition == i);
    }
#endif


    if(item && item->network.isNull() && item->connection.isNull()) {
        //qDebug() << "Removing item " << item->networkName;
        beginRemoveRows(QModelIndex(), item->listPosition, item->listPosition);
        int removedIndex = item->listPosition;

        m_Networks.remove(item->listPosition);

        // After deleting the item, renumbering all the items that follow
        // the deleted one is necessary. No need to emit dataChanged, listPosition is not a role.
        for(int idx = removedIndex; idx < m_Networks.length(); ++idx) {
            m_Networks[idx]->listPosition = idx;
        }
        endRemoveRows();
        return true;
    }
    else {
        //qWarning() << "Attempt has been made to remove " << (item ? "still valid" : "nonexistent") <<" item: " << (item ? item->networkName : "nullptr");
        return false;
    }
}

NetworkItem::Ptr WifiNetworkModel::addConnectionToItem(const QString &name, const QString &connectionPath)
{
    if(! m_Device) return nullptr;
    NetworkManager::Connection::Ptr connection = NetworkManager::findConnection(connectionPath);
    return addConnectionToItem(name, connection);

}

NetworkItem::Ptr WifiNetworkModel::addConnectionToItem(const QString &name, NetworkManager::Connection::Ptr connection)
{
#ifdef QT_DEBUG
    // This should actually be true for entire time the application is running
    for(int i = 0; i < m_Networks.length(); ++i) {
        Q_ASSERT(m_Networks[i]->listPosition == i);
    }
#endif
    if(! m_Device) return nullptr;
    if(connection.isNull()) {
        qWarning() << "Adding connection nullptr to " << name;
    }
    QString connectionPath = connection->path();
    if(connection) {
        auto wirelessSetting = connection->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
        if(wirelessSetting->ssid() == this->getLocalHotspotSSID() || connection->name() == "hotspot") {
            qDebug() << "Avoiding adding hotspot to the item list";
            return nullptr;
        }
    }

    auto item = insertOrGetItem(name);
    if(item) {

        if(item->connection.isNull() && item->connectionPath == "") {
            item->connection = connection;
            item->connectionPath = connectionPath;
            // Removed connection are detected in WifiNetworkModel
            connect(connection.data(), &NetworkManager::Connection::removed, this, &WifiNetworkModel::slotConnectionRemoved);
            auto index = createIndex(item->listPosition, 0);
            emit dataChanged(index, index, {RoleConnection, RoleHidden});

           refreshPsk(item); // Will set the password when it is loaded


            //--------------


            return item;
        }
        else {
            //qWarning() << "Adding connection " << connection->name() << ", " << connectionPath << " to item that has a connection already";
        }

    }
    else {
        qDebug() << "Adding connection "  << connectionPath << " to nonexistent item " << name;
    }
    return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::addConnectionToItem(NetworkManager::Connection::Ptr connection)
{
    if(! m_Device) return nullptr;
    if(connection) {
        auto ws = connection->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
        if(ws && ws->mode() != NetworkManager::WirelessSetting::Ap) {

            return addConnectionToItem(ws->ssid(), connection);
        }
        else return nullptr;
    }
    else return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::addNetworkToItem(const QString &name)
{
    if(! m_Device) return nullptr;
    auto network = m_Device->findNetwork(name);
    return addNetworkToItem(name, network);
}

NetworkItem::Ptr WifiNetworkModel::addNetworkToItem(const QString &name, NetworkManager::WirelessNetwork::Ptr network)
{
    if(! m_Device) return nullptr;
    if(name == this->getLocalHotspotSSID()) return nullptr;
    auto item = insertOrGetItem(name);
    if(item.isNull()) {
        qWarning() << "Adding network to nonexistent item: " << name;
        return nullptr;
    }

    if(network.isNull()) {
        qWarning() << "Adding nullptr network to item: " << name;
        return nullptr;
    }

    if(item->network.isNull()) {
        item->network = network;
        item->signalStrength = network->signalStrength();
        connect(network.data(), &NetworkManager::WirelessNetwork::signalStrengthChanged, item.data(), &NetworkItem::slot_SignalStrengthChanged);
        sortItems(); // emits dataChanged, no need to emit it 2x
        return item;
    }

    return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::itemRemoveNetwork(const QString &name)
{
#ifdef QT_DEBUG
    // This should actually be true for entire time the application is running
    for(int i = 0; i < m_Networks.length(); ++i) {
        Q_ASSERT(m_Networks[i]->listPosition == i);
    }
#endif
    if(! m_Device) return nullptr;
    auto item = getItem(name);
    if(item) {
        if(! item->network.isNull()) {
            item->network.clear();
            auto index = createIndex(item->listPosition, 0);
            emit dataChanged(index, index, {RoleNetwork});
            return item;
        }
        else {
            qDebug() << "Attempt to remove nonexistent network(nullptr): " << name;
        }
    }
    else {
        qDebug() << "Attempt to remove network from nonexistent item: " << name;
    }
    return nullptr;
}

NetworkItem::Ptr WifiNetworkModel::itemRemoveConnection(const QString &path)
{
#ifdef QT_DEBUG
    // This should actually be true for entire time the application is running
    for(int i = 0; i < m_Networks.length(); ++i) {
        Q_ASSERT(m_Networks[i]->listPosition == i);
    }
#endif
    if(! m_Device) return nullptr;
    Q_FOREACH(auto item, m_Networks) {
        if(item->connectionPath == path) {
            item->connection.clear();
            item->connectionPath = "";
            item->psk = "";
            auto index = createIndex(item->listPosition, 0);
            emit dataChanged(index, index, {RoleConnection, RolePsk});
            return item;
        }
    }
    return nullptr;
}

void WifiNetworkModel::refreshItems()
{
    if(! m_Device) return;
    /// Temporary storage, the number of items needs to be known
    ///  before inserting them into the model
    QVector<NetworkItem::Ptr> tmpStorage;

    m_PermanentHead = 0;
    m_PermanentTail  = 0;
    beginRemoveRows(QModelIndex(), 0, m_Networks.length() - 1);
    m_Networks.clear();
    endRemoveRows();

    // Fill-in data
    NetworkManager::WirelessNetwork::List networks = m_Device->networks();
    NetworkManager::WirelessNetwork::Ptr network;

    // The first item is the Wifi Switch
    {
        NetworkItem::Ptr item = NetworkItem::Ptr(new NetworkItem(this));
        item->section = SectionWifiOnOff;
        item->setObjectName("Wi-Fi OnOff Switch");
        tmpStorage.append(item);
    }

     //The second item is the WifiClient OnOff switch
    {
        NetworkItem::Ptr item = NetworkItem::Ptr(new NetworkItem(this));
        item->section = SectionWifiClientOnOff;
        item->setObjectName("Wifi Client Switch");
        tmpStorage.append(item);
    }


    // Add all active networks to the list
    Q_FOREACH(network, networks) {
        // The local AP is left-out on purpose, it does not belong to the network list
        if(network->ssid() == this->getLocalHotspotSSID() ) {
            continue;
        }

        NetworkItem::Ptr item(new NetworkItem(this));
        item->networkName = network->ssid();
        item->network = m_Device->findNetwork(network->ssid());
        item->connection = connectionBySsid(network->ssid());
        item->connectionPath = item->connection.isNull() ? "" : item->connection->path();
        item->signalStrength = item->network ? item->network->signalStrength() : 0;
        item->section = SectionNetwork;
        item->setObjectName(item->networkName);

        if(item->connection && item->connection->name() == "hotspot") continue;

        if(item->network) { // Signal strength should be updated on change
            connect(item->network.data(), &NetworkManager::WirelessNetwork::signalStrengthChanged, item.data(), &NetworkItem::slot_SignalStrengthChanged);
        }
        if(item->connection) {
            connect(item->connection.data(), &NetworkManager::Connection::removed, this, &WifiNetworkModel::slotConnectionRemoved);
            refreshPsk(item);
        }

        tmpStorage.append(item);
    }



    // Add networks that are not currently active, but have an existing connection saved in the NetworkManager
    Q_FOREACH(auto con, NetworkManager::listConnections()) {
        if(con->settings()->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            auto ws = con->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(ws->mode() == NetworkManager::WirelessSetting::Ap || con->name() == "hotspot" ) {
                continue; // Skip over all AP connections = local hostspots
            }
            QString ssid = ws->ssid();
            QList<NetworkManager::WirelessNetwork::Ptr>::iterator found = std::find_if(networks.begin(), networks.end(), [&ssid](NetworkManager::WirelessNetwork::Ptr n) {return n->ssid() == ssid; });
            if(found == networks.end()) {
                NetworkItem::Ptr item(new NetworkItem(this));
                item->networkName = ssid;
                item->setObjectName(ssid);
                item->connection = con;
                item->connectionPath = con->path();
                item->section = SectionNetwork;
                if(item->connection) {
                    connect(item->connection.data(), &NetworkManager::Connection::removed, this, &WifiNetworkModel::slotConnectionRemoved);
                }
                tmpStorage.append(item);
            }

        }
    }

    // The AddNetwork item at the end of the list
    {
        NetworkItem::Ptr item = NetworkItem::Ptr(new NetworkItem(this));
        item->networkName = "test_add_network";
        item->section = SectionAddNetwork;
        item->setObjectName("Add Network");
        tmpStorage.append(item);
    }

    // The last item in the list is the QRCode
    {
        NetworkItem::Ptr qrcodeItem = NetworkItem::Ptr(new NetworkItem(this));
        qrcodeItem->networkName = "test_qrcode";
        qrcodeItem->section = SectionQrcode;
        qrcodeItem->setObjectName("QRCode Item");
        tmpStorage.append(qrcodeItem);
    }

    std::stable_sort(tmpStorage.begin(), tmpStorage.end(), &NetworkItem::ItemLessEqual);
    for(int i = 0; i < tmpStorage.length(); ++i) {
        tmpStorage[i]->listPosition = i;
    }

    // Insert just the mandatory control items, the rest will
    // just call "beginInsertRows" individually.
    beginInsertRows(QModelIndex(), 0, tmpStorage.length() -1);
    m_Networks.swap(tmpStorage);
    endInsertRows();
    //emit dataChanged(createIndex(0, 0), createIndex(m_Networks.length() - 1, 0));
}



QString WifiNetworkModel::getLocalHotspotSSID() const
{
    if(! m_Device) return "";
    NetworkManager::Connection::List connections = NetworkManager::listConnections();

    NetworkManager::Connection::Ptr con;
    Q_FOREACH(con, connections) {
        if(con->settings()->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            auto wirelessSetting = con->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting && wirelessSetting->mode() == NetworkManager::WirelessSetting::Ap ) {
                return wirelessSetting->ssid();
            }
        }
    }


    // lastShot - test activeConnection
    if(m_ActiveConnection) {
        auto connection  = m_ActiveConnection->connection();
        if(connection && connection->settings()->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            auto wirelessSetting = connection->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            if(wirelessSetting && wirelessSetting->mode() == NetworkManager::WirelessSetting::Ap ) {
                return wirelessSetting->ssid();
            }
        }
    }
    return "";
}

void WifiNetworkModel::removeConnection(QString name, QJSValue callbackSuccess, QJSValue callbackFailure)
{
    if(! m_Device) return;
    NetworkManager::Connection::List listConnection = NetworkManager::listConnections();
    NetworkManager::Connection::List wirelessConnections;
    Q_FOREACH(auto c, listConnection) {
        if(c->settings()->connectionType() == NetworkManager::ConnectionSettings::Wireless) {
            wirelessConnections.append(c);
        }
    }

    NetworkManager::Connection::Ptr con = m_ActiveConnection ? m_ActiveConnection->connection() : nullptr;
   if(con && con->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>()->ssid() == name) {
       // Active connection
       auto reply  = NetworkManager::deactivateConnection(m_ActiveConnection->path());

       // Don't wait for it to be finished, start watcher and return
       QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
       connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * watcher) mutable {
           QDBusPendingReply<> reply = *watcher;
           if(con && reply.isValid()) {
               QDBusPendingReply<> reply2 = con->remove();
               QDBusPendingCallWatcher *watcher2 = new QDBusPendingCallWatcher(reply2, this);
               connect(watcher2, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * watcher) mutable {
                   QDBusPendingReply<> reply = *watcher;
                   if(reply.isError() && callbackFailure.isCallable()) callbackFailure.call({reply.error().message()});
                   else {
                       // Remove the connection reference
                       /*
                       auto item = getItem(name);
                       if(item) {
                           item->connection.clear();
                           item->connectionPath = "";
                       }

                       if(! tryRemoveItem(name) ) {
                           emit dataChanged(createIndex(item->listPosition,0), createIndex(item->listPosition,0), {RoleConnection});
                       }
                       */
                       if(callbackSuccess.isCallable()) {
                            callbackSuccess.call({name});
                       }
                   }
                   watcher->deleteLater();
               });
           }
           else {
               if(callbackFailure.isCallable()) callbackFailure.call({reply.error().message(), });
           }
           watcher->deleteLater();
       });
       return;
   }

   con = nullptr;
   Q_FOREACH(NetworkManager::Connection::Ptr current, wirelessConnections) {
       if(current->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>()->ssid() == name) {
           con = current;
           break;
       }
   }

   if(con) {
        QDBusPendingReply<> reply = con->remove();
        QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
        connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * watcher) mutable {
            QDBusPendingReply<> reply = *watcher;
            qDebug() << "connection removed? " << reply.isValid();
            if(reply.isError() && callbackFailure.isCallable()) {
                callbackFailure.call({reply.error().message()});
            }
            else {
                // Remove the connection reference
                /*
                auto item = getItem(name);
                if(item) {
                    item->connection.clear();
                    item->connectionPath = "";
                }

                if(! tryRemoveItem(name) ) {
                    emit dataChanged(createIndex(item->listPosition,0), createIndex(item->listPosition,0), {RoleConnection});
                }
                */
                if(callbackSuccess.isCallable()) {
                    callbackSuccess.call({name});
                }
                //tryRemoveItem(name);
            }
            watcher->deleteLater();
        });
   }

}

void WifiNetworkModel::disconnectWifi()
{
    if(m_ActiveConnection) {
        NetworkManager::deactivateConnection(m_ActiveConnection->path());
    }
}

void WifiNetworkModel::requestScan()
{
    if(! m_Device) return;
    m_Device->requestScan();
}

void WifiNetworkModel::refreshPsk(NetworkItem::Ptr item)
{
    if(! m_Device) return;
    Q_ASSERT( ! item.isNull());

    auto connection = item->connection;
    if(! connection->isValid() ) {
        return;
    }

    NetworkManager::ConnectionSettings::Ptr settings = connection->settings();
    NetworkManager::WirelessSecurityType securityType = NetworkManager::securityTypeFromConnectionSetting(settings);
    const auto key = QStringLiteral("802-11-wireless-security");
    QDBusPendingReply<NMVariantMapMap> reply = connection->secrets(key);
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher *watcher) mutable {
        QDBusPendingReply<NMVariantMapMap> reply = *watcher;
        if (reply.isError()) { qDebug() << "getPSK: error: " << QDBusError::errorString( reply.error().type()); }
        else {
            const auto response = reply.argumentAt<0>();
            if(!response.contains(key)) { qDebug() << "refreshPsk  " << connection->name() << " does not contain 802-11-wireless-security"; }
            const auto secret = response[key];
            QString pass;
            switch (securityType) {
                case NetworkManager::WpaPsk:
                case NetworkManager::Wpa2Psk:
                {
                    item->psk = secret["psk"].toString();
                    auto index = createIndex(item->listPosition, 0);
                    emit dataChanged(index, index, {RolePsk});

                }
                break;
                default:
                    qDebug() << "refreshPskpath " << connection->path() << " does not have a PSK set";
            }
        }
        watcher->deleteLater();
    });
}

void WifiNetworkModel::refreshPsk(QString ssid)
{
    refreshPsk(getItem(ssid));
}

QString WifiNetworkModel::getPskForNetwork(QString ssid)
{
    auto item  = getItem(ssid);
    if(item) {
        return item->psk;
    }
    else return "";
}


void WifiNetworkModel::accessPointAppeared(const QString &uni)
{
    Q_UNUSED(uni)

}

void WifiNetworkModel::accessPointDisappeared(const QString &uni)
{
    Q_UNUSED(uni)
}

void WifiNetworkModel::activeAccessPointChanged(const QString &uni)
{
    Q_UNUSED(uni)
    emit activeConnectionSignalStrengthChanged();

}

void WifiNetworkModel::modeChanged(NetworkManager::WirelessDevice::OperationMode newMode)
{
    if(! m_Device) return;
    qDebug() << "Wireless mode changed: " << newMode;
    this->m_OperationMode = newMode;
    emit isHotspotRunningChanged((m_OperationMode == NetworkManager::WirelessDevice::ApMode && !m_ActiveConnection.isNull() && m_OperationMode != NetworkManager::WirelessDevice::Infra));

}

void WifiNetworkModel::networkAppeared(const QString &ssid)
{
    if(! m_Device) return;
    // The local AP is left-out on purpose
    if(ssid == this->getLocalHotspotSSID() ) {
        return;
    }

    addNetworkToItem(ssid);
    return;
}

void WifiNetworkModel::networkDisappeared(const QString &ssid)
{
    auto item = itemRemoveNetwork(ssid);
    tryRemoveItem(item);
}

void WifiNetworkModel::slotActiveConnectionChanged()
{
    if(! m_Device) return;
    m_ActiveConnection = m_Device->activeConnection(); // Old connection pointer should be destroyed if there is a new active connection

#ifdef QT_DEBUG
    QString aState;
    if(m_ActiveConnection) {
        if(m_ActiveConnection->connection()) {
            auto ws = m_ActiveConnection->connection()->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
            aState = "Connected to " + ws->ssid();
        }
        else {
            aState = "No connection";
        }
    }
    else {
        aState = "No active connection";
    }
    qDebug() << "Wireless active connection changed to " << aState;
#endif

    /* FIXME: networksettingModel is disabled
    m_NetworkSettingModel->setConnection(m_ActiveConnection);
    emit networkSettingModelChanged(m_NetworkSettingModel);
    */

    emit hotspotPskChanged(); // no easy way to detect that, might as well emit it from here


    if(m_ActiveConnection.isNull()) {
        // If no active connection, just set the active flag to false for all
        for(int i = 0; i < m_Networks.length(); ++i) {
            if(m_Networks.at(i)->active) {
                m_Networks.at(i)->active = false;
            }
        }
        return;
    }
    else {
        // If there is an active connection, connect to its signals
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChangedReason, this, &WifiNetworkModel::slotActiveConnectionStateChangedReason);
        connect(m_ActiveConnection.data(), &NetworkManager::ActiveConnection::stateChanged, this, &WifiNetworkModel::slotActiveConnectionStateChanged);

        // There is an active connection, unset the flag on the old
        // item and set it on the current one.
        QString name  = m_ActiveConnection->connection()->name();
        QList<int> affectedIndexes;
        for(int i = 0; i < m_Networks.length(); ++i) {
            if(m_Networks.at(i)->active) {
                m_Networks.at(i)->active = false;
            }
            if(m_Networks.at(i)->networkName == name) {
                m_Networks.at(i)->active = true;
            }
        }

        emit activeConnectionNameChanged(activeConnectionName());
        emit activeConnectionSsidChanged(activeConnectionSsid());
    }
    sortItems();
}

void WifiNetworkModel::slotActiveConnectionStateChanged(NetworkManager::ActiveConnection::State state)
{
    if(! m_Device) return;
    refreshScratchpad();

    // Refresh PSK for items that were activated
    if(activeConnectionState() == Activated) {
        NetworkItem::Ptr item =  getItem(activeConnectionSsid());
        if( ! item.isNull()) {
            refreshPsk(item);
        }
    }
    emit activeConnectionStateChanged(WifiNetworkModel::ActiveConnectionState((int)state));
}

void WifiNetworkModel::slotActiveConnectionStateChangedReason(NetworkManager::ActiveConnection::State state, NetworkManager::ActiveConnection::Reason reason)
{
    if(! m_Device) return;
    emit activeConnectionStateChangedReason((int)state, (int)reason);
}

void WifiNetworkModel::slotAvailableConnectionAppeared(const QString &path)
{
    if(! m_Device) return;
    auto con =  NetworkManager::findConnection(path);
    if(con) {
        auto ws = con->settings()->setting(NetworkManager::Setting::Wireless).dynamicCast<NetworkManager::WirelessSetting>();
        if(ws && ws->mode() != NetworkManager::WirelessSetting::Ap) {
            addConnectionToItem(ws->ssid(), path);
            return;
        }
    }
    qDebug() << "Connection " << path << " not added";
}

void WifiNetworkModel::slotAvailableConnectionDisappeared(const QString &path)
{
    Q_UNUSED(path)
    qDebug() << "Wireless available connection disappeard, no action";
}

void WifiNetworkModel::sortItems()
{
    std::stable_sort(m_Networks.begin(), m_Networks.end(), &NetworkItem::ItemLessEqual);
    for(int i = 0; i < m_Networks.length(); ++i) {
        m_Networks[i]->listPosition = i;
    }
    emit dataChanged(createIndex(0,0), createIndex(m_Networks.length() -1, 0));
}

void WifiNetworkModel::slotConnectionRemoved(const QString &path)
{
    if(! m_Device) return;
    // It's important for the connection & connectionPath in the items to be consistent, otherwise this will not work.
    // BTW: This will not segfault just because of the QSharedPointer used for item
    auto item = itemRemoveConnection(path);
    if(! item.isNull()) {
        qDebug() << "Trying to remove connection:"<<item->networkName;
        if(tryRemoveItem(item)) {
            qDebug() << "Item " << " was removed because its connection " << path << " was removed.";
        }
        else {
            qDebug() << "Item "<< " was NOT removed by removing connection " << path << " (still has a visible network?)";
        }
    }
    else {
        qDebug() << "slotConnectionRemoved(not belonging to any item): " << path;
    }
}

void WifiNetworkModel::slotReloadConnections()
{
    refreshItems();
}

bool NetworkItem::ItemLessEqual(const NetworkItem::Ptr a, const NetworkItem::Ptr b)
{
    if(a->section == b->section) {
        if(a->active == b->active) {
            return a->signalStrength >= b->signalStrength;
        }
        else return a->active && ! b->active;
    }
    else if(a->section < b->section) return true;
    else return false; // keep order for items in different section -> sort only within a given section
}

void NetworkItem::slot_SignalStrengthChanged(int _strength)
{
    if(signalStrength != _strength) {
        signalStrength = _strength;
        if(QObject::parent()) {
            WifiNetworkModel* ptr = qobject_cast<WifiNetworkModel*>(QObject::parent());
            QModelIndex index = ptr->publicCreateIndex(listPosition, 0);
            emit ptr->dataChanged(index, index, {WifiNetworkModel::RoleStrength,});
        }
    }

}
