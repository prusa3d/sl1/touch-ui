/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NETWORKSETTINGMODEL_H
#define NETWORKSETTINGMODEL_H

#include <QAbstractListModel>
#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/Device>

#include "networksettingitem.h"

class NetworkSettingModel : public QAbstractListModel
{
    Q_OBJECT

public:

    enum SettingRoles {
        RoleSection = Qt::UserRole + 1, // enum section
        RoleDelegateType,
        RoleIdentifier,
        RoleLabel,
        RoleData,
        RoleReadOnly,
        RoleEditable,


    };


    explicit NetworkSettingModel(NetworkManager::Device::Ptr dev, QObject *parent = nullptr);

    void setConnection(NetworkManager::ActiveConnection::Ptr connection);
    void clean();

    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    void insertIpv4();
    bool applyIpv4();

    QHash<int, QByteArray> roleNames() const override;

private:
    QVector<SettingItem::Ptr> m_Items;
    NetworkManager::Connection::Ptr m_Connection;
    NetworkManager::Device::Ptr m_Device;
};

#endif // NETWORKSETTINGMODEL_H
