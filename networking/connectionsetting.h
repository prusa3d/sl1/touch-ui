/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONNECTIONSETTING_H
#define CONNECTIONSETTING_H

#include <QObject>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QJSValue>

#include <arpa/inet.h>

#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Device>
#include <NetworkManagerQt/WirelessDevice>
#include <NetworkManagerQt/AccessPoint>
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/WirelessSetting>
#include <NetworkManagerQt/WirelessSecuritySetting>
#include <NetworkManagerQt/Ipv4Setting>
#include <NetworkManagerQt/WiredDevice>
#include <QDBusMetaType>
#include <QUuid>


/*
54     enum State {
55         Unknown = 0,
56         Activating,
57         Activated,
58         Deactivating,
59         Deactivated
60     };
61
62     enum Reason {
63         UknownReason = 0,
64         None,
65         UserDisconnected,
66         DeviceDisconnected,
67         ServiceStopped,
68         IpConfigInvalid,
69         ConnectTimeout,
70         ServiceStartTimeout,
71         ServiceStartFailed,
72         NoSecrets,
73         LoginFailed,
74         ConnectionRemoved,
75         DependencyFailed,
76         DeviceRealizeFailed,
77         DeviceRemoved
78     };
 */
/**
 * @brief The ConnectionSetting class is meant to provide an interface
 * enabling to get/set settings of a network connection.
 *
 */
class ConnectionSetting : public QObject
{
    Q_OBJECT
    NetworkManager::Connection::Ptr m_Connection;
    NetworkManager::WirelessSetting::Ptr m_WirelessSetting;
    NetworkManager::WirelessSecuritySetting::Ptr m_WirelessSecuritySettings;
    NetworkManager::Ipv4Setting::Ptr m_Ipv4Settings;

public:
    explicit ConnectionSetting(NetworkManager::Connection::Ptr connection, QObject *parent = nullptr);

signals:

public slots:
    void onConnectionRemoved(QString path);
};

#endif // CONNECTIONSETTING_H
