/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "networksettingitem.h"
#include "networksettingmodel.h"
#include <QDebug>

#include <NetworkManagerQt/Ipv4Setting>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/WiredSetting>
#include <NetworkManagerQt/WirelessSetting>
#include <NetworkManagerQt/WirelessSecuritySetting>

NetworkSettingModel::NetworkSettingModel(NetworkManager::Device::Ptr dev, QObject *parent)
    : QAbstractListModel(parent)
{
    m_Device = dev;
}

void NetworkSettingModel::setConnection(NetworkManager::ActiveConnection::Ptr ActiveConnection)
{
    if(ActiveConnection.isNull()) {
        qDebug() << "NetworkSettingModel::setConnection(nullptr) !!!";
        this->clean();
        m_Connection = nullptr;

    }
    else {
        m_Connection = ActiveConnection->connection();
        this->clean();
        insertIpv4();
    }
    return;

}

void NetworkSettingModel::clean()
{
    beginRemoveRows(QModelIndex(), 0, m_Items.length() - 1);
    m_Items.clear();
    endRemoveRows();
}


QVariant NetworkSettingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)
    Q_UNUSED(role)

    // FIXME: Implement me!
    return QVariant();
}

bool NetworkSettingModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (value != headerData(section, orientation, role)) {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }
    return false;
}

int NetworkSettingModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;


    return m_Items.length();
}

QVariant NetworkSettingModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        qDebug() << "Invalid index" << index;
        return QVariant();
    }
    //qDebug() << "getting data: " << index << ", role: " << role;
    SettingItem::Ptr item = m_Items.at(index.row());
    switch(role) {
    case RoleSection:
        return static_cast<int>(item->section);
    case RoleDelegateType:
        return static_cast<int>(item->delegateType);
    case RoleIdentifier:
        return item->identifier;
    case RoleLabel:
        return item->label;
    case RoleData:
        return item->data;
    case RoleReadOnly:
        return item->readOnly;
    case RoleEditable:
        return item->editable;
    }
    qDebug() << "Role " << role << " does not exist";
    return QVariant();
}

bool NetworkSettingModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value && (data(index, RoleEditable) == true || role == RoleEditable)) {
        SettingItem::Ptr item = m_Items.at(index.row());
        switch(role) {
        case RoleSection:
            item->section = SettingItem::Sections(value.toInt());
            break;
        case RoleDelegateType:
            item->delegateType = SettingItem::DelegateType(value.toInt());
            break;
        case RoleIdentifier:
            item->identifier = value.toString();
            break;
        case RoleLabel:
            item->label = value.toString();
            break;
        case RoleData:
            item->data = value;
            break;
        case RoleReadOnly:
            item->readOnly = value.toBool();
            break;
        case RoleEditable:
            item->editable = value.toBool();
            break;
        default:
            qDebug() << "NetworkSettingModel::setData - unapplicable role: " << roleNames().value(role);
        }
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags NetworkSettingModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool NetworkSettingModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
    return false;
}

bool NetworkSettingModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
    return false;
}

void NetworkSettingModel::insertIpv4()
{
    if(m_Connection) {
        QList<SettingItem::Ptr> tmpList;
        NetworkManager::ConnectionSettings::Ptr settings = m_Connection->settings();
        NetworkManager::Ipv4Setting::Ptr ipv4Setting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();
        auto ipv4Method = ipv4Setting->method();
        // TODO: I recognize only one address for now
        QList<NetworkManager::IpAddress> addresses = ipv4Setting->addresses();

        //
        int i = 0;
        auto dhcp = SettingItem::create(m_Items.length() + i++, SettingItem::SectionIpv4, SettingItem::DelegateSwitch, "dhcp", tr("DHCP"), ipv4Method == NetworkManager::Ipv4Setting::Automatic, false, true);
        tmpList.append(dhcp);


        if(ipv4Method == NetworkManager::Ipv4Setting::Manual) {
            qDebug() << "NetworkManager::Ipv4Setting::Manual";
            if(addresses.length() > 0) {
                auto ip = SettingItem::create(m_Items.length() + i++, SettingItem::SectionIpv4, SettingItem::DelegateIp4, "ip", tr("IP Address"), addresses.first().ip().toString(), false, true);
                tmpList.append(ip);

                auto netmask = SettingItem::create(m_Items.length() + i++, SettingItem::SectionIpv4, SettingItem::DelegateIp4, "netmask", tr("Netmask"), addresses.first().netmask().toString(), false, true);
                tmpList.append(netmask);
                qDebug() << "Adding IP & Netmask";
            }
        }
        else if(ipv4Method == NetworkManager::Ipv4Setting::Automatic) {
            qDebug() << "NetworkManager::Ipv4Setting::Automatic";
            addresses = m_Device->ipV4Config().addresses();
            if(addresses.length() > 0) {
                auto ip = SettingItem::create(m_Items.length() + i++, SettingItem::SectionIpv4, SettingItem::DelegateIp4, "ip", tr("IP Address"), addresses.first().ip().toString(), false, false);
                tmpList.append(ip);

                auto netmask = SettingItem::create(m_Items.length() + i++, SettingItem::SectionIpv4, SettingItem::DelegateIp4, "netmask", tr("Netmask"), addresses.first().netmask().toString(), false, false);
                tmpList.append(netmask);
                qDebug() << "Adding IP & Netmask (automatic)" ;
            }
        }
        else {
            qDebug() << "Unknown method: " << ipv4Method;
        }
        // Actually put the items into the list
        beginInsertRows(QModelIndex(), m_Items.length(), m_Items.length() + i - 1); // <<<<< !!!! UPDATE WHEN THE COUNT CHANGES !!!!!
        Q_FOREACH(auto item, tmpList) {
            m_Items.append(item);
        }
        endInsertRows();
        //emit dataChanged(createIndex(0,0), createIndex( m_Items.length() -1, 0));
    }
}

bool NetworkSettingModel::applyIpv4()
{
    if(m_Connection) {
        NetworkManager::ConnectionSettings::Ptr settings = m_Connection->settings();
        NetworkManager::Ipv4Setting::Ptr ipv4Setting = settings->setting(NetworkManager::Setting::Ipv4).dynamicCast<NetworkManager::Ipv4Setting>();

        NetworkManager::IpAddress address;
        QHostAddress dnsAddress;

        Q_FOREACH(SettingItem::Ptr item, m_Items) {
            if(item->section == SettingItem::SectionIpv4) {
                if(item->identifier == "method") {
                    ipv4Setting->setMethod(NetworkManager::Ipv4Setting::ConfigMethod(item->data.toInt()));

                }
                if(item->identifier == "ip") {
                    QHostAddress addr = QHostAddress(item->data.toString());
                    address.setIp(addr);
                }
                else if(item->identifier == "netmask") {
                    QHostAddress addr = QHostAddress(item->data.toString());
                    address.setNetmask(addr);
                }
                else if(item->identifier == "gateway") {
                    QHostAddress addr = QHostAddress(item->data.toString());
                    address.setGateway(addr);
                }
                else if(item->identifier == "dns") {
                    QHostAddress addr = QHostAddress(item->data.toString());
                    dnsAddress = addr;
                }
            }
        }
        ipv4Setting->setAddresses({address,});

        QList<QHostAddress> dnss = ipv4Setting->dns();
        dnss.prepend(dnsAddress);
        ipv4Setting->setDns(dnss);
    }
    return true;
}

QHash<int, QByteArray> NetworkSettingModel::roleNames() const
{
    QHash<int, QByteArray> ret;
    ret[RoleSection ] = "section";
    ret[RoleDelegateType] = "delegateType";
    ret[RoleIdentifier] = "identifier";
    ret[RoleLabel] = "label";
    ret[RoleData] = "data";
    ret[RoleReadOnly] = "readOnly";
    ret[RoleEditable] = "editable";
    return ret;

}

