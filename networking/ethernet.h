/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ETHERNET_H
#define ETHERNET_H
#include <QAbstractListModel>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QObject>
#include <QJSValue>

#include <arpa/inet.h>

#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Device>
#include <NetworkManagerQt/WirelessDevice>
#include <NetworkManagerQt/AccessPoint>
#include <NetworkManagerQt/Connection>
#include <NetworkManagerQt/ConnectionSettings>
#include <NetworkManagerQt/WirelessSetting>
#include <NetworkManagerQt/WirelessSecuritySetting>
#include <NetworkManagerQt/Ipv4Setting>
#include <NetworkManagerQt/WiredDevice>
//#include <QDBusMetaType>

class Ethernet : public QObject
{
    Q_OBJECT
public:
    explicit Ethernet(QObject *parent = nullptr);

    enum ActiveConnectionState {
        Unknown = 0, /**< The active connection is in an unknown state */
        Activating, /**< The connection is activating */
        Activated, /**< The connection is activated */
        Deactivating, /**< The connection is being torn down and cleaned up */
        Deactivated /**< The connection is no longer active */
    };
    Q_ENUM(ActiveConnectionState)

    enum ActiveConnectionReason {
        UknownReason = 0,   /**< The reason for the active connection state change is unknown */
        None,               /**< No reason was given for the active connection state change */
        UserDisconnected,   /**< The active connection changed state because the user disconnected it */
        DeviceDisconnected, /**< The active connection changed state because the device it was using was disconnected */
        ServiceStopped,     /**< The service providing the VPN connection was stopped */
        IpConfigInvalid,    /**< The IP config of the active connection was invalid */
        ConnectTimeout,     /**< The connection attempt to the VPN service timed out */
        ServiceStartTimeout,/**< A timeout occurred while starting the service providing the VPN connection */
        ServiceStartFailed, /**< Starting the service providing the VPN connection failed */
        NoSecrets,          /**< Necessary secrets for the connection were not provided */
        LoginFailed,        /**< Authentication to the server failed */
        ConnectionRemoved,  /**< The connection was deleted from settings */
        DependencyFailed,   /**< Master connection of this connection failed to activate */
        DeviceRealizeFailed,/**< Could not create the software device link */
        DeviceRemoved       /**< The device this connection depended on disappeared */
    };
    Q_ENUM(ActiveConnectionReason)

    // properties to set/get ip4 network parameter. TODO: move into its own data model
    Q_PROPERTY(bool scratchpadDhcp MEMBER m_ScratchpadDhcp NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadIp MEMBER m_ScratchpadIp NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadMask MEMBER m_ScratchpadMask NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadGateway MEMBER m_ScratchpadGateway NOTIFY scratchPadChanged)
    Q_PROPERTY(QString scratchpadDns MEMBER m_ScratchpadDns NOTIFY dhcpChanged)
    Q_INVOKABLE void refreshScratchpad();
    Q_INVOKABLE void applyScratchpad(QJSValue callbackSuccess, QJSValue callbackFail);

    Q_PROPERTY(ActiveConnectionState activeConnectionState READ activeConnectionState NOTIFY activeConnectionStateChanged)
    Q_PROPERTY(QString hwAddress READ hwAddress NOTIFY hwAddressChanged)
    Q_PROPERTY(bool carrier READ carrier NOTIFY carrierChanged)
    Q_PROPERTY(QString activeConnectionName READ activeConnectionName NOTIFY activeConnectionNameChanged)
    Q_PROPERTY(QString ipAddress READ ipAddress NOTIFY ipAddressChanged)
    Q_PROPERTY(QString defaultStaticIpAddress READ defaultStaticIpAddress CONSTANT);
    Q_PROPERTY(QString defaultStaticNetmask READ defaultStaticNetmask CONSTANT);


    QVariantMap getActiveConnectionInfo() const;
    QString hwAddress() const;
    bool carrier() const;
    QString activeConnectionName() const;
    QString ipAddress() const;
    Ethernet::ActiveConnectionState activeConnectionState() const;
    int activeConnectionReason() const;
    const QString &defaultStaticIpAddress() const;
    const QString &defaultStaticNetmask() const;

signals:
    void scratchPadChanged();
    void dhcpChanged(bool val);
    void hwAddressChanged();
    void ipAddressChanged(QString ipAddress);
    void carrierChanged();
    void activeConnectionNameChanged();
    void activeConnectionStateChanged(Ethernet::ActiveConnectionState);
    void activeConnectionStateChangedReason(Ethernet::ActiveConnectionState, Ethernet::ActiveConnectionReason);
public slots:
    void    slotActiveConnectionChanged();
    void    slotActiveConnectionStateChanged(NetworkManager::ActiveConnection::State state);
    void    slotActiveConnectionStateChangedReason(NetworkManager::ActiveConnection::State state, NetworkManager::ActiveConnection::Reason reason);
    void slotHwAddressChanged();
    void slotCarrierChanged();


protected:
    QString m_ScratchpadIp;
    QString m_ScratchpadMask;
    QString m_ScratchpadGateway;
    QString m_ScratchpadDns;
    bool m_ScratchpadDhcp;

    NetworkManager::WiredDevice::Ptr m_Device;
    NetworkManager::ActiveConnection::Ptr m_ActiveConnection;
    NetworkManager::ActiveConnection::Reason m_ActiveConnectionStateReason;

    QString m_defaultStaticIpAddress{QStringLiteral("192.168.0.10")};
    QString m_defaultStaticNetmask{QStringLiteral("255.255.255.0")};
};


#endif // ETHERNET_H
