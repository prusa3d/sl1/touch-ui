#include <QMetaObject>
#include <QMetaProperty>
#include <QHash>
#include "prusaconnect.h"
#include "errors_sl1.h"
#include "dbusutils.h"

PrusaConnect::PrusaConnect(QJSEngine & engine, QObject *parent) :
    BaseDBusObject(engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    m_connectProxy("cz.prusa3d.connect0", "/cz/prusa3d/connect0", QDBusConnection::systemBus(), this),
    m_engine(engine),
    m_propertiesLoaded(false)
{
    m_connectProxy.setTimeout(210000);
    m_properties.setTimeout(2000);
    registerPrinterTypes();

    reload_properties();
}


bool PrusaConnect::is_registered() const{
    return m_is_registered;
}

void PrusaConnect::setIs_registered(bool new_val){
    if(new_val != m_is_registered) {
        m_is_registered = new_val;
        emit is_registeredChanged(new_val);
    }
}

QVariantMap PrusaConnect::last_exception() const {
    return m_last_exception;
}

void PrusaConnect::set_server(QString hostname, int port, bool tls, QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_connectProxy.set_server(hostname, port, tls), callback_ok, callback_fail);
}

void PrusaConnect::setLast_exception(QVariantMap new_val)  {
    if(new_val != m_last_exception) {
        m_last_exception = new_val;
        emit last_exceptionChanged(new_val);
    }
}

void PrusaConnect::register_printer(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_connectProxy.register_printer(), callback_ok, callback_fail);
}

void PrusaConnect::unregister_printer(QJSValue callback_ok, QJSValue callback_fail)
{
    handleCallbackWithError(m_connectProxy.unregister_printer(), callback_ok, callback_fail);
}

void PrusaConnect::reset_connect(QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_connectProxy.reset_connect(), callback_ok, callback_fail);
}

void PrusaConnect::confirm_registration(const QString & temp_token, QJSValue callback_ok, QJSValue callback_fail) {
    handleCallbackWithError(m_connectProxy.confirm_registration(temp_token), callback_ok, callback_fail);
}

bool PrusaConnect::set(const QString & name, const QVariant & value)
{
    qDebug() << "Setting" << name << "->" << value;
    if(name == QStringLiteral("last_exception")) {
        setLast_exception(DBusUtils::convert<QVariantMap>(value));
    }
    else if(name == QStringLiteral("settings")) {
        setSettings(DBusUtils::convert<QVariantMap>(value));
    } else {
        bool r = BaseDBusObject::set(name, value);
        if( ! r) qDebug() << "An attempt to set unknown property "<< name;
    }
    return true;

}

QVariantMap PrusaConnect::settings() const
{
    return m_settings;
}

void PrusaConnect::setSettings(const QVariantMap &newSettings)
{
    if (m_settings == newSettings)
        return;
    m_settings = newSettings;
    emit settingsChanged();
}

QString PrusaConnect::registration_status() const
{
    return m_registration_status;
}

void PrusaConnect::setRegistration_status(const QString &newRegistration_status)
{
    if (m_registration_status == newRegistration_status)
        return;
    m_registration_status = newRegistration_status;
    emit registration_statusChanged();
}

bool PrusaConnect::is_ok() const
{
    return m_is_ok;
}

void PrusaConnect::setIs_ok(bool newIs_ok)
{
    if (m_is_ok == newIs_ok)
        return;
    m_is_ok = newIs_ok;
    emit is_okChanged();
}

QString PrusaConnect::printer_registration_address() const
{
    return m_printer_registration_address;
}

void PrusaConnect::setPrinter_registration_address(const QString &newPrinter_registration_address)
{
    if (m_printer_registration_address == newPrinter_registration_address)
        return;
    m_printer_registration_address = newPrinter_registration_address;
    emit printer_registration_addressChanged();
}
