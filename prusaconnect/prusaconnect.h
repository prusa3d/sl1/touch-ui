#ifndef PRUSA_CONNECT_H
#define PRUSA_CONNECT_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include <QJSValue>
#include <QJSEngine>
#include "basedbusobject.h"
#include "printer_types.h"
#include "connect_proxy.h"
#include "properties_proxy.h"
#include "errors_sl1.h"

/*
 * Proxy class for interface cz.prusa3d.connect0
 */
class PrusaConnect : public BaseDBusObject
{
    Q_OBJECT
    ConnectProxy m_connectProxy;
    QJSEngine &m_engine;

    bool m_propertiesLoaded;

    QVariantMap m_last_exception;
    void setLast_exception(QVariantMap new_val);

    bool m_is_registered;
    void setIs_registered(bool new_val);

    virtual bool set(const QString & name, const QVariant & value) override;
public:
    inline static const QString service{"cz.prusa3d.connect0"};
    inline static const QString interface{"cz.prusa3d.connect0"};
    inline static const QString path{"/cz/prusa3d/connect0"};


    explicit PrusaConnect(QJSEngine &engine, QObject *parent = nullptr);

    bool is_registered() const;

    QVariantMap last_exception() const;

    Q_INVOKABLE void set_server(QString hostname, int port, bool tls, QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void register_printer(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void unregister_printer(QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void confirm_registration(const QString &temp_token, QJSValue callback_ok, QJSValue callback_fail);
    Q_INVOKABLE void reset_connect(QJSValue callback_ok, QJSValue callback_fail);

    Q_PROPERTY(QVariantMap last_exception READ last_exception NOTIFY last_exceptionChanged FINAL)
    /** @property settings
     * settings = {
            "hostname": self._connect.hostname,
            "port": self._connect.port,
            "tls": self._connect.tls,
        }
    **/
    Q_PROPERTY(QVariantMap settings READ settings WRITE setSettings NOTIFY settingsChanged FINAL)
    Q_PROPERTY(QString registration_status READ registration_status WRITE setRegistration_status NOTIFY registration_statusChanged FINAL)
    Q_PROPERTY(bool is_ok READ is_ok WRITE setIs_ok NOTIFY is_okChanged FINAL)
    Q_PROPERTY(bool is_registered READ is_registered WRITE setIs_registered NOTIFY is_registeredChanged FINAL)
    Q_PROPERTY(QString printer_registration_address READ printer_registration_address WRITE setPrinter_registration_address NOTIFY printer_registration_addressChanged FINAL)

    QVariantMap settings() const;
    void setSettings(const QVariantMap &newSettings);

    QString registration_status() const;
    void setRegistration_status(const QString &newRegistration_status);

    bool is_ok() const;
    void setIs_ok(bool newIs_ok);

    QString printer_registration_address() const;
    void setPrinter_registration_address(const QString &newPrinter_registration_address);

signals:

    void propertiesChanged(const QMap<QString, QVariant> &changed_properties);
    void last_exceptionChanged(QVariantMap last_exception);
    void is_registeredChanged(bool is_registered);
    void settingsChanged();

    void registration_statusChanged();

    void is_okChanged();

    void printer_registration_addressChanged();

public slots:


private:
    QVariantMap m_settings;
    QString m_registration_status;
    bool m_is_ok;
    QString m_printer_registration_address;
};

#endif // PRUSA_CONNECT_H
