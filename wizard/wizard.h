/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WIZARD_H
#define WIZARD_H

#include <QObject>
#include <QSortFilterProxyModel>

#include "wizard_proxy.h"
#include "types.h"
#include "wizardchecksmodel.h"
#include "basedbusobject.h"

class Wizard : public BaseDBusObject {
    Q_OBJECT

public:
    enum WizardId {
        UNKNOWN = 0,
        SELF_TEST = 1,
        CALIBRATION = 2,
        DISPLAY = 3,
        COMPLETE_UNBOXING = 4,
        KIT_UNBOXING = 5,
        FACTORY_RESET = 6,
        PACKING = 7,
        UV_CALIBRATION = 8,
        SL1S_UPGRADE = 9,
        SL1_DOWNGRADE = 10,
        NEW_EXPO_PANEL = 11,
        TANK_SURFACE_CLEANER = 12
    };
    Q_ENUM(WizardId)

    enum WizardState {
        INIT = 0,
        RUNNING = 1,
        DONE = 2,
        FAILED = 3,
        CANCELED = 4,
        STOPPED = 5,

        // Group enter states - user printer reconfiguration
        PREPARE_WIZARD_PART_1 = 1000,
        PREPARE_WIZARD_PART_2 = 1001,
        PREPARE_WIZARD_PART_3 = 1002,
        PREPARE_CALIBRATION_PLATFORM_INSERT = 1010,
        PREPARE_DISPLAYTEST = 1011,
        PREPARE_CALIBRATION_TANK_PLACEMENT = 1012, // deprecated
        PREPARE_CALIBRATION_TILT_ALIGN = 1013,
        PREPARE_CALIBRATION_PLATFORM_ALIGN = 1014, // deprecated
        PREPARE_CALIBRATION_FINISH = 1015,
        PREPARE_CALIBRATION_INSERT_PLATFORM_TANK = 1016,


        // User action required states
        CLOSE_COVER = 2000,
        TEST_DISPLAY = 2001,
        TEST_AUDIO = 2002,
        LEVEL_TILT = 2003,
        SHOW_RESULTS = 2004,

        OPEN_COVER = 2100,
        REMOVE_SAFETY_STICKER = 2101,
        REMOVE_SIDE_FOAM = 2102,
        REMOVE_TANK_FOAM = 2103,  // Remove resin tank
        REMOVE_DISPLAY_FOIL = 2104,  // Peel off exposure display foil
        INSERT_FOAM = 2105,  // At factory reset, insert packing foams

        UV_CALIBRATION_PREPARE = 2200,
        UV_CALIBRATION_PLACE_UV_METER = 2201,
        UV_CALIBRATION_REMOVE_UV_METER = 2202,
        UV_CALIBRATION_APPLY_RESULTS = 2203,

        SL1S_CONFIRM_UPGRADE = 2300,

        PREPARE_NEW_EXPO_PANEL = 2400,


        TANK_SURFACE_CLEANER_INIT = 2500, ///<Intro in the beginning of the tank cleaner wizard
        TANK_SURFACE_CLEANER_INSERT_CLEANING_ADAPTOR = 2501, ///< user confirms that cleaning adaptor has been inserted
        TANK_SURFACE_CLEANER_REMOVE_CLEANING_ADAPTOR = 2502, ///< user confirms that cleaning adaptor has been removed
    };
    Q_ENUM(WizardState)

    inline static const QString service{"cz.prusa3d.sl1.wizard0"};
    inline static const QString interface{"cz.prusa3d.sl1.wizard0"};
    inline static const QString path{"/cz/prusa3d/sl1/wizard0"};

    explicit Wizard(QJSEngine & engine, QObject *parent = nullptr);

    Q_PROPERTY(QAbstractItemModel* checksModel READ getChecksModel CONSTANT)

    Q_PROPERTY(int identifier READ identifier NOTIFY identifierChanged)
    Q_PROPERTY(int state READ state NOTIFY stateChanged)
    Q_PROPERTY(CheckStatesType checkStates READ checkStates NOTIFY checkStatesChanged)
    Q_PROPERTY(CheckDataType checkData READ checkData NOTIFY checkDataChanged)
    Q_PROPERTY(bool cancelable READ cancelable NOTIFY cancelableChanged)
    Q_PROPERTY(QVariantMap checkException READ checkException NOTIFY checkExceptionChanged)
    Q_PROPERTY(int totalChecks READ totalChecks NOTIFY totalChecksChanged)
    Q_PROPERTY(int finishedChecks READ finishedChecks NOTIFY finishedChecksChanged)
    Q_PROPERTY(int runningChecks READ runningChecks NOTIFY runningChecksChanged)
    Q_PROPERTY(QVariantMap data READ data NOTIFY dataChanged)
    Q_PROPERTY(QList<QVariantMap>  check_warnings READ check_warnings NOTIFY check_warningsChanged)

    Q_INVOKABLE void cancel(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void retry(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void abort(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void prepareWizardPart1Done(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void prepareWizardPart2Done(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void prepareWizardPart3Done(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void prepareDisplaytestDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void prepareCalibrationPlatformAlignDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void prepareCalibrationTiltAlignDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void prepareCalibrationFinishDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void prepareCalibrationPlatformTankDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void showResultsDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void tiltMove(int speed, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void tiltCalibrationDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void reportDisplay(bool ok, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void reportAudio(bool ok, QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void safetyStickerRemoved(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void sideFoamRemoved(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void tankFoamRemoved(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void displayFoilRemoved(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void foamInserted(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void uvCalibrationPrepared(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void uvMeterPlaced(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void uvCalibrationApplyResults(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void uvCalibrationDiscardResults(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void sl1sRejectUpgrade(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());
    Q_INVOKABLE void sl1sConfirmUpgrade(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void newExpoPanelDone(QJSValue callback_ok = QJSValue(), QJSValue callback_fail = QJSValue());

    Q_INVOKABLE void tank_surface_cleaner_init_done();
    Q_INVOKABLE void insert_cleaning_adaptor_done();
    Q_INVOKABLE void remove_cleaning_adaptor_done();

    const QList<QVariantMap> &check_warnings() const;
    void resetWizardChecksModel();

signals:
    void identifierChanged(const int identifier);
    void stateChanged(const int state);
    void checkStatesChanged(const CheckStatesType checkStates);
    void checkDataChanged(const CheckDataType checkData);
    void cancelableChanged(const bool cancelable);
    void checkExceptionChanged(const QVariantMap exception);
    void totalChecksChanged(const int totalChecks);
    void finishedChecksChanged(const int finishedChecks);
    void runningChecksChanged(const int runningChecks);
    void checksChanged(const CheckStatesType checkStates, CheckDataType checkData);
    void dataChanged(const QVariantMap data);

    void check_warningsChanged();

private:
    WizardProxy wizardProxy;
    QAbstractItemModel* getChecksModel();
    WizardChecksModel checksModel;
    QSortFilterProxyModel sortedChecksModel;

    int identifier();
    int state();
    CheckStatesType checkStates() const;
    CheckDataType checkData() const;
    bool cancelable();
    QVariantMap checkException();
    int totalChecks() const;
    int finishedChecks() const;
    int runningChecks() const;
    int countTotalChecks(const CheckStatesType &checkStates) const;
    int countFinishedChecks(const CheckStatesType &checkStates) const;
    int countRunningChecks(const CheckStatesType &checkStates) const;
    QVariantMap data() const;

    int m_state{WizardState::INIT};
    CheckStatesType m_checkStates;
    CheckDataType m_checkData;

    // BaseDBusObject interface
    QList<QVariantMap> m_check_warnings;

protected:
    virtual bool set(const QString &name, const QVariant &value) override;
};

#endif // WIZARD_H
