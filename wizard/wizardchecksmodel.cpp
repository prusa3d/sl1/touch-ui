/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMetaObject>
#include <QMetaEnum>
#include "wizardchecksmodel.h"
#include <QDebug>

WizardChecksModel::WizardChecksModel(QObject *parent): QAbstractListModel(parent) {}

void WizardChecksModel::change(const CheckStatesType &states, const CheckDataType &data) {
    // Remove elements not present in current checks
    for(int i = listData.size() - 1; i >= 0; i--) {
        if(states.find(listData[i].checkId) == states.end()) {
            beginRemoveRows(QModelIndex(), i, i);
            listData.removeAt(i);
            endRemoveRows();
        }
    }

    // Update states
    QMap<int, int> remainingStates(states);
    for(auto it = listData.begin(); it < listData.end(); it++) {
        if(remainingStates[it->checkId] != it->checkState) {
            it->hidden = false;
            it->checkState = CheckState(remainingStates[it->checkId]);
            auto index = createIndex(it - listData.begin(), 0);
            emit dataChanged(index, index, QVector<int>({RoleCheckState, RoleHidden}));
        }
        remainingStates.remove(it->checkId);
    }

    // Update data
    for(auto it = listData.begin(); it < listData.end(); it++) {
        if(data[it->checkId] != it->checkData) {
            it->checkData = QVariantMap(data[it->checkId]);
            auto index = createIndex(it - listData.begin(), 0);
            emit dataChanged(index, index, QVector<int>({RoleCheckData}));
        }
    }

    // Add new elements
    beginInsertRows(QModelIndex(), rowCount(), rowCount() + remainingStates.size() - 1);
    for(const int &key: remainingStates.keys()) {
        listData.append(CheckRecord(Check(key), CheckState(remainingStates[key]), QVariantMap(data[key])));
    }
    endInsertRows();
}


QHash<int, QByteArray> WizardChecksModel::roleNames() const {
    return QHash<int, QByteArray>({
        {RoleCheckId, "checkId"},
        {RoleCheckState, "checkState"},
        {RoleCheckData, "checkData"},
        {RoleHidden, "hidden"},
    });
}

int WizardChecksModel::rowCount(const QModelIndex &parent) const {
    if (parent.isValid()) {
        return 0;
    }

    return listData.size();
}

QVariant WizardChecksModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }

    switch(role) {
    case RoleCheckId:
        return listData[index.row()].checkId;
    case RoleCheckState:
        return listData[index.row()].checkState;
    case RoleCheckData:
        return listData[index.row()].checkData;
    case RoleHidden:
        return listData[index.row()].hidden;
    case Qt::DisplayRole:
    {
            int id = listData[index.row()].checkId;
            const QMetaObject metaObject = WizardChecksModel::staticMetaObject;
            int enumIndex = metaObject.indexOfEnumerator("Check");
            if(enumIndex == -1) {
                /* The enum does not contain the specified enum */
                return "";
            }
            QMetaEnum en = metaObject.enumerator(enumIndex);
            return QString(en.valueToKey(id));
    }
    default:
        qWarning() << "WizardCheckModel asked for an unknown role: " << role;
        return QVariant();
    }
}

bool WizardChecksModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (!index.isValid()) {
        return false;
    }

    if(role != RoleHidden) {
        return false;
    }

    listData[index.row()].hidden = value.toBool();
    emit dataChanged(index, index, QVector<int>({RoleHidden}));
    return true;
}

Qt::ItemFlags WizardChecksModel::flags(const QModelIndex& index) const {
    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}

void WizardChecksModel::reset()
{
    if(listData.empty()) return;
    beginResetModel();
    listData.clear();
    endResetModel();
}
