image: martin357/test:testing

stages:
  - build
  - test

show_environment:
  stage: build
  when: manual
  script:
    - env | grep CI

build:
  stage: build
  before_script:
    - git submodule update --init --recursive
  script:
    - pushd build
    - cmake ..
    - make -j $(nproc)
    - popd
  artifacts:
    untracked: true
    expire_in: 3 days

create_translation_file:
  stage: test
  when: manual
  script:
    - pushd translations
    - bash ./refresh_translations.sh
    - popd
  artifacts:
    untracked: false
    expire_in: 3 days
    paths:
      - translations/en_US_sorted_for_translators.ts
  dependencies:
    - build

check_translations:
  stage: test
  allow_failure: true
  script:
    - pushd translations
    - bash ./refresh_translations.sh
    - python3 ./check_translations.py *.ts --verbose
    - popd
  dependencies:
    - build

clazy:
  stage: test
  allow_failure: true
  variables:
    # Max number of Clazy warnings, above which this job will fail. Should be lowered as the reasons for those warnings are removed.
    # 
    MAX_CLAZY_WARNINGS: "1411"
  before_script:
    - git submodule update --init --recursive
  script:
    - pushd build
    - cmake ..
    - make -j $(nproc)
    - clazy-standalone --checks=level1,assert-with-side-effects,container-inside-loop -extra-arg=-Wno-unknown-warning-option --ignore-included-files -p . $(find .. -name "*.cpp" -not -path "*/3rdparty/*" -and -not -path "*/build/*") 2>&1 | tee clazy.txt
    - cat clazy.txt | grep "warning:" | grep -v /home/martin/src/SLAGUI/build/ | grep -v 3rdparty | wc -l > warning_count.txt
    - cat warning_count.txt
    - if (( $(cat warning_count.txt) <= "${MAX_CLAZY_WARNINGS}" )); then exit 0; else exit 1; fi
    - popd
  dependencies:
    - build

dbus_consistency:
  stage: test
  allow_failure: true
  variables:
    SLAFW_BRANCH: "master"
  before_script:
    - mkdir -p packages
    - pushd packages

    # Install prusaerrors
    - git clone https://github.com/prusa3d/Prusa-Error-Codes.git prusaerrors
    - pushd prusaerrors
    - pip3 install .
    - popd

    # Install filemanager
    - git clone https://gitlab.com/prusa3d/sl1/filemanager.git
    - pushd filemanager
    - cp ./systemd/cz.prusa3d.sl1.filemanager.conf /usr/share/dbus-1/system.d/
    - pip3 install watchdog
    - pip3 install .
    - popd

    # Install the current backend slafw
    - git clone https://gitlab.com/prusa3d/sl1/sla-fw.git
    - pushd sla-fw
    - git checkout "${SLAFW_BRANCH}"
    - git submodule update --init --recursive
    - python3 setup.py build
    - sh build_sim.sh
    - pip3 install .
    - cp -r slafw/tests/samples/* /usr/local/lib/python3.8/dist-packages/slafw/tests/samples/
    - cp dbus/cz.prusa3d.sl1.conf /usr/share/dbus-1/system.d/cz.prusa3d.sl1.conf
    - popd
    - popd
  script:
    # Set the printer model to SL1
    - mkdir -p $CI_PROJECT_DIR/model
    - touch $CI_PROJECT_DIR/model/sl1

    # Start the dbus daemon with new machine-id
    - dbus-uuidgen > /var/lib/dbus/machine-id
    - mkdir -p /var/run/dbus
    - dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address

    # Start the minimal set of services needed for backend to run
    - /lib/systemd/systemd-hostnamed &
    - /lib/systemd/systemd-localed &
    - mkdir -p /run/rauc
    - NetworkManager
    - /usr/bin/python3 -OO -m filemanager &
    - echo $PWD
    - ls packages
    
    - pushd packages/sla-fw/slafw
    - PATH="${PWD}/..:${PWD}:${PATH}" python3 virtual.py &
    - sleep 5
    - popd
    - python3 ./tests/dbus_interfaces/assert_interfaces_equal.py
    - kill $(pidof python3 virtual)
