/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "notificationmodel.h"
#include "fileupload_notification.h"
#include <QList>

NotificationModel::NotificationModel(QObject *parent)
    : QAbstractListModel(parent),
      m_notificationStore(new NotificationStore(this))
{
    connect(m_notificationStore, &NotificationStore::dataChanged, this, [=](int idx, QVector<int> roles){
        QModelIndex index = createIndex(idx, 0);
        emit this->dataChanged(index, index, roles);
    });
    connect(m_notificationStore, &NotificationStore::preAddNotification, this, [=](int idx) {
        this->beginInsertRows(QModelIndex(), idx, idx);
    });
    connect(m_notificationStore, &NotificationStore::preRemoveNotification, this, [=](int idx) {
        this->beginRemoveRows(QModelIndex(), idx, idx);
    });
    connect(m_notificationStore, &NotificationStore::postAddNotification, this, [=]() {
        this->endInsertRows();
        emit newNotification();
    });
    connect(m_notificationStore, &NotificationStore::postRemoveNotification, this, [=]() {
        this->endRemoveRows();
    });
    connect(m_notificationStore, &NotificationStore::topChanged, this, [=](Notification * notification){
        emit topChanged(notification);
    });
    connect(m_notificationStore, &NotificationStore::countChanged, this, [=](int _count){
        emit countChanged(_count);
    });

}

QHash<int, QByteArray> NotificationModel::roleNames() const {
    return Notification::roleNames();
}

int NotificationModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return notificationStore()->count();
}

QVariant NotificationModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if(index.row() < 0 || index.row() >= notificationStore()->count() || index.column() != 0) {
        return QVariant();
    }
    if( ! roleNames().contains(role)) {
        qDebug() << "Invalid role requested:" << role;
        return QVariant();
    }

    int notificationIdx = index.row();

    const Notification & notification = notificationStore()->at(notificationIdx);
    if(role == Notification::RoleObject) {
        return QVariant::fromValue((QObject*)&notification);
    }


    QVariant result = notification.property(roleNames()[role]);
    return result;
}

bool NotificationModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    if(row + count > m_notificationStore->count()) return false;
    for(int i = row; i < row + count; ++i) {
        notificationStore()->removeNotification(row);
    }

    return true;
}


void NotificationModel::test()
{
    NotificationModel m;
    struct test_sig_t {
        QString sig;
        int start;
        int end;
    };

    QList<test_sig_t> results;
    //QModelIndex index = createIndex(idx, 0);
    //emit this->dataChanged(index, index, roles);
    connect(&m, &NotificationModel::dataChanged, this, [=, &results](QModelIndex indexFirst, QModelIndex indexLast, QVector<int> roles){
        qDebug() << "dataChanged" << "indexFirst: " << indexFirst << "indexLast:" << indexLast << "roles:" << roles;
        test_sig_t r;
        r.sig = "dataChanged";
        r.start = indexFirst.row();
        r.end = indexLast.row();
        results.push_back(r);
    });
    connect(&m, &NotificationModel::rowsAboutToBeInserted, this, [=, &results](const QModelIndex &parent, int start, int end) {
        Q_UNUSED(parent)
        qDebug() << "rowsAboutToBeInserted" << "start:" << start << "end:" << end;
        test_sig_t r;
        r.sig = "rowsAboutToBeInserted";
        r.start = start;
        r.end = end;
        results.push_back(r);
    });
    connect(&m, &NotificationModel::rowsAboutToBeRemoved, this, [=, &results](const QModelIndex &parent, int start, int end) {
        Q_UNUSED(parent)
        qDebug() << "rowsAboutToBeRemoved" << "start:" << start << "end:" << end;
        test_sig_t r;
        r.sig = "rowsAboutToBeRemoved";
        r.start = start;
        r.end = end;
        results.push_back(r);
    });
    connect(&m, &NotificationModel::rowsRemoved, this, [=, &results](const QModelIndex &parent, int start, int end) {
        Q_UNUSED(parent)
        qDebug() << "rowsAboutToBeRemoved" << "start:" << start << "end:" << end;
        test_sig_t r;
        r.sig = "rowsAboutToBeRemoved";
        r.start = start;
        r.end = end;
        results.push_back(r);
    });

    auto n = new FileUploadNotification();
    n->setText("Test text");
    n->setProgress(0.5);
    Q_ASSERT(rowCount() == 0);
    int handle = m.notificationStore()->addNotification(n);
    Q_ASSERT(handle == 10001);
    Q_ASSERT(m.notificationStore()->handleByIndex(0) == 10001);
    Q_ASSERT(m.notificationStore()->indexByHandle(handle) == 0);
    Q_ASSERT(m.notificationStore()->changeNotification(0, "severity", 9));
    Q_ASSERT(m.rowCount() == 1);
    Q_ASSERT(m.notificationStore()->removeNotification(0));
    Q_ASSERT(m.rowCount() == 0);
    Q_ASSERT(results.at(0).sig == "rowsAboutToBeInserted");
    Q_ASSERT(results.at(1).sig == "dataChanged");
    Q_ASSERT(results.at(2).sig == "rowsAboutToBeRemoved");
}

NotificationStore *NotificationModel::notificationStore() const { return m_notificationStore; }

Notification * NotificationModel::top()
{
    return  m_notificationStore->top();
}

Notification *NotificationModel::get(int i)
{
    if(i >= 0 && i < count()) {
        return m_notificationStore->data()[i];
    }
    else return nullptr;
}

int NotificationModel::count() const
{
    return m_notificationStore->count();
}

void NotificationModel::removeAll(Notification::NotificationType type) {
    for(int i = 0; i < count();) {
        if(type == Notification::General || m_notificationStore->at(i).type() == type){
            removeRow(i);
        }
        else {
            ++i;
        }
    }
}

