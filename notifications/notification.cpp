/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QMetaProperty>
#include "notification.h"

Notification::Notification(NotificationType type, QObject *parent) :
    QObject(parent),
    m_type(type),
    m_handle(-100),
    m_severity(10),
    m_timestamp(0),
    m_text(QStringLiteral("")),
    m_errorCode(ErrorsSL1::Errors::NONE)
{

}

Notification::NotificationType Notification::type() const { return m_type; }

int Notification::handle() const { return m_handle; }

int Notification::severity() const { return m_severity; }

uint Notification::timestamp() const { return m_timestamp; }

QString Notification::text() const { return m_text; }



ErrorsSL1::Errors Notification::errorCode() const
{
    return m_errorCode;
}

void Notification::setType(Notification::NotificationType type) {
    if(type != m_type) {
        m_type = type;
        emit typeChanged(type);
    }
}

void Notification::setHandle(int handle) {
    if(m_handle != handle) {
        m_handle = handle;
        emit handleChanged(handle);
    }
}

void Notification::setSeverity(int severity) {
    if(m_severity != severity) {
        m_severity = severity;
        emit severityChanged(severity);
    }
}

void Notification::setTimestamp(uint timestamp) {
    if(m_timestamp != timestamp) {
        m_timestamp = timestamp;
        emit timestampChanged(timestamp);
    }
}

void Notification::setText(QString text) {
    if(m_text != text) {
        m_text = text;
        emit textChanged(text);
    }
}

void Notification::setErrorCode(ErrorsSL1::Errors errorCode)
{
    if (m_errorCode == errorCode)
        return;

    m_errorCode = errorCode;
    emit errorCodeChanged(m_errorCode);
}
