cmake_minimum_required(VERSION 3.14)
project(notifications LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core DBus Quick REQUIRED)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_BINARY_DIR}")


# Does not generate the adaptor correctly(with QMetaObject::invokeMethod)
#qt5_add_dbus_adaptor(DBUS_SRCS
#    ${CMAKE_CURRENT_SOURCE_DIR}/cz.prusa3d.sl1.Notify1.xml
#    QObject#notificationdbus.h
#    QObject#NotificationDBus
#    notificationdbus_adaptor
#    NotificationDBusAdaptor)

set_property(SOURCE ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor.h PROPERTY GENERATED ON)
set_property(SOURCE ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor.cpp PROPERTY GENERATED ON)

add_custom_command(
    OUTPUT
    ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor.cpp
    ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor.h
    COMMAND qdbusxml2cpp -V -c NotificationDBusAdaptor -a  ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor ${CMAKE_CURRENT_SOURCE_DIR}/cz.prusa3d.sl1.Notify1.xml
    )


# Add a library with the above sources
add_library(
    ${PROJECT_NAME} STATIC
    filedownload_notification.cpp
    filedownload_notification.h
    fileupload_notification.cpp
    fileupload_notification.h
    finishedprintjob_notification.cpp
    finishedprintjob_notification.h
    notification.cpp
    notificationdbus.cpp
    notificationdbus.h
    notification.h
    notificationmodel.cpp
    notificationmodel.h
    notificationstore.cpp
    notificationstore.h
    update_notification.cpp
    update_notification.h
    ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor.cpp
    ${CMAKE_CURRENT_BINARY_DIR}/notificationdbus_adaptor.h

    ${DBUS_SRCS}
    )
add_library(touch-ui::notifications ALIAS notifications)
target_include_directories( ${PROJECT_NAME}
    PUBLIC ${PROJECT_SOURCE_DIR}
)
target_compile_definitions(${PROJECT_NAME}
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME}
  PRIVATE
  Qt${QT_VERSION_MAJOR}::Core
  Qt${QT_VERSION_MAJOR}::Quick
  Qt${QT_VERSION_MAJOR}::DBus
  touch-ui::dbus_utils
  sl1_errors
  )

