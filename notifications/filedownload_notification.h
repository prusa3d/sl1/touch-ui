/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FILEDOWNLOAD_NOTIFICATION_H
#define FILEDOWNLOAD_NOTIFICATION_H

#include <QObject>
#include "fileupload_notification.h"
class FileDownloadNotification : public FileUploadNotification
{
    Q_OBJECT
public:
    explicit FileDownloadNotification(QObject *parent = nullptr);

signals:

public slots:
};

#endif // FILEDOWNLOAD_NOTIFICATION_H
