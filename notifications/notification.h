/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QObject>
#include <QString>
#include <QDebug>
#include <QDateTime>
#include <QVariantMap>
#include <QMetaObject>
#include <QMetaProperty>
#include "errors_sl1.h"
#include "errorcodes_text.h"

class Notification : public QObject
{
    Q_OBJECT


public:
    /** Different types of notification with different attributes and
     * different delegates.
    **/
    enum NotificationType {
        General = 0,
        UpdateNotification,
        FileUpload,
        FileDownload,
        FinishedPrintJob,
        Error,

        NotificationTypeCount // Last item = count
    };
    Q_ENUM(NotificationType)

    /** Roles are declared here instead of in the model to ease
     * reporting changes to the properties of notifications stored
     * in NotificationStore.
     *
     * Only the roles common for all notifications will be defined here,
     * the rest of the information(properties) will be accessible via
     * RoleObject - returns the notification object, QVariant(QObject*).
    **/
    enum NotificationRoles {
        RoleType = Qt::UserRole + 1,
        RoleHandle,
        RoleSeverity,
        RoleTimestamp,
        RoleText,

        RoleErrorCode,
        RoleObject
    };
    Q_ENUM(NotificationRoles)

    // Basic properties common to all notifications
    Q_PROPERTY(NotificationType type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int handle READ handle WRITE setHandle NOTIFY handleChanged)
    Q_PROPERTY(int severity READ severity WRITE setSeverity NOTIFY severityChanged)
    Q_PROPERTY(uint timestamp READ timestamp WRITE setTimestamp NOTIFY timestampChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(ErrorsSL1::Errors errorCode READ errorCode WRITE setErrorCode NOTIFY errorCodeChanged)

    explicit Notification(Notification::NotificationType type = General, QObject *parent = nullptr);

    static QHash<int, QByteArray> roleNames() {
        static const QHash<int, QByteArray> ret{
            {Notification::RoleType, "type"},
            {Notification::RoleHandle, "handle"},
            {Notification::RoleSeverity, "severity"},
            {Notification::RoleTimestamp, "timestamp"},
            {Notification::RoleText, "text"},
            {Notification::RoleErrorCode, "errorCode"},
            {Notification::RoleObject, "object"}
        };
        return ret;
    }

signals:
    void typeChanged(NotificationType type);
    void handleChanged(int handle);
    void severityChanged(int severity);
    void timestampChanged(uint timestamp);
    void textChanged(QString text);
    void errorCodeChanged(ErrorsSL1::Errors errorCode);

public slots:
    /** @property Notification::type
     *  @brief Contains type of the notification
     *  Must be one of Notification::NotificationType
     *  @default General
    **/
    NotificationType type() const;

    /** @property Notification::handle
     *  @brief Contains handle of the notification
     *  Handle can be used to update the notification later,
     *  or eventually even receive feedback
     *  @default -100
    **/
    int handle() const;

    /** @property Notification::severity
     *  @brief Severity == importance of the property
     *  The higher the number, the more important this notification is.
     *  Might be utilized in the view to highlight it.
     *  @default 10
    **/
    int severity() const;

    /** @property Notification::timestamp
     *  @brief Timestamp of the time the notification was created
     *  and added to the storage.
     *  @default 0
    **/
    uint timestamp() const;

    /** @property Notification::text
     *  @brief Text for notification types that utilize that.
     *  Can contain HTML.
     *  @default ""
    **/
    QString text() const;


    ErrorsSL1::Errors errorCode() const;

    // Setters
    void setType(NotificationType type);
    void setHandle(int handle);
    void setSeverity(int severity);
    void setTimestamp(uint timestamp);

    void setText(QString text);

    void setErrorCode(ErrorsSL1::Errors errorCode);

private:
    NotificationType m_type;
    int m_handle;
    int m_severity;
    uint m_timestamp;
    QString m_text;

    ErrorsSL1::Errors m_errorCode;

    friend QDebug operator<<(QDebug dbg, const Notification &notification)
    {
        QDebugStateSaver saver(dbg);
        auto meta = notification.metaObject();
        dbg << "Notification{";
        for(int i = 0; i < meta->propertyCount(); ++i) {
            dbg << meta->property(i).name() << ":" << meta->property(i).read(&notification);
        }
        dbg << "}";
        return dbg;
    }


};

#endif // NOTIFICATION_H
