/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef FINISHEDPRINTJOBNOTIFICATION_H
#define FINISHEDPRINTJOBNOTIFICATION_H

#include <QObject>
#include <QDateTime>
#include "notification.h"

class FinishedPrintJobNotification : public Notification
{
    Q_OBJECT
    QString m_projectName;

    int m_printTime;

    int m_layers;

    double m_consumedResin;

    int m_exposureTimeMs;

    int m_exposureTimeFirstMs;

    int m_exposureTimeCalibrationMs;

    double m_layerHeight;

    QDateTime m_timestampStart;

    QDateTime m_timestampExpectedEnd;

    QString m_exposurePath;

    QString m_projectFile;

    int m_currentLayer;

    QDateTime m_timestampFinish;


public:
    explicit FinishedPrintJobNotification(QObject *parent = nullptr);
    Q_PROPERTY(QString projectName READ projectName WRITE setProjectName NOTIFY projectNameChanged)
    Q_PROPERTY(QString projectFile READ projectFile WRITE setProjectFile NOTIFY projectFileChanged)
    Q_PROPERTY(QString exposurePath READ exposurePath WRITE setExposurePath NOTIFY exposurePathChanged)


    QString projectName() const
    {
        return m_projectName;
    }

    int printTime() const
    {
        return m_printTime;
    }

    QString exposurePath() const
    {
        return m_exposurePath;
    }

    QString projectFile() const
    {
        return m_projectFile;
    }

signals:

    void projectNameChanged(QString projectName);
    void exposurePathChanged(QString exposurePath);
    void projectFileChanged(QString projectFile);

public slots:
    void setProjectName(QString projectName);  
    void setExposurePath(QString exposurePath);
    void setProjectFile(QString projectFile);

};



#endif // FINISHEDPRINTJOBNOTIFICATION_H
