/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "fileupload_notification.h"

FileUploadNotification::FileUploadNotification(QObject *parent) : Notification(Notification::FileUpload, parent),
    m_filename(QStringLiteral("")),
    m_size(0),
    m_progress(0.0),
    m_done(0)
{

}

QString FileUploadNotification::filename() const
{
    return m_filename;
}

quint64 FileUploadNotification::size() const
{
    return m_size;
}

double FileUploadNotification::progress() const
{
    return m_progress;
}

bool FileUploadNotification::done() const
{
    return m_done;
}

void FileUploadNotification::setFilename(QString filename)
{
    if (m_filename == filename)
        return;

    m_filename = filename;
    emit filenameChanged(m_filename);
}

void FileUploadNotification::setSize(quint64 size)
{
    if (m_size == size)
        return;

    m_size = size;
    emit sizeChanged(m_size);
}

void FileUploadNotification::setProgress(double progress)
{
    if (qFuzzyCompare(m_progress, progress))
        return;

    m_progress = progress;
    emit progressChanged(m_progress);
}

void FileUploadNotification::setDone(bool done)
{
    if (m_done == done)
        return;

    m_done = done;
    emit doneChanged(m_done);
}
