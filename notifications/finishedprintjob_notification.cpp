/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "finishedprintjob_notification.h"

FinishedPrintJobNotification::FinishedPrintJobNotification(QObject *parent) : Notification(Notification::FinishedPrintJob, parent)
{

}


void FinishedPrintJobNotification::setProjectName(QString projectName)
{
    if (m_projectName == projectName)
        return;

    m_projectName = projectName;
    emit projectNameChanged(m_projectName);
}

void FinishedPrintJobNotification::setExposurePath(QString exposurePath)
{
    if (m_exposurePath == exposurePath)
        return;

    m_exposurePath = exposurePath;
    emit exposurePathChanged(m_exposurePath);
}

void FinishedPrintJobNotification::setProjectFile(QString projectFile)
{
    if (m_projectFile == projectFile)
        return;

    m_projectFile = projectFile;
    emit projectFileChanged(m_projectFile);
}


