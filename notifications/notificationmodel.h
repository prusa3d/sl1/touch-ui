/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NOTIFICATIONMODEL_H
#define NOTIFICATIONMODEL_H

#include <QAbstractListModel>
#include "notifications/notificationstore.h"
class NotificationModel : public QAbstractListModel
{
    Q_OBJECT

public:
    Q_PROPERTY(NotificationStore* notificationStore READ notificationStore)
    Q_PROPERTY(Notification * top READ top NOTIFY topChanged)
    Q_PROPERTY(int count READ count  NOTIFY countChanged)

    explicit NotificationModel(QObject *parent = nullptr);

    /** Role -> Role name mapping */
    QHash<int, QByteArray> roleNames() const override;

    /** Count of notifications */
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    /** @brief Method for reading properties of the Notifications
     *  @param index row and column of the Notification, column must be 0
     *  @param role One of the roles defined in Notification::NotificationRoles
    **/
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    /** @brief Remove the indicated rows
     *  Notification will be removed by calling the corresponding method of the
     *  notificationStore.
     *  @param row First row to remove
     *  @param count How many rows to remove
     *  @param parent parent index for hierarchical models, does not apply here
     *  @return false if trying to delete more items than is in the underlying notificationStore
    **/
    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    /** Test the internal working of the model */
    void test();

public slots:
    NotificationStore * notificationStore() const;
    Notification *top();
    Notification *get(int i);

    int count() const;

    /** @brief Remove all rows of a type
     * @param type Type of notifications to remove, General means all of them
     */
    void removeAll(Notification::NotificationType type = Notification::General);

private:
    NotificationStore * m_notificationStore;
signals:
    void topChanged(Notification* notification);
    void countChanged(int);
    void newNotification();
};

#endif // NOTIFICATIONMODEL_H
