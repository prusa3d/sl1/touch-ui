/*
	Copyright 2020, Prusa Research a.s.

	This file is part of touch-ui

	touch-ui is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BACKLIGHT_H
#define BACKLIGHT_H

#include <QObject>

class Backlight : public QObject
{
	Q_OBJECT

public:
	explicit Backlight(QObject *parent = nullptr);

	Q_PROPERTY(int intensity READ getIntensity WRITE setIntensity NOTIFY intensityChanged)
	Q_PROPERTY(bool enabled READ getEnabled WRITE setEnabled NOTIFY enabledChanged)

	Q_INVOKABLE int getIntensity();
	Q_INVOKABLE void setIntensity(int intensity);
	Q_INVOKABLE bool getEnabled();
	Q_INVOKABLE void setEnabled(bool enabled);

signals:
	void intensityChanged(const int intensity);
	void enabledChanged(const bool enabled);

private:
    const QString BRIGHTNESS_PATH = "/sys/class/backlight/backlight/brightness";
    const QString ACTUAL_BRIGHTNESS_PATH = "file:///sys/class/backlight/backlight/actual_brightness";
    const QString POWER_DOWN_PATH = "/sys/class/backlight/backlight/bl_power";
    const QString MAX_BRIGHTNESS_PATH = "/sys/class/backlight/backlight/max_brightness";

	int max_brightness = 10;
};

#endif // BACKLIGHT_H
