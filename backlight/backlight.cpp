/*
	Copyright 2020, Prusa Research a.s.

	This file is part of touch-ui

	touch-ui is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "backlight.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>


Backlight::Backlight(QObject *parent): QObject(parent) {
	QFile file(MAX_BRIGHTNESS_PATH);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream in(&file);
		max_brightness = in.readLine().toInt();
	}
}


void Backlight::setIntensity(int intensity) {
    qDebug() << "Setting backlight intensity to " << intensity;
    if(getIntensity() != intensity) {
        if (intensity < 1) {
            intensity = 1;
        }
        if (intensity > max_brightness) {
            intensity = max_brightness;
        }

        QFile file(BRIGHTNESS_PATH);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream out(&file);
        out << intensity;
        file.close();

        emit this->intensityChanged(intensity);
    }
}

int Backlight::getIntensity() {
	QFile file(BRIGHTNESS_PATH);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		return -1;
	}
	QTextStream in(&file);
	return in.readLine().toInt();
}

void Backlight::setEnabled(bool enabled) {
    qDebug() << (enabled ? "Enabling" : "Disabling") << "backlight.";
    if(getEnabled() != enabled) {
        QFile file(POWER_DOWN_PATH);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
        QTextStream out(&file);
        out << (enabled ? "0" : "1");
        file.close();

        emit this->enabledChanged(enabled);
    }
}

bool Backlight::getEnabled() {
	QFile file(POWER_DOWN_PATH);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		return true;
	}
	QTextStream in(&file);
	return in.readLine().trimmed() == "0";
}
