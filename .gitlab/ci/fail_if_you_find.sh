#!/bin/bash
# The script goes recursively through the current directory content and checks 
# for the string $1. If it is found, it returns 1(Failure), otherwise 0(Success).
#
# Optionally, a mask to restrict the processed files can be passed in $2
#
# Usage: $0 <string> <file mask>

FILE_MASK="*.*"

if [ "${2}" != "" ]; then
	FILE_MASK="${2}"
fi

if grep -r --include="${FILE_MASK}" -l "${1}"; then
	exit 1
else
	exit 0
fi
