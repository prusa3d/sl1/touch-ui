#ifndef ERRORSSL1_H
#define ERRORSSL1_H

#include <QObject>

/** Class exposing SL1 Error enums and methods to manipulate them
 *
**/
class ErrorsSL1 : public QObject
{
    Q_OBJECT
public:
    explicit ErrorsSL1(QObject *parent = nullptr);
    // TODO: autogenerate this enum from Prusa-Error-Codes
    enum Errors {
        TILT_HOME_FAILED = 101,
        TOWER_HOME_FAILED = 102,
        TOWER_MOVE_FAILED = 103,
        TILT_MOVE_FAILED = 104,
        FAN_FAILED = 106,
        RESIN_TOO_LOW = 108,
        RESIN_TOO_HIGH = 109,
        NOT_MECHANICALLY_CALIBRATED = 113,
        TOWER_ENDSTOP_NOT_REACHED = 114,
        TILT_ENDSTOP_NOT_REACHED = 115,
        TOWER_AXIS_CHECK_FAILED = 118,
        TILT_AXIS_CHECK_FAILED = 119,
        DISPLAY_TEST_FAILED = 120,
        INVALID_TILT_ALIGN_POSITION = 121,
        FAN_RPM_OUT_OF_TEST_RANGE = 122,
        TOWER_BELOW_SURFACE = 123,
        RESIN_MEASURE_FAILED = 124,
        CLEANING_ADAPTOR_MISSING = 125,
        FAN_FAILED_ID = 126,
        FAN_RPM_OUT_OF_TEST_RANGE_ID = 127,
        TEMP_SENSOR_FAILED = 205,
        UVLED_HEAT_SINK_FAILED = 206,
        A64_OVERHEAT = 207,
        TEMPERATURE_OUT_OF_RANGE = 208,
        UV_TEMP_SENSOR_FAILED = 209,
        TEMP_SENSOR_FAILED_ID = 210,
        TEMPERATURE_OUT_OF_RANGE_ID = 211,
        MOTION_CONTROLLER_WRONG_REVISION = 301,
        MOTION_CONTROLLER_EXCEPTION = 306,
        RESIN_SENSOR_FAILED = 307,
        NOT_UV_CALIBRATED = 308,
        UVLED_VOLTAGE_DIFFER_TOO_MUCH = 309,
        SOUND_TEST_FAILED = 310,
        UV_LED_METER_NOT_DETECTED = 311,
        UV_LED_METER_NOT_RESPONDING = 312,
        UV_LED_METER_COMMUNICATION_ERROR = 313,
        DISPLAY_TRANSLUCENT = 314,
        UNEXPECTED_UV_INTENSITY = 315,
        UNKNOWN_UV_MEASUREMENT_ERROR = 316,
        UV_TOO_BRIGHT = 317,
        UV_TOO_DIMM = 318,
        UV_INTENSITY_DEVIATION_TOO_HIGH = 319,
        BOOSTER_ERROR = 320,
        UV_LEDS_DISCONNECTED = 321,
        UV_LEDS_ROW_FAILED = 322,
        UNKNOWN_PRINTER_MODEL = 323,
        MQTT_SEND_FAILED = 401,
        NOT_CONNECTED_TO_NETWORK = 402,
        CONNECTION_FAILED = 403,
        DOWNLOAD_FAILED = 404,
        INVALID_API_KEY = 405,
        UNAUTHORIZED = 406,
        REMOTE_API_ERROR = 407,
        INVALID_PASSWORD = 408,
        NONE = 500,
        UNKNOWN = 501,
        PRELOAD_FAILED = 503,
        PROJECT_FAILED = 504,
        CONFIG_EXCEPTION = 505,
        NOT_AVAILABLE_IN_STATE = 506,
        DBUS_MAPPING_ERROR = 507,
        REPRINT_WITHOUT_HISTORY = 508,
        MISSING_WIZARD_DATA = 509,
        MISSING_CALIBRATION_DATA = 510,
        MISSING_UV_CALIBRATION_DATA = 511,
        MISSING_UVPWM_SETTINGS = 512,
        FAILED_UPDATE_CHANNEL_SET = 513,
        FAILED_UPDATE_CHANNEL_GET = 514,
        WARNING_ESCALATION = 515,
        NOT_ENOUGH_INTERNAL_SPACE = 516,
        ADMIN_NOT_AVAILABLE = 517,
        FILE_NOT_FOUND = 518,
        INVALID_EXTENSION = 519,
        FILE_ALREADY_EXISTS = 520,
        INVALID_PROJECT = 521,
        WIZARD_NOT_CANCELABLE = 522,
        MISSING_EXAMPLES = 523,
        FAILED_TO_LOAD_FACTORY_LEDS_CALIBRATION = 524,
        FAILED_TO_SERIALIZE_WIZARD_DATA = 525,
        FAILED_TO_SAVE_WIZARD_DATA = 526,
        SERIAL_NUMBER_IN_WRONG_FORMAT = 527,
        NO_EXTERNAL_STORAGE = 528,
        FAILED_TO_SET_LOGLEVEL = 529,
        FAILED_TO_SAVE_FACTORY_DEFAULTS = 530,
        FAILED_TO_DISPLAY_IMAGE = 531,
        NO_UV_CALIBRATION_DATA = 532,
        DATA_FROM_UNKNOWN_UV_SENSOR = 533,
        UPDATE_FAILED = 534,
        NO_DISPLAY_USAGE_DATA = 535,
        FAILED_TO_SET_HOSTNAME = 536,
        FAILED_PROFILE_IMPORT = 537,
        FAILED_PROFILE_EXPORT = 538,
        PROJECT_ERROR_CANT_READ = 539,
        PROJECT_ERROR_NOT_ENOUGH_LAYERS = 540,
        PROJECT_ERROR_CORRUPTED = 541,
        PROJECT_ERROR_ANALYSIS_FAILED = 542,
        PROJECT_ERROR_CALIBRATION_INVALID = 543,
        PROJECT_ERROR_WRONG_PRINTER_MODEL = 544,
        PROJECT_ERROR_CANT_REMOVE = 545,
        DIRECTORY_NOT_EMPTY = 546,
        LANGUAGE_ERROR = 547,
        OLD_EXPO_PANEL = 548,
        SYSTEM_SERVICE_CRASHED = 549,
        ALTERNATIVE_SLOT_BOOT = 601,
        NONE_WARNING = 700,
        UNKNOWN_WARNING = 701,
        AMBIENT_TOO_HOT_WARNING = 702,
        AMBIENT_TOO_COLD_WARNING = 703,
        PRINTING_DIRECTLY_WARNING = 704,
        PRINTER_MODEL_MISMATCH_WARNING = 705,
        RESIN_NOT_ENOUGH_WARNING = 706,
        PROJECT_SETTINGS_MODIFIED_WARNING = 707,
        PERPARTES_NOAVAIL_WARNING = 708,
        MASK_NOAVAIL_WARNING = 709,
        OBJECT_CROPPED_WARNING = 710,
        PRINTER_VARIANT_MISMATCH_WARNING = 711,
        RESIN_LOW = 712,
        FAN_WARNING = 713,
        EXPECT_OVERHEATING = 714,
        FILL_THE_RESIN = 715,
    };
    Q_ENUM(Errors)

    static void registerTypes();

    /** Convert a number into a string representation used by the backend
     *  Wrapper for use in QML
     * @param err One of the Errors enum
     * @return String representation of the error code, i.e. "#10501"
    */
    Q_INVOKABLE QString toStringCode(ErrorsSL1::Errors err);
    static QString static_toStringCode(ErrorsSL1::Errors err);

    /** Convert the backend string representation into an enum
     *  Wrapper for use in QML.
     * @param errStr string error code i.e. "#10501"
     * @return converted error or UNKNOWN if unable
    */
    Q_INVOKABLE ErrorsSL1::Errors fromStringCode(QString errStr);
    static ErrorsSL1::Errors static_fromStringCode(QString errStr);

    /** Get a name of an error(by the enum)
     *  Wrapper for use in QML
     * @param err One of the Errors enum
     * @return Name of the error, i.e. "UNKNOWN" or "GENERAL_NOT_CONNECTED_TO_NETWORK"
     */
    Q_INVOKABLE QString toString(ErrorsSL1::Errors err);
    static QString static_toString(ErrorsSL1::Errors err);

    /** Convert the error name into its enum
     *  Wrapper for use in QML
     * @param str name of the errro
     * @return converted error or UNKNOWN if unable
     */
    Q_INVOKABLE ErrorsSL1::Errors fromString(QString str);
    static ErrorsSL1::Errors static_fromString(QString str);

    /**
     * @brief parseFromDBus
     * @param str A string to be parsed, example: "e10544.ProjectErrorWrongPrinterModel"
     * @return processed error code or Errors::UNKNOWN
     */
    Q_INVOKABLE ErrorsSL1::Errors parseFromDBus(QString str);
    static ErrorsSL1::Errors static_parseFromDBus(QString str);

    static void tests();


signals:

public slots:
};

#endif // ERRORSSL1_H
