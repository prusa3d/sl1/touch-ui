
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Are You Sure?")
    name: "YesNoSwipe"
    pictogram: Theme.pictogram.yes
    back_button_enabled: false
    screensaver_enabled: false

    property bool swipable: true
    property string text

    signal yes()
    signal no()
    signal result(bool result)

    function popDialog() { if(StackView.view.currentItem === root) root.StackView.view.pop(); }

    StackView.onActivated: ()=>{
        swipeSignYesNo.shake()
        swipe.setCurrentIndex(0)
        console.log("Showing dialog: ", title)
    }

    SwipeView {
        id: swipe
        anchors.fill: parent
        interactive:  root.swipable
        currentIndex: 0

        Item {
            id: element
            clip: true

            Text {
                anchors {
                    verticalCenterOffset: -40
                    right: parent.right
                    rightMargin: 55
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: 55
                }
                text: root.text
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }

            SwipeSign {
                id: swipeSignYesNo
                text: qsTr("Swipe to proceed")
                anchors {
                    bottom: parent.bottom
                    right: parent.right
                }
                onClicked: ()=>{
                    swipe.incrementCurrentIndex()
                }
            }
        }

        Item {
            clip: true

            Row {
                anchors {
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset:  -40
                    rightMargin: 55
                }
                spacing: 30

                PictureButton {
                    id: btnYes
                    name: "Yes"
                    text: qsTr("Yes")
                    pictogram: Theme.pictogram.yes
                    backgroundColor: "green"
                    onClicked: ()=>{
                        root.yes()
                        root.result(true)
                        swipe.setCurrentIndex(0)
                    }
                }

                PictureButton {
                    id: btnNo
                    name: "No"
                    text: qsTr("No")
                    pictogram: Theme.pictogram.no
                    backgroundColor: "#ffb8381e"
                    onClicked: ()=>{
                        root.no()
                        root.result(false)
                        swipe.setCurrentIndex(0)
                    }
                }
            }
        }

    }

    PageIndicator {
        id: indicator
        count: swipe.count
        currentIndex: swipe.currentIndex
        visible: swipe.interactive//root.initPage != 0
        delegate: Item {
            height: 15
            width: 30
            Rectangle {
                width: 15
                height: 15
                color: index === indicator.currentIndex ? Theme.color.highlight : "grey"
                radius: 7
                anchors.margins: 10
            }
        }
        anchors.bottom: parent.bottom
        anchors.margins: 5
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ImageVersionText {}
}
