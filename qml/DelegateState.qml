/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

Item {
    id: root
    height: 100
    width: 760

    Column {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 5

        Text {
            text: qsTr("Network Info")
        }

        Text {
            text: model.text
        }

        Item {
            height: 2
        }
    }

    HorizontalSeparator {
        anchors.bottom: parent.bottom
        width: parent.width
    }
}
