
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.12
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PrusaPage {
    id: root
    title: qsTr("Settings")
    name: "Settings"
    pictogram: Theme.pictogram.settingsSimple

    back_handler: function() {
        root.saveValues()
        root.StackView.view.pop()
    }

    function saveValues() {
        printerConfig0.save()
    }

    ListView {
        id: settingsList
        height: 420
        interactive: true
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            rightMargin: 10
            leftMargin: 10
        }

        clip: true
        model: ObjectModel {
            id: settingsModel

            DelegateRef {
                name: "Calibration"
                text: qsTr("Calibration")
                pictogram: Theme.pictogram.calibration
                onClicked: () =>  {
                    root.StackView.view.push("PageSettingsCalibration.qml")
                }
            }

            DelegateRef {
                name: "Network"
                text: qsTr("Network")
                pictogram: Theme.pictogram.network
                onClicked: () =>  {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsNetwork.qml")
                }
            }

            DelegateRef {
                name: "PlatformAndResinTank"
                text: qsTr("Platform & Resin Tank")
                pictogram: Theme.pictogram.platformTank
                onClicked: () =>  {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsPlatformResinTank.qml")
                }
            }

            DelegateRef {
                name: "Firmware"
                text: qsTr("Firmware")
                pictogram: Theme.pictogram.firmware
                onClicked: () => {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsFirmware.qml")
                }
            }

            DelegateRef {
                name: "SettingsAndSensors"
                text: qsTr("Settings & Sensors")
                pictogram: Theme.pictogram.sensor
                onClicked: () => {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsSensors.qml")
                }
            }

            DelegateRef {
                name: "Touchscreen"
                text: qsTr("Touchscreen")
                pictogram: Theme.pictogram.touchscreen
                onClicked: () => {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsTouchscreen.qml")
                }
            }

            DelegateRef {
                name: "LanguageAndTime"
                text: qsTr("Language & Time")
                pictogram: Theme.pictogram.language
                onClicked: () => {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsLanguageTime.qml")
                }
            }

            DelegateRef {
                name: "SystemLogs"
                text: qsTr("System Logs")
                pictogram: Theme.pictogram.logs
                onClicked: () => {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsSystemLogs.qml")
                }
            }

            DelegateRef {
                name: "Support"
                text: qsTr("Support")
                pictogram: Theme.pictogram.support
                onClicked: () => {
                    root.saveValues()
                    root.StackView.view.push("PageSettingsSupport.qml")
                }
            }

            DelegateRef {
                name: "EnterAdmin"
                visible: printer0.admin_enabled
                text: qsTr("Enter Admin")
                pictogram: Theme.pictogram.adminSimple
                onClicked: () => {
                    root.saveValues()
                    adminApi.enter()
                }
            }
        }

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }
}
