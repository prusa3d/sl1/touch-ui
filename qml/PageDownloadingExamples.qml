import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.sl1.examples0 1.0
import PrusaComponents 1.0

PrusaPage {
    id: root
    name: "DownloadExamples"
    title: qsTr("Examples")
    pictogram: Theme.pictogram.download
    cancel_instead_of_back: state !== Examples0.FAILURE && state !== Examples0.COMPLETED

    // The examples0 object
    property var examples
    property var copy_progress
    property var download_progress
    property var unpack_progress
    property var state


    Binding on examples {
        when: printer0.examplesObject !== null
        value: printer0.examplesObject
    }

    Binding on copy_progress {
        when: printer0.examplesObject !== null
        value: printer0.examplesObject.copy_progress
    }

    Binding on download_progress {
        when: printer0.examplesObject !== null
        value: printer0.examplesObject.download_progress
    }

    Binding on unpack_progress {
        when: printer0.examplesObject !== null
        value: printer0.examplesObject.unpack_progress
    }

    Binding on state {
        when: printer0.examplesObject !== null
        value: printer0.examplesObject.state
    }

    Text {
        y: 89 - 60
        anchors {
            left: progress.left
            right: progress.right
        }
        font.pixelSize: Theme.font.big
        font.bold: true
        text: qsTr("Downloading examples...")
        clip: true
    }

    ProgressBar {
        id: progress
        y: 157 - 60
        height: 22
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: 49
            rightMargin: 49
        }

        value: (root.download_progress + root.unpack_progress + root.copy_progress)/3 * 100

        ProgressBar {
            id: partialProgress
            width: parent.width
            anchors {
                bottom: parent.bottom
            }
            indicator.color: "lightgrey"
            height: 1
            value: {
                switch(root.state) {
                case Examples0.DOWNLOADING:
                    return root.download_progress * 100
                case Examples0.UNPACKING:
                    return root.unpack_progress * 100
                case Examples0.COPYING:
                    return root.copy_progress * 100
                default:
                    return 0
                }
            }
        }
    }

    Text {
        id: txtPercent
        font.pixelSize: 40
        font.bold: true
        text: Math.round(progress.value) + "\u202F%"
        anchors {
            horizontalCenter: progress.horizontalCenter
            top: progress.bottom
            topMargin: 21
        }
    }


    Row {
        anchors {
            top: txtPercent.bottom
            topMargin: 80
            horizontalCenter: parent.horizontalCenter
        }
        spacing: 30

        Text {
            height: 60
            verticalAlignment: Qt.AlignVCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            font.pixelSize: Theme.font.big
            text: {
                switch(root.state) {
                case Examples0.INITIALIZING: return qsTr("Initializing...")
                case Examples0.DOWNLOADING: return qsTr("Downloading ...")
                case Examples0.COPYING: return qsTr("Copying...")
                case Examples0.UNPACKING: return qsTr("Unpacking...")
                case Examples0.COMPLETED: return qsTr("Done.")
                case Examples0.CLEANUP: return qsTr("Cleanup...")
                case Examples0.FAILURE: return qsTr("Failure.")
                default:
                    console.warn("PageDownloadingExamples: Unknown state")
                    return qsTr("Unknown state.")
                }
            }
        }

        TextButton {
            name: "ViewExamples"
            width: 300
            visible: root.state === Examples0.COMPLETED
            text: qsTr("View Examples")
            onClicked: () => root.StackView.view.replace("PageProjectSelection.qml", {initial_path: "/projects/examples"})
        }
    }
}
