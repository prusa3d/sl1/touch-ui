import QtQuick 2.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
Rectangle {
    color: Theme.color.background
    clip: true

    property alias model: pathView.model
    property alias delegate: pathView.delegate
    property alias currentItem: pathView.currentItem
    property alias currentIndex: pathView.currentIndex
    property int pathVerticalOffset: -40

    Component {
        id: appHighlight
        Column {
            width: 80
            Rectangle { width: parent.width; height: 2; color: "grey"; opacity: 1 }
            Item {
                height: 48
                width: parent.width
            }
            Rectangle { width: parent.width; height: 2; color: "grey"; opacity: 1 }
        }
    }

    PathView {
        id: pathView
        anchors.centerIn: parent
        width: parent.width
        height: parent.height
        highlightRangeMode: PathView.StrictlyEnforceRange
        highlight: appHighlight
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
        focus: true
        snapMode: PathView.SnapToItem
        pathItemCount: 5
        dragMargin: 100
        path: Path {
            startX: 50
            startY: 0
            PathAttribute { name: "iconOpacity"; value: 0.0 }
            PathAttribute { name: "iconScale"; value: 0.9 }
            PathLine { x: 50; y: pathView.height/2 + pathVerticalOffset }
            PathAttribute { name: "iconOpacity"; value: 1.0 }
            PathAttribute { name: "iconScale"; value: 1.0 }
            PathLine { x: 50; y: pathView.height + pathVerticalOffset }
            PathAttribute { name: "iconOpacity"; value: 0.0 }
            PathAttribute { name: "iconScale"; value: 0.9 }
        }
    }
}
