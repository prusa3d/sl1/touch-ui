/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    name: "UnpackingWizard"
    title: qsTr("Unpacking")
    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.REMOVE_DISPLAY_FOIL: return take_off_foil
        case Wizard0.SHOW_RESULTS: // fall-through
        case Wizard0.DONE: return wizard_done
        default: return null// Otherwise use BasicWizard default
        }
    }

    Component {
        id: take_off_foil
        PageConfirm {
            name: "UnpackingKitRemoveSticker"
            text: qsTr("Carefully peel off the protective sticker from the exposition display.")
            image_source: "unboxing-remove_sticker_screen.jpg"
            onConfirmed: ()=>{ wizard0.displayFoilRemoved() }
        }
    }

    Component {
        id: wizard_done
        PageConfirm {
            name: "UnpackingKitDone"
            text: qsTr("Unpacking done.")
            onConfirmed: ()=>{ wizard0.showResultsDone() }
        }
    }
}
