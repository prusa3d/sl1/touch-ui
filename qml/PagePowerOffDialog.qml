import QtQuick 2.0
import ErrorsSL1 1.0
PageYesNoSimple {
    id: root
    name: "PowerOffDialog"
    title: qsTr("Power Off?")
    text: qsTr("Do you really want to turn off the printer?")
    onYes: () => {
               printer0.poweroff(true, false,
                                 () => {root.StackView.view.replace("PageWait.qml", {text: qsTr("Powering Off...")})},
                                 () => {
                                     root.StackView.view.pop();
                                     push_error({code: ErrorsSL1.toStringCode(ErrorsSL1.UNKNOWN)}) // show error if it fails
                                 })
           }
    onNo: () => popDialog()
}
