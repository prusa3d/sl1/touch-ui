/*
    Copyright 2020, Prusa Research a.s.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtMultimedia 5.9
import "3rdparty/qrcode-generator"
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Page QR code")
    name: "pageQrCode"
    pictogram: Theme.pictogram.infoSimple
    property string qrContent: ""
    property string qrDescription: qrContent
    property bool showQrDescription: false
    property bool showVersion: true
    property string text: ""
    property int qrCodeOffset: 0
    readonly property int _margin: 30

    RowLayout {
        id: qrRow
        width: parent.width / 3 - root._margin
        height: 242
        anchors {
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
            left: parent.left
            leftMargin: root._margin
        }

        spacing: 5

        Rectangle {
            color: "white"
            width: 242
            height: 242

            QRCode {
                width: parent.width - 10
                height: parent.height - 10
                anchors.centerIn: parent
                value: root.qrContent
                background: "transparent"
                level: "M"
            }
        }
    }

    Text {
        visible: root.showQrDescription
        anchors {
            top: qrRow.bottom
            topMargin: 5
            left: qrRow.left
        }
        text: root.qrDescription
        width: qrRow.width
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        horizontalAlignment: Qt.AlignCenter
    }

    ScrollView {
        id: sView
        width: 2 * (parent.width / 3) - 1.5 * root._margin
        height: 270
        contentWidth: width - root._margin / 2
        clip: true
        anchors {
            top: qrRow.top
            right: parent.right
            rightMargin: root._margin / 2
        }
        ScrollBar.vertical.policy: contentHeight > height ? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
        ScrollBar.vertical.interactive: true
        Component.onCompleted: () => {
            contentItem.flickableDirection = Flickable.VerticalFlick
            contentItem.interactive = sView.contentHeight > sView.height ? true : false
        }

        Text {
            width: parent.width
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            text: root.text
        }
    }

    ImageVersionText {
        visible: root.showVersion
    }
}
