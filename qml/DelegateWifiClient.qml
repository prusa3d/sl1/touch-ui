/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12

import PrusaComponents 1.0

Rectangle {
    visible: false
    id: root
    signal changedOn()
    signal changedOff()
    signal switched(string who, bool onOff)

    property var setOn: function() {
        console.log("DelegateAPSwitch - received setOn()")
        sw.checked = true; root.changedOn()
    }
    property var setOff: function() {
        console.log("DelegateAPSwitch - received setOff()")
        sw.checked = false; root.changedOff()
    }

    width: 760
    height: 70
    color: "black"

    Text {
        text: model.text
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: 10
        }
    }

    PrusaSwitch {
        id: sw
        name: model.name;
        clickSwitch: false
        anchors {
            right: parent.right
            margins: 10
            verticalCenter: parent.verticalCenter
        }
        checked: networkPage.clientSwitch

        onClicked: ()=>{
            root.switched("clientDelegate", !sw.checked)
        }
    }

    HorizontalSeparator {
        width: parent.width
        anchors.bottom: parent.bottom
    }
}
