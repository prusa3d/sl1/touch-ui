
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import "3rdparty/qrcode-generator"

import PrusaComponents 1.0

/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title: qsTr("Hotspot")
    name: "apsettings"
    pictogram: Theme.pictogram.accessPoint
    property var saved_psk: ""


    StackView.onActivated: () => {
        chShowPassword.checked = false
        txtSSID.text = ""
        var lastSSID_PSK = Wifi.getSsidAndPskForId("hotspot") // returns list

        if(lastSSID_PSK.length === 0) {
            txtSSID.text = hostname.hostname
        }
        else {
            txtSSID.text = lastSSID_PSK[0]
            txtPSK.text = ""
            txtPSK.placeholderText = qsTr("(unchanged)")
            saved_psk = lastSSID_PSK[1]
            //txtPSK.text = lastSSID_PSK[1]
        }
        apSwipe.currentIndex = 0
    }

    StackView.onDeactivated: () => {
        apSwipe.currentIndex = 0
    }

    states: [
        State {
            name: "active"
            when: wifiNetworkModel.isHotspotRunning
            PropertyChanges {
                target: btnStartAP
                text: qsTr("Stop AP")
                pictogram: Theme.pictogram.no
                onClicked: () => {
                    wifiNetworkModel.isHotspotRunning = false
                }
                backgroundColor: Theme.color.red
            }
            PropertyChanges {
                target: txtState
                text: global.connectionStateText(wifiNetworkModel.activeConnectionState)
            }
            PropertyChanges {
                target: txtPSK
                enabled: false
            }
            PropertyChanges {
                target: txtSSID
                enabled: false
            }
            PropertyChanges {
                target: apSwipe
                interactive: true

            }
            PropertyChanges {
                target: swipeSign0
                visible: true
            }

            StateChangeScript {
                name: "run_animation"
                script: {
                    swipeSign0.shake()
                }
            }
        }
    ]

    SwipeView {
        id: apSwipe
        width: 800
        anchors.fill: parent
        interactive:  false
        currentIndex: 0


        Item {
            id: apSettingSwipeItem
            clip: true
            width: 800

            GridLayout {
                id: settingsGrid
                rowSpacing: 30
                columnSpacing: 30
                rows: 2
                columns: 2
                anchors {
                    leftMargin: 55
                    rightMargin: 55 + 20
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -20
                    right: btnStartAP.left
                    left: parent.left
                }

                Item {
                    width: 100
                    height: childrenRect.height
                    Text {
                        text: qsTr("State:")
                        font.pixelSize: 28
                        width: 100
                    }
                }

                Text {
                    id: txtState
                    font.pixelSize: 28
                    width: 300
                    Layout.alignment: Qt.AlignRight
                    text: qsTr("Inactive")
                }

                Text {
                    text: qsTr("SSID") + ":"
                    font.pixelSize: 28
                }

                TextField {
                    id: txtSSID
                    property bool valid: text.length >= 1
                    Layout.alignment: Qt.AlignRight
                    text: ""
                    width: 300
                    implicitWidth: width
                    font.family: prusaFont.name
                    font.pixelSize: 28
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    onActiveFocusChanged: () => {
                        if(activeFocus) {
                            selectAll()
                        }
                    }
                }

                Text {
                    text: qsTr("Password") + ":"
                    font.pixelSize: 28
                }

                TextField {
                    id: txtPSK
                    property bool valid: text.length >= 8 || (text.length == 0 && saved_psk.length >= 8)
                    Layout.alignment: Qt.AlignRight
                    text: ""
                    width: 300
                    implicitWidth: width
                    font.family: prusaFont.name
                    font.pixelSize: 28
                    placeholderTextColor: "grey"
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
                    echoMode: chShowPassword.checked ? TextInput.Normal : TextInput.Password
                    passwordCharacter: "*"
                    passwordMaskDelay: 20 * 1000
                    onActiveFocusChanged: () => {
                        if(activeFocus) {
                            selectAll()
                        }
                    }
                }

                Item {width: 10; height: 10}

                RowLayout {
                    Layout.alignment: Qt.AlignRight
                    CheckBox {
                        id: chShowPassword
                        focusPolicy: Qt.NoFocus
                        font.pixelSize: 28
                        font.family: prusaFont.name
                        checked: false
                        Layout.alignment: Qt.AlignVCenter
                    }
                    Item {
                        width: childrenRect.width
                        height: childrenRect.height
                        Text {
                            text: qsTr("Show Password")
                            font.pixelSize: 28
                            Layout.alignment: Qt.AlignVCenter
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: () => {
                                chShowPassword.toggle()
                            }
                        }
                    }
                }
                Item {
                    width: 100
                    height: childrenRect.height
                    Text {
                        text: qsTr("Security") + ":"
                        font.pixelSize: 28
                        width: 100
                    }
                }

                Text {
                    text: "WPA2"
                    font.pixelSize: 28
                    Layout.alignment: Qt.AlignRight
                }
            }
            WarningText {
                item: txtPSK
                y: txtPSK.y + txtPSK.height*1.6
                text: qsTr("PSK must be at least 8 characters long.")
            }

            WarningText {
                item: txtSSID
                y: txtSSID.y + txtSSID.height * 1.6
                text: qsTr("SSID must not be empty")
            }

            PictureButton {
                id: btnStartAP
                anchors {
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset:  -40
                    rightMargin: 55
                }

                text: qsTr("Start AP")
                pictogram: Theme.pictogram.yes
                name: "StartAP"
                backgroundColor: "green"
                onClicked: () => {
                    var psk = txtPSK.text
                    if(psk === "" || psk === null || psk === undefined) psk = saved_psk

                    // Effectively disable the button for an invalid input(and notify the user)
                    if( ! txtSSID.valid ) {
                        txtSSID.forceActiveFocus()
                        return
                    }
                    else if( psk.length < 8) {
                        txtPSK.forceActiveFocus()
                        return
                    }

                    forceActiveFocus()

                    console.log("Creating hotspot SSID: " + txtSSID.text + " PSK: " + psk)
                    if(psk.length >= 8) {
                        var r = Wifi.addConnectionAP(txtSSID.text, psk)
                    }

                }
            }

            SwipeSign {
                id: swipeSign0
                visible: false
                text: qsTr("Swipe for QRCode")
                anchors {
                    bottom: parent.bottom
                    right: parent.right
                }
                onClicked: () => {
                    apSwipe.incrementCurrentIndex()
                }
            }

            ImageVersionText {}
        }



        Item {
            id: qrcodeSwipeItem
            clip: true
            width: 800
            property string hotspot_psk: ""
            property string hotspot_ssid: ""

            Connections {
                target: wifiNetworkModel
                function onActiveConnectionNameChanged() {
                    if(wifiNetworkModel.activeConnectionName === "hotspot") {
                        wifiNetworkModel.getPSKbyName("hotspot", function(password){qrcodeSwipeItem.hotspot_psk=password}, function(e){});
                        qrcodeSwipeItem.hotspot_ssid = wifiNetworkModel.getLocalHotspotSSID()
                    }
                }
            }

            Component.onCompleted: () => {
                wifiNetworkModel.getPSKbyName("hotspot", function(password){qrcodeSwipeItem.hotspot_psk=password}, function(e){});
                qrcodeSwipeItem.hotspot_ssid = wifiNetworkModel.getLocalHotspotSSID()
            }

            Rectangle {
                y: 10
                anchors.horizontalCenter: parent.horizontalCenter
                id: r1
                color: "white"
                width: 320
                height: 320
                visible: root.text !== ""
                QRCode {
                    anchors {
                        verticalCenterOffset: 2
                        horizontalCenterOffset: 2
                        centerIn: parent
                    }
                    id: netinfoQRCode
                    value : "WIFI:T:WPA;S:" + qrcodeSwipeItem.hotspot_ssid + ";P:"+ qrcodeSwipeItem.hotspot_psk + ";;"
                    width: 300
                    height: 300
                    level: "Q"
                }
            }

            Item {
                anchors {
                    horizontalCenter: r1.horizontalCenter
                    top: r1.bottom
                    margins: 20
                }
                height: 30
                width: 760

                Text {
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    font.pixelSize: 24
                    text:  "WIFI:T:WPA;S:" + qrcodeSwipeItem.hotspot_ssid + ";P:"+ qrcodeSwipeItem.hotspot_psk + ";;"
                    anchors.centerIn: parent
                }
            }



            SwipeSign {
                id: swipeSignQrcode
                visible: true
                text: qsTr("Swipe for settings")
                direction: "left"
                anchors {
                    bottom: parent.bottom
                }
                x: 30
                onClicked: () => {
                    apSwipe.decrementCurrentIndex()
                }
            }
        }
    }

    PageIndicator {
        id: indicator
        count: apSwipe.count
        currentIndex: apSwipe.currentIndex
        visible: apSwipe.interactive
        delegate: Item {
            height: 15
            width: 30

            Rectangle {
                width: 15
                height: 15
                color: index === indicator.currentIndex ? Theme.color.highlight : "grey"
                radius: 7
                anchors.margins: 10
            }
        }
        anchors.bottom: parent.bottom
        anchors.margins: 5
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
