
/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml 2.2
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Set Timezone")
    name: "settimezone"
    pictogram: Theme.pictogram.timezone

    // Pre-cached regions
    readonly property var regions: timedate.timezoneRegions()

    // Region of the current timezone
    property string region: ""

    // City of the current timezone
    property string city: ""

    signal timezoneSet(var timezone)

    function selectCurrentTimezone() {
        root.region = timedate.timezoneRegion()
        root.city = timedate.timezoneCity()
        viewRegion.currentIndex = regions.indexOf(timedate.timezoneRegion())
        var cities = timedate.timezoneCities(timedate.timezoneRegion())
        viewCity.currentIndex = cities.indexOf(timedate.timezoneCity())
    }

    Component.onCompleted: ()=>{ selectCurrentTimezone() }

    Connections {
        target: timedate
        function onTimezoneChanged() {
            selectCurrentTimezone()
        }
    }

    RowLayout {
        anchors.centerIn: parent
        height: parent.height

        PathSelectorView {
            id: viewRegion
            width: 250
            height: 300
            color: "black"
            clip: false
            property var currentRegion: regions[viewRegion.currentIndex]

            model: timedate.timezoneRegions()
            delegate: PathTextDelegate {
                color: modelData === region ? Theme.color.highlight : "white"
                text: modelData.replace("_", " ").replace('\u005F', " ")
                font.pixelSize: Theme.font.big
                font.family: prusaFont.name
            }
            function refresh_city() {
                var cities =  timedate.timezoneCities(viewRegion.currentRegion)
                var currentCityIdx = cities.indexOf(timedate.timezoneCity())
                if(currentCityIdx !== -1) {
                    viewCity.currentIndex = currentCityIdx
                }
                else {
                    viewCity.currentIndex = 0
                }

            }
            onCurrentIndexChanged: ()=>{ refresh_city() }
            onModelChanged: ()=>{ refresh_city() }
            Component.onCompleted: ()=>{ refresh_city() }
        }

        Item {
            height: 1
            width: 10
        }

        PathSelectorView {
            id: viewCity
            width: 250
            height: 300
            color: "black"
            clip: false
            model: timedate.timezoneCities(viewRegion.currentRegion)
            delegate: PathTextDelegate {
                color: modelData === city ? Theme.color.highlight : "white"
                text: modelData.replace("_", " ").replace('\u005F', " ")
                font.pixelSize: Theme.font.big
                font.family: prusaFont.name
            }
        }
    }

    Text {
        x: 307
        y: 170
        font.pixelSize: 50
        text:"/"
    }

    Text {
        x: 145
        y: 365

        text: qsTr("Region")
    }

    Text {
        x: 430
        y: 365
        text: qsTr("City")
    }

    PictureButton {
        pictogram: Theme.pictogram.yes
        name: "SetTimezone"
        text: qsTr("Set")
        backgroundColor: "green"
        anchors {
            rightMargin: 55
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
        }

        onClicked: ()=>{
            var cities =  timedate.timezoneCities(viewRegion.currentRegion)
            var region = regions[viewRegion.currentIndex]
            var city = cities[viewCity.currentIndex]
            var timezone = region + "/" + city
            timedate.setTimezone(timezone,
                                 function(){console.log("Set timezone to " + timezone)
                                 },
                                 function(){console.log("Could not set timezone to " + timezone)}
                                 )
            root.timezoneSet(timezone)

            global.initialSettings.timezone = true
            global.initialSettings.sync()

            // If the back button is disabled, this is the only way to leave the page
            if( ! root.back_button_enabled) root.StackView.view.pop()
        }
    }

    ImageVersionText {}
}


