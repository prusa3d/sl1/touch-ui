
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title: qsTr("PrusaLink")
    name: "PrusaLink"
    pictogram: Theme.pictogram.key

    StackLayout {
        id: localView
        anchors.fill: parent

        Item {
            RowLayout {
                id: nnn
                spacing: 30
                width: 500
                anchors {
                    //centerIn: parent
                    left: rrr.left
                    bottom: rrr.top
                    bottomMargin: 10
                }

                Text {
                    id: t1
                    Layout.alignment: Qt.AlignVCenter
                    text: qsTr("User Name") + ":"
                    font.pixelSize: 24
                }

                Item {
                    width: t2.width - t1.width - 30
                }

                TextField {
                    id: txtUserName
                    Layout.alignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    text: "maker"
                    width: 350
                    font.family: prusaFont.name
                    font.pixelSize: 24
                    enabled: false
                    property var valid: text.match(/^[0-9a-zA-Z][0-9a-zA-Z,\-,\.]*$/)
                    inputMethodHints: Qt.ImhNoAutoUppercase
                }                
            }

            RowLayout {
                id: rrr
                spacing: 30
                width: 500
                anchors {
                    leftMargin: 55
                    rightMargin: 55
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -20
                    left: parent.left
                }

                Text {
                    id: t2
                    Layout.alignment: Qt.AlignVCenter
                    text: qsTr("Printer Password") + ":"
                    font.pixelSize: 24
                }
                
                TextField {
                    id: txtPassword
                    Layout.alignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    text: printer0.http_digest_password
                    width: 350
                    font.family: prusaFont.name
                    font.pixelSize: 24
                    inputMethodHints: Qt.ImhNoAutoUppercase
                    property var valid: text.length >= 8
                }
            }

            Text {
                anchors {
                    top: rrr.bottom
                    topMargin: 20
                    left: rrr.left
                }

                font.pixelSize: 24
                text: qsTr("Must be at least 8 chars long")
                color: "grey"
                
            }

            WarningText {
                item: txtPassword
                anchors.bottom: rrr.top
                text: qsTr("Must be at least 8 chars long")
            }

            WarningText {
                item: txtUserName
                anchors.bottom: nnn.top
                text: qsTr("Can contain only characters a-z, A-Z, 0-9 and  \"-\".")
            }
        }
    }
    
    PictureButton {
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset:  -40
            rightMargin: 55
        }

        text: qsTr("Save")
        name: "Save"
        backgroundColor:"green"
        pictogram: Theme.pictogram.yes
        z: -1
        enabled: txtPassword.valid
        onClicked: () => {
            if (!(printer0.set_http_digest_password(txtPassword.text)))
                errorStack.push("PageError.qml", {code: 10408})
        }
    }

    ImageVersionText {}
}
