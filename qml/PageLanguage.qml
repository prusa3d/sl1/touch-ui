
/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml 2.2
import PrusaComponents 1.0
import Native 1.0

PrusaPage {
    id: root
    title: qsTr("Set Language")
    name: "setlanguage"
    pictogram: Theme.pictogram.language
    readonly property var languageKeys: Translator.languageKeys
    signal languageSet(var language)

    onLanguageSet: (lang) => {
                       LocaleSetting.locale = Translator.languageToLocale(lang)
                   }

    function selectLanguage(lang) {
        if(lang === Translator.Default) {
            viewLanguage.currentIndex = Translator.languageKeys.indexOf(Translator.defaultLanguage)
        }
        else viewLanguage.currentIndex = Translator.languageKeys.findIndex(function(x){return x === lang})
    }

    StackView.onActivated:()=>{
        selectLanguage(Translator.language)
    }

    Connections {
        target: Translator
        function onLanguageChanged(lang) {
            selectLanguage(lang)
        }
    }


    PathSelectorView {
        id: viewLanguage
        anchors.centerIn: parent
        width: 200
        height: 300
        model: Translator.languages
        clip: false
        delegate: PathTextDelegate {
            color: Translator.languageKeys[index] === Translator.language
                   || (Translator.language === Translator.Default
                       && Translator.languageKeys[index] === Translator.defaultLanguage
                       )  ? Theme.color.highlight : "white"
            text: modelData
            font.pixelSize: Theme.font.big
            font.family: prusaFont.name
        }
    }

    PictureButton {
        pictogram: Theme.pictogram.yes
        name: "SetLanguage"
        text: qsTr("Set")
        backgroundColor: "green"
        anchors {
            rightMargin: 55
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
        }

        onClicked:()=> {
            // Set the actual language for the whole system
            root.languageSet(Translator.languageKeys[viewLanguage.currentIndex])
            global.initialSettings.language = true
            global.initialSettings.sync()
            // If the back button is disabled, this is the only way to leave the page
            if( ! root.back_button_enabled) root.StackView.view.pop()
        }
    }


    ImageVersionText {}
}


