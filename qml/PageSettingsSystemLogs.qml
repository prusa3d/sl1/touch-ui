
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import cz.prusa3d.logs0 1.0
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    title: qsTr("System Logs")
    name: "settingsSystemLogs"
    pictogram: Theme.pictogram.logs
    model: ObjectModel {
        DelegateText {
            id: logStatus
            name: "LogUploadIdentifier"
            pictogram:  Theme.pictogram.systemInfo
            text: logStatus.logStatusText()

            function logStatusText(){
                return qsTr("Last Seen Logs:") + " " + (logs0.lastLogUploadIdentifier() ? `<b>${logs0.lastLogUploadIdentifier()}</b>` : qsTr("<b>No logs have been uploaded yet.</b>"));
            }

            Connections {
                target: logs0
                function onStateChanged() {
                    if (logs0.state === Logs0.LogsState.FINISHED){
                        logStatus.text = logStatus.logStatusText();
                    }
                }
            }
        }

        DelegateRef {
            name:"ExportLogsToFlashdisk"
            text: qsTr("Save to USB Drive")
            pictogram: Theme.pictogram.save
            onClicked: () => {
                logs0.usbSave()
                root.StackView.view.push("PageLogs.qml")
            }
        }

        DelegateRef {
            name:"LogsUpload"
            text: qsTr("Upload to Server")
            pictogram: Theme.pictogram.uploadToCloud
            onClicked: () => {
                logs0.serverUpload()
                root.StackView.view.push("PageLogs.qml")
            }
        }
    }
}
