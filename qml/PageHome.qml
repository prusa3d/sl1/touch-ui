
/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0

PageButtons {
    id: root
    title: qsTr("Home")
    name: "Home"
    pictogram: Theme.pictogram.home
    back_button_enabled: false

    PictureButton {
        name: "Print"
        pictogram: Theme.pictogram.play
        text: qsTr("Print")
        onClicked: () => {
            if(printer0.state === Printer0.PRINTING) {
                root.StackView.view.push("PagePrint.qml")
            }
            else {
                console.log("Pushing PageProjectSelect")
                root.StackView.view.push("PageProjectSelection.qml")
            }
        }
    }
    PictureButton {
        name: "Control"
        pictogram: Theme.pictogram.control
        text: qsTr("Control")
        onClicked: () => root.StackView.view.push("PageMovementControl.qml")
    }

    PictureButton {
        name: "Settings"
        pictogram: Theme.pictogram.settings
        text: qsTr("Settings")
        onClicked: () => root.StackView.view.push("PageSettings.qml")
    }

    PictureButton {
        name: "TurnOff"
        pictogram: Theme.pictogram.powerOff
        text: qsTr("Turn Off")
        onClicked: () => root.StackView.view.push("PagePowerOffDialog.qml")
    }
}
