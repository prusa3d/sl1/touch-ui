
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title: qsTr("Hidden Network")
    name: "hiddennetwork"
    pictogram: Theme.pictogram.wifi

    StackView.onActivated: () => {
        chShowPassword.checked = false
        txtSSID.text = ""
        txtPSK.text = ""
        banner.state = ""
    }

    GridLayout {
        id: settingsGrid
        rowSpacing: 30
        columnSpacing: 30
        rows: 2
        columns: 2

        anchors {
            leftMargin: 55
            rightMargin: 55
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
            right: btnStartAP.left
            left: parent.left
        }

        Text {
            text: qsTr("SSID") + ":"
            font.pixelSize: Theme.font.labelSize
        }

        TextField {
            id: txtSSID
            property bool valid: text.length >= 1
            text: ""
            width: 550
            font.family: prusaFont.name
            font.pixelSize: Theme.font.labelSize
            inputMethodHints: Qt.ImhNoAutoUppercase
            Layout.alignment: Qt.AlignRight
        }

        Text {
            text: qsTr("Password") + ":"
            font.pixelSize: Theme.font.labelSize
        }

        TextField {
            id: txtPSK
            property bool valid: text.length >= 8
            text: ""
            width: 550
            font.family: prusaFont.name
            font.pixelSize: Theme.font.labelSize
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
            echoMode: {
                if(chShowPassword.checked) {
                    return TextInput.Normal
                }
                else return TextInput.Password
            }
            passwordCharacter: "*"
            passwordMaskDelay: 20 * 1000

            Layout.alignment: Qt.AlignRight
        }

        Item {width: 10; height: 10}

        RowLayout {
            Layout.alignment: Qt.AlignRight
            CheckBox {
                id: chShowPassword
                focusPolicy: Qt.NoFocus
                font.pixelSize: Theme.font.labelSize
                font.family: prusaFont.name
                checked: false
                Layout.alignment: Qt.AlignVCenter
            }
            Text {
                text: qsTr("Show Password")
                font.pixelSize: Theme.font.labelSize
                Layout.alignment: Qt.AlignVCenter
            }
        }

        Text {
            text: qsTr("Security") + ":"
            font.pixelSize: Theme.font.labelSize
        }

        Text {
            text: "WPA"
            font.pixelSize: Theme.font.labelSize
            Layout.alignment: Qt.AlignRight
        }
    }
    WarningText {
        item: txtPSK
        anchors {
            top: settingsGrid.top
            topMargin: 0
        }

        text: qsTr("PSK must be at least 8 characters long.")
    }

    WarningText {
        item: txtSSID
        anchors {
            bottom: settingsGrid.top
        }
        text: qsTr("PSK must be at least 8 characters long.")
    }

    Rectangle {
        id: banner
        anchors {
            centerIn: parent
            verticalCenterOffset: -40
        }
        opacity: 0.9
        visible: false
        width: 500
        height: 100
        radius: 30
        color: "black"
        z: 10
        border.color: Theme.color.highlight
        Text {
            id: bannerText
            anchors.centerIn: parent
            font.pixelSize: Theme.font.labelSize
        }
        states: [
            State {
                name: ""
                PropertyChanges {
                    target: banner
                    visible: false
                }
            },
            State {
                name: "busy"
                PropertyChanges {
                    target: banner
                    visible: true
                }
                PropertyChanges {
                    target: bannerText
                    text: qsTr("Working...")
                }
            },
            State {
                name: "failed"
                PropertyChanges {
                    target: banner
                    visible: true
                }
                PropertyChanges {
                    target: bannerText
                    text: qsTr("Connection to %1 failed.").txtSSID.text
                }
            },
            State {
                name: "succeeded"
                PropertyChanges {
                    target: banner
                    visible: true
                }
                PropertyChanges {
                    target: bannerText
                    text: qsTr("Connected to %1").arg(txtSSID.text)
                }
            }

        ]
        Timer {
            id: stateResetter
            interval: 5000
            onTriggered: () => {
                banner.state = ""
            }
        }
        onStateChanged: () => {
            if(state != "") {
                stateResetter.start()
            }
        }
    }

    PictureButton {
        id: btnStartAP
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset:  -40
            rightMargin: 55
        }

        text: qsTr("Start AP")
        pictogram: Theme.pictogram.yes
        visible: txtSSID.valid
        name: "StartAP"
        backgroundColor: "green"
        enabled: txtSSID.valid
        onClicked: () => {
            forceActiveFocus()
            console.log("Creating hotspot SSID: " + txtSSID.text + " PSK: " + txtPSK.text)
            var r = Wifi.addConnectionAuto(txtSSID.text, txtPSK.text)
            if(r) {
                banner.state = "succeeded"
            }
            else {
                banner.state = "failed"
            }
        }
    }

    ImageVersionText {}
}
