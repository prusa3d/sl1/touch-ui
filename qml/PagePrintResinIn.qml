/*
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0
import cz.prusa3d.sl1.filemanager 1.0

/** Needs to be inserted in a StackView */
PrusaPage {
    id: root
    title: qsTr("Pour in resin")
    slaName: "printresinin"
    pictogram: Theme.pictogram.printSimple
    property var exposure
    function back_handler() {
        if(exposure) {
            exposure.cancel(function(){}, function(){})
            root.StackView.view.pop()
        }
    }


    RowLayout {
        height: 340
        anchors {
            verticalCenter: parent.verticalCenter
            verticalCenterOffset:  -40
            leftMargin: 55
            rightMargin: 55
            bottomMargin: 30
            left: parent.left
            right: parent.right
        }
        PrusaText {
            anchors.left: parent.left
            Layout.alignment: Qt.AlignVCenter
            Layout.preferredWidth: 490
            text: {
                var requiredResin = Math.round(exposure.total_resin_required_percent)
                var text = ""
                if (requiredResin > 100) {
                    text += "<b>" + qsTr("The project requires %1 % of the resin. It will be necessary to refill the resin during print.").arg(requiredResin) + "</b><br/><br/>"
                    requiredResin = 100
                }
                text += qsTr("Please fill the resin tank to at least %1 % and close the cover.").arg(requiredResin)
                return text
            }
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        PictureButton {
            anchors.right: parent.right
            pictogram: Theme.pictogram.yes
            backgroundColor: "green"
            text: qsTr("Continue")
            name: "continue"
            onClicked: {
                root.exposure.confirm_resin_in(
                            function(){console.log("Resin in confirmed, starting...")},
                            function(err){console.warn("Could not move to the preprint checks because: ", err)}
                            )
            }
        }

    }

    ImageVersionText {}
}
