/*
    Copyright 2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.0
import PrusaComponents 1.0

SwipeDelegate {
    id: root
    property string pictogram: ""
    property Component textComponent: Component {
        Text {
            text: root.text
            color: Theme.color.textInverse
        }
    }
    height: 80
    width: parent.width
    background: Rectangle {
        color: root.swipe.position === -1.0 ? Qt.darker(Theme.color.backgroundInverse, 1.5) : Theme.color.backgroundInverse
    }
    contentItem: ColumnLayout {
        spacing: 15
        id: mainColumn
        width: root.width - 20
        RowLayout {
            id: itemRow
            width: parent.width
            height: 80
            Item {
                width: 60
                height: 50
                Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                Image {
                    anchors.centerIn: parent
                    height: 50
                    sourceSize.width: width
                    sourceSize.height: height
                    fillMode: Image.PreserveAspectFit
                    source: Theme.pictogram.path + "/" + root.pictogram
                }
            }
            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true
                Loader {
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        right: parent.right
                        verticalCenterOffset: mainColumn.spacing/2 - 6
                    }
                    sourceComponent: root.textComponent
                }
            }
        }
    }

    HorizontalSeparator {
        width: parent.width
        anchors.bottom: parent.bottom
    }

    swipe.right: Rectangle {
        width: root.height
        height: root.height
        color: Qt.darker(Theme.color.backgroundInverse, 1.8)
        anchors {
            right: parent.right
        }

        PictureButton {
            anchors {
                centerIn: parent
            }
            height: 60
            width: height
            pictogram: Theme.pictogram.cancelInverse
            name: "Delete"
            onClicked: ()=>{
                notificationModel.removeRows(model.index, 1)
            }
            background: null
            innerMargin: 0
        }
    }
    swipe.onCompleted: ()=>{
        if(swipe.position === -1.0) {
            notificationModel.removeRows(model.index, 1)
        }
    }
}
