/*
    Copyright 2020, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Native 1.0
import cz.prusa3d.sl1.printer0 1.0
PageQrCode {
    title: qsTr("About Us")
    qrContent: {
        if(printer0.printer_model === Printer0.M1) {
            return "prusa3d.com/medical";
        }

        switch(LocaleSetting.locale.substring(0, 2)) {
        case "cs":
            return "https://www.prusa3d.cz/o-nas/";
        case "it":
            return "https://www.prusa3d.it/su-di-noi/";
        case "de":
            return "https://www.prusa3d.de/uber-uns/";
        case "fr":
            return "https://www.prusa3d.fr/compagnie/";
        case "pl":
            return "https://www.prusa3d.pl/o-firmie/";
        case "es":
            return "https://www.prusa3d.es/sobre-nosotros/";
        case "en":
        default:
            return "https://www.prusa3d.com/about-us/";
        }
    }
    text:
        qsTr("To find out more about us please scan the QR code or use the link below:")
        + "\n\n"
        + qrContent
        + "\n\n"
        + qsTr("Prusa Research a.s.") + "\n"
        + AppInfo.buildYear
    qrCodeOffset: 1
}
