/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Please Wait")
    name: "PleaseWait"
    pictogram: Theme.pictogram.sandclock
    back_button_enabled: false
    property string text: ""

    ColumnLayout {
        y: 40
        width: parent.width - 40
        anchors {
            horizontalCenter: parent.horizontalCenter
        }

        Text {
            text: root.text
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            width: parent.width
        }
    }

    BusySign {
        running: root.visible
        width: 160
        height: 160
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: 20
        }
    }

    ImageVersionText {}
}
