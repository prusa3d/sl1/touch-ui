/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
/*
 Depends on:
    StackView view

*/
PrusaPage {
    id: root
    title: qsTr("Add Hidden Network")
    name: "addnetwork"
    pictogram: Theme.pictogram.wifi

    StackView.onActivated: ()=>{
        chShowPassword.checked = false
        txtSSID.text = ""
        txtPSK.text = ""
    }

    StackView.onDeactivated: ()=>{
        txtSSID.text = ""
        txtPSK.text = ""
    }

    GridLayout {
        id: settingsGrid
        rowSpacing: 30
        columnSpacing: 30
        rows: 2
        columns: 2
        anchors {
            leftMargin: 55
            rightMargin: 55 + 20
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -20
            right: btnConnect.left
            left: parent.left
        }

        Text {
            text: qsTr("SSID") + ":"
            font.pixelSize: Theme.font.labelSize
        }

        TextField {
            id: txtSSID
            property bool valid: text.length >= 1
            text: ""
            width: 350
            implicitWidth: width
            font.family: prusaFont.name
            font.pixelSize: Theme.font.labelSize
            inputMethodHints: Qt.ImhNoAutoUppercase
            onActiveFocusChanged: ()=>{
                if(activeFocus) {
                    selectAll()
                }
            }
            Layout.alignment: Qt.AlignRight
        }

        Text {
            text: qsTr("PASS") + ":"
            font.pixelSize: Theme.font.labelSize
        }

        TextField {
            id: txtPSK
            property bool valid: text.length >= 8
            text: ""
            width: 350
            implicitWidth: width
            font.family: prusaFont.name
            font.pixelSize: Theme.font.labelSize
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhSensitiveData
            echoMode: {
                if(chShowPassword.checked) {
                    return TextInput.Normal
                }
                else return TextInput.Password
            }
            passwordCharacter: "*"
            passwordMaskDelay: 20 * 1000
            onActiveFocusChanged: {
                if(activeFocus) {
                    selectAll()
                }
            }
            Layout.alignment: Qt.AlignRight
        }

        Item {width: 10; height: 10}

        RowLayout {
            Layout.alignment: Qt.AlignRight
            CheckBox {
                id: chShowPassword
                font.pixelSize: Theme.font.labelSize
                font.family: prusaFont.name
                checked: false
                Layout.alignment: Qt.AlignVCenter
                focusPolicy: Qt.NoFocus
            }
            Item {
                width: childrenRect.width
                height: childrenRect.height
                Text {
                    text: qsTr("Show Password")
                    font.pixelSize: Theme.font.labelSize
                    Layout.alignment: Qt.AlignVCenter
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: ()=>{
                        chShowPassword.toggle()
                        console.log("clicked")
                    }
                }
            }
        }

        Item {
            width: 100
            height: childrenRect.height
            Text {
                text: qsTr("Security") + ":"
                font.pixelSize: Theme.font.labelSize
                width: 100
            }
        }

        ComboBox {
            id: cmbSecurity
            Layout.alignment: Qt.AlignRight
            enabled: false
            model: ["WPA2", "Auto","None"]
        }
    }
    WarningText {
        item: txtPSK
        y: txtPSK.y + txtPSK.height*2.5
        text: qsTr("PSK must be at least 8 characters long.")
    }

    WarningText {
        item: txtSSID
        y: txtSSID.y + txtSSID.height*2.5
        text: qsTr("SSID must not be empty.")
    }

    PictureButton {
        id: btnConnect
        name: "Connect"
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset:  -40
            rightMargin: 55
        }

        text: qsTr("Connect")
        pictogram: Theme.pictogram.yes
        backgroundColor: "green"
        onClicked: ()=>{
            // Effectively disable the button for an invalid input(and notify the user)
            if(! txtSSID.valid) {
                txtSSID.forceActiveFocus()
                return
            }
            else if( ! txtPSK.valid) {
                txtPSK.forceActiveFocus()
                return
            }

            forceActiveFocus()
            console.log("Connecting to (hidden) SSID: " + txtSSID.text + " PSK: " + txtPSK.text)
            Wifi.addConnectionAuto(txtSSID.text, txtPSK.text, true /* chHidden.checked */, true,
                                   function(){console.log("Created to " + txtSSID.text); wifiNetworkModel.requestScan(); view.pop()},
                                   function(e){console.log("Failed to create " + txtSSID.text + ": " + e)}
                                   )

        }
    }

    ImageVersionText {}
}
