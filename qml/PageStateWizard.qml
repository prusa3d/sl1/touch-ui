/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

/** Page with a container for other wizards, so that it can be switched on state change */
PrusaPage {
    id: root
    title: wizardStack.currentItem.title
    name: "wizard"
    pictogram: wizardStack.currentItem.pictogram
    back_button_enabled: wizardStack.currentItem.back_button_enabled
    cancel_instead_of_back: wizardStack.currentItem.cancel_instead_of_back
    back_handler: wizardStack.currentItem.back_handler
    header_rollup: wizardStack.currentItem.header_rollup
    header_can_expand: wizardStack.currentItem.header_can_expand
    screensaver_enabled: false

    function pageByIdentifier(identifier) {
        console.log("Wizard identifier changed:", identifier)
        switch(identifier) {
        case Wizard0.SELF_TEST: return "PageSelftestWizard.qml"
        case Wizard0.COMPLETE_UNBOXING: return "PageUnpackingCompleteWizard.qml"
        case Wizard0.KIT_UNBOXING: return "PageUnpackingKitWizard.qml"
        case Wizard0.DISPLAY: return "PageDisplaytestWizard.qml"
        case Wizard0.CALIBRATION: return "PageCalibrationWizard.qml"
        case Wizard0.FACTORY_RESET: return "PageFactoryResetWizard.qml"
        case Wizard0.PACKING: return "PagePackingWizard.qml"
        case Wizard0.UV_CALIBRATION: return "PageUvCalibrationWizard.qml"
        case Wizard0.SL1S_UPGRADE: return "PageUpgradeWizard.qml"
        case Wizard0.SL1_DOWNGRADE: return "PageDowngradeWizard.qml"
        case Wizard0.NEW_EXPO_PANEL: return "PageNewExpoPanelWizard.qml"
        case Wizard0.TANK_SURFACE_CLEANER: return "PageTankSurfaceCleanerWizard.qml"
        default:
            console.log("Unknown wizard id:", identifier)
            return "PageError.qml"
        }
    }

    Connections {
        target: wizard0
        function onIdentifierChanged() {
            wizardStack.replace(null, pageByIdentifier(wizard0.identifier))
        }
    }

    StackView {
        id: wizardStack
        name: "StateWizardStackView"
        anchors.fill: parent
        initialItem: pageByIdentifier(wizard0.identifier)
    }
}
