
/*
    Copyright 2019-2023, Prusa Research s.r.o.
    Copyright 2024, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.12
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0
import cz.prusa3d.connect0 1.0

PageVerticalList {
    id: root
    title: qsTr("Prusa Connect")
    name: "PrusaConnectRegistration"
    pictogram: Theme.pictogram.key
    model: ObjectModel {
        /*
        DelegateSwitch {
            id: sw_enabled
            name: "PrusaconnectEnabled"
            pictogram: Theme.pictogram.powerOff
            text: qsTr("Enabled")
            Component.onCompleted: ()=>{
                sw_enabled.checked = PrusaConnect.is_ok
            }

            onClicked: ()=>{
                // TODO
            }

            Connections {
                target: PrusaConnect
                function onIs_okChanged() {
                    sw_enabled.checked = PrusaConnect.is_ok
                }
            }
        }
        */

        /*
        DelegateText {
            property var settings: PrusaConnect.settings
            text: "Host: %1:%2, tls=%3".arg(settings["hostname"]).arg(settings["port"]).arg(settings["tls"] ? "YES" : "NO")
        }
        */

        DelegateRefWithStatus {
            arrowVisible: false
            pictogram: {
                switch(PrusaConnect.registration_status) {
                    case "NO_REGISTRATION": return Theme.pictogram.no;
                    case "IN_PROGRESS": return Theme.pictogram.waiting;
                    case "FINISHED": return Theme.pictogram.yes;
                    default: return Theme.pictogram.error;
                }
            }
            text: qsTr("Registration Status")
            status: {
                switch(PrusaConnect.registration_status) {
                    case "NO_REGISTRATION": return qsTr("Not Registered");
                    case "IN_PROGRESS": return qsTr("In Progress");
                    case "FINISHED": return qsTr("Registered");
                    default: return qsTr("Unknown");
                }
            }
        }

        DelegateRef {
            id: register
            name: "RegisterPrinter"
            pictogram: Theme.pictogram.key
            text: qsTr("Add Printer to Prusa Connect")
            onClicked: function() {
                let settings = PrusaConnect.settings
                PrusaConnect.set_server(settings["hostname"], settings["port"], settings["tls"], function(){
                    PrusaConnect.register_printer(function(addr){
                        root.StackView.view.push("PageQrCode.qml", {
                                                     title: qsTr("Register Printer"),
                                                     qrContent: addr,
                                                     text: qsTr("Scan the QR Code or visit <b>prusa.io/add</b>. Log into your Prusa Account to add this printer.<br /><br /><br />Code: %1").arg(addr.split("/").pop()),
                                                 })
                    }, function(e){/*err*/})
                },
                function(e) {console.log("PrusaConnect: Couldn't set server:" + e)})
            }
            visible: PrusaConnect.registration_status == "NO_REGISTRATION"
        }

        DelegateRef {
            id: unregister
            name: "UnRegisterPrinter"
            pictogram: Theme.pictogram.cancel
            text: qsTr("Unregister Printer from Prusa Connect")
            enabled: !printer0.m1_modern_dental_enabled
            onClicked: function() {
                PrusaConnect.unregister_printer(function(){/*ok*/}, function(e){/*err*/})
            }
            visible: PrusaConnect.is_registered
        }

        DelegateRef {
            id: reviewQR
            name: "ConfirmRegistration"
            pictogram: Theme.pictogram.handQr
            text: qsTr("Review the Registration QR Code")
            onClicked: function() {
                root.StackView.view.push("PageQrCode.qml", {
                                             title: qsTr("Register Printer"),
                                             qrContent: PrusaConnect.printer_registration_address,
                                             text: qsTr("Scan the QR Code or visit <b>prusa.io/add</b>. Log into your Prusa Account to add this printer.<br /><br /><br />Code: %1").arg(PrusaConnect.printer_registration_address.split("/").pop()),
                                         })
            }
            visible: PrusaConnect.printer_registration_address != "" && ! PrusaConnect.is_registered
        }
    }

}
