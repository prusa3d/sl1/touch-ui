
/*
    Copyright 2024, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.12
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0
import Native 1.0
PrusaPage {
    id: root
    title: qsTr("Edit Layer Profile") + (root.below ? "\u2193" : "\u2190")
    name: "LayerProfileCustomization"
    pictogram: Theme.pictogram.edit

    back_handler: function() {
        root.saveValues()
        root.StackView.view.pop()
    }
    property var exposure
    property var below
    property var profile

    function saveValues() {
        console.log("Save Profile: ", (root.below?"below":"above"), " ", root.profile)
        root.exposure.user_profile_set(root.below,
                                       root.profile,
                                       ()=>console.log("LayerProfile saved!"),
                                       ()=>console.log("Saving LayerProfile failed!"))

    }

    readonly property var towerProfileNames: [
        "homingFast",   // Omitted
        "homingSlow",   // Omitted
        "moveFast",     // Omitted
        "moveSlow",     // Omitted
        "resinSensor",  // Omitted
        "layer1",
        "layer2",
        "layer3",
        "layer4",
        "layer5",
        "layer8",
        "layer11",
        "layer14",
        "layer18",
        "layer22",
        "layer24"
    ]
    readonly property var towerProfileSpeeds: [/*homingFast, homingSlow, moveFast, moveSlow, resinSensor, */ 1, 2, 3, 4, 5, 8, 11, 14,  18, 22, 24]
    readonly property int towerProfileIndexDelta: towerProfileNames.length - towerProfileSpeeds.length

    readonly property var tiltProfileNames: [
        "homingFast", // Omitted
        "homingSlow", // Omitted
        "move120",
        "layer200",
        "move300",
        "layer400",
        "layer600",
        "layer800",
        "layer1000",
        "layer1250",
        "layer1500",
        "layer1750",
        "layer2000",
        "layer2250",
        "move5120",
        "move8000"
    ]
    /// First two items are forbiden to the user, index will be adjusted by towerProfileIndexDelta
    readonly property var tiltProfileSpeeds: [
        // homingFast,
        // homingSlow,
        120,
        200,
        300,
        400,
        600,
        800,
        1000,
        1250,
        1500,
        1750,
        2000,
        2250,
        5120,
        8000
    ]
    readonly property int tiltProfileIndexDelta: tiltProfileNames.length - tiltProfileSpeeds.length

    readonly property int control_width: 200

    ListView {
        id: settingsList
        height: 420
        interactive: true
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            rightMargin: 10
            leftMargin: 10
        }

        clip: true
        model: ObjectModel {
            id: settingsModel

            DelegatePlusMinus {
                name: "Delay Before Exposure"
                text: qsTr("Delay Before Exposure")
                units: qsTr("s", "seconds")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 300000
                step: 100
                valuePresentation: function(val){return (val/1000).toFixed(1)}
                initValue: root.profile.delay_before_exposure_ms
                onValueModified: () => root.profile.delay_before_exposure_ms = value
            }


            DelegatePlusMinus {
                name: "Delay After Exposure"
                text: qsTr("Delay After Exposure")
                units: qsTr("s", "seconds")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 30000
                step: 100
                valuePresentation: function(val){return (val/1000).toFixed(1)}
                initValue: root.profile.delay_after_exposure_ms
                onValueModified: () => root.profile.delay_after_exposure_ms = value
            }


            DelegatePlusMinus {
                id: towerHopHeight
                name: "Tower Hop Height"
                text: qsTr("Tower Hop Height")
                units: qsTr("mm", "millimeters")
                pictogram: Theme.pictogram.edit
                from: 0
                to: Math.min(100000000, root.exposure.max_z_hop_nm)
                step: 100000
                valuePresentation: function(val){return (val/1000000).toFixed(1)}
                initValue: root.profile.tower_hop_height_nm
                onValueModified: () => root.profile.tower_hop_height_nm = value
            }


            DelegateSelect {
                name: "Tower Speed"
                text: qsTr("Tower Speed")
                units: qsTr("mm/s", "millimeters per second")
                pictogram: Theme.pictogram.edit
                items: root.towerProfileSpeeds
                index: root.profile.tower_profile - towerProfileIndexDelta
                onIndexChanged: () => root.profile.tower_profile = index + towerProfileIndexDelta
            }


            DelegateSwitch {
                id: use_tilt
                name: "Use Tilt"
                text: qsTr("Use Tilt")
                pictogram: Theme.pictogram.edit
                checked: root.profile.use_tilt
                onClicked: () => {
                    checked = !checked
                    root.profile.use_tilt = checked
                    console.log("use_tilt " + root.profile.use_tilt)
                    if(
                        root.profile.tower_hop_height_nm < 5000000 &&
                        !root.profile.use_tilt
                    ){
                        root.profile.tower_hop_height_nm = Math.min(5000000, root.exposure.max_z_hop_nm)
                        towerHopHeight.value = root.profile.tower_hop_height_nm
                        root.StackView.view.push(useTiltDisabled)
                    }
                    if(
                        root.profile.tower_hop_height_nm > 0 &&
                        root.profile.use_tilt
                    ){
                        root.profile.tower_hop_height_nm = 0
                        towerHopHeight.value = root.profile.tower_hop_height_nm
                        root.StackView.view.push(useTiltEnabled)
                    }
                }
            }


            DelegateSelect {
                name: "Tilt Down Initial Speed"
                text: qsTr("Tilt Down Initial Speed")
                pictogram: Theme.pictogram.edit
                units: qsTr("\u03BC-step/s", "microsteps per second")
                items: root.tiltProfileSpeeds
                index: root.profile.tilt_down_initial_profile - tiltProfileIndexDelta
                onIndexChanged: () => root.profile.tilt_down_initial_profile = index + tiltProfileIndexDelta
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Down Offset"
                text: qsTr("Tilt Down Offset")
                units: qsTr("\u03BC-step", "tilt microsteps")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 10000
                initValue: root.profile.tilt_down_offset_steps
                onValueModified: () => root.profile.tilt_down_offset_steps = value
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Down Offset Delay"
                text: qsTr("Tilt Down Offset Delay")
                units: qsTr("s", "seconds")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 20000
                step: 100
                valuePresentation: function(val){return (val/1000).toFixed(1)}
                initValue: root.profile.tilt_down_offset_delay_ms
                onValueModified: () => root.profile.tilt_down_offset_delay_ms = value
                enabled: use_tilt.checked
            }


            DelegateSelect {
                name: "Tilt Down Finish Speed"
                text: qsTr("Tilt Down Finish Speed")
                pictogram: Theme.pictogram.edit
                units: qsTr("\u03BC-step/s", "microsteps per second")
                items: root.tiltProfileSpeeds
                index: root.profile.tilt_down_finish_profile - tiltProfileIndexDelta
                onIndexChanged: () => root.profile.tilt_down_finish_profile = index + tiltProfileIndexDelta
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Down Cycles"
                text: qsTr("Tilt Down Cycles")
                pictogram: Theme.pictogram.edit
                from: 1
                to: 10
                initValue: root.profile.tilt_down_cycles
                onValueModified: () => root.profile.tilt_down_cycles = value
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Down Delay"
                text: qsTr("Tilt Down Delay")
                units: qsTr("s", "seconds")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 20000
                step: 100
                valuePresentation: function(val){return (val/1000).toFixed(1)}
                initValue: root.profile.tilt_down_delay_ms
                onValueModified: () => root.profile.tilt_down_delay_ms = value
                enabled: use_tilt.checked
            }


            DelegateSelect {
                name: "Tilt Up Initial Speed"
                text: qsTr("Tilt Up Initial Speed")
                pictogram: Theme.pictogram.edit
                units: qsTr("\u03BC-step/s", "microsteps per second")
                items: root.tiltProfileSpeeds
                index: root.profile.tilt_up_initial_profile - tiltProfileIndexDelta
                onIndexChanged: () => root.profile.tilt_up_initial_profile = index + tiltProfileIndexDelta
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Up Offset"
                text: qsTr("Tilt Up Offset")
                units: qsTr("\u03BC-step", "tilt micro steps")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 10000
                initValue: root.profile.tilt_up_offset_steps
                onValueModified: () => root.profile.tilt_up_offset_steps = value
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Up Offset Delay"
                text: qsTr("Tilt Up Offset Delay")
                units: qsTr("s", "seconds")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 20000
                step: 100
                valuePresentation: function(val){return (val/1000).toFixed(1)}
                initValue: root.profile.tilt_up_offset_delay_ms
                onValueModified: () => root.profile.tilt_up_offset_delay_ms = value
                enabled: use_tilt.checked
            }


            DelegateSelect {
                name: "Tilt Up Finish Speed"
                text: qsTr("Tilt Up Finish Speed")
                pictogram: Theme.pictogram.edit
                units: qsTr("\u03BC-step/s", "microsteps per second")
                items: root.tiltProfileSpeeds
                index: root.profile.tilt_up_finish_profile - tiltProfileIndexDelta
                onIndexChanged: () => root.profile.tilt_up_finish_profile = index + tiltProfileIndexDelta
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Up Cycles"
                text: qsTr("Tilt Up Cycles")
                pictogram: Theme.pictogram.edit
                from: 1
                to: 10
                initValue: root.profile.tilt_up_cycles
                onValueModified: () => root.profile.tilt_up_cycles = value
                enabled: use_tilt.checked
            }


            DelegatePlusMinus {
                name: "Tilt Up Delay"
                text: qsTr("Tilt Up Delay")
                units: qsTr("s", "seconds")
                pictogram: Theme.pictogram.edit
                from: 0
                to: 20000
                step: 100
                valuePresentation: function(val){return (val/1000).toFixed(1)}
                initValue: root.profile.tilt_up_delay_ms
                onValueModified: () => root.profile.tilt_up_delay_ms = value
                enabled: use_tilt.checked
            }
        }

        Component {
            id: useTiltDisabled
            PageConfirm {
                back_button_enabled: false
                title: qsTr("Warning")
                text:  qsTr("Disabling the 'Use tilt' causes the object to separate away from the film in the vertical direction only.\n\n'Tower hop height' has been set to recommended value of 5 mm.")
                onConfirmed: () => StackView.view.pop()
            }
        }

        Component {
            id: useTiltEnabled
            PageConfirm {
                back_button_enabled: false
                title: qsTr("Warning")
                text: qsTr("Enabling the 'Use tilt' causes the object to separate away from the film mainly by tilt.\n\n'Tower hop height' has been set to 0 mm.")
                onConfirmed: () => StackView.view.pop()
            }
        }

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }
}
