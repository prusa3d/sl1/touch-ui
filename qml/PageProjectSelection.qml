import QtQuick 2.12
import cz.prusa3d.sl1.filemanager 1.0

PageFileBrowser  {
    show_origin: [FMFile.Local, FMFile.Usb]
}
