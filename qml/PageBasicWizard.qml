/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.2

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0


/** Abstracts common code for wizards, not intended to be used on its own. */
PrusaPage {
    id: root
    title: qsTr("Wizard")
    name: "wizard"
    pictogram: Theme.pictogram.wizard
    back_button_enabled: wizard0.cancelable
    cancel_instead_of_back: true // Movement through wizard is one-way only
    back_handler: () => root.StackView.view.push(cancelWizardDialog)
    header_rollup: wizardStack.currentItem.header_rollup
    header_can_expand: wizardStack.currentItem.header_can_expand

    // Will set a more threatening warning message when the user
    // is attempting to cancel an essential calibration wizard.
    property bool essentialCalibration: false

    // Allows for further specialization of the state->component
    // mapping while keeping the defaults
    property var overloadStateMapping: (wizardState) => {return null}

    Component {
        id: cancelWizardDialog
        PageYesNoSimple {
            id: cancelWizardDialogRoot
            name: "CancelWizardDialog"
            title: qsTr("Are you sure?")
            text: qsTr("Do you really want to cancel the wizard?")
                  + (root.essentialCalibration ?
                      ("\n\n" + qsTr("The machine will not work without a completed wizard procedure.")) : "")
            onYes: () => {
                       var tmpView = cancelWizardDialogRoot.StackView.view
                       wizard0.cancel();
                       //wizard0.abort();
                   }
            onNo: () => cancelWizardDialogRoot.StackView.view.pop()
        }
    }

    /* Sensible default components for the most common states
       with the possibility to overload them by the derived component */
    property Component currentComponent: {
        var mapping = overloadStateMapping(wizard0.state)
        if(mapping) {
            return mapping
        }
        else {
            switch(wizard0.state) {
            case Wizard0.REMOVE_DISPLAY_FOIL: return take_of_foil
            case Wizard0.CLOSE_COVER: return close_cover
            case Wizard0.RUNNING: return wizard_running
            case Wizard0.SHOW_RESULTS: // fall-through
            case Wizard0.DONE: return wizard_done
            case Wizard0.FAILED: return wizard_failed
            case Wizard0:CANCELED: return wizard_canceled
            case Wizard0.STOPPED: return wizard_stopped
            case Wizard0.TEST_DISPLAY: return test_display
            case Wizard0.INIT: return blankPage
            default:
                console.warn("Unhandled wizard state(identifier:", wizard0.identifier, " state:", wizard0.state, ")")
                return blankPage
            }
        }
    }

    // There is always one item on the wizardStack, which one depends on the wizard0.state
    onCurrentComponentChanged: ()=>{ wizardStack.replace(null, currentComponent) }

    // Standard way of handling errors
    Connections {
        target: wizard0
        function onCheckExceptionChanged() {
            push_error(wizard0.checkException)
        }
    }

    Component {
        id: test_display
        PageYesNoWithPicture {
            name: "CompanyLogoVisibleDialog"
            text: qsTr("Can you see the company logo on the exposure display through the cover?")
                  + "\n\n"
                  + qsTr("Tip: If you can't see the logo clearly, try placing a sheet of paper onto the screen.")
                  + "\n\n"
                  + qsTr("Once you place the paper inside the printer, do not forget to CLOSE THE COVER!")
            onYes: ()=>{ wizard0.reportDisplay(true) }
            onNo: ()=>{ wizard0.reportDisplay(false) }
            image_source: "selftest-prusa_logo.jpg"
            Component.onCompleted: () => global.soundError()
        }
    }

    Component {
        id: take_of_foil
        PageConfirm {
            name: "PeelOffSticker"
            text: qsTr("Carefully peel off the protective sticker from the exposition display.")
            image_source: "unboxing-remove_sticker_screen.jpg"
            onConfirmed: ()=>{ wizard0.displayFoilRemoved() }
        }
    }

    Component {
        id: wizard_done
        PageConfirm {
            name: "WizardFinished"
            text: qsTr("Wizard finished sucessfuly!")
            onConfirmed:()=>{ wizard0.showResultsDone() }
        }
    }

    Component {
        id: wizard_failed
        PageConfirm {
            name: "WizardFailed"
            text: qsTr("Wizard failed")
            onConfirmed: ()=>{ wizard0.showResultsDone() }
        }
    }

    Component {
        id: wizard_canceled
        PageConfirm {
            name: "WizardCanceled"
            text: qsTr("Wizard canceled")
            onConfirmed: ()=>{ wizard0.showResultsDone() }
        }
    }

    Component {
        id: wizard_stopped
        PageYesNoSimple {
            name: "WizardStoppedRetry"
            text: qsTr("Wizard stopped due to a problem, retry?")
            onYes: ()=>{ wizard0.retry() }
            onNo: ()=>{ wizard0.abort() }
        }
    }

    Component {
        id: close_cover
        PageWait {
            name: "CloseCover"
            title: qsTr("Cover")
            text: qsTr("Close the cover.")
        }
    }

    Component {
        id: wizard_running
        PageWizardChecks {
            name: "WizardChecks"
        }
    }

    Component {
        id: blankPage
        PrusaPage {
            name: "BlankPage"
        }
    }

    StackView {
        id: wizardStack
        name: "BasicWizardStack"
        anchors.fill: parent
        initialItem: currentComponent
    }
}
