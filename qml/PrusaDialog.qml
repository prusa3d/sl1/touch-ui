import QtQuick 2.12

import PrusaComponents 1.0

Rectangle {
    id: root
    parent: basicBottomPage

    anchors {
        centerIn: parent
        verticalCenterOffset: -100
    }
    visible: false
    color: "black"
    border.width: 2
    border.color: Theme.color.highlight
    radius: 10

    implicitHeight: 100
    implicitWidth: 400

    signal accepted()
    signal rejected()
    signal opened()

    function open() {
        visible = true
        root.opened()
    }
    function accept() {
        visible = false
        root.accepted()
    }
    function reject() {
        visible = false
        root.rejected()
    }
}
