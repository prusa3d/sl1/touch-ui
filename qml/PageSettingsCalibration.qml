
/*
    Copyright 2019-2020, Prusa Research s.r.o.
              2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    id: root
    title: qsTr("Calibration")
    name: "calibrationSubMenu"
    pictogram: Theme.pictogram.calibrationSimple

    property bool display_replaced
    property bool led_module_replaced

    property bool wizardStarted: false
    property bool waitingForWizard: printer0.state !== Printer0.WIZARD && wizardStarted

    Connections { // Disable wait overlay when no longer needed
        target: printer0
        function onStateChanged(state) {if(state === Printer0.WIZARD) root.wizardStarted = false}
    }

    model: ObjectModel {

        DelegateRef {
            name:"Selftest"
            text: qsTr("Selftest")
            pictogram: Theme.pictogram.wizard
            onClicked: ()=>{ printer0.run_self_test_wizard() }
        }

        DelegateRef {
            name:"Calibration"
            text: qsTr("Printer Calibration")
            pictogram: Theme.pictogram.calibration
            onClicked: ()=>{ printer0.run_calibration_wizard() }
        }

        /* Before the UV calibration starts, the user needs to answer a few questions:
          1) Was the EXPOSURE DISPLAY changed(for a new one) ?
          2) Was the UV LED MODULE changed? */
        DelegateRef {
            name:"UvCalibration"
            text: qsTr("UV Calibration")
            pictogram: Theme.pictogram.uvCalibration
            onClicked: ()=>{ root.StackView.view.push(question_was_display_replaced) }
            visible: printer0.printer_model !== Printer0.SL1S && printer0.printer_model !== Printer0.M1
        }

        DelegateRef {
            name:"DisplayTest"
            text: qsTr("Display Test")
            pictogram: Theme.pictogram.displayTest
            onClicked: ()=>{ printer0.run_displaytest_wizard() }
        }
    }

    children: [
        PrusaWaitOverlay {
            anchors.fill: parent
            doWait: root.waitingForWizard
        }
    ]

    Component {
        id: question_was_display_replaced

        PageYesNoSimple {
            title: qsTr("New Display?")
            text: qsTr("Did you replace the EXPOSITION DISPLAY?")
            onResult: (result) => {
                          root.display_replaced = result;
                          root.StackView.view.replace(question_was_led_module_replaced)
                      }
        }
    }

    Component {
        id: question_was_led_module_replaced

        PageYesNoSimple {
            title: qsTr("New UV LED SET?")
            text: qsTr("Did you replace the UV LED SET?")
            onResult: (result) => {
                          root.wizardStarted = true // Activate WaitOverlay until the wizard starts
                          root.led_module_replaced = result
                          printer0.run_uv_calibration_wizard(
                              root.display_replaced,
                              root.led_module_replaced)
                          popDialog()
                      }
        }
    }
}
