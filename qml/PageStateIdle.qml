/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import PrusaComponents 1.0


/** Page with a container for other wizards, so that it can be switched on state change */
PrusaPage {
    id: root
    title: view.currentItem.title
    name: view.currentItem.name
    pictogram:  view.currentItem.pictogram
    back_button_enabled: view.currentItem.back_button_enabled
    cancel_instead_of_back: view.currentItem.cancel_instead_of_back
    back_handler: view.currentItem.back_handler
    screensaver_enabled: view.currentItem.screensaver_enabled
    header_rollup: view.currentItem.header_rollup
    header_can_expand: view.currentItem.header_can_expand

    property alias view: view

    StackView {
        id: view
        name: "IdleStack"
        anchors.fill: parent
        initialItem: PageHome {}
    }
}
