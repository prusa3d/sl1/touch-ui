
/*
    Copyright 2019, Prusa Research s.r.o.
    
    This file is part of touch-ui
    
    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Control")
    name: "Control"
    pictogram: Theme.pictogram.controlSimple
    property bool homing_tower: false
    property bool homing_tilt: false

    PageButtons {
        anchors.fill: parent
        anchors.verticalCenterOffset: -40
        PictureButton {
            id: btnMoveToTop
            text: qsTr("Home\nPlatform")
            pictogram: Theme.pictogram.homePlatform
            name: "HomePlatform"
            onClicked: () => {
                           root.homing_tower = true
                           printer0.tower_home(()=>{root.homing_tower = false}, (e)=>{root.homing_tower = false})
                       }
        }

        PictureButton {
            id: btnResetTank
            text: qsTr("Home\nTank")
            pictogram: Theme.pictogram.homeTank
            name: "HomeTank"
            onClicked: () => {
                           root.homing_tilt = true
                           printer0.tilt_home(()=>{root.homing_tilt = false}, (e)=>{root.homing_tilt = false})
                       }
        }

        PictureButton {
            id: btnDisableSteppers
            text: qsTr("Disable\nSteppers")
            pictogram: Theme.pictogram.disableSteppers
            name: "DisableSteppers"
            onClicked: ()=>{ printer0.disable_motors() }
        }

        PictureButton {
            name: "CleanTank"
            text: qsTr("Resin Tank Cleaning")
            pictogram: Theme.pictogram.wizardTankSurfaceCleaner
            onClicked: ()=>{ printer0.run_tank_surface_cleaner_wizard() }
        }
    }

    PrusaWaitOverlay {
        doWait: root.homing_tilt || root.homing_tower
        text: homing_tilt ? qsTr("Homing the tank, please wait...") : qsTr("Homing the tower, please wait...")
    }
}
