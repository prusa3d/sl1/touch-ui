import QtQuick 2.12
import QtQuick.Controls 2.5

import PrusaComponents 1.0

/* Simple item for puting directly into columns */
    Item {
    id: root
    property string label: ""
    property alias checked: sw.checked
    property alias name: sw.name
    property string regexp: ".*"
    property bool enabled: false
    property alias clickSwitch: sw.clickSwitch
    signal clickSwitched()
    signal clicked()
    PrusaFont {
        id: prusaFont
    }

    width: 760
    height: 70
    Text {
        id: txtLabel
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }
        font.pixelSize: 28
        text: label
    }
    PrusaSwitch {
        id: sw
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        onClickSwitched: () => {
            root.clickSwitched()
        }
        onClicked: () => {
                      console.log(LoggingCategories.trajectory, "SwitchItem", name, "clicked")
                      root.clicked()
                   }
    }

    HorizontalSeparator {
        width: root.width
        anchors.bottom: root.bottom
    }
}
