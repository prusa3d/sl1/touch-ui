/*
    Copyright 2020, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.logs0 1.0
import ErrorsSL1 1.0
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Logs export")
    name: "logs"
    pictogram: Theme.pictogram.sandclock
    cancel_instead_of_back: true
    back_handler: function() {
        logs0.cancel()
    }

    Connections {
        target: logs0
        function onStateChanged() {
            console.log("Logs0 state changed: ", logs0.state)
            switch(logs0.state) {
            case Logs0.LogsState.EXPORTING:
                // fall-through
            case Logs0.LogsState.SAVING:
                break // do nothing
            case Logs0.LogsState.FINISHED:
                if(logs0.type === Logs0.StoreType.UPLOAD) {
                    view.replace("PageShowToken.qml",
                        {
                            title: qsTr("Log upload finished"),
                            token: logs0.logUploadIdentifier,
                            description: qsTr("Logs has been successfully uploaded to the Prusa server.<br /><br />Please contact the Prusa support and share the following code with them:"),
                            description_size: 24
                        }
                    );
                } else {
                    view.replace("PageContinue.qml",  {text: qsTr("Logs export finished")})
                }
                break
            case Logs0.LogsState.CANCELED:
                view.replace("PageContinue.qml", {text: qsTr("Logs export canceled")})
                break
            case Logs0.LogsState.FAILED:
                view.replace("PageContinue.qml", {text: qsTr("Logs export failed.\n\nPlease check your flash drive / internet connection.")})
                break
            default:
                console.warn("Unknown Logs0.LogsState: " + state)
                view.pop()
            }
        }

        function onFailureReasonChanged() {
                                console.log("Logs export exception: ", logs0.failureReason["code"]);
                                push_error(logs0.failureReason)
                            }
    }

    ColumnLayout {
        anchors.centerIn: parent
        spacing: 20

        Text {
            Layout.alignment: Qt.AlignCenter
            font.pixelSize: Theme.font.big
            font.bold: true
            text: {
                switch(logs0.state) {
                case Logs0.LogsState.EXPORTING: return qsTr("Extracting log data")
                case Logs0.LogsState.SAVING:
                    switch(logs0.type) {
                    case Logs0.StoreType.USB: return qsTr("Saving data to USB drive")
                    case Logs0.StoreType.UPLOAD: return qsTr("Uploading data to server")
                    default: return "Running unknown log store operation"
                    }
                default: "Unknow log processing state"
                }
            }
            clip: true
        }

        ProgressBar {
            id: progress
            Layout.alignment: Qt.AlignCenter
            visible: logs0.state === Logs0.LogsState.SAVING
            height: 22
            value: {
                switch(logs0.state) {
                case Logs0.LogsState.EXPORTING: return logs0.exportProgress * 100
                case Logs0.LogsState.SAVING: return logs0.storeProgress * 100
                default: return 0
                }
            }
        }

        Text {
            Layout.alignment: Qt.AlignCenter
            id: txtPercent
            visible: progress.visible
            font.pixelSize: 40
            font.bold: true
            text: Math.round(progress.value) + "\u202F%"
        }

        BusySign {
            Layout.alignment: Qt.AlignCenter
            visible: logs0.state === Logs0.LogsState.EXPORTING
            width: 160
            height: 160
        }
    }
}
