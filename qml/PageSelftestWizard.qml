/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtMultimedia 5.12

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    title: qsTr("Selftest")
    essentialCalibration: true
    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.PREPARE_WIZARD_PART_1: return prepare_wizard_part1
        case Wizard0.PREPARE_WIZARD_PART_2: return prepare_wizard_part2
        case Wizard0.PREPARE_WIZARD_PART_3: return prepare_wizard_part3
        case Wizard0.TEST_AUDIO: return test_audio
        default: return null// Otherwise use BasicWizard default
        }
    }

    Component {
        id: prepare_wizard_part1
        PageConfirm {
            id: prepare_wizard_part1_root
            name: "SelfTestWizardPart1"
            text: qsTr("Welcome to the selftest wizard.")
                  + "\n\n"
                  + qsTr("This procedure is mandatory and it will check all components of the printer.")
            onConfirmed: ()=>{ state = "remove_tank" }

            states: [
                State {
                    name: "remove_tank"
                    PropertyChanges {
                        target: prepare_wizard_part1_root
                        text: qsTr("Please unscrew and remove the resin tank.")
                        image_source: "selftest-remove_tank.jpg"
                        onConfirmed: ()=>{ state = "remove_platform" }
                    }
                },
                State {
                    name: "remove_platform"
                    PropertyChanges {
                        target: prepare_wizard_part1_root
                        text: qsTr("Loosen the black knob and remove the platform.")
                        image_source: "selftest-remove_platform.jpg"
                        onConfirmed: ()=>{ state = "close_cover" }
                    }
                },
                State {
                    name: "close_cover"
                    PropertyChanges {
                        target: prepare_wizard_part1_root
                        text: qsTr("Close the cover.")
                        image_source: "close_cover_no_tank.jpg"
                        onConfirmed: ()=>{ wizard0.prepareWizardPart1Done() }
                        enabled: printerConfig0.coverCheck ? printer0.cover_state === true : true
                    }
                }
            ]
        }
    }

    Component {
        id: test_audio
        PageYesNoSimple {
            name: "SelfTestWizardAudio"
            Audio {
                audioRole: Audio.MusicRole
                // FIXME: Running the audio in a loop causes the app to hang
                // loops: MediaPlayer.Infinite
                source: Theme.sound.path + "/" + Theme.sound.testFile
                autoPlay: true

                onError: (error, errorString) => {console.log("Audio Error: ", errorString); stop()}
                onStatusChanged: () => console.log("Audio status changed: ", JSON.stringify(status))
            }

            text: qsTr("Can you hear the music?")
            onResult: (result) => wizard0.reportAudio(result)
        }
    }

    // This is further parametrized by the printer model: {SL1S, *}
    Component {
        id: prepare_wizard_part2
        PageConfirm {
            id: prepare_wizard_part2_root
            name: "SelfTestWizardPart2"
            text: qsTr("Please install the resin tank and tighten the screws evenly.")
            image_source: "tighten_screws.jpg"
            onConfirmed: ()=>{ state = (printer0.printer_model === Printer0.SL1S || printer0.printer_model === Printer0.M1)? "install_platform_sl1s_m1" : "install_platform" }

            states: [
                State {
                    name: "install_platform"
                    PropertyChanges {
                        target: prepare_wizard_part2_root
                        text: qsTr("Please install the platform under 60° angle and tighten it with the black knob.")
                        image_source: "selftest-insert_platform_60deg.jpg"
                        onConfirmed: ()=>{ state = "close_cover" }
                    }
                },
                State {
                    name: "install_platform_sl1s_m1"
                    PropertyChanges {
                        target: prepare_wizard_part2_root
                        text: qsTr("Release the platform from the cantilever and place it onto the center of the resin tank at a 90° angle.")
                        image_source: "selftest-insert_platform_90deg.jpg"
                        onConfirmed: ()=>{ state = "close_cover" }
                    }
                },
                State {
                    name: "close_cover"
                    PropertyChanges {
                        target: prepare_wizard_part2_root
                        text: qsTr("Close the cover.")
                        image_source: "close_cover_no_tank.jpg"
                        onConfirmed: ()=>{ wizard0.prepareWizardPart2Done() }
                        enabled: printerConfig0.coverCheck ? printer0.cover_state === true : true
                    }
                }
            ]
        }
    }

    Component {
        id: prepare_wizard_part3
        PageConfirm {
            id: prepare_wizard_part3_root
            name: "SelfTestWizardPart3"
            text: qsTr("Make sure that the resin tank is installed and the screws are tight.")
            image_source: "tighten_screws.jpg"
            onConfirmed: ()=>{ state = "install_platform" }

            states: [
                State {
                    name: "install_platform"
                    PropertyChanges {
                        target: prepare_wizard_part3_root
                        text: qsTr("Please install the platform under 0° angle and tighten it with the black knob.")
                        image_source: "calibration-tighten_knob.jpg"
                        onConfirmed: ()=>{ state = "close_cover" }
                    }
                },
                State {
                    name: "close_cover"
                    PropertyChanges {
                        target: prepare_wizard_part3_root
                        text: qsTr("Close the cover.")
                        image_source: "close_cover_no_tank.jpg"
                        onConfirmed: ()=>{ wizard0.prepareWizardPart3Done() }
                        enabled: printerConfig0.coverCheck ? printer0.cover_state === true : true
                    }
                }
            ]
        }
    }
}
