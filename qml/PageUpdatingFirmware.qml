/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import cz.prusa3d.updater 1.0
import ErrorsSL1 1.0
import PrusaComponents 1.0

/**
    Contains a single StackView as a container for components,
    that will be created on runtime. There is a single component
    for every state of the page, showing mostly similar, but slightly
    different set of controls on each one and allowing different
    for set of actions.
*/

PrusaPage {
    id: root
    title: qsTr("Printer Update")
    pictogram: Theme.pictogram.up
    name: "UpdateFirmware"
    back_button_enabled:  false

    // Reported to the user on error
    property string errorReason: qsTr("Unknown")

    /**
    If the bundle path is defined on the page activation,
    state will be switched to the "installing" and rauc will
    begin installation of the set bundle.

    Otherwise downloading of the most recent update will start.
    **/
    property string bundle_path: ""



    /**
    Name by which the update will be refered to. It should be
    version if known, or a bundle filename
    */
    property string bundle_name: ""

    /** Initiate firmware download immediately when the page is loaded */
    property bool initiateDownload: false



    /**

                     /--------------------> error
                    /             /-------> error
      idle -> downloading -> installing  -> done
        \_______________________/|

    */
    states: [
        State {
            name: "downloading"
            StateChangeScript {
                name: "scriptDownload"
                script: {
                    mainContainer.replace(downloading)
                }
            }
            PropertyChanges {
                target: root
                cancel_instead_of_back: true
                back_button_enabled: true
                back_handler: function(){
                    console.warn("Canceling");
                    updater.cancelDownload()
                    root.StackView.view.pop()
                }
            }
            PropertyChanges {
                target: root
                cancel_instead_of_back: true
                back_button_enabled: true
            }
        },
        State {
            name: "installing"
            StateChangeScript {
                name: "scriptInstall"
                script: {
                    mainContainer.replace(installing)
                }
            }
            PropertyChanges {
                target: root
                back_button_enabled: false
            }
        },
        State {
            name: "done"
            StateChangeScript {
                name: "scriptDone"
                script: {
                    mainContainer.replace(done)
                    errorStack.push(done) // Will be kept on top even when the printer leaves the printer0.UPDATE state
                }
            }
        },
        State {
            name: "error"
            StateChangeScript {
                name: "scriptError"
                script: {
                    mainContainer.replace(error)
                    push_error({code: ErrorsSL1.toStringCode(ErrorsSL1.UPDATE_FAILED)})
                }
            }
            PropertyChanges {
                target: root
                back_button_enabled: true
                cancel_instead_of_back: false
            }
        }
    ]

    StackView.onActivating: () => {
        if(root.bundle_path !== "") { // Bundle is known/ready, skip downloading
            root.state = "installing"
        }
        else {// Bundle is not set, check updater
            if(updater.operation === Updater.UpdateStatusDownloaded) { // Already downloaded
                root.bundle_path = updater.bundlePath
                root.bundle_name = updater.nextVersion
                root.state = "installing"
            }
            else {// Still needs to be downloaded
                root.bundle_name = updater.nextVersion
                root.state = "downloading"
            }
        }
    }

    /** Downloading view, starts downloading on creation */
    Component {
        id: downloading

        PrusaPage {

            Text {
                y: 89 - 60
                anchors {
                    left: progress.left
                    right: progress.right
                }
                font.pixelSize: Theme.font.big
                font.bold: true
                text: qsTr("Downloading") + " " + root.bundle_name
                clip: true
            }


            ProgressBar {
                id: progress
                y: 157 - 60
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: 49
                    rightMargin: 49
                }

                height: 22
                value: updater.progress * 100

            }

            Text {
                id: txtPercent
                font.pixelSize: 40
                font.bold: true
                text: Math.round(progress.value) + "\u202F%"
                anchors {
                    horizontalCenter: progress.horizontalCenter
                    top: progress.bottom
                    topMargin: 21
                }
            }

            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: txtPercent.bottom
                    topMargin: 30
                }
                width: progress.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: Theme.font.big
                text: qsTr("Downloading firmware, installation will begin immediately after.")
            }

            // Switch state when the downloading has finished
            Connections {
                target: updater
                function onDownloaded(path) {
                    if(root.state == "downloading") {
                        root.bundle_path = updater.bundlePath
                        root.state = "installing"
                    }
                    else {
                        console.warn("onDownloaded signal in invalid state")
                    }
                }

                function onOperationChanged(operation) {
                    if(operation == Updater.UpdateStatusDownloadFailed) {
                        root.errorReason = qsTr("Download failed.")
                        root.state = "error"
                    }
                }
            }

            Component.onCompleted: () => {
                if(root.initiateDownload) updater.download()
            }
        }
    }

    /** Installing view, starts installing on creation */
    Component {
        id: installing

        PrusaPage {

            Text {
                y: 89 - 60
                anchors {
                    left: progress.left
                    right: progress.right
                }
                font.pixelSize: Theme.font.big
                font.bold: true
                text: qsTr("Installing Firmware") + " " + root.bundle_name
                clip: true
            }


            ProgressBar {
                id: progress
                y: 157 - 60
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: 49
                    rightMargin: 49
                }

                height: 22
                value: rauc.progressPercent

            }

            Text {
                id: txtPercent
                font.pixelSize: 40
                font.bold: true
                text: Math.round(progress.value) + "\u202F%"
                anchors {
                    horizontalCenter: progress.horizontalCenter
                    top: progress.bottom
                    topMargin: 21
                }
            }

            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: txtPercent.bottom
                    topMargin: 30
                }
                width: progress.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: Theme.font.big
                text: qsTr("Do not power off the printer while updating!<br/>Printer will be rebooted after a successful update.")
            }

            Connections {
                target: rauc
                function onLastErrorChanged() {
                    if(rauc.lastError) {
                        root.errorReason = rauc.lastError
                        root.state = "error"
                    }
                }

                function onCompleted(result) {
                    if(result === 0) {
                        console.log("Update instalation completed, restarting")
                        printer0.poweroff(true, true, function(){console.log("Rebooting after update")})
                    }
                    else {
                        console.log("Update instalation failed.")
                    }
                }
            }

            Component.onCompleted: () => {
                rauc.install(root.bundle_path)
            }
        }
    }

    /** Done view, will initiate restart on creation */
    Component {
        id: done

        PrusaPage {
            name: "RestartingPrinter"

            Text {
                id: infoTitle
                y: 89 - 60
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: 49
                    rightMargin: 49
                }
                font.pixelSize: Theme.font.big
                font.bold: true
                text: qsTr("Updated to %1 failed.").arg(root.bundle_name)
                clip: true
            }
            ColumnLayout {
                id: errorDescription
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: infoTitle.bottom
                    topMargin: 10
                }
                Text {
                    Layout.alignment: Qt.AlignHCenter
                    font.pixelSize: 40
                    text: "Done"
                }
                Text {
                    Layout.maximumWidth: 700
                    text: qsTr("Printer is being restarted into the new firmware(%1), please wait").arg(root.bundle_name)
                    Layout.alignment: Qt.AlignHCenter
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                }
            }
        }
    }

    /** Error view, allows user to return to the previous page */
    Component {
        id: error
        PrusaPage {
            name: "UpdateError"

            Text {
                id: errorTitle
                y: 89 - 60
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: 49
                    rightMargin: 49
                }
                font.pixelSize: Theme.font.big
                font.bold: true
                text: qsTr("Update to %1 failed.").arg(root.bundle_name)
                clip: true
            }
            ColumnLayout {
                id: errorDescription
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: errorTitle.bottom
                    topMargin: 10
                }
                Text {
                    Layout.alignment: Qt.AlignHCenter
                    font.pixelSize: 40
                    text: "ERROR"
                }
                Text {
                    Layout.maximumWidth: 700
                    text: root.errorReason
                    Layout.alignment: Qt.AlignHCenter
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                }
            }

            Text {
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: errorDescription.bottom
                    topMargin: 30
                }
                width: 700
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: Theme.font.big
                text: qsTr("A problem has occurred while updating, please let us know (send us the logs). Do not panic, your printer is still working the same as it did before. Continue by pressing \"back\"")
            }

            Component.onCompleted: {
                console.log("Error updating firmware: " + root.errorReason)
            }
        }
    }

    StackView {
        id: mainContainer
        name: "FirmwareUpdateStackVIew"
        anchors.fill: parent
    }
}

