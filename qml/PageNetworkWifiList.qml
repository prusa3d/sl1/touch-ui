
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import Native 1.0
import "3rdparty/qrcode-generator"
import PrusaComponents 1.0

PrusaPage {
    id: root
    title: qsTr("Wi-Fi")
    name: "NetworkList"
    pictogram: Theme.pictogram.wifiSimple
    property int textSize: 16
    property var connectedAP: null

    StackView.onActivated: ()=>{
        wifiNetworkModel.requestScan()
    }

    Component.onCompleted: ()=>{
       wifiNetworkModel.requestScan()
    }

    Timer {
        interval: 30 * 1000
        running: root.StackView.view.currentItem === root
        repeat: true
        onTriggered: ()=>{
            wifiNetworkModel.requestScan()
        }
    }

    Item {
        id: sectionEnum
        readonly property int unknown: 0
        readonly property int wifiOnOff: 1
        readonly property int wifiClientOnOff: 2
        readonly property int network: 3
        readonly property int addNetwork: 4
        readonly property int qrcode: 5
    }
    Component {
        id: baseDelegate
        Loader {
            source: {
                switch(model.section) {
                case sectionEnum.network: return "DelegateWifiNetwork.qml"
                case sectionEnum.wifiOnOff: return "DelegateWifiOnOff.qml"
                case sectionEnum.qrcode: return "DelegateQRCode.qml"
                case sectionEnum.addNetwork: return "DelegateAddHiddenNetwork.qml"
                case sectionEnum.wifiClientOnOff: return "DelegateWifiClientOnOff.qml"
                default:
                    console.log("ERROR: No delegate for object " + type)
                }
            }
            onLoaded: ()=>{
                // setup signals according to the item type
                switch(model.section) {
                case sectionEnum.wifiOnOff:
                    if(name === "wifioff") {
                        item["onChangedOff"].connect(removeWifiNetworks)
                    }
                    break;
                default:
                }
            }
        }
    }

    ListView {
        id: networkListView
        height: 420
        interactive: true
        anchors {
          bottom: parent.bottom
          left: parent.left
          right: parent.right
          rightMargin: 10
          leftMargin: 10
        }
        clip: true
        model: wifiNetworkModel


        delegate: baseDelegate   // provide delegate component.
        spacing: 0

        ScrollBar.vertical: ScrollBar {
            active: true
            policy: ScrollBar.AlwaysOn
        }
    }
}
