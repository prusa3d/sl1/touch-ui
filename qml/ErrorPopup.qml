/*
    Copyright 2020, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.2

import PrusaComponents 1.0

Popup {
    id: root
    closePolicy: Popup.NoAutoClose
    modal: true
    focus: true

    width: 800
    height: 480

    background: Rectangle {
        color: "black"
    }

    property string text: qsTr("Unknown error")

    RowLayout {
        anchors.fill: parent
        spacing: 20
        anchors.margins: 50

        Text {
            Layout.fillWidth: true
            text: root.text
            wrapMode: Text.Wrap
        }

        PictureButton {
            name: "Ok"
            Layout.rightMargin: 20
            text: qsTr("Understood")
            pictogram: Theme.pictogram.error
            backgroundColor: Theme.color.errorBackground
            onClicked: () => root.close()
        }
    }
}
