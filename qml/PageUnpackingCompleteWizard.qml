/*
    Copyright 2021, Prusa Development a.s.

    This file is part of touch-ui

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

import cz.prusa3d.sl1.printer0 1.0
import cz.prusa3d.wizard0 1.0
import cz.prusa3d.wizardcheckmodel 1.0
import PrusaComponents 1.0

PageBasicWizard {
    id: root
    name: "UnpackingCompleteWizard"
    title: qsTr("Unpacking")
    overloadStateMapping: (wizardState) => {
        switch(wizardState) {
        case Wizard0.REMOVE_SAFETY_STICKER: return remove_safety_sticker
        case Wizard0.REMOVE_SIDE_FOAM: return remove_side_foam
        case Wizard0.REMOVE_TANK_FOAM: return remove_tank_foam
        case Wizard0.REMOVE_DISPLAY_FOIL: return take_off_foil
        case Wizard0.SHOW_RESULTS: // fall-through
        case Wizard0.DONE: return wizard_done
        default: return null// Otherwise use BasicWizard default
        }
    }

    Component {
        id: remove_tank_foam
        PageConfirm {
            name: "UnpackingRemoveTankFoam"
            text: qsTr("Unscrew and remove the resin tank and remove the black foam underneath it.")
            image_source: "unboxing-remove_bottom_foam.jpg"
            onConfirmed: ()=>{ wizard0.tankFoamRemoved() }
        }
    }

    Component {
        id: remove_side_foam
        PageConfirm {
            name: "UnpackingRemoveSideFoam"
            text: qsTr("Remove the black foam from both sides of the platform.")
            image_source: "unboxing-remove_foam.jpg"
            onConfirmed: () =>{ wizard0.sideFoamRemoved() }
        }
    }

    Component {
        id: remove_safety_sticker
        PageConfirm {
            name: "UnpackingRemoveSticker"
            text: qsTr("Please remove the safety sticker and open the cover.")
            image_source: "unboxing-sticker_open_cover.jpg"
            onConfirmed: ()=>{ wizard0.safetyStickerRemoved() }
        }
    }

    Component {
        id: take_off_foil
        PageConfirm {
            name: "UnpackingRemoveSticker"
            text: qsTr("Carefully peel off the protective sticker from the exposition display.")
            image_source: "unboxing-remove_sticker_screen.jpg"
            onConfirmed: ()=>{ wizard0.displayFoilRemoved() }
        }
    }

    Component {
        id: wizard_done
        PageConfirm {
            name: "UnpackingDone"
            text: qsTr("Unpacking done.")
            onConfirmed: ()=>{ wizard0.showResultsDone() }
        }
    }
}
