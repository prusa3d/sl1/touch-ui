
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0
/*
 Depends on:
    StackView view
*/
PrusaPage {
    id: root
    title: qsTr("Hostname")
    name: "sethostname"
    pictogram: Theme.pictogram.hostname

    RowLayout {
        id: hostnameRow
        spacing: 30
        width: 500
        anchors {
            leftMargin: 55
            rightMargin: 55
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
            left: parent.left
        }
        Text {
            Layout.alignment: Qt.AlignVCenter
            text: qsTr("Hostname") + ":"
            font.pixelSize: 28
        }

        TextField {
            id: txtHostname
            property var valid: text.match(/(^[0-9a-z]$)|(^[0-9a-z][0-9a-z\-]*[0-9a-z]$)/) && text.length <= 63
            Layout.alignment: Qt.AlignVCenter
            Layout.fillWidth: true
            width: 350
            font.family: prusaFont.name
            font.pixelSize: 28
            inputMethodHints: Qt.ImhNoAutoUppercase
            Component.onCompleted: ()=>{ text = hostname.staticHostname }
            Connections {
                target: hostname
                function onStaticHostnameChanged() {
                    txtHostname.text = hostname.staticHostname
                }
            }
        }

    }
    WarningText {
        item: txtHostname
        y:90
        text: qsTr("Can contain only a-z, 0-9 and  \"-\". Must not begin or end with \"-\".")
    }

    PictureButton {
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset:  -40
            rightMargin: 55
        }

        text: qsTr("Set")
        pictogram: Theme.pictogram.yes
        enabled: txtHostname.valid
        name: "SetHostname"
        backgroundColor: "green"
        onClicked: ()=>{
            hostname.setStaticHostname(txtHostname.text)
            view.pop()
        }
    }

    ImageVersionText {}
}
