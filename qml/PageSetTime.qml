
/*
    Copyright 2019, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import PrusaComponents 1.0

/*
 Depends on:
    StackView view
*/
PrusaPage {
    id: root
    title: qsTr("Set Time")
    name: "settime"
    pictogram: Theme.pictogram.clock

    property int myVerticalOffset: -40
    property var currentTime: timedate.time
    property int currentHour: parseInt(Qt.formatDateTime(currentTime, "HH"))
    property int currentMinute: parseInt(Qt.formatDateTime(currentTime, "mm"))

    function pad(num, size) {
        var s = "000000000" + num;
        return s.substr(s.length-size);
    }
    StackView.onActivated: ()=>{
        viewMinute.currentIndex =  currentMinute
        viewHour.currentIndex = currentHour
    }  

    RowLayout {
        anchors.centerIn: parent
        anchors.top: parent.top

        PathSelectorView {
            id: viewHour
            width: 100
            height: 300
            color: "black"
            clip: true
            model: 24
            delegate: PathTextDelegate {
                color: index === currentHour ? Theme.color.highlight : "white"
                text:  pad(modelData,2)
                font.pixelSize: 50
            }
            Component.onCompleted: ()=>{ currentIndex = parseInt(Qt.formatDateTime(currentTime, "HH")) }
        }

        PathSelectorView {
            id: viewMinute
            width: 100
            height: 300
            color: "black"
            clip: true
            model: 60
            delegate: PathTextDelegate {
                color: index === currentMinute ? Theme.color.highlight : "white"
                text:  pad(modelData,2)
                font.pixelSize: 50
            }
            Component.onCompleted: ()=>{ currentIndex = parseInt(Qt.formatDateTime(currentTime, "mm")) }
        }
    }

    Text {
        visible: true
        y: viewHour.height/2 + myVerticalOffset + height/8 + 50
        x: 393
        font.pixelSize: 50
        text:":"
    }

    Text {
        visible: true
        x: 300
        y: 365

        text: qsTr("Hour")
    }

    Text {
        visible: true
        x: 415
        y: 365
        text: qsTr("Minute")
    }

    PictureButton {
        visible: true
        pictogram: Theme.pictogram.yes
        name: "SetTime"
        text: qsTr("Set")
        backgroundColor: "green"
        anchors {
            rightMargin: 55
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -40
        }
        onClicked: ()=>{
            var newHour = parseInt(viewHour.currentIndex)
            var newMinute = parseInt(viewMinute.currentIndex)
            var newTime = new Date(currentTime)

            newTime.setHours( newHour )
            newTime.setMinutes(newMinute)
            timedate.setTime(newTime,
                function(){console.log("Time set to " + newTime)},
                function(){console.log("Failed to set time to " + newTime)}
                )
        }
    }

    ImageVersionText {}
}

