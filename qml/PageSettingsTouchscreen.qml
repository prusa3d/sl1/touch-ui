
/*
    Copyright 2019-2020, Prusa Research s.r.o.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import PrusaComponents 1.0
import PrusaComponents.Delegates 1.0

PageVerticalList {
    title: qsTr("Touchscreen")
    name: "SettingsTouchscreen"
    pictogram: Theme.pictogram.touchscreen
    model: ObjectModel {
        DelegatePlusMinus {
            id: screensavertimer
            // Special case, value is just an index in an array
            name: "ScreensaverTimer"
            text: qsTr("Screensaver timer")

            function idxByValue(v) {
                var idx = allowed_values.indexOf(v)
                if(idx == -1) return  0
                else return  idx
            }

            initValue: idxByValue(screenSaver.delayMs)
            from: 0
            to: allowed_values.length - 1
            step: 1
            pictogram: Theme.pictogram.screensaverTimeout
            property var allowed_values: [0, 10*1000, 30*1000, 60*1000, 5*60*1000, 10*60*1000, 30*60*1000] // times in milliseconds

            function presentTime(millis) {
                var seconds = Math.floor(millis/1000)
                var minutes = Math.floor(seconds/60)
                var hours = Math.floor(minutes/60)
                var result = ""
                if(hours) result += hours + " " + qsTr("h", "hours short") + " "
                if(minutes%60) result += minutes%60 + " " + qsTr("min", "minutes short") + " "
                if(seconds%60) result += seconds%60 + " " + qsTr("s", "seconds short") + " "
                return result
            }

            valuePresentation: function(v) { // Original value is in milliseconds, but present it in seconds
                return value !== 0 ? presentTime(allowed_values[v]) : qsTr("Off")
            }

            onValueChanged: ()=>screenSaver.delayMs = allowed_values[ value ]

            Connections {
                target: screenSaver
                function onDelayMsChanged() {
                    screensavertimer.value = screensavertimer.idxByValue(screenSaver.delayMs)
                    console.log("Screensaver delay changed: " + screenSaver.delayMs)
                }
            }
        }

        DelegatePlusMinus {
            id: brightness
            name: "Brightness"
            text: qsTr("Touch Screen Brightness")
            pictogram: Theme.pictogram.brightness

            // Effectively a 2-directional binding
            initValue:  backlight.intensity
            from: 1
            to: 10

            onValueChanged: ()=>{
                backlight.intensity = value
            }
            valuePresentation: function(v) {
                if(v>=0){
                    enabled = true
                    return v*10
                }
                else {
                    enabled = false
                    return qsTr("N/A")
                }
            }
            units: "%"
            Connections {
                target: backlight
                function onIntensityChanged() {
                    brightness.value = backlight.intensity
                }
            }
        }
    }
}
