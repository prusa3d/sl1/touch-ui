/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef LICENSEMODEL_H
#define LICENSEMODEL_H

#include <QAbstractListModel>
#include "license/licensedatasource.h"
class LicenseModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles {
        RolePackageName = Qt::UserRole + 1,
        RolePackageVersion,
        RoleRecipeName,
        RoleLicense
    };


    explicit LicenseModel(QObject *parent = nullptr);
    Q_PROPERTY(LicenseDataSource* dataSource READ dataSource WRITE setDataSource NOTIFY dataSourceChanged)

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // DataSource getter
    LicenseDataSource *dataSource() const;

    /// Translate Roles enum to names of properties
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE QVariantList findPackageName(QString _nameBeginning);

    Q_INVOKABLE int count() const;

public slots:
    void setDataSource(LicenseDataSource* dataSource);

signals:
    void dataSourceChanged(LicenseDataSource* dataSource);

private:
    LicenseDataSource* m_dataSource;
};

#endif // LICENSEMODEL_H
