cmake_minimum_required(VERSION 3.14)
project(license LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core DBus Quick Xml REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core DBus Quick Xml REQUIRED)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

# Add a library with the above sources
add_library(
    ${PROJECT_NAME} STATIC
    licensedatasource.cpp
    licensedatasource.h
    licensemodel.cpp
    licensemodel.h
    licenserecord.cpp
    licenserecord.h
    )
add_library(touch-ui::license ALIAS ${PROJECT_NAME})
target_include_directories( ${PROJECT_NAME}
    PUBLIC ${PROJECT_SOURCE_DIR}
)
target_compile_definitions(${PROJECT_NAME}
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME}
        PRIVATE
	Qt${QT_VERSION_MAJOR}::Core 
	Qt${QT_VERSION_MAJOR}::Quick
        Qt${QT_VERSION_MAJOR}::Xml
	)

