#!/usr/bin/python3
""" This script will show differences between two DBus interfaces defined by their introspection XML data """

import sys
import xml.etree.ElementTree as ET

class DBusInterface:
    def __init__(self, file: str = None, interface: str = None):
        self.interface = str()
        self.properties = set()
        self.methods = set()
        self.signals = set()
        if file is not None:
            self.parse(file, interface)
    
    def __str__(self):
        lines = list()
        lines.append(f"""Interface: {self.interface}""")
        lines.append("Properties:")
        for p in self.properties:
            lines.append(f"""  {p}""")
        lines.append("Methods:")
        for m in self.methods:
            lines.append(f"""  {m}""")
        lines.append("Signals:")
        for s in self.signals:
            lines.append(f"""  {s}""")
        return "\n".join(lines)

    def parse(self, path: str, interface: str):
        tree = ET.parse(path)
        root = tree.getroot()
        interfaces = root.findall("interface")
        for i in interfaces:
            if i.get("name") == interface:
                self.interface = i.get("name")
                self.properties = {p.get("name") for p in i.findall("property")}
                self.methods = {m.get("name") for m in i.findall("method")}
                self.signals = {s.get("name") for s in i.findall("signal")}
                break
    
    def diff(self, other):
        print(f"""Interface: {self.interface}""")
        if self.interface != other.interface:
            print(f"""{self.interface} != {other.interface}""")
        print(f"""+ properties: {self.properties - other.properties}""")
        print(f"""+ methods: {self.methods - other.methods}""")
        print(f"""+ signals: {self.signals - other.signals}""")

        print(f"""- properties: {other.properties - self.properties}""")
        print(f"""- methods: {other.methods - self.methods}""")
        print(f"""- signals: {other.signals - self.signals}""")
        return sum([
            0 if self.interface == other.interface else 1,
            len(self.properties - other.properties),
            len(self.methods - other.methods),
            len(self.signals - other.signals),
            len(other.properties - self.properties),
            len(other.methods - self.methods),
            len(other.signals - self.signals)
        ])
        
        
def compare_interfaces(file1: str, file2: str, interface: str):
    i1 = DBusInterface(file1, interface)
    i2 = DBusInterface(file2, interface)
    return i1.diff(i2)


if __name__ == "__main__":
    if "-h" in sys.argv or "--help" in sys.argv:
        print(f"""example:\n\t{sys.argv[0]} original_cz.prusa3d.sl1.printer0.xml introspection_cz.prusa3d.sl1.printer0.xml cz.prusa3d.sl1.printer0 """)
        sys.exit(0)
    #r = compare_interfaces("/home/martin/src/SLAGUI/tests/dbus_interfaces/new_printer0.xml", "/home/martin/src/SLAGUI/printer_interfaces/cz.prusa3d.sl1.printer0.xml", "cz.prusa3d.sl1.printer0")
    r = compare_interfaces(sys.argv[1], sys.argv[2], sys.argv[3])
    sys.exit(r)
