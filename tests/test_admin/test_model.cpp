/*
    Copyright 2019-2020, Prusa Research s.r.o.
    Copyright 2021, Prusa Research a.s.

    This file is part of touch-ui

    touch-ui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>

#include <QtTest/QAbstractItemModelTester>
#include <QObject>
#include <QTimer>

#include <adminmodel.h>

int main(int argc, char** argv) {
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QObject allfather;
    QPointer store = new AdminItemStore(&allfather);
    QPointer model = new AdminModel(store, &allfather);
    auto tester = new QAbstractItemModelTester(model, QAbstractItemModelTester::FailureReportingMode::Fatal, &allfather);
    Q_ASSERT(store->count() == 0);

    store->refresh({AdminItemPointer("cz.prusa3d.sl1.admin0.value.int", QDBusObjectPath("/fake/path")), AdminItemPointer("cz.prusa3d.sl1.admin0.value.int", QDBusObjectPath("/fake/path"))});
    Q_ASSERT(store->count() == 2);
    store->refresh({
                       AdminItemPointer("cz.prusa3d.sl1.admin0.value.int", QDBusObjectPath("/fake/path")),
                       AdminItemPointer("cz.prusa3d.sl1.admin0.value.int", QDBusObjectPath("/fake/path")),
                       AdminItemPointer("cz.prusa3d.sl1.admin0.value.int", QDBusObjectPath("/fake/path")),
                       AdminItemPointer("cz.prusa3d.sl1.admin0.value.int", QDBusObjectPath("/fake/path"))
                   });
    Q_ASSERT(store->count() == 4);
    Q_ASSERT(store->get(0) != nullptr);
    Q_ASSERT(store->get(3) != nullptr);

    QTimer::singleShot(1000, &app, [&app](){app.quit();});
    return app.exec();
}
