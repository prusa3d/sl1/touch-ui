# Need an ubuntu version with Qt at least 5.14.2
FROM ubuntu:groovy
USER root
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install git python3-yaml python3-pydbus cmake extra-cmake-modules libkf5networkmanagerqt-dev \
libkf5networkmanagerqt6 libqt53danimation5 libqt53dcore5 libqt53dextras5 libqt53dinput5 libqt53dlogic5 libqt53dquick5 libqt53dquickanimation5 \
libqt53dquickextras5 libqt53dquickinput5 libqt53dquickrender5 libqt53drender5 libqt5concurrent5 libqt5core5a libqt5dbus5 libqt5gui5 \
libqt5hunspellinputmethod5 libqt5location5 libqt5multimedia5 libqt5multimedia5-plugins libqt5multimediagsttools5 libqt5multimediaquick5 \
libqt5network5 libqt5opengl5 libqt5opengl5-dev libqt5positioning5 libqt5positioningquick5 libqt5qml5 libqt5quick5 libqt5quickcontrols2-5 \
libqt5quickparticles5 libqt5quickshapes5 libqt5quicktemplates2-5 libqt5quicktest5 libqt5remoteobjects5 libqt5sensors5 libqt5serialport5 \
libqt5svg5 libqt5svg5-dev libqt5test5 libqt5virtualkeyboard5 libqt5waylandclient5 libqt5waylandcompositor5 libqt5x11extras5 libqt5xml5 \
libqt5xmlpatterns5 libqt5xmlpatterns5-dev libsignon-qt5-1 libsnapd-qt1 libsoqt520 python3-dbus.mainloop.pyqt5 python3-paho-mqtt qdbus-qt5 \
qml-module-qt-labs-folderlistmodel qml-module-qt-labs-platform qml-module-qt-labs-qmlmodels qml-module-qt-labs-settings qml-module-qt3d \
qml-module-qtgraphicaleffects qml-module-qtmultimedia qml-module-qtqml-models2 qml-module-qtquick-controls qml-module-qtquick-controls2 \
qml-module-qtquick-dialogs qml-module-qtquick-extras qml-module-qtquick-layouts qml-module-qtquick-particles2 qml-module-qtquick-scene3d \
qml-module-qtquick-templates2 qml-module-qtquick-virtualkeyboard qml-module-qtquick-window2 qml-module-qtquick-xmllistmodel \
qml-module-qtquick2 qt5-image-formats-plugins qt5-qmake qt5-qmake-bin qt5-qmltooling-plugins qtattributionsscanner-qt5 qtbase5-dev \
qtbase5-dev-tools qtbase5-private-dev qtdeclarative5-dev qtdeclarative5-dev-tools qtdeclarative5-private-dev qtmultimedia5-dev \
qtquickcontrols2-5-dev qttools5-dev qttools5-dev-tools qttranslations5-l10n qtvirtualkeyboard-plugin qtwayland5 qtxmlpatterns5-dev-tools \
dbus network-manager systemd vim rauc-service rauc llvm clang clang-tidy

# The go-omaha updater version will change rarely, can be installed here
RUN apt-get -y install golang
RUN git clone https://gitlab.com/prusa3d/updater.git
RUN cd updater/cmd/updater && go build && install -m 755 updater /usr/bin
RUN install -m 0644 updater/_release/cz.prusa3d.Updater1.conf /usr/share/dbus-1/system.d/

COPY ["de.pengutronix.rauc.conf", "/usr/share/dbus-1/system.d/de.pengutronix.rauc.conf"]

## SL1FW required packages
ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install native deps
RUN apt-get update && apt-get install -y python3 python3-pip python3-paho-mqtt python3-systemd python3-serial python3-numpy python3-pydbus \
python3-gi python3-bitstring python3-toml python3-mock python3-future git openssh-client build-essential xxd gawk gettext python3-coverage \
python3-networkmanager python3-dbusmock python3-pil python3-distro python3-sphinx python3-psutil python3-deprecated python3-deprecation \
python3-evdev lftp python3-pyinotify inotify-tools python3-aiohttp python3-yaml libwayland-dev pkg-config graphviz black

# Install python deps
RUN pip3 install gpio readerwriterlock pylint==2.7.4 pysignal pywayland smbus2 mypy==0.812

# Setup ssh to trust git server
RUN mkdir -p .ssh
RUN chmod 700 .ssh
RUN ssh-keyscan gitlab.com >> .ssh/known_hosts

# Clazy package
COPY ["clazy-1.11.0-Linux.sh", "/clazy-1.11.0-Linux.sh"]
RUN /clazy-1.11.0-Linux.sh --skip-license --prefix=/usr/local
RUN rm  /clazy-1.11.0-Linux.sh

CMD ["qmake", "--version"]