cmake_minimum_required(VERSION 3.14)
project(hostname LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core DBus Quick REQUIRED)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

set_source_files_properties(${CMAKE_CURRENT_SOURCE_DIR}/org.freedesktop.hostname1.xml PROPERTIES
    CLASSNAME HostnameProxy
    )
qt5_add_dbus_interface(DBUS_SRCS ${CMAKE_CURRENT_SOURCE_DIR}/org.freedesktop.hostname1.xml hostname_proxy)


# Add a library with the above sources
add_library(
    ${PROJECT_NAME} STATIC
    hostname.cpp
    hostname.h
    ${DBUS_SRCS}
    )
add_library(touch-ui::hostname ALIAS ${PROJECT_NAME})
target_include_directories( ${PROJECT_NAME}
    PUBLIC ${PROJECT_SOURCE_DIR}
)
target_compile_definitions(${PROJECT_NAME}
  PRIVATE $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)
target_link_libraries(${PROJECT_NAME}
        PRIVATE
	Qt${QT_VERSION_MAJOR}::Core 
	Qt${QT_VERSION_MAJOR}::DBus
	Qt${QT_VERSION_MAJOR}::Quick
        dbus_utils
	)

