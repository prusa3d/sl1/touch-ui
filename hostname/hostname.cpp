/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <dbusutils.h>
#include "hostname.h"

Hostname::Hostname(QJSEngine &_engine, QObject *parent) :
    BaseDBusObject(_engine, service, interface, path, QDBusConnection::systemBus(), parent),
    m_hostnameProxy(service, path, QDBusConnection::systemBus(), this)
{
    m_hostnameProxy.setTimeout(2000);
    setServiceIsOnDemand(true);
    reload_properties();

}

void Hostname::setStaticHostname(QString staticHostname, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_hostnameProxy.SetStaticHostname(staticHostname, false);
    handleCallbackWithError(pending, callback_ok, callback_fail);
}

void Hostname::setHostname(QString hostname, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_hostnameProxy.SetHostname(hostname, false);
    handleCallbackWithError(pending, callback_ok, callback_fail);
}

void Hostname::setLocation(QString location, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_hostnameProxy.SetLocation(location, false);
    handleCallbackWithError(pending, callback_ok, callback_fail);
}

void Hostname::setDeployment(QString deployment, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_hostnameProxy.SetDeployment(deployment, false);
    handleCallbackWithError(pending, callback_ok, callback_fail);
}

void Hostname::describe(QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = m_hostnameProxy.Describe();
    handleCallbackWithError(pending, callback_ok, callback_fail);
}

void Hostname::_setLocation(QString location)
{
    if (m_location == location)
        return;
    m_location = location;
    emit locationChanged(m_location);
}

void Hostname::_setKernelRelease(QString kernelRelease)
{
    if (m_kernelRelease == kernelRelease)
        return;
    m_kernelRelease = kernelRelease;
    emit kernelReleaseChanged(m_kernelRelease);
}

void Hostname::_setKernelName(QString kernelName)
{
    if (m_kernelName == kernelName)
        return;
    m_kernelName = kernelName;
    emit kernelNameChanged(m_kernelName);
}

void Hostname::_setHostname(QString hostname)
{
    if (m_hostname == hostname)
        return;
    m_hostname = hostname;
    emit hostnameChanged(m_hostname);
}

void Hostname::_setDeployment(QString deployment)
{
    if (m_deployment == deployment)
        return;
    m_deployment = deployment;
    emit deploymentChanged(m_deployment);
}

void Hostname::_setKernelVersion(QString kernelVersion)
{
    if (m_kernelVersion == kernelVersion)
        return;
    m_kernelVersion = kernelVersion;
    emit kernelVersionChanged(m_kernelVersion);
}


void Hostname::_setStaticHostname(QString staticHostname)
{
    if (m_staticHostname == staticHostname)
        return;
    m_staticHostname = staticHostname;
    emit staticHostnameChanged(m_staticHostname);
}

void Hostname::_setOperatingSystemPrettyName(QString operatingSystemPrettyName)
{
    if(m_operatingSystemPrettyName == operatingSystemPrettyName)
        return;
    m_operatingSystemPrettyName = operatingSystemPrettyName;
    emit operatingSystemPrettyNameChanged(m_operatingSystemPrettyName);

}

void Hostname::_setHomeURL(QString homeURL)
{
    if (m_homeURL == homeURL)
        return;
    m_homeURL = homeURL;
    emit homeURLChanged(m_homeURL);
}

bool Hostname::set(const QString &key, const QVariant &value)
{
    if(key == QStringLiteral("Chassis")) {
    }
    else if(key == "Deployment") {
        _setDeployment(value.toString());
    }
    else if(key == "HomeURL") {
        _setHomeURL(value.toString());
    }
    else if(key == "Hostname") {
        _setHostname(value.toString());
    }
    else if(key == "IconName") {
    }
    else if(key == "KernelName") {
        _setKernelName(value.toString());
    }
    else if(key == "KernelRelease") {
        _setKernelRelease(value.toString());
    }
    else if(key == "KernelVersion") {
        _setKernelVersion(value.toString());
    }
    else if(key == "Location") {
        _setLocation(value.toString());
    }
    else if(key == "OperatingSystemCPEName") {
    }
    else if(key == "OperatingSystemPrettyName") {
        _setOperatingSystemPrettyName(value.toString());
    }
    else if(key == "PrettyHostname") {
    }
    else if(key == "StaticHostname") {
        _setStaticHostname(value.toString());
    }
    else if(key == "DefaultHostname") {
        _setDefaultHostname(value.toString());
    }
    else if(key == "HardwareVendor") {
        _setHardwareVendor(value.toString());
    }
    else if(key == "HardwareModel") {
        _setHardwareModel(value.toString());
    }
    else if(key == "HostnameSource") {
        _setHostnameSource(value.toString());
    }
    else return BaseDBusObject::set(key, value);
    return true;
}

void Hostname::_setHostnameSource(QString hostnameSource)
{
    if (m_hostnameSource == hostnameSource)
        return;

    m_hostnameSource = hostnameSource;
    emit hostnameSourceChanged(m_hostnameSource);
}

void Hostname::_setHardwareModel(QString hardwareModel)
{
    if (m_hardwareModel == hardwareModel)
        return;

    m_hardwareModel = hardwareModel;
    emit hardwareModelChanged(m_hardwareModel);
}

void Hostname::_setHardwareVendor(QString hardwareVendor)
{
    if (m_hardwareVendor == hardwareVendor)
        return;

    m_hardwareVendor = hardwareVendor;
    emit hardwareVendorChanged(m_hardwareVendor);
}

void Hostname::_setDefaultHostname(QString defaultHostname)
{
    if (m_defaultHostname == defaultHostname)
        return;

    m_defaultHostname = defaultHostname;
    emit defaultHostnameChanged(m_defaultHostname);
}
