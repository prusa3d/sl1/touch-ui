#ifndef LAYERPROFILE_H
#define LAYERPROFILE_H
#include "qmetaobject.h"
#include <QDebug>
#include <QDBusArgument>
#include <QObject>

class LayerProfile;
/**
 * @brief The LayerProfile class - container for the LayerProfile data
 *
 * Intended to be passed from/to QML
 *
 */
class LayerProfile
{
    Q_GADGET
    // QML_ELEMENT
public:
    Q_PROPERTY(int delay_before_exposure_ms MEMBER m_delay_before_exposure_ms)
    Q_PROPERTY(int delay_after_exposure_ms MEMBER m_delay_after_exposure_ms)
    Q_PROPERTY(int tower_hop_height_nm MEMBER m_tower_hop_height_nm)
    Q_PROPERTY(int tower_profile MEMBER m_tower_profile)
    Q_PROPERTY(bool use_tilt MEMBER m_use_tilt)
    Q_PROPERTY(int tilt_down_initial_profile MEMBER m_tilt_down_initial_profile)
    Q_PROPERTY(int tilt_down_offset_steps MEMBER m_tilt_down_offset_steps)
    Q_PROPERTY(int tilt_down_offset_delay_ms MEMBER m_tilt_down_offset_delay_ms)
    Q_PROPERTY(int tilt_down_finish_profile MEMBER m_tilt_down_finish_profile)
    Q_PROPERTY(int tilt_down_cycles MEMBER m_tilt_down_cycles)
    Q_PROPERTY(int tilt_down_delay_ms MEMBER m_tilt_down_delay_ms)
    Q_PROPERTY(int tilt_up_initial_profile MEMBER m_tilt_up_initial_profile)
    Q_PROPERTY(int tilt_up_offset_steps MEMBER m_tilt_up_offset_steps)
    Q_PROPERTY(int tilt_up_offset_delay_ms MEMBER m_tilt_up_offset_delay_ms)
    Q_PROPERTY(int tilt_up_finish_profile MEMBER m_tilt_up_finish_profile)
    Q_PROPERTY(int tilt_up_cycles MEMBER m_tilt_up_cycles)
    Q_PROPERTY(int tilt_up_delay_ms MEMBER m_tilt_up_delay_ms)
    virtual ~LayerProfile() {}

private:
    int m_delay_before_exposure_ms{0};
    int m_delay_after_exposure_ms{0};
    int m_tower_hop_height_nm{0};
    bool m_use_tilt{false};
    int m_tilt_down_initial_profile{0};
    int m_tilt_down_offset_steps{0};
    int m_tilt_down_offset_delay_ms{0};
    int m_tilt_down_finish_profile{0};
    int m_tilt_down_cycles{0};
    int m_tilt_down_delay_ms{0};
    int m_tilt_up_initial_profile{0};
    int m_tilt_up_delay_ms{0};
    int m_tower_profile{0};
    int m_tilt_up_offset_steps{0};
    int m_tilt_up_offset_delay_ms{0};
    int m_tilt_up_finish_profile{0};
    int m_tilt_up_cycles{0};

    friend QDBusArgument &operator<<(QDBusArgument &arg, const LayerProfile &obj);
    friend const QDBusArgument &operator>>(const QDBusArgument &arg, LayerProfile &obj);
    friend QDebug &operator<<(QDebug &arg, const LayerProfile &obj);
    friend const QDebug &operator>>(const QDebug &arg, LayerProfile &obj);
};

Q_DECLARE_METATYPE(LayerProfile);

inline QDBusArgument &operator<<(QDBusArgument &arg, const LayerProfile &obj) {
    arg.beginStructure();
    arg << obj.m_delay_before_exposure_ms;
    arg << obj.m_delay_after_exposure_ms;
    arg << obj.m_tower_hop_height_nm;
    arg << obj.m_tower_profile;

    arg << obj.m_use_tilt;

    arg << obj.m_tilt_down_initial_profile;
    arg << obj.m_tilt_down_offset_steps;
    arg << obj.m_tilt_down_offset_delay_ms;
    arg << obj.m_tilt_down_finish_profile;

    arg << obj.m_tilt_down_cycles;
    arg << obj.m_tilt_down_delay_ms;
    arg << obj.m_tilt_up_initial_profile;
    arg << obj.m_tilt_up_offset_steps;

    arg << obj.m_tilt_up_offset_delay_ms;
    arg << obj.m_tilt_up_finish_profile;
    arg << obj.m_tilt_up_cycles;
    arg << obj.m_tilt_up_delay_ms;
    arg.endStructure();
    return arg;
}

inline const QDBusArgument &operator>>(const QDBusArgument &arg, LayerProfile &obj) {
    arg.beginStructure();
    arg >> obj.m_delay_before_exposure_ms;
    arg >> obj.m_delay_after_exposure_ms;
    arg >> obj.m_tower_hop_height_nm;
    arg >> obj.m_tower_profile;

    arg >> obj.m_use_tilt;

    arg >> obj.m_tilt_down_initial_profile;
    arg >> obj.m_tilt_down_offset_steps;
    arg >> obj.m_tilt_down_offset_delay_ms;
    arg >> obj.m_tilt_down_finish_profile;

    arg >> obj.m_tilt_down_cycles;
    arg >> obj.m_tilt_down_delay_ms;
    arg >> obj.m_tilt_up_initial_profile;
    arg >> obj.m_tilt_up_offset_steps;

    arg >> obj.m_tilt_up_offset_delay_ms;
    arg >> obj.m_tilt_up_finish_profile;
    arg >> obj.m_tilt_up_cycles;
    arg >> obj.m_tilt_up_delay_ms;

    arg.endStructure();
    return arg;
}



inline QDebug &operator<<(QDebug &arg, const LayerProfile &obj) {
    auto meta = obj.staticMetaObject;
    arg << "LayerProfile {";
    for(int index = 0; index < meta.propertyCount(); index++) {
        const auto & prop = meta.property(index);
        arg << prop.name() << "=" << prop.readOnGadget(&obj);
    }
    arg << "";

    return arg;
}

#endif // LAYERPROFILE_H
