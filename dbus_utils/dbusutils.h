#ifndef DBUSUTILS_H
#define DBUSUTILS_H
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusInterface>
#include <QtDBus/QDBusConnectionInterface>
#include <QtDBus/QDBusVariant>
#include <QtDBus/QDBusArgument>
#include <QtDBus/QDBusMessage>
#include <QtDBus/QDBusReply>
#include <QtDBus/QDBusPendingReply>
#include <QVariant>
#include <QJSEngine>
#include <QJSValue>
#include <QDebug>

/**
 * @brief The asKeyValueRange class - wrapper around associative containers
 * Instead of passing a Qt associative container directly to the range-based for loop,
 * we just need to pass it wrapped inside of the asKeyValueRange class,
 * and we will finally be able to use the idiomatic C++17 way of iterating over
 * key-value pairs with QHash and QMap:
 * for (auto [key, value]: asKeyValueRange(map)) {
 *   // ...
 * }
 * Instances should not be stored as the container reference can become invalid.
 */
template <typename T>
class asKeyValueRange
{
public:
    asKeyValueRange(T &data)
        : m_data{data}
    {
    }

    auto begin() { return m_data.keyValueBegin(); }

    auto end() { return m_data.keyValueEnd(); }

private:
    T &m_data;
};


class DBusUtils
{
public:
    DBusUtils();

    /** List objects belonging to the service starting at path, synchronous
     * @param service i.e. cz.prusa3d.sl1.exposure0
     * @param path starting path of the objects, i.e. /cz/prusa3d/sl1/exposures0
    */
    static QStringList listObjects(const QString &service, const QString &path);

    /** Introspect an object(or service)
     * @param service i.e. cz.prusa3d.sl1.exposure0
     * @param path starting path of the objects, i.e. /cz/prusa3d/sl1/exposures0
     * @param callback_ok void func(QString xml_string)
     * @param callback_fail void func()
     */
    static void Introspect(const QString &service, const QString &path, std::function<void(QString)> callback_ok = [](QString){}, std::function<void()> callback_fail=[](){});

    /** Universal function to handle callbacks of asynchronous calls.
     * Intended for use with QML, hence the QJSValue callbacks.
     * @param pending QDBusPendingReply
     * @param callback_ok Javascript function with up to 8 arguments
     * @param callback_fail Javascript function with up to 8 arguments
    */
    template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
    static void handle_callbacks(
            QJSEngine & engine,
            QDBusPendingReply<T1, T2, T3, T4, T5, T6, T7, T8> pending,
            QJSValue callback_ok = QJSValue(),
            QJSValue callback_fail = QJSValue(),
            std::function<void(QString, QString)> second_error_handler = [](QString error_name, QString error_message){Q_UNUSED(error_name); Q_UNUSED(error_message)})
    {
        handle_callbacks(pending,
            [=, &engine](QVariantList vl) mutable {
                if(callback_ok.isCallable()) {
                    QJSValueList params;
                    foreach(const QVariant & varg, vl) {
                        params.append( engine.toScriptValue<>(varg));
                    }
                    callback_ok.call(params);
                }
            },
            [=, &engine](QVariantList vl) mutable {
                if(callback_fail.isCallable()) {
                    QJSValueList params;
                    foreach(const QVariant & varg, vl) {
                        params.append( engine.toScriptValue<>(varg));
                    }
                    callback_fail.call(params);
                }
            },
            second_error_handler
        );
    }

    /** Universal function to handle callbacks of asynchronous calls.
     * Intended for use with C++.
     * @param pending QDBusPendingReply
     * @param callback_ok Std::function with up to 8 arguments
     * @param callback_fail Std::function with up to 8 arguments
    */
    template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
    static void handle_callbacks(
        QDBusPendingReply<T1, T2, T3, T4, T5, T6, T7, T8> pending,
        std::function<void(QVariantList)> callback_ok = [](QVariantList){},
        std::function<void(QVariantList)> callback_fail = [](QVariantList){},
        std::function<void(QString, QString)> second_error_handler = [](QString error_name, QString error_message){Q_UNUSED(error_name); Q_UNUSED(error_message)})
    {
        QDBusPendingCallWatcher*  watcher = new QDBusPendingCallWatcher(pending);
        QObject::connect(watcher, &QDBusPendingCallWatcher::finished, [=](QDBusPendingCallWatcher * replyWatcher) mutable {
            QDBusPendingReply<T1, T2, T3, T4, T5, T6, T7, T8> reply = *replyWatcher;
            if(reply.isError()) {
                qDebug() << "DBusUtils::handle_callbacks got DBusError: name: "
                         << reply.error().name() << " message: " << reply.error().message();
                callback_fail({reply.error().name(), reply.error().message()});
                second_error_handler(reply.error().name(), reply.error().message());
            }
            else {
                QVariantList params;
                for(int i = 0; i < reply.count(); ++i) {
                    auto arg = reply.argumentAt(i);
                    params.append(arg);
                }
                callback_ok(params);
            }
            replyWatcher->deleteLater();
        });
    }

    /** Convert from possible QVariant(QDBusArgument) to the correct type */
    template<class T = QVariant>
    static T convert(const QVariant & value) {
        T orig = value.value<T>();
        if (value.canConvert<QDBusArgument>())
        {
            QDBusArgument arg = value.value<QDBusArgument>();
            arg >> orig;
        }
        return orig;
    }

    /**
     * @brief Parse error code from DBus exception data
     * @param name string A string to be parsed, example: "e10544.ProjectErrorWrongPrinterModel"
     * @param message string to be parsed, example: {"code": "#10319", "name": "e10319.UVDeviationTooHigh", "text": "(240, 250)", "found": 240, "allowed": 250}"
     * @return processed error data or generic data including Errors::UNKNOWN code
     */
    static QVariantMap parseErrorData(QString name, QString message);
};

#endif // DBUSUTILS_H
