/*
    Copyright 2021-2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef UPDATER_H
#define UPDATER_H

#include <QObject>
#include <QDBusPendingReply>
#include <QDBusUnixFileDescriptor>
#include <QDBusPendingCallWatcher>
#include <QFileSystemWatcher>
#include <QQmlEngine>
#include "updater_proxy.h"
#include "dbusutils.h"
#include "basedbusobject.h"



/** @brief QML accessible wrapper around the Updater proxy object
 * adn updater settigns. Allows access to the go-omaha client's
 * properties and methods.
 *
 * Further, it breaks-out individual parts of Status & UpdateInfo
 * properties into individual properties for convenience.
 */
class Updater : public BaseDBusObject
{
    Q_OBJECT
    UpdaterProxy m_updater;

public:
    /// Operation is the first item in Updater1.Status
    enum Operation {
        UpdateStatusIdle = 0,
        UpdateStatusCheckingForUpdate,
        UpdateStatusUpdateAvailable,
        UpdateStatusDownloading,
        UpdateStatusDownloaded,
        UpdateStatusDownloadFailed,
        UpdateStatusUpToDate,
        UpdateStatusCheckFailed,
    };
    Q_ENUM(Operation)

    enum UpdateChannel {
        Stable = 1,
        Beta,
        Development,
        Medic
    };
    Q_ENUM(UpdateChannel)

private:
    Operation m_operation{UpdateStatusIdle};
    double m_progress{0.0};
    QString m_bundlePath;
    QString m_currentVersion;
    QString m_nextVersion;
    QString m_url;
    QByteArray m_digest;
    int m_date{0};
    QString m_releaseNotes;
    QFileSystemWatcher m_watcher;

public:
    explicit Updater(QQmlEngine &_engine, QObject *parent = nullptr);

    /** Current state of the updater */
    Q_PROPERTY(Operation operation READ operation WRITE setOperation NOTIFY operationChanged)

    /** Download progress */
    Q_PROPERTY(double progress READ progress WRITE setProgress NOTIFY progressChanged)

    /** Path to the downloaded bundle */
    Q_PROPERTY(QString bundlePath READ bundlePath WRITE setBundlePath NOTIFY bundlePathChanged)

    /** Current firmware version (whole name) */
    Q_PROPERTY(QString currentVersion READ currentVersion WRITE setCurrentVersion NOTIFY currentVersionChanged)

    /** Next(to be downloaded and installed)  firmware version */
    Q_PROPERTY(QString nextVersion READ nextVersion WRITE setNextVersion NOTIFY nextVersionChanged)

    /** URL from which the update bundle will be downloaded from */
    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)

    /** Digest of the bundle to be downloaded */
    Q_PROPERTY(QByteArray digest READ digest WRITE setDigest NOTIFY digestChanged)

    /** Timestamp of the bundle to be downloaded */
    Q_PROPERTY(int date READ date WRITE setDate NOTIFY dateChanged)

    /** @brief Release notes to the current available update bundle
     *  Refreshed when the update becomes available(or at the start
     *  of the program).
     */
    Q_PROPERTY(QString releaseNotes READ releaseNotes WRITE setReleaseNotes NOTIFY releaseNotesChanged)

    /** Update channel */
    Q_PROPERTY(UpdateChannel updateChannel READ updateChannel WRITE setUpdateChannel NOTIFY updateChannelChanged)


    /** @brief Starts downloading the update bundle
     * Wrapper around Download method of Updater1 interface
     */
    Q_INVOKABLE void download();

    /** @brief Initiate check for update
     * Wrapper around Check method of Updater1 interface
     */
    Q_INVOKABLE void check();

    /** @brief Wrapper GetReleaseNotes method of Updater1 interface
     * The value is retrieved asynchronously and stored in property
     * releaseNotes.
     */
    Q_INVOKABLE void getReleaseNotes();

    Operation operation() const;
    double progress() const;
    QString bundlePath() const;
    QString currentVersion() const;
    QString nextVersion() const;
    QString url() const;
    QByteArray digest() const;
    int date() const;
    QString releaseNotes() const;
    UpdateChannel updateChannel() const;
    Q_INVOKABLE void cancelDownload();

    inline static const QString service{"cz.prusa3d.Updater1"};
    inline static const QString interface{"cz.prusa3d.Updater1"};
    inline static const QString path{"/cz/prusa3d/Updater1"};

signals:
    /// Downloaded signal trampolined from Updater1 interface
    void downloaded(const QString &path);
    void operationChanged(Operation operation);
    void progressChanged(double progress);
    void bundlePathChanged(QString bundlePath);
    void currentVersionChanged(QString currentVersion);
    void nextVersionChanged(QString nextVersion);
    void urlChanged(QString url);
    void digestChanged(QByteArray digest);
    void dateChanged(int date);
    void releaseNotesChanged(QString releaseNotes);
    void updateArrived(QString bundlePath,
                       QString currentVersion,
                       QString nextVersion,
                       QString url,
                       QByteArray digest,
                       int date,
                       QString releaseNotes);
    void updateChannelChanged(UpdateChannel channel);

public slots:

private:
    const QString CHANNEL_SWITCH_BINARY = "/usr/sbin/set-update-channel.sh";
    const QString CURRENT_UPDATE_CHANNEL_FILE = "/etc/update_channel";

    void setOperation(Operation operation);
    void setProgress(double progress);
    void setBundlePath(QString bundlePath);
    void setCurrentVersion(QString currentVersion);
    void setNextVersion(QString nextVersion);
    void setUrl(QString url);
    void setDigest(QByteArray digest);
    void setDate(int date);
    void setReleaseNotes(QString releaseNotes);
    void setUpdateChannel(UpdateChannel channel);

    /** Reload the Updater1.Status property and update all properties that
     * are extracted from it
     */
    void refreshStatus(QVariant status);

    /** Reload the Updater1.UpdateInfo property and update all properties that
     * are extracted from it
     */
    void refreshUpdateInfo(QVariant info);



    // BaseDBusObject interface
protected:
    virtual bool set(const QString &name, const QVariant &value) override;
};

#endif // UPDATER_H
