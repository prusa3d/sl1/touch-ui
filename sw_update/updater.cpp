/*
    Copyright 2021-2022, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDBusArgument>
#include <QVariant>
#include <QDebug>
#include <memory>
#include <string>

#include "updater.h"
#include "custom_types.h"
void Updater::refreshStatus(QVariant status)
{
    QDBusArgument arg = status.value<QDBusArgument>();
    if(arg.currentType() == QDBusArgument::StructureType) {
        if(! arg.atEnd()) {
            StatusStruct newStatusStruct;
            arg >> newStatusStruct;
            auto previousOperation = operation();
            setOperation(static_cast<Operation>(newStatusStruct.operation));
            setProgress(newStatusStruct.progress);
            setBundlePath(newStatusStruct.bundlePath);

            if(UpdateStatusUpdateAvailable != previousOperation && operation() == UpdateStatusUpdateAvailable) {
                this->getReleaseNotes();
                emit updateArrived(bundlePath(),
                                   currentVersion(),
                                   nextVersion(),
                                   url(),
                                   digest(),
                                   date(),
                                   releaseNotes());
            }
        }
        else {
            qWarning() << "Updater1.Status empty ?!";
        }
    }
    else if(arg.currentType() == QDBusArgument::UnknownType) {
        qDebug("Could not read cz.prusa3d.Updater1.Status(this is not an error!)");
    }
    else {
        qWarning("Updater1.Status: wrong message type");
    }
}

void Updater::refreshUpdateInfo(QVariant info)
{
    if( ! info.isValid()) {
        return;
    }
    Q_ASSERT(info.canConvert<QDBusArgument>() ||
             info.value<QDBusVariant>().variant().canConvert<QDBusArgument>());

    QDBusArgument arg;
    if(info.canConvert<QDBusArgument>()) {
        arg = info.value<QDBusArgument>();
    }
    else if(info.canConvert<QDBusVariant>()) {
        arg = info.value<QDBusVariant>().variant().value<QDBusArgument>();
    }

    if(arg.currentType() == QDBusArgument::StructureType) {
        if(! arg.atEnd()) {
            UpdateInfoStruct updateInfoStruct;

            arg >> updateInfoStruct;
            setCurrentVersion(updateInfoStruct.currentVersion);
            setNextVersion(updateInfoStruct.nextVersion);
            setUrl(updateInfoStruct.url);
            setDigest(updateInfoStruct.digest);
            setDate(updateInfoStruct.date);
        }
        else {
            qWarning() << "Updater1.UpdateInfo empty ?!";
        }
    }
    else if(arg.currentType() == QDBusArgument::UnknownType) {
        qDebug("Could not read cz.prusa3d.Updater1.UpdateInfo(this is not an error!)");
    }
    else {
        qWarning("Updater1.UpdateInfo: wrong message type");
    }
}


Updater::Updater(QQmlEngine &_engine, QObject *parent) :
    BaseDBusObject(_engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    m_updater(service, path,QDBusConnection::systemBus(), this)
{
    // Register custom D-Bus types
    qRegisterMetaType<UpdateInfoStruct>("UpdateInfoStruct");
    qRegisterMetaType<StatusStruct>("StatusStruct");
    qDBusRegisterMetaType<UpdateInfoStruct>();
    qDBusRegisterMetaType<StatusStruct>();


    // Re-emit the Downloaded signal
    connect(&m_updater, &UpdaterProxy::Downloaded, this, [=](const QString & path) {
        emit downloaded(path);
    });

    // When an update becomes available, get its releaseNotes
    connect(this, &Updater::operationChanged, this, [=](Operation op) {
        if(op == UpdateStatusUpdateAvailable) {
            this->getReleaseNotes();
        }
    });

    m_watcher.addPath(CURRENT_UPDATE_CHANNEL_FILE);
    connect(&m_watcher, &QFileSystemWatcher::fileChanged, this, [this](QString path){
        if(path == CURRENT_UPDATE_CHANNEL_FILE) {
            emit updateChannelChanged(updateChannel());
        }
    });
}



void Updater::download() {
    m_updater.Download();
}

void Updater::check() {
    m_updater.Check();
}

void Updater::getReleaseNotes() {
    QDBusPendingReply<QDBusUnixFileDescriptor> reply = m_updater.GetReleaseNotes();
    QDBusPendingCallWatcher * watcher = new QDBusPendingCallWatcher(reply, this);
    connect(watcher, &QDBusPendingCallWatcher::finished, this, [=](QDBusPendingCallWatcher * watcher) mutable {
        QDBusPendingReply<QDBusUnixFileDescriptor> reply = *watcher;
        if(reply.isValid()) {
            if(reply.argumentAt<0>().isValid()) {

                int fd = reply.argumentAt<0>().fileDescriptor();
                QFile f;
                if (f.open(fd, QIODevice::ReadOnly | QIODevice::Text)) {
                    f.seek(0);
                    QTextStream in(&f);
                    int idx = 0;
                    QString releaseNotes;
                    while (!in.atEnd())
                    {
                        QString line = in.readLine(1000);
                        if((idx > 2 && line.contains("# Version")) || idx > 1000) {
                            break;
                        }
                        releaseNotes.append(line + "\n");
                        ++idx;
                    }
                    f.close();

                    setReleaseNotes(releaseNotes);

                }
                else {
                    qWarning() << "getReleaseNotes: Can't read file with release notes";
                    setReleaseNotes("");
                    emit BaseDBusObject::dbusError(QVariantMap{{"code",QVariant("#10501")}});
                }
            }
        }
        else {
            qWarning() << "getReleaseNotes: Received invalid reply";
            setReleaseNotes("");
            emit BaseDBusObject::dbusError(QVariantMap{{"code",QVariant("#10501")}});
        }
        watcher->deleteLater();
    });
}

Updater::Operation Updater::operation() const
{
    return m_operation;
}

double Updater::progress() const
{
    return m_progress;
}

QString Updater::bundlePath() const
{
    return m_bundlePath;
}

QString Updater::currentVersion() const
{
    return m_currentVersion;
}

QString Updater::nextVersion() const
{
    return m_nextVersion;
}

QString Updater::url() const
{
    return m_url;
}

QByteArray Updater::digest() const
{
    return m_digest;
}

int Updater::date() const
{
    return m_date;
}

QString Updater::releaseNotes() const {
    return m_releaseNotes;
}


bool Updater::set(const QString &name, const QVariant &value)
{
    if(name == "UpdateInfo") {
        QVariant vinfo = value;
        QDBusVariant info = vinfo.value<QDBusVariant>();
        refreshStatus(info.variant());
    }
    else if(name == "Status") {
        QVariant vstatus = value;
        QDBusVariant status = vstatus.value<QDBusVariant>();
        refreshUpdateInfo(m_updater.updateInfo());
        refreshStatus(status.variant());
    }
    else {
        return BaseDBusObject::set(name, value);
    }
    return true;
}

void Updater::setOperation(Updater::Operation operation)
{
    if (m_operation == operation)
        return;

    m_operation = operation;
    emit operationChanged(m_operation);
}

void Updater::setProgress(double progress)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_progress, progress))
        return;

    m_progress = progress;
    emit progressChanged(m_progress);
}

void Updater::setBundlePath(QString bundlePath)
{
    if (m_bundlePath == bundlePath)
        return;

    m_bundlePath = bundlePath;
    emit bundlePathChanged(m_bundlePath);
}

void Updater::setCurrentVersion(QString currentVersion)
{
    if (m_currentVersion == currentVersion)
        return;

    m_currentVersion = currentVersion;
    emit currentVersionChanged(m_currentVersion);
}

void Updater::setNextVersion(QString nextVersion)
{
    if (m_nextVersion == nextVersion)
        return;

    m_nextVersion = nextVersion;
    emit nextVersionChanged(m_nextVersion);
}

void Updater::setUrl(QString url)
{
    if (m_url == url)
        return;

    m_url = url;
    emit urlChanged(m_url);
}

void Updater::setDigest(QByteArray digest)
{
    if (m_digest == digest)
        return;

    m_digest = digest;
    emit digestChanged(m_digest);
}

void Updater::setDate(int date)
{
    if (m_date == date)
        return;

    m_date = date;
    emit dateChanged(m_date);
}

void Updater::setReleaseNotes(QString releaseNotes)
{
    if (m_releaseNotes == releaseNotes)
        return;

    m_releaseNotes = releaseNotes;
    emit releaseNotesChanged(m_releaseNotes);
}

Updater::UpdateChannel Updater::updateChannel() const {
    QFile file(CURRENT_UPDATE_CHANNEL_FILE);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Failed to open " << CURRENT_UPDATE_CHANNEL_FILE << " considering current channel to be stable";
        return UpdateChannel::Stable;
    }
    QTextStream in(&file);
    QString strChannel = in.readAll().trimmed();
    if(strChannel == "stable") return UpdateChannel::Stable;
    else if(strChannel == "beta") return UpdateChannel::Beta;
    else if(strChannel == "dev") return UpdateChannel::Development;
    else if(strChannel == "medic") return UpdateChannel::Medic;
    else {
        qDebug() << "Invalid content of" << CURRENT_UPDATE_CHANNEL_FILE << ":" << strChannel << "considering current channel to be stable";
        return UpdateChannel::Stable;
    }
}

void Updater::cancelDownload()
{
    m_updater.CancelDownload();
}

void Updater::setUpdateChannel(UpdateChannel channel) {
    QString strChannel;
    switch(channel) {
    case UpdateChannel::Stable: strChannel = "stable"; break;
    case UpdateChannel::Beta: strChannel = "beta"; break;
    case UpdateChannel::Development: strChannel = "dev"; break;
    case UpdateChannel::Medic: strChannel = "medic"; break;
    default: strChannel = "stable";
    }

    QProcess::execute(CHANNEL_SWITCH_BINARY,  {strChannel});
    // The read-back is on purpose, needs to assert valid values via script
    emit updateChannelChanged(updateChannel());
}
