/*
    Copyright 2020-2022, Prusa Research a.s.


	This file is part of touch-ui

	touch-ui is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "rauc.h"
#include "toggle_bootpart.h"

Rauc::Rauc(QQmlEngine &_engine, QObject *parent):
    BaseDBusObject(_engine,  service, interface, path, QDBusConnection::systemBus(),  parent),
    raucProxy(service, path, QDBusConnection::systemBus(), this)
{

	// Completed signal pass-through
	connect(&raucProxy, &RaucProxy::Completed, this, [=](const int & result) {
		emit completed(static_cast<Result>(result));
	});

    raucProxy.setTimeout(20000);

	qRegisterMetaType<RaucSlotEntry>("RaucSlotEntry");
	qDBusRegisterMetaType<RaucSlotEntry>();

	qRegisterMetaType<QList<RaucSlotEntry>>("SlotEntryList");
	qDBusRegisterMetaType<QList<RaucSlotEntry>>();

	qRegisterMetaType<RaucProgress>("RaucProgress");
	qDBusRegisterMetaType<RaucProgress>();
}

void Rauc::switchToAlternativeSystem(QJSValue callback_ok , QJSValue callback_fail) {
    int r = toggle_bootpart();
    if(r < 0) { // error
        qDebug() << "rauc.toggle_bootpart failed: retcode = " << r;
        if(callback_fail.isCallable()) callback_fail.call();
        return;
    }

    auto pending = raucProxy.Mark("active", "other");
    DBusUtils::handle_callbacks(pending,
        [=](QVariantList argv) mutable{
            Q_UNUSED(argv)
            if(callback_ok.isCallable()) callback_ok.call();
        },
        [=](QVariantList argv) mutable {
            qDebug() << "rauc.Mark() error -> " << argv;
            int r = toggle_bootpart(); // Revert back on failure
            if(r<0) {
                qDebug() << "rauc.toggle_bootpart REVERT failed: retcode = " << r;
            }
            if(callback_fail.isCallable()) callback_fail.call();
    });
}

void Rauc::install(QString bundlePath) {
    raucProxy.InstallBundle(bundlePath, QVariantMap());
}

void Rauc::info(QString bundlePath, QJSValue callback_ok, QJSValue callback_fail)
{
    auto pending = raucProxy.Info(bundlePath);
    DBusUtils::handle_callbacks(m_engine,  pending, callback_ok, callback_fail);
}

QString Rauc::alternativeSystemVersion() {
    // Get slot status from Rauc service
    auto reply = raucProxy.GetSlotStatus();
    reply.waitForFinished();

    // Return no alternative slot on call error
    if (reply.isError()) {
        return "";
    }

    // Look for alternative slot
    for(auto & slot: reply.value()) {
        if (!slot.name.startsWith("rootfs")) {
            continue;
        }

        if (qvariant_cast<QString>(slot.data["state"]) != "inactive") {
            continue;
        }

        return qvariant_cast<QString>(slot.data["bundle.version"]);
    }

    return "";
}



bool Rauc::set(const QString &name, const QVariant &value)
{
    if(name == QStringLiteral("Operation")) {
        setOperation(value.toString());
    }
    else if(name == QStringLiteral("LastError")) {
        setLastError(value.toString());
    }
    else if(name == QStringLiteral("Compatible")) {
        setCompatible(value.toString());
    }
    else if(name == QStringLiteral("BootSlot")) {
        setBootSlot(value.toString());
    }
    else if(name == QStringLiteral("Progress")) {
        RaucProgress progress;
        qvariant_cast< QDBusArgument >(value) >> progress;
        setProgressPercent(progress.percent);
        setProgressMessage(progress.message);
        setProgressDepth(progress.depth);
    }
    else if(name == QStringLiteral("Variant")) {
        setVariant(value.toString());
    }
    else {
        return BaseDBusObject::set(name, value);
    }
    return true;
}

const QString &Rauc::operation() const
{
    return m_operation;
}

void Rauc::setOperation(const QString &newOperation)
{
    if (m_operation == newOperation)
        return;
    m_operation = newOperation;
    emit operationChanged();
}

const QString &Rauc::lastError() const
{
    return m_lastError;
}

void Rauc::setLastError(const QString &newLastError)
{
    if (m_lastError == newLastError)
        return;
    m_lastError = newLastError;
    emit lastErrorChanged();
}

const QString &Rauc::compatible() const
{
    return m_compatible;
}

void Rauc::setCompatible(const QString &newCompatible)
{
    if (m_compatible == newCompatible)
        return;
    m_compatible = newCompatible;
    emit compatibleChanged();
}

const QString &Rauc::bootSlot() const
{
    return m_bootSlot;
}

void Rauc::setBootSlot(const QString &newBootSlot)
{
    if (m_bootSlot == newBootSlot)
        return;
    m_bootSlot = newBootSlot;
    emit bootSlotChanged();
}

int Rauc::progressPercent() const
{
    return m_progressPercent;
}

void Rauc::setProgressPercent(int newProgressPercent)
{
    if (m_progressPercent == newProgressPercent)
        return;
    m_progressPercent = newProgressPercent;
    emit progressPercentChanged();
}

const QString &Rauc::progressMessage() const
{
    return m_progressMessage;
}

void Rauc::setProgressMessage(const QString &newProgressMessage)
{
    if (m_progressMessage == newProgressMessage)
        return;
    m_progressMessage = newProgressMessage;
    emit progressMessageChanged();
}

int Rauc::progressDepth() const
{
    return m_progressDepth;
}

void Rauc::setProgressDepth(int newProgressDepth)
{
    if (m_progressDepth == newProgressDepth)
        return;
    m_progressDepth = newProgressDepth;
    emit progressDepthChanged();
}

const QString &Rauc::variant() const
{
    return m_variant;
}

void Rauc::setVariant(const QString &newVariant)
{
    if (m_variant == newVariant)
        return;
    m_variant = newVariant;
    emit variantChanged();
}
