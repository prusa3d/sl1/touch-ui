/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License v2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 021110-1307, USA.
 *
 * Modified to add field firmware update support,
 * those modifications are Copyright (c) 2016 SanDisk Corp.
 *
 * (This code is based on btrfs-progs/btrfs.c.)
 */

#include <errno.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

#include <linux/mmc/ioctl.h>

#define MMC_BLOCK_MAJOR         179

/* From kernel linux/mmc/mmc.h */
#define MMC_SWITCH		6	/* ac	[31:0] See below	R1b */
#define MMC_SEND_EXT_CSD	8	/* adtc				R1  */
#define MMC_SWITCH_MODE_WRITE_BYTE	0x03	/* Set target to value */

#define EXT_CSD_BOOT_CFG		179
#define EXT_CSD_PART_CONFIG		179

#define EXT_CSD_CMD_SET_NORMAL		(1<<0)
/* NOTE: The eMMC spec calls the partitions "Area 1" and "Area 2", but Linux
 * calls them mmcblk0boot0 and mmcblk0boot1. To avoid confustion between the two
 * numbering schemes, this tool uses 0 and 1 throughout. */
#define EXT_CSD_BOOT_CFG_EN		(0x38)


/* From kernel linux/mmc/core.h */
#define MMC_RSP_PRESENT	(1 << 0)
#define MMC_RSP_CRC	(1 << 2)		/* expect valid crc */
#define MMC_RSP_BUSY	(1 << 3)		/* card may send busy */
#define MMC_RSP_OPCODE	(1 << 4)		/* response contains opcode */

#define MMC_CMD_AC	(0 << 5)
#define MMC_CMD_ADTC	(1 << 5)

#define MMC_RSP_SPI_S1	(1 << 7)		/* one status byte */
#define MMC_RSP_SPI_BUSY (1 << 10)		/* card may send busy */

#define MMC_RSP_SPI_R1	(MMC_RSP_SPI_S1)
#define MMC_RSP_SPI_R1B	(MMC_RSP_SPI_S1|MMC_RSP_SPI_BUSY)

#define MMC_RSP_R1	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE)
#define MMC_RSP_R1B	(MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE|MMC_RSP_BUSY)

#define EMMC_DEVICE	"/dev/mmcblk2"

static const struct mmc_ioc_cmd empty_ioc_cmd;

static int read_extcsd(int fd, __u8 *ext_csd)
{
	struct mmc_ioc_cmd idata = empty_ioc_cmd;
	idata.write_flag = 0;
	idata.opcode = MMC_SEND_EXT_CSD;
	idata.arg = 0;
	idata.flags = MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	idata.blksz = 512;
	idata.blocks = 1;
	mmc_ioc_cmd_set_data(idata, ext_csd);

	return ioctl(fd, MMC_IOC_CMD, &idata);
}

static int write_extcsd_value(int fd, __u8 index, __u8 value)
{
	struct mmc_ioc_cmd idata = empty_ioc_cmd;

	idata.write_flag = 1;
	idata.opcode = MMC_SWITCH;
	idata.arg = (MMC_SWITCH_MODE_WRITE_BYTE << 24) |
		(index << 16) |
		(value << 8) |
		EXT_CSD_CMD_SET_NORMAL;
	idata.flags = MMC_RSP_SPI_R1B | MMC_RSP_R1B | MMC_CMD_AC;

	return ioctl(fd, MMC_IOC_CMD, &idata);
}

int toggle_bootpart(void)
{
	__u8 ext_csd[512] = {0};
	__u8 value;
	__u8 part;
	int fd, ret;

	fd = open(EMMC_DEVICE, O_RDWR);
	if (fd < 0)
		return errno;

	ret = read_extcsd(fd, ext_csd);
	if (ret) {
		close(fd);
		return ret;
	}

#define EXT_CSD_EXTRACT_BOOT_PART(x)	(((x) >> 3) & 0x7)
#define EXT_CSD_BOOT_PART_MASK		(0x7 << 3)
#define EXT_CSD_BOOT_PART_NUM(x)	((x) << 3)

	value = ext_csd[EXT_CSD_PART_CONFIG];
	part = EXT_CSD_EXTRACT_BOOT_PART(value);
	switch (part) {
	case 1:
		part = 2;
		break;
	case 2:
		part = 1;
		break;
	default:
		close(fd);
		return -EINVAL;
	}
	value = (value & ~EXT_CSD_BOOT_PART_MASK) | EXT_CSD_BOOT_PART_NUM(part);
	ret = write_extcsd_value(fd, EXT_CSD_PART_CONFIG, value);

	close(fd);
	return ret;
}
