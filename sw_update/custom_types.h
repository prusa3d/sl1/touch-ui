/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CUSTOM_TYPES_H
#define CUSTOM_TYPES_H

#include <QString>
#include <QObject>
#include <QDBusArgument>

/** @brief Representing the Updater1.Status structure */
struct StatusStruct
{
    int operation{0};
    double progress{0.0};
    QString bundlePath;
};
Q_DECLARE_METATYPE(StatusStruct)

// Marshall the StatusStruct data into a D-Bus argument
QDBusArgument &operator<<(QDBusArgument &argument, const StatusStruct &statusStruct);

// Retrieve the StatusStruct data from the D-Bus argument
const QDBusArgument &operator>>(const QDBusArgument &argument, StatusStruct &statusStruct);


/** @brief Representing the Updater1.UpdateInfo */
struct UpdateInfoStruct
{
    QString currentVersion;
    QString nextVersion;
    QString url;
    QByteArray digest;
    int date{0};
};
Q_DECLARE_METATYPE(UpdateInfoStruct)

// Marshall the UpdateInfoStruct data into a D-Bus argument
QDBusArgument &operator<<(QDBusArgument &argument, const UpdateInfoStruct &updateInfoStruct);

// Retrieve the UpdateInfoStruct data from the D-Bus argument
const QDBusArgument &operator>>(const QDBusArgument &argument, UpdateInfoStruct &updateInfoStruct);


/** @brief Rauc slot entry structure */
struct RaucSlotEntry {
    QString name;
    QMap<QString, QVariant> data;
};
Q_DECLARE_METATYPE(RaucSlotEntry);
QDBusArgument &operator<<(QDBusArgument &argument, const RaucSlotEntry &slotEntry);
const QDBusArgument &operator>>(const QDBusArgument &argument, RaucSlotEntry &slotEntry);

typedef QList<RaucSlotEntry> RaucSlotStatusArray;


/** @brief Rauc progress structure */
struct RaucProgress {
    int percent{0};
    QString message;
    int depth{0};
};
Q_DECLARE_METATYPE(RaucProgress);
QDBusArgument &operator<<(QDBusArgument &argument, const RaucProgress &raucProgress);
const QDBusArgument &operator>>(const QDBusArgument &argument, RaucProgress &raucProgress);


#endif // CUSTOM_TYPES_H
