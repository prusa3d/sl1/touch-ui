/*
    Copyright 2021, Prusa Development a.s.

    This file is part of SLAGUI

    SLAGUI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "nativefunctions.h"
#include <QProcess>
#include <QPixmap>
#include <QQuickWindow>
#include <QQuickItem>
#include <QQuickItemGrabResult>
#include <QFile>
#include <QFileInfo>
#include <QQuickWindow>
#include <QPixmap>

#include <QDebug>
#include <QDateTime>

NativeFunctions::NativeFunctions(QQmlApplicationEngine & _engine, QObject *parent) :
    QObject(parent),
    m_appEngine(_engine)
{

}


QObject* NativeFunctions::FindByName(QList<QObject*> nodes, const QString& name)
{
    for(int i = 0; i < nodes.size(); i++){
        if (nodes.at(i) && nodes.at(i)->objectName() == name){
            return qobject_cast<QObject*>(nodes.at(i));
        }
        else if (nodes.at(i) && nodes.at(i)->children().size() > 0){
            QObject* item = FindByName(nodes.at(i)->children(), name);
            if (item) {
                return item;
            }
        }
    }
    return nullptr;
}

void NativeFunctions::runProcess(QString program, QStringList args, std::function<void (int)> ok_callback, std::function<void(int)> error_callback)
{
    QProcess * q = new QProcess(this);
    connect(q, &QProcess::errorOccurred, this, [=](QProcess::ProcessError e) mutable {
        Q_UNUSED(e)
        error_callback(-1);
    });
    connect(q, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, [=](int exitCode, QProcess::ExitStatus exitStatus) mutable{
        Q_UNUSED(exitStatus)
        if(exitCode != 0) {
            error_callback(exitCode);
        }
        else {
            ok_callback(exitCode);
        }
        q->deleteLater();

    });
    q->setProgram(program);
    q->setArguments(args);
    q->start();
}

void NativeFunctions::reboot(QJSValue error_callback)
{
    runProcess("reboot", {},
               [](int){},
               [=](int) mutable {
                    if(error_callback.isCallable())  {
                        error_callback.call();
                    }
               });
}

void NativeFunctions::screenshotWindow(QString path, QJSValue ok_callback, QJSValue error_callback) {
    QQuickWindow *window = qobject_cast<QQuickWindow*>(m_appEngine.rootObjects().first());

    if (window && window->grabWindow().save(path)) {
        if (ok_callback.isCallable()) ok_callback.call();
    } else {
        if (error_callback.isCallable()) error_callback.call();
    }
}

bool NativeFunctions::syncFileExists(QString path) {
    return QFileInfo::exists(path);
}


